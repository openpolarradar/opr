# Open Polar Radar (OPR) Toolbox
OPR Toolbox: Polar radar software toolbox (currently mostly Matlab based)

Contains code for calibrating, processing, analyzing, and viewing radar data. The tools are focused on radar sounders.

Main guide for setting up, using, citing, and acknowledging the cresis toolbox:
https://gitlab.com/openpolarradar/opr/-/wikis/

matlab: Matlab code. This is the main directory with most of the source code.

idl: IDL code. Only includes support for reading data product files

python: Python code. Includes support for reading ECMWF weather model files, tape library raw data retrieval.

vim: Vim code. Only includes setup files for using vimdiff.
