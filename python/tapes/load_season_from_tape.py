"""
This script pulls an entire season from tape.
Uses much of the functionality defined in load_files_from_tape.py.

Example usage: python3 load_season_from_tape.py "/ras/ARCHIVE/CRESIS/General/data2/MCoRDS/2014_Antarctica_DC8" /kucresis/scratch/data/MCoRDS/

Author: Reece Mathews
"""
from pathlib import Path
from argparse import ArgumentParser
import subprocess
import os
import re
import tqdm
import hashlib
import json
from collections import defaultdict
from itertools import chain

from load_files_from_tape import is_tape_here, CRESIS_TAPES, load_tape, mount_drive, find_tape_devices, TAPE_MOUNT_PATH, check_for_missing_tapes, extract_small_file_archive, is_file_ok


# TODO: Check if already exists on /snfs1/cresis and don't duplicate if so.
# TODO: Check if already exists on /kucresis/data as well probably and warn
# TODO: clean up verification flags -- maybe abstract the file saving part
# TODO: replace zero checking with random point content matching

ras_tape_file = Path("/root/utilities/RAS_files_tapes_table.txt")


def determine_tapes_for_season(season_root_path: str):
    """
    Parse the list of all tapes and their contents to find which tapes contain
    the given season.
    """
    print("Reading tape file to determine required tapes")

    tapes: set[tuple[str, str]] = set()
    files: dict[tuple[str], set[Path]] = defaultdict(set)

    # Find required tapes for season
    with open(ras_tape_file) as f:
        for line in f:
            path_tapes, *path = line.split(" ")
            path = " ".join(path)
            if not path.startswith(season_root_path):
                continue

            tape_pair = tuple(path_tapes.split(","))
            tapes.add(tape_pair)
            files[tape_pair].add(Path(path.strip()).relative_to(Path(season_root_path).parent))

    # Figure out which tapes we have
    our_tapes: set[str] = set()
    for tape_pair in tapes:
        for tape in tape_pair:
            if tape in CRESIS_TAPES:
                our_tapes.add(tape)
                break
        else:
            # print off tape locations
            for tape in tape_pair:
                is_tape_here(tape)

            raise RuntimeError(f"No tape from pair: {tape_pair}")

    return sorted(our_tapes), files


def copy_season_dir(root_path: Path, target_path: Path):
    """Copy the season directory from the mounted tape to the target location"""
    root_dir = TAPE_MOUNT_PATH / root_path.relative_to("/")

    if not target_path.exists():
        os.makedirs(target_path, exist_ok=True)

    with tqdm.tqdm(total=100, leave=False, desc="Copying files") as pbar:
        # Perform the copy
        with subprocess.Popen([f"find {root_dir} -xtype f | ltfs_ordered_copy -p -t {target_path} --keep-tree={root_dir.parent}"], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT) as p:
            while True:
                # watch output from copy command and update progress bar accordingly (read1 returns whatever bytes are in buffer immediately instead of waiting for EOL)
                text = p.stdout.read1().decode()
                if match := re.match(r"File copy from tape is on going: ([0-9]+)/([0-9]+)", text.strip()):
                    pbar.total = int(match.group(2))
                    pbar.n = int(match.group(1)) - 1
                    pbar.refresh()
                if p.poll() is not None:
                    pbar.n = pbar.total
                    pbar.refresh()
                    break

    tqdm.tqdm.write("Searching for small file archives.")
    archives = subprocess.check_output([f"find {target_path / root_dir.name} -name '*small_file_archive.tar' -xtype f"], shell=True, text=True)
    for archive in archives.split():
        extract_small_file_archive(Path(archive))


verification_data = {
    "verify_segments": {},
    "verify_hash": {},
    "verify_size": {},
}


def get_segments(file_path):
    """
    Pull data from the beginning, middle, and end of the given file so that it can be compared
    against the file that exists on the fileserver to try to find files that weren't fully
    written due to crash or user interrupt.
    """
    file_size = os.path.getsize(file_path)
    contents: list[bytes] = []
    with open(file_path, 'rb') as f:
        if file_size > 300:
            # read first 100 bytes
            contents.append(f.read(100))
            # move forward to before halfway point
            f.seek((file_size // 2) - 100)
            # read up to halfway point
            contents.append(f.read(100))
            # move forward another half
            f.seek((file_size // 2) - 101)
            # read to end
            contents.append(f.read(100))
        else:
            contents = [f.read()]  # small enough to just read whole file

    return list(int(byte) for byte in (chain(*contents)))


verification_functions = {
    "verify_hash": lambda file_path: subprocess.check_output(["xxhsum", str(file_path)], text=True, stderr=subprocess.DEVNULL).split()[0],
    "verify_size": os.path.getsize,
    "verify_segments": get_segments,
}

verification_data_files = {

}


if __name__ == "__main__":
    assert __doc__
    parser = ArgumentParser( prog=Path(__file__).name, description=__doc__.split("\n")[1])
    parser.add_argument('root_path', help=f"The root path for the season you wish to pull off tape as found in {ras_tape_file.name}")
    parser.add_argument('target_path', help=f"The location to copy the data to")
    parser.add_argument('--skip_copy', action='store_true', help=f"Do not actually copy any files -- just verify existence")
    parser.add_argument('--verify_segments', action='store_true', help=f"Check random segments of each file and compare against what was written to the filesystem (to try to catch files having been allocated but then not being written fully due to a user interrupt or crash). Much faster than checking a file hash. Loads tapes if this data hasn't been cached.")
    parser.add_argument('--verify_hash', action='store_true', help=f"Verify the hash of each file. Requires xxhash to be installed. Loads tapes if this data hasn't been cached.")
    parser.add_argument('--verify_size', action='store_true', help=f"Verify the size of each file. Loads tapes if this data hasn't been cached.")
    args = parser.parse_args()

    tapes, files = determine_tapes_for_season(args.root_path)

    load_tapes = not args.skip_copy
    need_verification_data = {}
    verify = args.verify_segments or args.verify_hash or args.verify_size

    # load verification data from file if it already exists
    for verification_type in verification_data:
        if not args.__dict__.get(verification_type, False):
            continue

        verification_data_file = Path(f"/root/utilities/file_verification/{verification_type}/{Path(args.root_path).parent.name}/{Path(args.root_path).name}_{verification_type}.json")
        verification_data_files[verification_type] = verification_data_file

        os.makedirs(verification_data_file.parent, exist_ok=True)
        if verification_data_file.exists():
            # verification data already cached, pull from file
            print(f"Using existing verification data for {verification_type} from file {verification_data_file}. Delete file to force update of this data from tape.")
            need_verification_data[verification_type] = False
            with open(verification_data_file) as f:
                verification_data[verification_type] = json.load(f)
        else:
            # Need to load tapes to get this verification data
            load_tapes = True
            need_verification_data[verification_type] = True

    if load_tapes:
        find_tape_devices()
        check_for_missing_tapes(list(tapes))
        print("All tapes accounted for:", ", ".join(tapes))

        pbar = tqdm.tqdm(tapes, leave=False)
        for tape in pbar:
            pbar.set_description(f"Processing tapes (currently on {tape})")
            drive_num = load_tape(tape, print_func=tqdm.tqdm.write)

            with mount_drive(drive_num, print_func=tqdm.tqdm.write):
                if not args.skip_copy:
                    tqdm.tqdm.write(f"Copying files from tape {tape}")
                    copy_season_dir(Path(args.root_path), Path(args.target_path))

                if not any(need_verification_data.values()):
                    continue

                # get verification data if any verify flags were set
                tqdm.tqdm.write(f"Reading files from tape {tape} to get verification data")
                tape_pair, files_set = [(tape_pair, files_set) for tape_pair, files_set in files.items() if tape in tape_pair][0]  # filter for the fileset that corresponds to the current tape

                for file in tqdm.tqdm(files_set, leave=False, desc="Getting verification data from files"):
                    file_path = TAPE_MOUNT_PATH / Path(args.root_path).relative_to("/").parent / file

                    for verification_type in verification_data:
                        if not args.__dict__.get(verification_type, False):
                            continue

                        if not need_verification_data[verification_type]:
                            # already pulled this data from the json cache
                            continue

                        verification_data[verification_type][str(file)] = verification_functions[verification_type](file_path)

    # cache the verification data
    if any(need_verification_data.values()):
        print("Dumping verification data to json cache")
        for verification_type in verification_data:
            if not args.__dict__.get(verification_type, False):
                continue

            with open(verification_data_files[verification_type], "w") as f:
                json.dump(verification_data[verification_type], f)

    if verify:
        print("Verifying files")
        # verify existence of all files
        for file in tqdm.tqdm(chain(*files.values()), leave=False, desc="Verifying files"):
            file_path = Path(args.target_path) / Path(file)
            if file_path.name.endswith("small_file_archive.tar"):
                continue
            if not file_path.exists() or os.path.getsize(file_path) == 0:
                tqdm.tqdm.write(f"Missing/empty file {file_path}")

            for verification_type in verification_data:
                if not args.__dict__.get(verification_type, False):
                    continue

                if verification_data[verification_type][str(file)] != verification_functions[verification_type](file_path):
                    tqdm.tqdm.write(f"{verification_type} failed for file {file_path}")

        print("Verification complete")
