"""
Automatically load tapes in the Qualstar Q40 and pull the files from the given list.

TAPES_FILE should point to a file produced by run_get_raw_files.m and map files
to tapes.

Author: Reece Mathews
"""
from subprocess import check_output
import subprocess
from collections import defaultdict, namedtuple
from contextlib import contextmanager
from pathlib import Path
import re
import os
import shutil
import tarfile
import time
import random
from getpass import getpass
import xattr
import time

# TODO: ltfs_ordered_copy directories or manually pull up the start block LTFS index and sort by that

CRESIS_TAPES = "CR0001L8 CR0002L8 CR0003L8 CR0004L8 CR0005L8 CR0006L8 CR0007L8 CR0008L8 CR0009L8 CR0010L8 CR0011L8 CR0012L8 CR0013L8 CR0014L8 CR0015L8 CR0016L8 CR0041L8 CR0042L8 CR0043L8 CR0044L8 CR0045L8 CR0046L8 CR0047L8 CR0048L8 CR0049L8 CR0050L8 CR0051L8 CR0052L8 CR0053L8 CR0054L8 CR0055L8 DM0000M8 DM0001M8 DM0002M8 DM0003M8 DM0004M8 DM0005M8 DM0006M8 DM0007M8 DM0008M8 DM0009M8 DM0010M8 DM0011M8 DM0012M8 DM0013M8 DM0014M8 DM0015M8 DM0016M8 DM0017M8 DM0018M8 DM0019M8 DM0020M8 DM0021M8 DM0022M8 DM0023M8 DM0024M8 DM0025M8 DM0026M8 DM0027M8 DM0028M8 DM0029M8 DM0030M8 DM0031M8 DM0032M8 DM0033M8 DM0034M8 DM0036M8 DM0037M8 DM0038M8 DM0039M8 DM0040M8 DM0041M8 DM0042M8 DM0043M8 DM0044M8 DM0045M8 DM0046M8 DM0047M8 DM1035L8 DM1036L8 DM1037L8 DM1038L8 DM1039L8 DM1040L8 DM1041L8 DM1042L8 DM1043L8 DM1044L8 DM1045L8 DM1046L8 DM1047L8 OIB000L8 OIB001L8 OIB002L8 OIB003L8 OIB004L8 OIB005L8 OIB006L8 OIB007L8 OIB008L8 OIB009L8 OIB010L8 OIB011L8 OIB014L8 OIB016L8 OIB018L8 OIB023L8 OIB024L8 OIB026L8 OIB028L8 OIB032L8 OIB033L8 OIB034L8 OIB036L8 OIB038L8 OIB040L8 OIB042L8 OIB044L8 OIB046L8 OIB048L8 OIB050L8 OIB052L8 OIB054L8 OIB056L8 OIB058L8 OIB060L8 OIB062L8 OIB064L8 OIB066L8 OIB068L8 OIB070L8 OIB072L8 OIB074L8 OIB076L8 OIB078L8 OIB080L8 OIB082L8 OIB084L8 OIB086L8 OIB088L8 OIB090L8 OIB092L8 OIB094L8 OIB096L8 OIB098L8 OIB100L8 OIB102L8 OIB104L8 OIB106L8 OIB108L8 OIB110L8 OIB112L8 OIB114L8 OIB116L8 OIB118L8 OIB120L8 OIB122L8 OIB124L8 OIB202L8 OIB204L8 OIB206L8 OIB208L8 OIB216L8 OIB218L8 OIB220L8 OIB222L8 OIB224L8 OIB226L8 OIB228L8 OIB230L8 OIB232L8 OIB234L8 OIB236L8 OIB238L8 OIB240L8 OIB242L8 OIB244L8 OIB246L8 OIB248L8 OIB250L8 OIB252L8 OIB254L8 OIB256L8 OIB258L8 OIB260L8 OIB262L8 OIB264L8 OIB266L8 OIB268L8 OIB270L8 OIB272L8 OIB274L8 OIB276L8 OIB278L8 OIB280L8 OIB282L8 OIB284L8 OIB286L8 OIB288L8 OIB290L8 OIB292L8 OIB294L8 OIB296L8 OIB298L8 OIB300L8 OIB302L8 OIB304L8 OIB306L8 OIB308L8 OIB310L8 OIB312L8 OIB314L8 OIB316L8 OIB318L8 OIB320L8 OIB322L8 OIB324L8"
OTHER_TAPES = "OIB015L8 OIB017L8 OIB025L8 OIB027L8 OIB029L8 OIB031L8 OIB035L8 OIB037L8 OIB039L8 OIB041L8 OIB043L8 OIB045L8 OIB047L8 OIB049L8 OIB051L8 OIB053L8 OIB055L8 OIB057L8 OIB059L8 OIB061L8 OIB063L8 OIB065L8 OIB067L8 OIB069L8 OIB071L8 OIB073L8 OIB075L8 OIB077L8 OIB079L8 OIB081L8 OIB083L8 OIB085L8 OIB087L8 OIB089L8 OIB091L8 OIB093L8 OIB095L8 OIB097L8 OIB099L8 OIB101L8 OIB103L8 OIB105L8 OIB107L8 OIB109L8 OIB111L8 OIB113L8 OIB115L8 OIB117L8 OIB119L8 OIB121L8 OIB123L8 OIB203L8 OIB205L8 OIB207L8 OIB209L8 OIB215L8 OIB217L8 OIB219L8 OIB221L8 OIB223L8 OIB225L8 OIB227L8 OIB229L8 OIB231L8 OIB233L8 OIB235L8 OIB237L8 OIB239L8 OIB241L8 OIB243L8 OIB245L8 OIB247L8 OIB249L8 OIB251L8 OIB253L8 OIB255L8 OIB257L8 OIB259L8 OIB261L8 OIB263L8 OIB265L8 OIB267L8 OIB269L8 OIB271L8 OIB273L8 OIB275L8 OIB277L8 OIB279L8 OIB281L8 OIB283L8 OIB285L8 OIB287L8 OIB289L8 OIB291L8 OIB293L8 OIB295L8 OIB297L8 OIB299L8 OIB301L8 OIB303L8 OIB305L8 OIB307L8 OIB309L8 OIB311L8 OIB313L8 OIB315L8 OIB317L8 OIB319L8 OIB321L8 OIB323L8"
TAPES_FILE = '/root/utilities/christoffersen_2019_antarctica_gv_20191109_test.txt'  # Produced from run_get_raw_files.m
TAPE_LIBRARY_MODEL = "MULTISTAK"  # 4th column returned from lsscsi -g
TAPE_MOUNT_PATH = "/mnt/ltfs"

# Determine order with `ltfs -o device_list`
# these values are determined automatically and overwritten if find_tape_devices is ran
TAPE_DRIVE_DEVS = {
    "0": "/dev/sg8",  # nst0/st0
    "1": "/dev/sg7",  # nst1/st1
}
# determine with lsscsi -g. Also determined automatically and overwritten if find_tape_devices is ran
TAPE_LIBRARY_DEV = '/dev/sg11'
SKIP_EXISTING = True  # Do not ask to overwrite existing files and just skip instead
FS_PATH_SUBS = {  # Path subsitutions for destinations on the filesystem
   "/N/dc2/projects/cresis/": "/kucresis/scratch/data/MCoRDS/",
   "/N/dcwan/projects/cresis/": "/kucresis/scratch/data/MCoRDS/"
}

SSH_SERVER = "dtn.ku.edu"
SSH_PORT = 22


def find_tape_devices():
    """Find the tape device by trying mtx commands on various /dev/sg* devices."""
    global TAPE_LIBRARY_DEV, TAPE_DRIVE_DEVS
    unmount_drive()

    print("Finding tape devices")

    # Find library
    device_output = check_output(["lsscsi", "-g"]).decode()
    for line in device_output.split("\n"):
        if (match := re.match(r"[\[0-9:\]]+\s*mediumx\s*[A-Z]+\s*%s\s*[A-Z0-9.]+\s*[/a-z0-9]+\s*([/a-z0-9]+)" % TAPE_LIBRARY_MODEL, line)):
            TAPE_LIBRARY_DEV = match.group(1)
            print(f"Tape library found at {TAPE_LIBRARY_DEV}")
            break
    else:
        raise RuntimeError("Could not find tape library in output of lscsci -g")

    # find tape drives
    drive_num = 0
    device_output = check_output(["/bin/sh", "-c", "ltfs -o device_list ; exit 0"], stderr=subprocess.STDOUT).decode()
    for line in device_output.split("\n"):
        if (match := re.match(r"Device Name = ([/a-z0-9]+)", line)):
            TAPE_DRIVE_DEVS[str(drive_num)] = match.group(1)
            print(f"Tape drive {drive_num} found at {TAPE_DRIVE_DEVS[str(drive_num)]}")
            drive_num += 1

    if drive_num == 0:
        raise RuntimeError("Did not find any tape drives in output of `ltfs -o device_list`")


cresis_tapes = set(CRESIS_TAPES.split())
other_tapes = set(OTHER_TAPES.split())
def is_tape_here(tape):
    """Determine if tape is located at CReSIS."""
    if tape in cresis_tapes:
        return True
    elif tape in other_tapes:
        print(f"Tape {tape} located at backup center")
    else:
        print(f"Tape {tape} is unknown")

    return False


def is_blank(file_path, print_output=True):
    """
    Check if the file at file_path is composed entirely of zeros by checking a
    random section. This happens if the file was allocated for SCP but then SCP fails,
    leaving the file filled with zeros behind.
    """
    file_size = os.path.getsize(file_path)
    with open(file_path, 'rb') as f:
        if file_size > 100:
            # File is large enough to read a random section instead of from the start
            f.seek(random.randrange(0, file_size - 100))

        contents = f.read(100)
        if any(contents):
            return False

    if print_output:
        print(f"Existing file {file_path} appears to be blank and will be overwritten")
    return True


def is_file_ok(file_path: Path, print_output=True):
    """Checks if a file exists and isn't blank."""
    if not file_path.exists() or os.path.getsize(file_path) == 0 or is_blank(file_path, print_output):
        return False
    return True


def transfer_tape(source_slot: str, target_slot: str):
    """Move a tape from the source to the target slot."""
    check_output(["mtx", "-f", TAPE_LIBRARY_DEV, "transfer", source_slot, target_slot])


def mail_tapes(tapes_to_unload: 'list[str]'):
    """Move all the provided tapes into the mailslots."""
    inv = inventory()
    for tape_barcode in tapes_to_unload:
        tape = find_tape_slot(tape_barcode, inv)
        if tape.mailslot:
            continue  # tape is already in a mailslot
        free_mailslot = find_empty_slot(inv, True)

        # no mailslots available, move something out of mailslot
        if free_mailslot is None:
            free_slot = find_empty_slot(inv, False)
            if free_slot is None:
                raise RuntimeError("No free slots to move a potentially needed mailslot tape into")

            for tape_ in inv:
                if tape_.mailslot and tape_.barcode not in tapes_to_unload:
                    # found a tape to remove from mailslot and have a free slot to move it to
                    print(f"Freeing mailslot {tape_.slot_num} of tape {tape_.barcode} by moving it to slot {free_slot}")
                    transfer_tape(tape_.slot_num, free_slot)
                    inv = inventory()
                    free_mailslot = tape_.slot_num
                    break
            else:
                # no tapes found in mailslots that can be moved into main machine.
                # this means all tapes to remove are already in the mailslots
                # which means the first continue at the top of this function should have been hit
                raise RuntimeError("All tapes already in mailslot. Perhaps this function has an error in it.")

        # move the tape into the mailslot
        print(f"Moving unneeded tape {tape_barcode} into mailslot {free_mailslot} from slot {tape.slot_num}")
        transfer_tape(tape.slot_num, free_mailslot)
        inv = inventory()

    # check that all tapes in mailslot should be removed by user
    for tape_ in inv:
        if tape_.mailslot and tape_.barcode not in tapes_to_unload:
            free_slot = find_empty_slot(inv, False)
            if free_slot is None:
                raise RuntimeError("No free slots to move an extraneous mailslot tape into")
            print(f"Removing extraneous tape {tape_.barcode} from mailslot {tape_.slot_num} to slot {free_slot}")
            transfer_tape(tape_.slot_num, free_slot)
            inv = inventory()

    print("All tapes in mailslot can now be removed and replaced with required tapes")


def check_for_missing_tapes(tapes_to_check: 'list[str]'):
    """Check if any of the tapes in the given list need to be added to the library."""
    # check if any tapes are not in the library
    while True:
        missing_tapes = set()
        for tape in tapes_to_check:
            try:
                find_tape_slot(tape)
            except RuntimeError:
                missing_tapes.add(tape)

        if missing_tapes:

            print("The following tapes need to be added to the tape library:")
            print(", ".join(sorted(missing_tapes)))

            # find tapes that can be removed
            inv = inventory()
            print("The following tapes can be removed")
            other_tapes = [(tape.slot_num, tape.slot_type, tape.barcode) for tape in inv if tape.barcode not in tapes_to_check and tape.barcode.strip()]
            print(", ".join([f"{slot} ({slot_type}): {barcode}" for slot, slot_type, barcode in sorted(other_tapes)]))

            num_to_unload = min([len(missing_tapes), 5])

            if (inp := input(f"Try again (y), continue without (c), or load the first {num_to_unload} into mailslots (l)?").lower()).startswith("y"):
                continue
            elif inp.startswith("c"):
                break
            elif inp.startswith("l"):
                # Find unneeded tapes that aren't in a drive and put them in the mailslots
                mail_tapes([tape[-1] for tape in other_tapes if tape[1] == "Storage"][:num_to_unload])
        else:
            break


def parse_tapes_file():
    """Read the tapes file into a dict."""
    tape_mapping = defaultdict(list)
    path_mapping = {}
    all_files = set()

    with open(TAPES_FILE) as f:
        season = None
        for line in f:
            if season is None:
                season = line
                continue
            if line.startswith("Filelist: "):
                continue
            if line.startswith("tapes filename stored_filename"):
                continue
            if line.strip() == "":
                season = None
                continue

            parts = line.split()
            tapes = parts[0].split(",")
            original_path = parts[1]
            tape_path = parts[2]

            path_mapping[tape_path] = original_path

            if tape_path in all_files:
                print("Ignoring duplicate file " + tape_path)
            all_files |= {tape_path}

            for tape in tapes:
                if is_tape_here(tape):
                    if tape_path not in tape_mapping[tape]:
                        tape_mapping[tape].append(tape_path)
                    break
            else:
                raise RuntimeError("No tape for " + tape_path)

    if SKIP_EXISTING:
        # Remove existing files from list
        print("Checking if existing files are empty and need replaced")
        for tape, file_list in tape_mapping.items():

            files = []
            for file in file_list:
                # Keep files that don't exist or are 0 bytes
                file_path = Path(path_subs(path_mapping[file]))
                if not is_file_ok(file_path):
                    files.append(file)

            tape_mapping[tape] = files

    check_for_missing_tapes(list(tape_mapping))

    return tape_mapping, path_mapping


InvSlot = namedtuple("InvSlot", "slot_type slot_num mailslot full original_slot_num barcode")
def inventory():
    """Check the tape library's current inventory."""
    INV_RE_MATCH_INDICES = {
        "slot_type": 0,
        "slot_num": 1,
        "mailslot": 2,
        "full": 3,
        "original_slot_num": 7,
        "barcode": 8
    }
    INV_RE_PATTERN = r"(Storage|Data Transfer) Element ([0-9]+)\s?(IMPORT\/EXPORT)?:(Full|Empty)( (\((Unknown )?Storage Element ([0-9]*)\s?Loaded\))?:VolumeTag\s?=\s?(\w+))?"

    inv_output = check_output(["mtx", "-f", TAPE_LIBRARY_DEV, "status"]).decode()
    inv_output = "\n".join(line.strip() for line in inv_output.split("\n"))
    matches = re.findall(INV_RE_PATTERN, inv_output)
    return [InvSlot(**{k: match[i] for k, i in INV_RE_MATCH_INDICES.items()}) for match in matches]


def find_empty_slot(inv: 'list[InvSlot]', mailslot=None):
    """Find an empty slot for a tape to be placed in."""
    for slot_ in inv:
        if slot_.slot_type != "Data Transfer" and slot_.full == "Empty":
            if mailslot is None or bool(mailslot) == bool(slot_.mailslot):
                return slot_.slot_num

    return None


def find_tape_slot(tape_barcode: str, inv=None):
    """Find which slot a tape is in in the tape library."""
    if inv is None:
        inv = inventory()
    for slot_ in inv:
        if slot_.barcode == tape_barcode:
            return slot_
    raise RuntimeError("Tape not present: " + tape_barcode)


def load_tape(tape_barcode, print_func=print):
    """Load a tape into a drive"""
    inv = inventory()

    slot = find_tape_slot(tape_barcode, inv)

    if slot.slot_type == "Data Transfer":
        # tape already in drive
        print_func("Tape " + slot.barcode + " already loaded in drive " + slot.slot_num)
        return slot.slot_num

    # Find open drive
    drive_slot = None
    for slot_ in inv:
        if slot_.slot_type == "Data Transfer":
            drive_slot = slot_
            if slot_.full == "Empty":
                break
    else:
        # All drives full, unload last drive found
        if drive_slot is None:
            raise RuntimeError("No drives found")

        original_slot_num = drive_slot.original_slot_num or find_empty_slot(inv)
        if not original_slot_num:
            raise RuntimeError(f"Could not find a slot to place the tape {drive_slot.barcode} back into")

        print_func("Unloading " + drive_slot.barcode + " from drive " + drive_slot.slot_num + " to slot " + original_slot_num)
        check_output(["mtx", "-f", TAPE_LIBRARY_DEV, "unload", original_slot_num, drive_slot.slot_num])

    print_func("Loading " + slot.barcode + " from slot " + slot.slot_num + " to drive " + drive_slot.slot_num)
    check_output(["mtx", "-f", TAPE_LIBRARY_DEV, "load", slot.slot_num, drive_slot.slot_num])

    return drive_slot.slot_num


def unmount_drive(print_func=print):
    """Unmount the TAPE_MOUNT_PATH."""
    if os.path.ismount(TAPE_MOUNT_PATH):
        print_func(f"Unmounting {TAPE_MOUNT_PATH}")
        check_output(["umount", TAPE_MOUNT_PATH])


@contextmanager
def mount_drive(drive_num, print_func=print):
    """Mount the given drive with LTFS."""
    while True:
        unmount_drive(print_func)
        drive_dev = TAPE_DRIVE_DEVS[drive_num]
        # TODO[reece]: Handle mount hanging forever
        print_func("Mounting " + drive_dev)
        subprocess.call(["ltfs", "-o", f"devname={drive_dev}", TAPE_MOUNT_PATH],
                        stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        if not os.listdir(TAPE_MOUNT_PATH):
            print_func("Failed to mount " + drive_dev + f" at {TAPE_MOUNT_PATH}")
            time.sleep(1)
            continue
        break

    try:
        yield
    finally:
        unmount_drive(print_func)


def path_subs(path):
    """Perform each path substitution on the given path."""
    for sub in FS_PATH_SUBS:
        path = path.replace(sub, FS_PATH_SUBS[sub])
    return path.replace('\\', '/')


def get_ssh_client():
    """Prompt the user for login details for the ssh server."""
    import paramiko
    from scp import SCPClient

    try:
        # See if we have a previous connection
        return get_ssh_client.scp_client

    except AttributeError:
        pass # No previous connection, make a new one

    while True:
        try:
            # See if credentials are still stored (as in when a timeout occurs)
            ssh_user = get_ssh_client.ssh_user
            ssh_password = get_ssh_client.ssh_password
        except AttributeError:
            # Ask for credentials instead
            ssh_user = input(f"Enter your SSH username for {SSH_SERVER}: ")
            ssh_password = getpass(f"Enter your SSH password for {SSH_SERVER}: ")

        # Create the client
        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        get_ssh_client.ssh_client = ssh_client

        # Attempt to connect to the SSH Server
        print(f"Attempting to connect to scp server {SSH_SERVER}")
        try:
            ssh_client.connect(SSH_SERVER, port=SSH_PORT, username=ssh_user, password=ssh_password)
            get_ssh_client.ssh_user = ssh_user
            get_ssh_client.ssh_password = ssh_password
            break

        except paramiko.AuthenticationException:
            print("Username or password incorrect")

    scp_client = SCPClient(ssh_client.get_transport())
    get_ssh_client.scp_client = scp_client

    return scp_client


def scp_file_from_server(remote_file_path, fs_file_path, attempts=3):
    """Copy a file not on our tapes from the ssh server."""
    from scp import SCPException
    import paramiko

    scp_client = get_ssh_client()

    try:
        time.sleep(.1)
        scp_client.get(remote_path=remote_file_path, local_path=fs_file_path)
    except (SCPException, paramiko.buffered_pipe.PipeTimeout, paramiko.ssh_exception.SSHException) as e:
        # Try connecting again if there is a timeout
        if attempts < 0:
            print(f"**Failed to copy {e.args} file from server: " + remote_file_path)
            return

        if type(e.args) is tuple and len(e.args) > 0 and "Permission denied" in e.args[0]:
            print(f"User does not have access permissions on file: {remote_file_path}. This likely means the CRC does not have the tape which this file is on.")
            return

        print("Timeout during SCP, waiting and trying again")
        time.sleep(.5)
        get_ssh_client.scp_client.close()
        get_ssh_client.ssh_client.close()
        del get_ssh_client.scp_client  # Reset stored connection
        return scp_file_from_server(remote_file_path, fs_file_path, attempts=attempts-1)


def extract_small_file_archive(file_path: Path):
    """If the provided file is a small_file_archive, extract it and then delete."""
    cwd = os.getcwd()

    if file_path.name.endswith("small_file_archive.tar"):
        print(f"Extracting and removing {file_path}")
        os.chdir(file_path.parent)
        with tarfile.open(file_path) as tar:
            tar.extractall()
        if os.path.exists("delete_this_zero_file"):
            os.remove("delete_this_zero_file")

        os.remove(file_path)

    os.chdir(cwd)


def copy_file(file, path_mapping, attempts=1, remote=False):
    """Copy the given file to its original locations."""
    # Perform sanity checks on file and destination
    # fs = filesystem (destination) paths as opposed to tape (source) paths

    # Source checks
    if remote:
        remote_file_path = "/" + file.replace('\\', '/').lstrip('/')
        print(f"\nsource {SSH_SERVER}:{remote_file_path}")
    else:
        tape_file_path = Path(TAPE_MOUNT_PATH) / file.replace('\\', '/').lstrip('/')
        tape_file_exists = tape_file_path.exists()
        tape_file_size = os.path.getsize(tape_file_path) / 1024 ** 2 if tape_file_exists else None
        print("\nsource (exists:)", tape_file_exists, f"{tape_file_size if tape_file_size is not None else 0} MB", tape_file_path)

        if not tape_file_exists:
            if attempts > 0:
                print("Could not find source file on tape, waiting one second and retrying.")
                time.sleep(1)
                return copy_file(file, path_mapping, attempts=attempts-1, remote=remote)
            input("**Could not find source file on tape, perhaps try remounting. Press enter to skip.**")

    # Destination checks
    fs_file_path = Path(path_subs(path_mapping[file]))
    fs_file_exists = fs_file_path.exists()
    fs_file_size = os.path.getsize(fs_file_path) / 1024 ** 2 if fs_file_exists else None
    print("-> destination (exists:)", fs_file_exists, f"{fs_file_size if fs_file_size is not None else 0} MB", fs_file_path)

    # Check if destination already exists on filesystem
    if not SKIP_EXISTING:
        if fs_file_exists and (fs_file_size is not None and fs_file_size > 0):
            if input("**File already exists on file system**, skip (y) or halt (n)?") == "y":
                print("Already exists, skipping")
                return
            else:
                raise RuntimeError("File already exists on file system")

    # Check if parent folder path exists
    fs_parent_path = fs_file_path.parent
    if not fs_parent_path.exists():
        os.makedirs(fs_parent_path, exist_ok=True)

    # Perform copy
    if remote:
        scp_file_from_server(remote_file_path, fs_file_path)
    else:
        # TODO: replace with ltfs_ordered_copy.
        shutil.copy2(tape_file_path, fs_file_path)

    extract_small_file_archive(fs_file_path)

    os.chdir("/")


def get_startblock(file: Path):
    """Taken and modified from ltfs_ordered_copy CopyItem.eval method."""
    part = b'a'
    start = 0

    try:
        vuuid = xattr.getxattr(file, 'user.ltfs.volumeUUID')
    except Exception:
        vuuid = ''
        return (vuuid, part, start)

    try:
        part  = xattr.getxattr(file, 'user.ltfs.partition')
        start_str  = xattr.getxattr(file, 'user.ltfs.startblock')
        start = int(start_str)
    except Exception:
        pass

    return (vuuid, part, start)


def load_tapes(tape_mapping, path_mapping):
    """Load each tape and retrieve the files back to their original location."""
    for tape in tape_mapping:

        if not tape_mapping[tape]:
            # Skip empty file sets
            print("Skipping tape with no files: " + tape)
            continue

        files = tape_mapping[tape]

        if not is_tape_here(tape):
            check_ras = input("Tape might be in RAS: " + tape + f" should we attempt to retrieve from {SSH_SERVER}? [yn]")
            if not check_ras.lower().startswith("y"):
                print(f"Skipping tape: {tape}")
                continue
            for file in files:
                copy_file(file, path_mapping, remote=True)

            continue

        # Load files from tape
        try:
            drive_num = load_tape(tape)
        except RuntimeError:
            print("Skipping missing tape: " + tape)
            print("- Did not restore files:")
            for file in files:
                print("* ", TAPE_MOUNT_PATH + file, "->", path_subs(path_mapping[file]))
            continue

        with mount_drive(drive_num):
            # get ltfs-optimized sort order
            file_blocks = defaultdict(list)
            for file in files:
                tape_file_path = Path(TAPE_MOUNT_PATH) / file.replace('\\', '/').lstrip('/')
                file_blocks[get_startblock(tape_file_path)].append(file)

            # Copy files in this order
            start_time = time.time()
            for start_block in sorted(file_blocks.keys()):
                partitioned_files = file_blocks[start_block]
                for file in sorted(partitioned_files):
                    copy_file(file, path_mapping)
            print(f"Finished in {(time.time() - start_time) / 60} minutes")


if __name__ == "__main__":
    if os.geteuid() != 0:
        raise RuntimeError("Must be ran as root to access tape device")

    find_tape_devices()
    tape_mapping, path_mapping = parse_tapes_file()
    load_tapes(tape_mapping, path_mapping)

    print("Finished restoring files.")

    try:
        # Close scp connection if it was opened
        get_ssh_client.scp_client.close()

    except AttributeError:
        pass
