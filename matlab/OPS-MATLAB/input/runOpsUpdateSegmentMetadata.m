% script runOpsUpdateSegmentMetadata.m
%
% Example script for running opsUpdateSegmentMetadata.m.
%
% Authors: Reece Mathews
%
sys = 'rds';

% params = read_param_xls(ct_filename_param('rds_param_2006_Greenland_TO.xls'), '20060601_01', {'metadata'});
params = read_param_xls(ct_filename_param('rds_param_2006_Greenland_TO.xls'), '20060601_01');
params = ct_set_params(params,'cmd.generic',1);
params = ct_set_params(params,'cmd.generic',0,'cmd.notes','do not process');

params(1).properties.metadata.rors = {'04t2m2598'};
params(1).properties.metadata.dois = {'10.18739/A2FX73Z5C'};
params(1).properties.metadata.funding_sources = {'0424589'};

params(1).properties.segment = '20060601_01';
params(1).properties.season = '2006_Greenland_TO';

for param_idx = 1:length(params)
  param = params(param_idx);
  if ~isfield(param.cmd,'generic') || iscell(param.cmd.generic) ...
      || ischar(param.cmd.generic) || ~param.cmd.generic
    % Skip if generic is not true
    continue;
  end
  param = merge_structs(param,gRadar);

  opsUpdateSegmentMetadata(sys, param);
end
