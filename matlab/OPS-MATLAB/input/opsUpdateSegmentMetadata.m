function [status,data] = opsUpdateSegmentMetadata(sys,param)
%
% [status,data] = opsUpdateSegmentMetadata(sys,param)
%
% Update the metadata associated with a segment in the database.
%
% Input:
%   sys: (string) sys name ('rds','accum','snow',...)
%   param: structure with fields
%     properties.segment_id = integer
%         or
%     properties.segment = string
%     properties.season = string
%
% Optional Input:
%     properties.metadata.dois = cell string (or string)
%     properties.metadata.funding_sources = cell string (or string)
%     properties.metadata.rors = cell string (or string)
%
% Output:
%   status: integer (0:Error,1:Success,2:Warning)
%
% Author: Reece Mathews

% CONSTRUCT THE JSON STRUCTURE
[param,~,~] = opsAuthenticate(param);
jsonStruct = struct('properties',param.properties);

% CONVERT THE JSON STRUCTURE TO A JSON STRING
try
  jsonStr = tojson(jsonStruct);
catch ME
  jsonStr = savejson('',jsonStruct,'FloatFormat','%2.10f','NaN','null');
end

% SEND THE COMMAND TO THE SERVER
opsCmd;
if gOps.profileCmd
  [jsonResponse,~] = opsUrlRead(strcat(gOps.serverUrl,'profile'),gOps.dbUser,gOps.dbPswd,...
    'Post',{'app' sys 'data' jsonStr 'view' 'updateSegmentMetadata'});
else
  [jsonResponse,~] = opsUrlRead(strcat(gOps.serverUrl,'update/segment/metadata'),gOps.dbUser,gOps.dbPswd,...
    'Post',{'app' sys 'data' jsonStr});
end

% DECODE THE SERVER RESPONSE
[status,decoded_json] = jsonResponseDecode(jsonResponse);
end