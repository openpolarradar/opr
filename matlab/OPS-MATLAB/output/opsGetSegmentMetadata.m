function [status,data] = opsGetSegmentMetadata(sys,param)
%
% [status,data] = opsGetSegmentMetadata(sys,param)
%
% Retrieves metadata for a single segment id from the database.
%
% Input:
%   sys: (string) sys name ('rds','accum','snow',...)
%   param: structure with fields
%     properties.segment_id = integer
%         or
%     properties.segment = string
%     properties.season = string
%
% Output:
%   status: integer (0:Error,1:Success,2:Warning)
%   data: structure with fields (or error message)
%       properties.metadata.dois = string array 
%       properties.metadata.funding_sources = string array
%       properties.metadata.rors = string array
%
% Author: Reece Mathews

% CONSTRUCT THE JSON STRUCTURE
jsonStruct = struct('properties',param.properties);

% CONVERT THE JSON STRUCTURE TO A JSON STRING
try
    jsonStr = tojson(jsonStruct);
catch ME
    jsonStr = savejson('',jsonStruct,'FloatFormat','%2.10f','NaN','null');
end

% SEND THE COMMAND TO THE SERVER
opsCmd;
if gOps.profileCmd
    [jsonResponse,~] = opsUrlRead(strcat(gOps.serverUrl,'profile'),gOps.dbUser,gOps.dbPswd,...
        'Post',{'app' sys 'data' jsonStr 'view' 'getSegmentMetadata'});
else
    [jsonResponse,~] = opsUrlRead(strcat(gOps.serverUrl,'get/segment/metadata'),gOps.dbUser,gOps.dbPswd,...
        'Post',{'app' sys 'data' jsonStr});
end

% DECODE THE SERVER RESPONSE
[status,decodedJson] = jsonResponseDecode(jsonResponse);

% CREATE THE DATA OUPUT STRUCTURE OR MESSAGE
data.properties.metadata.dois = decodedJson.dois;
data.properties.metadata.funding_sources = decodedJson.funding_sources;
data.properties.metadata.rors = decodedJson.rors;
end