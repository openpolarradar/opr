% script runOpsGetSegmentMetadata
%
% Example script for running opsGetSegmentetadata
%

%% User Settings
sys = 'rds';

param = [];
param.properties.season = '2014_Greenland_P3';
param.properties.segment = '20140313_05';

%% Query database
[status,message] = opsGetSegmentMetadata(sys,param);
