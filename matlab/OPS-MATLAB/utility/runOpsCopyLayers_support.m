% script runOpsCopyLayers_support.m
%
% Support script for runOpsCopyLayers.m called at the end.
%
% Authors: John Paden

% =====================================================================
%% Automated Section
% =====================================================================

global gRadar;

% Input checking
if exist('param_override','var')
  param_override = merge_structs(gRadar,param_override);
else
  param_override = gRadar;
end

%% Copy layers for each of the enabled segments
failed_segments = [];
ops_pause_en = false;
ops_pause_time = 180;
for param_idx = 1:length(params)
  param = params(param_idx);
  if ~isfield(param.cmd,'generic') || iscell(param.cmd.generic) || ischar(param.cmd.generic) || ~param.cmd.generic
    continue;
  end
  param = merge_structs(param,param_override);
  if iscell(copy_param.layer_source.name) && iscell(copy_param.layer_dest.name) ...
      && numel(copy_param.layer_source.name) == numel(copy_param.layer_dest.name)
    for layer_source_idx = 1:length(copy_param.layer_source.name)
      fprintf('opsCopyLayers %s from %s:%s to %s:%s (%s)\n', param.day_seg, copy_param.layer_source.source, copy_param.layer_source.name{layer_source_idx}, copy_param.layer_dest.source, copy_param.layer_dest.name{layer_source_idx}, datestr(now));
    end
  else ischar(copy_param.layer_source.name)
    fprintf('opsCopyLayers %s from %s:%s to %s:%s (%s)\n', param.day_seg, copy_param.layer_source.source, copy_param.layer_source.name, copy_param.layer_dest.source, copy_param.layer_dest.name, datestr(now));
  end
  try
    if strcmpi(copy_param.layer_source.source,'OPS') || strcmpi(copy_param.layer_dest.source,'OPS')
      if ~ops_pause_en
        ops_pause_en = true;
      else
        fprintf(' Pausing %d sec to let OPS database finish writes\n', ops_pause_time);
        pause(ops_pause_time);
      end
    end
    opsCopyLayers(param,copy_param);
  catch ME
    failed_segments(end+1).param_idx = param_idx;
    failed_segments(end).report = ME.getReport;
    failed_segments(end).message = ME.message;
    %keyboard
  end
  fprintf('  Complete (%s)\n', datestr(now));
end

for failed_idx = 1:length(failed_segments)
  fprintf('%s: %s\n', params(failed_segments(failed_idx).param_idx).day_seg, ...
    failed_segments(failed_idx).message);
end
