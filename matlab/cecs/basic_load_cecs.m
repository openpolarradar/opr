function [hdr, data] = basic_load_cecs(fn,param)
% [hdr, data] = basic_load_cecs(fn,param)

% ===================================================================
%% Check input arguments
% ===================================================================
if ~exist('param','var')
  param = struct();
end
% param.truncate_records: Enable to truncate records so each channel has
% the same number of records.
if ~isfield(param,'truncate_records')
  param.truncate_records = true;
end
% param.fix_pps: correct for the PPS isolated/single sample errors
if ~isfield(param,'fix_pps')
  param.fix_pps = true;
end
% param.fix_nmea_time: correct for the NMEA time lag
if ~isfield(param,'fix_nmea_time')
  param.fix_nmea_time = true;
end
% param.clk: clock frequency for pps counter field
if ~isfield(param,'clk')
  param.clk = 200e6;
end
if param.clk ~= 200e6
  error('param.clk should be 200e6.');
end
% param.recs: specify which records to load from the file [NOT SUPPORTED]
if ~isfield(param,'recs') || isempty(param.recs)
  param.recs = [0 inf];
else
  warning('param.recs is not supported. Reading whole file.');
  param.recs = [0 inf];
end
% param.first_byte: skip this many bytes in the file before starting to
% read
if ~isfield(param,'first_byte') || isempty(param.first_byte)
  param.first_byte = 0;
end
% param.Nt_RDS: Number of samples per trace (fixed)
if ~isfield(param,'Nt_RDS') || isempty(param.Nt_RDS)
  param.Nt_RDS = 4096;
end
% param.N_traces_max: Maximum number of traces per file
if ~isfield(param,'N_traces_max') || isempty(param.N_traces_max)
  param.N_traces_max = 16001;
end

% CH1: channel 1, high gain, 10 us chirp pulse
% CH2: channel 2, low gain, 1 us chirp pulse

% Indexes for each channel
cnt1 = 0;
cnt2 = 0;

data = {};
hdr = {};

% fprintf('Reading file: %s\n', fn);
fid = fopen(fn,'rb','l'); % Open file in little-endian format
fseek(fid,0,'eof');
hdr{1}.file_size = ftell(fid);
hdr{2}.file_size = hdr{1}.file_size - param.first_byte;
fseek(fid,param.first_byte,'bof');
preallocated = false;
sync = hex2dec('FFFF1234');
while ~feof(fid)
  sync_tmp = fread(fid,1,'uint32'); % SYNC word (0xFFFF1234)
  while ~feof(fid) && sync_tmp ~= sync
    sync_tmp = fread(fid,1,'uint32'); % SYNC word (0xFFFF1234)
  end
  offset = ftell(fid)-4; % Byte offset into file
  epri = fread(fid,1,'uint32')/2;  % Trace or pulse counter (divide by 2 to account for there being 2 waveform records per EPRI)
  wf = fread(fid,1,'uint32'); % Channel number

  if wf == 0

    cnt1 = cnt1 + 1;

    hdr{1}.sync(:,cnt1) = sync_tmp;
    hdr{1}.epri(:,cnt1) = epri;
    hdr{1}.wf(:,cnt1) = wf+1;
    hdr{1}.offset(:,cnt1) = offset;
    hdr{1}.pps(:, cnt1) = fread(fid, 1, 'uint32');
    hdr{1}.time(:, cnt1) = fread(fid, 1, 'uint32');
    hdr{1}.nmea_gpsquality(:, cnt1) = fread(fid, 1, 'uint32');
    hdr{1}.nmea_time(:, cnt1) = fread(fid, 1, 'float');
    hdr{1}.nmea_lat(:, cnt1) = fread(fid, 1, 'float');
    hdr{1}.nmea_lon(:, cnt1) = fread(fid, 1, 'float');
    hdr{1}.nmea_elev(:, cnt1) = fread(fid, 1, 'float');
    hdr{1}.nmea_geoid(:, cnt1) = fread(fid, 1, 'float');
    hdr{1}.nmea_vel(:, cnt1) = fread(fid, 1, 'float');
    hdr{1}.Nt(:, cnt1) = fread(fid, 1, 'uint32');

    [I_ch1, count] = fread(fid, hdr{1}.Nt(cnt1), 'int32');
    if count ~= hdr{1}.Nt(cnt1)
      warning('ERROR samples CH1(I)');
      cnt1 = cnt1 - 1;
      break
    end
    [Q_ch1, count] = fread(fid, hdr{1}.Nt(cnt1), 'int32');
    if count ~= hdr{1}.Nt(cnt1)
      warning('ERROR samples CH1(Q)');
      cnt1 = cnt1 - 1;
      break
    end

    data{1}(:, cnt1) = I_ch1 + 1i*Q_ch1;

  elseif wf == 1

    cnt2 = cnt2 + 1;

    hdr{2}.sync(:,cnt2) = sync_tmp;
    hdr{2}.epri(:,cnt2) = epri;
    hdr{2}.wf(:,cnt2) = wf+1;
    hdr{2}.offset(:,cnt2) = offset;
    hdr{2}.pps(:, cnt2) = fread(fid, 1, 'uint32');
    hdr{2}.time(:, cnt2) = fread(fid, 1, 'uint32');
    hdr{2}.nmea_gpsquality(:, cnt2) = fread(fid, 1, 'uint32');
    hdr{2}.nmea_time(:, cnt2) = fread(fid, 1, 'float');
    hdr{2}.nmea_lat(:, cnt2) = fread(fid, 1, 'float');
    hdr{2}.nmea_lon(:, cnt2) = fread(fid, 1, 'float');
    hdr{2}.nmea_elev(:, cnt2) = fread(fid, 1, 'float');
    hdr{2}.nmea_geoid(:, cnt2) = fread(fid, 1, 'float');
    hdr{2}.nmea_vel(:, cnt2) = fread(fid, 1, 'float');
    hdr{2}.Nt(:, cnt2) = fread(fid,1, 'uint32');

    [I_ch2, count] = fread(fid, hdr{2}.Nt(cnt2), 'int32');
    if count ~= hdr{2}.Nt(cnt2)
      warning('ERROR samples CH2(I)');
      cnt2 = cnt2 - 1;
      break;
    end
    [Q_ch2, count] = fread(fid, hdr{2}.Nt(cnt2), 'int32');
    if count ~= hdr{2}.Nt(cnt2)
      warning('ERROR samples CH2(Q)');
      cnt2 = cnt2 - 1;
      break;
    end

    data{2}(:, cnt2) = I_ch2 + 1i*Q_ch2;
  end

  if preallocated == false && cnt1 >= 1 && cnt2 >= 1
    preallocated = true;
    rec_size = 52 + hdr{1}.Nt*8 + 52 + hdr{2}.Nt*8;
    N_traces_max = ceil(hdr{1}.file_size / rec_size);
    if N_traces_max > cnt1
      % CH1 vars initiation
      data{1}(1,N_traces_max) = 0;
      hdr{1}.sync(1,N_traces_max) = 0;             % frame sync 0xFFFF1234
      hdr{1}.offset(1,N_traces_max) = 0;           % Byte offset into file
      hdr{1}.epri(1,N_traces_max) = 0;             % Pulse counter
      hdr{1}.wf(1,N_traces_max) = 0;               % Which channel (called waveform or wf in OPR toolbox)
      hdr{1}.pps(1,N_traces_max) = 0;              % PPS counter, 200 MHz clk
      hdr{1}.time(1,N_traces_max) = 0;             % Host PC time, ANSI-C format, UTC
      hdr{1}.nmea_gpsquality(1,N_traces_max) = 0;  % NMEA GGA field 6, GPS Quality indicator, 1 is good
      hdr{1}.nmea_time(1,N_traces_max) = 0;        % NMEA GGA field 1, UTC time, NMEA HHMMSS.FF format, incrementing at about 0.2 sec
      hdr{1}.nmea_lat(1,N_traces_max) = 0;         % NMEA GGA Latitude in degrees North
      hdr{1}.nmea_lon(1,N_traces_max) = 0;         % NMEA GGA Longitude in degrees East
      hdr{1}.nmea_elev(1,N_traces_max) = 0;        % NMEA GGA field 9, orthometric height (m) relative to the geoid
      hdr{1}.nmea_geoid(1,N_traces_max) = 0;       % NMEA GGA field 11, geoid separation from WGS-84  (m)
      hdr{1}.nmea_vel(1,N_traces_max) = 0;         % NMEA VTG field 7, speed over ground (km/h)
      hdr{1}.Nt(1,N_traces_max) = 0;               % Nt: Number of fast-time samples
    end
    if N_traces_max > cnt2
      % CH2 vars initiation
      data{2}(1,N_traces_max) = 0;
      hdr{2}.sync(1,N_traces_max) = 0;
      hdr{2}.offset(1,N_traces_max) = 0;
      hdr{2}.epri(1,N_traces_max) = 0;
      hdr{2}.wf(1,N_traces_max) = 0;
      hdr{2}.pps(1,N_traces_max) = 0;
      hdr{2}.time(1,N_traces_max) = 0;
      hdr{2}.nmea_gpsquality(1,N_traces_max) = 0;
      hdr{2}.nmea_time(1,N_traces_max) = 0;
      hdr{2}.nmea_lat(1,N_traces_max) = 0;
      hdr{2}.nmea_lon(1,N_traces_max) = 0;
      hdr{2}.nmea_elev(1,N_traces_max) = 0;
      hdr{2}.nmea_geoid(1,N_traces_max) = 0;
      hdr{2}.nmea_vel(1,N_traces_max) = 0;
      hdr{2}.Nt(1,N_traces_max) = 0;
    end
  end
end

fclose(fid);

% Truncate variables according to cnt1 & cnt2
data{1} = data{1}(:, 1:cnt1);
data{2} = data{2}(:, 1:cnt2);

hdr{1}.sync = hdr{1}.sync(:, 1:cnt1);
hdr{1}.offset = hdr{1}.offset(:, 1:cnt1);
hdr{1}.epri = hdr{1}.epri(:, 1:cnt1);
hdr{1}.wf = hdr{1}.wf(:, 1:cnt1);
hdr{1}.pps = hdr{1}.pps(:, 1:cnt1);
hdr{1}.time = hdr{1}.time(:, 1:cnt1);
hdr{1}.nmea_gpsquality = hdr{1}.nmea_gpsquality(:, 1:cnt1);
hdr{1}.nmea_time = hdr{1}.nmea_time(:, 1:cnt1);
hdr{1}.nmea_lat = hdr{1}.nmea_lat(:, 1:cnt1);
hdr{1}.nmea_lon = hdr{1}.nmea_lon(:, 1:cnt1);
hdr{1}.nmea_elev = hdr{1}.nmea_elev(:, 1:cnt1);
hdr{1}.nmea_geoid = hdr{1}.nmea_geoid(:, 1:cnt1);
hdr{1}.nmea_vel = hdr{1}.nmea_vel(:, 1:cnt1);
hdr{1}.Nt = hdr{1}.Nt(:, 1:cnt1);

hdr{2}.sync = hdr{2}.sync(:, 1:cnt2);
hdr{2}.offset = hdr{2}.offset(:, 1:cnt2);
hdr{2}.epri = hdr{2}.epri(:, 1:cnt2);
hdr{2}.wf = hdr{2}.wf(:, 1:cnt2);
hdr{2}.pps = hdr{2}.pps(:, 1:cnt2);
hdr{2}.time = hdr{2}.time(:, 1:cnt2);
hdr{2}.nmea_gpsquality = hdr{2}.nmea_gpsquality(:, 1:cnt2);
hdr{2}.nmea_time = hdr{2}.nmea_time(:, 1:cnt2);
hdr{2}.nmea_lat = hdr{2}.nmea_lat(:, 1:cnt2);
hdr{2}.nmea_lon = hdr{2}.nmea_lon(:, 1:cnt2);
hdr{2}.nmea_elev = hdr{2}.nmea_elev(:, 1:cnt2);
hdr{2}.nmea_geoid = hdr{2}.nmea_geoid(:, 1:cnt2);
hdr{2}.nmea_vel = hdr{2}.nmea_vel(:, 1:cnt2);
hdr{2}.Nt = hdr{2}.Nt(:, 1:cnt2);

for wf = 1:length(hdr)
  % Convert nmea_time from clock time HHMMSS.FF to decimal seconds of day
  hdr{wf}.nmea_time = floor(hdr{wf}.nmea_time/1e4) * 3600 ...
    + floor(mod(hdr{wf}.nmea_time,1e4)/1e2) * 60 ...
    + floor(mod(hdr{wf}.nmea_time,1e2)) + 0.2;

  % Set the "standard" wfs(wf).num_sam header fields
  hdr{wf}.wfs(1).num_sam = hdr{1}.Nt(1);
  hdr{wf}.wfs(2).num_sam = hdr{2}.Nt(1);

  %% Fix the PPS field
  % This code attempts to fixes isolated/single record errors in hdr.pps
  if param.fix_pps
    Nx = length(hdr{wf}.nmea_time);
    % Find the median step size in hdr{wf}.pps. This should correspond to the
    % EPRI.
    median_pps = median(diff(hdr{wf}.pps));
    % Convert from param.clk counts to radians and then unwrap. Remove the
    % expected monotonically increasing step.
    pps_u = unwrap((2*pi/param.clk) * (hdr{wf}.pps - median_pps*(0:Nx-1)));

    % Search for jumps and change with NaNs
    pps_u(abs(pps_u - mean(pps_u)) > 1.0) = NaN;

    % Interpolate NaNs
    t = 1:Nx; nant = isnan(pps_u);
    pps_u(nant) = interp1(t(~nant), pps_u(~nant), t(nant));

    % Restore PPS
    hdr{wf}.pps = pps_u/(2*pi/param.clk) + median_pps*(0:Nx-1);
    hdr{wf}.pps = mod(hdr{wf}.pps, param.clk);
  end

  %% Fix the nmea_time lag and create utc_time_sod field
  % The nmea_time lags slightly, so the second transitions need to be realigned
  % to the PPS reset of the fractions counter.
  
  if param.fix_nmea_time
    Nx = length(hdr{wf}.nmea_time);
    median_pps = median(diff(hdr{wf}.pps));
    last_rec = 1;
    hdr{wf}.utc_time_sod = nan(size(hdr{wf}.pps));
    for rec = 2:Nx
      if hdr{wf}.pps(rec)-hdr{wf}.pps(rec-1) < -param.clk/2
        % fprintf('wrap %d\n', rec); % For debugging
        hdr{wf}.utc_time_sod(last_rec:rec-1) = floor(hdr{wf}.nmea_time(rec-1)) + hdr{wf}.pps(last_rec:rec-1)/param.clk;
        last_rec = rec;
      end
    end
    hdr{wf}.utc_time_sod(last_rec:Nx) = floor(hdr{wf}.nmea_time(Nx)) + hdr{wf}.pps(last_rec:Nx)/param.clk;
    % Apply correction to nmea_time
    hdr{wf}.nmea_time = floor(hdr{wf}.utc_time_sod);

    if false % Enable for debugging
      figure(1); clf;
      plot(hdr{wf}.utc_time_sod)
      figure(2); clf;
      plot(diff(hdr{wf}.utc_time_sod))
    end
  end
end

%% Finish

% fprintf('CH1: %d traces\n', cnt1);
% fprintf('CH2: %d traces\n', cnt2);
