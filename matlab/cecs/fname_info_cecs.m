function fname = fname_info_cecs(fn)
% fname = fname_info_cecs(fn)
%
% Parses CECS filename.
%
% fn = CECS file name
%
% fname = structure containing each of the fields of the CECS
%   filename
%
%   For example: P3_20231118_135606_0006.dat
%                P3_YYYYMMDD_HHMMSS_FFFF.dat
%  .name = P3
%  .file_idx = 6
%  .datenum = time stamp converted to Matlab date number. Common
%     usage: "[year month day hour min sec] = datevec(fname.datenum)"
%
% Example
%  fn = 'P3_20231118_135606_0006.dat';
%  fname = fname_info_cecs(fn)
%
% Author: John Paden
%
% See also datenum

[fn_dir fn_name fn_ext] = fileparts(fn);
fn = [fn_name fn_ext];

fname = [];

[fname.name fn] = strtok(fn,'_');

[day_str fn] = strtok(fn,'_');
[time_str fn] = strtok(fn,'_');
fname.datenum = datenum([day_str time_str],'yyyymmddHHMMSS');

[fname.file_idx fn] = strtok(fn(2:end),'.');
fname.file_idx = str2double(fname.file_idx);
