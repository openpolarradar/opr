function merged = merge_vectors(master, slave, interp_finite_slave_flag)
% merged = merge_vectors(master, slave, interp_finite_slave_flag)
%
% Merges two vectors and removes discontinuities at the merge points.
% The master vector takes precedence. The merged output will be equal to
% the master everywhere the master is finite. Whereever the master is not
% defined, the slave vector will be used, but a "correction" will be added
% to the slave vector to remove discontinuities with the master.
%
% The slave vector is interpolated through all of its NaN values by default
% so that the output merged vector will not have any NaN (unless the slave
% vector has all NaN). This can be disabled with
% interp_finite_slave_flag=false so that the slave vector only fills in
% values where it has ~isnan() values.
%
% master: N length numeric vector
% slave: N length numeric vector
% interp_finite_slave_flag: optional logical scalar, default is true, if
% empty, then will be set to true
%
% merged: N length merged vector
% 
% Examples:
% 
% master = [1 2 3 NaN NaN 6 7];
% slave  = [0 1 2  3   4  5 6];
% merged = merge_vectors(master, slave)
% 
% master = [1 NaN NaN 4 NaN 6 7];
% slave  = [0 1 2  3   4  5 6];
% merged = merge_vectors(master, slave)
% 
% master = [NaN NaN 3 4 5 6 7];
% slave  = [ 0   1  2 3 4 5 6];
% merged = merge_vectors(master, slave)
% 
% master = [NaN NaN 3 4 5 NaN NaN];
% slave  = [ 0   1  2 3 4  5   6];
% merged = merge_vectors(master, slave)
% 
% master = [1 2 3 NaN NaN 6 7];
% slave  = [0 1 2  3.5   5  6.5 8];
% merged = merge_vectors(master, slave)
%
% master = [1 2 3 NaN NaN 6 7];
% slave  = [0 1 NaN  3   4  NaN 6];
% merged = merge_vectors(master, slave)
%
% master = [1 2 3 NaN NaN 6 7];
% slave  = [0 1 NaN  3   NaN  NaN 6];
% merged = merge_vectors(master, slave)
%
% master = [1 2 3 NaN NaN 6 7];
% slave  = [0 1 NaN  3   NaN  NaN 6];
% merged = merge_vectors(master, slave, false)
%
% master = [1 2 3 NaN NaN 6 7];
% slave  = nan(size(master));
% merged = merge_vectors(master, slave)
%
% Author: John Paden

if ~exist('interp_finite_slave_flag','var') || isempty(interp_finite_slave_flag)
  interp_finite_slave_flag = true;
end

merged = master;
if interp_finite_slave_flag
  slave = interp_finite(slave, NaN);
  slave_interp = slave;
else
  % We still use an interpolated slave vector to determine the
  % discontinuities at each end of the segment(s) to be filled in by the
  % slave vector.
  slave_interp = interp_finite(slave, NaN);
end

start_idx = 1;
% Find the next bad ~isfinite point
merge_idx = find(~isfinite(merged(start_idx:end)),1);
while ~isempty(merge_idx)
  merge_idx = merge_idx + start_idx - 1;
  % Find the next good isfinite point
  end_merge_idx = find(isfinite(merged(merge_idx:end)),1);
  
  %% Merge this bad section
  % merge_idx: index containing a bad point, merge_idx-1 contains a good
  %   point since merge_idx was the next first bad point
  % end_merge_idx: index containing a good point (or empty if there are no
  %   good points left in the vector
  if merge_idx == 1 && isempty(end_merge_idx)
    % master is all bad so just set merged to slave
    end_merge_idx = length(merged)+1;
    merged = slave;
  elseif merge_idx == 1
    % master has a bad section at the beginning, the correction is just
    % a constant term based on the discontinuity on the right side of the
    % bad section
    end_merge_idx = end_merge_idx + merge_idx - 1;
    merged(merge_idx:end_merge_idx-1) = slave(merge_idx:end_merge_idx-1) ...
      - (slave_interp(end_merge_idx) - merged(end_merge_idx));
  elseif isempty(end_merge_idx)
    % master has a bad section at the end, the correction is just
    % a constant term based on the discontinuity on the left side of the
    % bad section
    end_merge_idx = length(merged)+1;
    merged(merge_idx:end_merge_idx-1) = slave(merge_idx:end_merge_idx-1) ...
      - slave_interp(merge_idx-1) + merged(merge_idx-1);
  else
    % master has a bad section at the middle, the correction is a linear
    % interpolation from the discontinuity on the left side to the
    % discontinuity on the right side
    end_merge_idx = end_merge_idx + merge_idx - 1;
    merged(merge_idx:end_merge_idx-1) = slave(merge_idx:end_merge_idx-1) ...
      - interp1([merge_idx-1 end_merge_idx], ...
      [slave_interp(merge_idx-1) - merged(merge_idx-1), slave_interp(end_merge_idx) - merged(end_merge_idx)], ...
      merge_idx:end_merge_idx-1);
  end
  start_idx = end_merge_idx;
  % Find the next bad ~isfinite point
  merge_idx = find(~isfinite(merged(start_idx:end)),1);
end
