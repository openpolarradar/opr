function new_fn = filename_ext_swap(fn,suffix_str,prefix_str)
% new_fn = filename_ext_swap(fn,suffix_str,prefix_str)
%
% Swaps the extension of a filename and allows a suffix and prefix to be
% added to the filename.
%
% Examples:
% chan = 1; new_fn = filename_ext_swap('C:\metadata\2024_Antarctica_GroundGHOST\myfile.mat',sprintf('_chan%d.fig',chan)) 
% chan = 1; new_fn = filename_ext_swap('C:\metadata\2024_Antarctica_GroundGHOST\myfile.mat',sprintf('_chan%d.fig',chan),'optional_prefix_') 

if ~exist('suffix_str','var')
  suffix_str = '';
end

if ~exist('prefix_str','var')
  prefix_str = '';
end

[fn_dir,fn_name] = fileparts(fn);
new_fn = fullfile(fn_dir,[prefix_str fn_name suffix_str]);
