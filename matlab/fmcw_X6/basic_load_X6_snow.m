function [hdr,data] = basic_load_X6_snow(fn,param)
% [hdr,data] = basic_load_X6_snow(fn, param)
%
% This is the only function which loads X6 mini snow radar raw data directly.
%
% If data is not specified as an output argument, only the header is returned
%
% fn = filename of X6 mimi snaow data
% param = struct controlling loading of data
%   .clk = clock (Hz), default one, used to interpret
%     counts in the header fields
%     first system used 122.88e6
%   .recs = 2 element vector for records to load [start_rec num_rec]
%     start_rec uses zero-indexing (negative start_recs read from the
%     end of the file and only work with single header loading)
%   .file_version = default is 14
%   .records
%     .en = NOT USED. If true, all
%       headers will be loaded and the output arguments become:
%       hdr --> success flag (returns 1)
%       data --> hdr (all headers)
%
% hdr = file header for each record (if data is not defined, behavior
%   depends on param.records.en variable, if it is false only the first hdr
%   is returned, otherwise all the headers are returned)
% data = Depends on param.records.en. When false, it is an optional output
%   array of radar data where dimensions are
%   1: fast-time/range-bin
%   2: slow-time/range-line/records
%
% Examples: See bottom of file
%   fn = 'P:\X6_Pro_RCFlightTest\20240201\flight1\X6snow100003.dat';
%   [hdr,data] = basic_load_X6_snow(fn);
%
% Authors: John Paden Jilu Li 
%

% ===================================================================
% Check input arguments
% ===================================================================
if ~exist('param','var') || isempty(param)
  param.clk = 100e6;
  param.recs = [];
end
if ~isfield(param,'clk')
  param.clk = 100e6;
end
if ~isfield(param,'file_version')
  param.file_version = 14;
end
if ~isfield(param,'records')
  param.records.en = false;
end
if ~isfield(param,'recs')
  param.recs = [];
end
if ~isfield(param,'sync') || isempty(param.sync)
  param.sync = reshape(dec2hex(double('HFRD'))',[1,8]);
end

% Reset/clear hdr struct
hdr = [];

% ===================================================================
%% Data Format
% ===================================================================
% 32-bit frame sync 0x48465244 (ASCII for HFRD)
% 32-bit EPRI
% 32-bit PRI count (zero indexed)?
% 32-bit seconds in DCB (0 0 H H M M S S)
% 32-bit fraction
% 16-bit DATA

HEADER_SIZE = 20;
SAMPLE_SIZE = 2;

% ===============================================================
% Get first record position
% ===============================================================
if ~isempty(param.recs) && param.recs(1) < 0
  hdr.finfo.syncs = get_first10_sync_mfile(fn,0,struct('sync',reshape(dec2hex(double(reverse('HFRD')))',[1,8]),'last',true));
else
  hdr.finfo.syncs = get_first10_sync_mfile(fn,0,struct('sync',reshape(dec2hex(double(reverse('HFRD')))',[1,8])));
end

% ===============================================================
% Open file little-endian for reading
% ===============================================================
[fid,msg] = fopen(fn,'r','ieee-le');
if fid < 1
  fprintf('Could not open file %s\n', fn);
  error(msg);
end

fseek(fid, 0, 1);
eof_pos = ftell(fid);

% ===============================================================
% Read in waveform information + record size
% ===============================================================

if nargout < 2 && ~param.records.en
  % Seek to first record
  fseek(fid, hdr.finfo.syncs(1), -1);
  hdr.frame_sync = fread(fid,1,'uint32');
  hdr.epri = fread(fid,1,'uint32');
  hdr.pri_count = fread(fid,1,'uint32') + 1;
  % fseek(fid,4,0);
  time = fread(fid,1,'uint32').'; % From NMEA string converted to DCB
  hour = bitand(bitshift(time,-20),15)*10 + bitand(bitshift(time,-16),15);
  min =  bitand(bitshift(time,-12),15)*10 + bitand(bitshift(time,-8),15);
  sec =  bitand(bitshift(time,-4),15)*10 + bitand(bitshift(time,-0),15);
  hdr.seconds = hour*3600 + min*60 + sec;
  hdr.fraction = fread(fid,1,'uint32');
  hdr.start_idx = 11;
  hdr.stop_idx = 32768;
  hdr.wfs.presums = 48;
  hdr.wfs.num_sam = hdr.stop_idx - hdr.start_idx + 1;
  fclose(fid);
  return;
elseif param.records.en
  error('Not supported');
end

% Seek to first record
fseek(fid, hdr.finfo.syncs(1), -1);

rline = 0;
hdr.finfo.rec_size = [];
FRAME_SYNC = hex2dec(reshape(dec2hex(double('HFRD'))',[1,8]));
data = zeros(0,0,'single');
while ftell(fid) <= eof_pos-HEADER_SIZE
  rline = rline + 1;
  hdr.frame_sync(rline) = fread(fid,1,'uint32');
  if hdr.frame_sync(rline) == FRAME_SYNC
    hdr.finfo.syncs(rline) = ftell(fid)-4;
  else
    % Search for next frame sync
%     keyboard
    found = false;
    while ~feof(fid)
      test = fread(fid,1,'uint32');
      if test == FRAME_SYNC
        found = true;
        break;
      end
    end
    if ~found
      rline = rline - 1;
      break;
    end
    hdr.finfo.syncs(rline) = ftell(fid)-4;
  end
  if ftell(fid) > eof_pos-HEADER_SIZE
    rline = rline - 1;
    break;
  end
  hdr.epri(rline) = fread(fid,1,'uint32');
  hdr.pri_count(rline) = fread(fid,1,'uint32') + 1;
  time = fread(fid,1,'uint32').'; % From NMEA string converted to DCB
  hour = bitand(bitshift(time,-20),15)*10 + bitand(bitshift(time,-16),15);
  min =  bitand(bitshift(time,-12),15)*10 + bitand(bitshift(time,-8),15);
  sec =  bitand(bitshift(time,-4),15)*10 + bitand(bitshift(time,-0),15);
  hdr.seconds(rline) = hour*3600 + min*60 + sec;
  hdr.fraction(rline) = fread(fid,1,'uint32');
  hdr.presums(rline) = 48;
  hdr.start_idx(rline) = 11;
  hdr.stop_idx(rline) = 32768;
  hdr.wfs.presums(rline) = 48;
  hdr.wfs.num_sam (rline) = hdr.stop_idx(rline) - hdr.start_idx(rline) + 1;

  % Raw data
  hdr.num_sam(rline) = hdr.stop_idx(rline) - hdr.start_idx(rline) + 1;
  
  if ftell(fid) > eof_pos - hdr.num_sam(rline)*SAMPLE_SIZE
    rline = rline - 1;
    break;
  end
  
  if param.records.en
    fseek(fid,hdr.num_sam(rline)*SAMPLE_SIZE,0);
  else
    % Real data
    data(1:hdr.num_sam(rline),rline) = fread(fid,hdr.num_sam(rline),'int16=>single');
  end
end
fclose(fid);

hdr.finfo.syncs = hdr.finfo.syncs(1:rline);
hdr.frame_sync = hdr.frame_sync(1:rline);
hdr.epri = hdr.epri(1:rline);
hdr.seconds = hdr.seconds(1:rline);
hdr.fraction = hdr.fraction(1:rline);
hdr.start_idx = hdr.start_idx(1:rline);
hdr.stop_idx = hdr.stop_idx(1:rline);
hdr.utc_time_sod = hdr.seconds + hdr.fraction / param.clk;
hdr.wfs.presums = hdr.wfs.presums(1:rline);
hdr.wfs.num_sam = hdr.wfs.num_sam(1:rline);
data = data(:,1:rline);
return

% ===============================================================
% ===============================================================
% Example
% ===============================================================
% ===============================================================

fn = 'P:\X6_Pro_RCFlightTest\20240201\flight1\X6snow100003.dat';
[hdr,data] = basic_load_X6_snow(fn);

