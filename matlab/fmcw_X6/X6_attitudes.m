function [gps_time,lat,lon,alt,roll,pitch,yaw] = X6_attitudes(GPS_fns,AHR2_fns,year)
% [gps_time,lat,lon,alt,roll,pitch,yaw] = X6_attitudes(GPS_fns,AHR2_fns,year)

% This script extract the gps_time,lat,lon, alt (elev), roll, pitch and yaw (heading) angles in degrees  from X6_pro's DataFlash log files (*.bin).
% INPUTS: 
%   GPS_fns,AHR2_fns are GPS and AHR2 data in csv files exported from X6_pro's DataFlash log files using Mission Planner
%   year:this is required as the number of GPS variables in log files may change from year to year
% OUTPUTS:
%   gps_time: GPS time (sec)
%   lat: Latitude  (degree)
%   lon: Longitude (degree)
%   alt: Elevation in WGS 84 (m)
%   roll: Roll angle (degree)
%   pitch: Pitch angle (degree)
%   yaw: Heading angle (degree)

% Author:Jilu Li

    tmp_gps = [];
    for idx = 1:2
        if idx == 1
            fns = GPS_fns;
            if year == 2024
                % only read column 2, 7 and 8 for LocalTime (yyyy-mm-dd HH:MM:SS.SSS),GMS (milliseconds since start of GPS Week)
                % and GWK (weeks since 6 Jan 1980)
                format_str = '%*d%s%*s%*d%*d%*d%d%d%*[^\n]';
            elseif year == 2025 % the fourth GPS output varible in 2024 log files was removed in 2025 log files 
                % only read column 2, 6 and 7 for LocalTime (yyyy-mm-dd HH:MM:SS.SSS),GMS (milliseconds since start of GPS Week)
                format_str = '%*d%s%*s%*d%*d%d%d%*[^\n]';
            end
            types = {'LocalTime1','GMS','GWk'};
        else
            fns = AHR2_fns;
            % only read column 2,5-10 for LocalTime, Roll,Pitch and Yaw,Alt,Lat,Lon
            format_str = '%*d%s%*s%*d%f%f%f%f%f%f%*[^\n]';
            types = {'LocalTime2','Roll','Pitch','Yaw','Alt','Lat','Lng'};
        end
        for fn_idx = 1:length(fns)
            [fid,msg] = fopen(fns{fn_idx},'r');
            if fid < 0
                error('Error opening %s: %s', fn, msg);
            end
            % Read in lines of file
            finfo = dir(fns{fn_idx});
            C_final = {};
            while ftell(fid) < finfo.bytes
                % This reader uses textscan to read in the file. If textscan finds a bad
                % line, it stops reading before the end of the file
                C = textscan(fid,format_str,'HeaderLines',0,'Delimiter',',');
                if ftell(fid) < finfo.bytes
                    good_lines = length(C{end});
                    fprintf('Bad line %d: %s\n', good_lines+1, fgets(fid));
                else
                    good_lines = length(C{end});
                end
                for field_idx = 1:length(C)
                    if length(C_final) < field_idx
                        C_final{field_idx} = C{field_idx}(1:good_lines);
                    else
                        C_final{field_idx} = cat(1,C_final{field_idx}, C{field_idx}(1:good_lines));
                    end
                end
                % Convert from cell to struct/fieldnames for better readability below
                for idx = 1:min(length(C_final),length(types))
                    if fn_idx == 1
                        tmp_gps.(types{idx}) = reshape(C_final{idx},[1 length(C_final{idx})]);
                    else
                        tmp_gps.(types{idx}) = cat(2,tmp_gps.(types{idx}),reshape(C_final{idx},[1 length(C_final{idx})]));
                    end
                end
            end
            fclose(fid);
        end
    end
    gps_time = double(tmp_gps.GWk)*7*86400 + double(tmp_gps.GMS)*1e-3;
    gps_time = gps_time + (datenum(1980,1,6)-datenum(1970,1,1))*86400;
    [gps_time,sort_idxs] = sort(gps_time);
    LocalTime1 = datenum(tmp_gps.LocalTime1(sort_idxs))*86400; 
    LocalTime2 = datenum(tmp_gps.LocalTime2)*86400; 
    [LocalTime2,sort_idxs] = sort(LocalTime2);
    lat = interp1(LocalTime2,tmp_gps.Lat(sort_idxs),LocalTime1)';
    lon = interp1(LocalTime2,tmp_gps.Lng(sort_idxs),LocalTime1)';
    alt = interp1(LocalTime2,tmp_gps.Alt(sort_idxs),LocalTime1)';
    roll = interp1(LocalTime2,tmp_gps.Roll(sort_idxs),LocalTime1)';
    pitch = interp1(LocalTime2,tmp_gps.Pitch(sort_idxs),LocalTime1)';
    yaw = interp1(LocalTime2,tmp_gps.Yaw(sort_idxs),LocalTime1)';
    
    % remove data before GPS locking (same GPS time)
    [gps_time,good_idxs,tmp] = unique(gps_time,'last');
    lat = lat(good_idxs);
    lon = lon(good_idxs);
    alt = alt(good_idxs);    
    roll = roll(good_idxs);
    pitch = pitch(good_idxs);
    yaw = yaw(good_idxs);   
end