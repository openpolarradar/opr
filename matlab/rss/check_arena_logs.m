function logs = check_arena_logs(param,param_override)
% logs = check_arena_logs(param,param_override)
%
% Checks the contents of ARENA log files
%
% See run_check_arena_logs.m for an example of how to run
%
% 1. Generates console log files. These log files contain:
%    * bad values for temperature, voltage, alignment, and link errors
%    * mean/std/min/max temperature values
%    * user notes from arena software (only some versions support notes)
%
% 2. Generates .mat file with all logs stored in nested structure so that
% they can be easily loaded.
%
% 3. Generates jpg and fig plots of temperature fields. The plots are
% versus record number. The GPS time of each record can be found from the
% correspondign relTimeCntr field for that temperature field.
%    * datestr(epoch_to_datenum(logs.CTU.relTimeCntr(1)/10e6))
%
% Author: John Paden
%
% See also: run_check_arena_logs.m, check_arena_logs.m,
% check_arena_logs_plot.m, read_arena_logs.m


%% General Setup
% =====================================================================
param = merge_structs(param, param_override);

fprintf('=====================================================================\n');
fprintf('%s: %s (%s)\n', mfilename, param.day_seg, datestr(now));
fprintf('=====================================================================\n');

%% Determine which files to load and read/load them

[~,config_fn_name] = fileparts(param.records.config_fn);
config_timestamp = config_fn_name(1:15);
metadata_dir = param.day_seg(1:8);

log_dir = fullfile(param.data_support_path,param.season_name,metadata_dir);

fns = get_filenames(log_dir,config_timestamp,'','.txt');

logs = read_arena_logs(fns);

%% Check for issues, store log into .mat file, and plot temperatures
console_log_output_fn = [ct_filename_ct_tmp(param,'','check_arena_logs','console_log') '.txt'];
console_log_output_fn_dir = fileparts(console_log_output_fn);
if ~exist(console_log_output_fn_dir,'dir')
  mkdir(console_log_output_fn_dir);
end

log_output_fn = [ct_filename_ct_tmp(param,'','check_arena_logs','logs') '.mat'];
fprintf('Saving logs: %s\n', log_output_fn);
ct_save(log_output_fn,'param','logs');

fprintf('Saving console output: %s\n', console_log_output_fn);
[fid,msg] = fopen(console_log_output_fn,'wb');
if fid < 1
  fprintf('Could not open file %s\n', console_log_output_fn);
  error(msg);
end

check_arena_logs_sub(logs,[1, fid]);
fclose(fid);

check_arena_logs_plot(param,logs);

end

%% check_arena_logs_sub(logs,fid_list,parent_str)
%
% Recursive function that traverses log structure checking all the fields
% that can be checked (voltage, temperature, alignment, JESDES ADC
% link-errors) and compiling notes.
function check_arena_logs_sub(logs,fid_list,parent_str)

if ~exist('parent_str','var')
  parent_str = '';
end

log_fieldnames = fieldnames(logs);

voltage_threshold = 0.05; % No official recommendation, but maybe +/-5%
temperature_threshold = 80; % 80 is RSS/Torry recommended max, but actually can support higher
alignment_threshold = 8; % No official recommendation
link_error_threshold = 1; % 1 is RSS/Torry recommended max

% Look through all fields in the structure array
for field_idx = 1:length(log_fieldnames)
  fieldname = log_fieldnames{field_idx};
  if isempty(parent_str)
    new_parent_str = fieldname;
  else
    new_parent_str = [parent_str '.' fieldname];
  end
  if 1
    % Debug
    for fid = fid_list(:).'
      fprintf(fid,'%s %s\n',new_parent_str,repmat('=', [1, 80-length(new_parent_str)]));
    end
  end

  if isstruct(logs.(fieldname))
    % If field is a structure, recurse on this field
    check_arena_logs_sub(logs.(fieldname), fid_list, new_parent_str);

  else
    % Voltage field
    if ~isempty(regexp(fieldname,'V')) && isnumeric(logs.(fieldname))
      mean_val = mean(logs.(fieldname));
      std_val = std(logs.(fieldname));
      if std_val/mean_val > voltage_threshold
        for fid = fid_list(:).'
          fprintf(fid,'%s.%s: std(%.2g)/mean(%.2g) > threshold(%.2g)!!!\n', parent_str, fieldname, std_val, mean_val, voltage_threshold);
        end
      end
    end

    % Temperature field
    if ~isempty(regexpi(fieldname,'temp')) && isnumeric(logs.(fieldname))
      mean_val = mean(logs.(fieldname));
      min_val = min(logs.(fieldname));
      max_val = max(logs.(fieldname));
      std_val = std(logs.(fieldname));
      if max_val > temperature_threshold
        for fid = fid_list(:).'
          fprintf(fid,'%s.%s: mean(%.2g) std(%.2g) mintemp(%.2g) maxtemp(%.2g) > threshold(%.2g)!!!\n', parent_str, fieldname, ...
            mean_val, std_val, min_val, max_val, temperature_threshold);
        end
      else
        for fid = fid_list(:).'
          fprintf(fid,'%s.%s: mean(%.2g) std(%.2g) mintemp(%.2g) maxtemp(%.2g)\n', parent_str, fieldname, ...
            mean_val, std_val, min_val, max_val);
        end
      end
    end

    % Alignment field
    if ~isempty(regexpi(fieldname,'alignment'))
      alignment = [];
      for idx=1:length(logs.(fieldname))
        tmp = textscan(logs.(fieldname){idx},'%d:%d:%d','CollectOutput',true);
        if numel(tmp{1}) < 3
          continue
        end
        if length(alignment) < tmp{1}(1)
          alignment{tmp{1}(1)}(1:2,1) = tmp{1}(2:3);
        else
          alignment{tmp{1}(1)}(1:2,end+1) = tmp{1}(2:3);
        end
      end

      % NOT REALLY SURE HOW TO INTERPRET THE alignment FIELD
      for idx = 1:length(alignment)
        % New alignment check
        % Calculate stats for minimum field
        min_max = max(alignment{idx}(1,:));
        % Calculate stats for maximum field
        max_min = min(alignment{idx}(2,:));
        if max_min-min_max <= alignment_threshold
          for fid = fid_list(:).'
            fprintf(fid,'%s.%s: ADC-%d of %d min(max_alignment)(%.2g) - max(min_alignment)(%.2g) <= alignment_threshold(%.2g)!!!\n', ...
              parent_str, fieldname, idx, length(alignment), ...
              max_min, min_max, alignment_threshold);
          end
        end
        % Previous alignment check... not sure which is better
        if 0
          % Calculate stats for minimum field
          min_min = min(alignment{idx}(1,:));
          min_max = max(alignment{idx}(1,:));
          % Calculate stats for maximum field
          max_min = min(alignment{idx}(2,:));
          max_max = max(alignment{idx}(2,:));
          if abs(min_max-min_min) >= alignment_threshold ...
              || abs(max_max-max_min) >= alignment_threshold
            for fid = fid_list(:).'
              fprintf(fid,'Alignment range is more than alignment_threshold: %d!!!\n', alignment_threshold);
              fprintf(fid,'%s.%s: ADC-%d of %d [%d to %d] : [%d to %d]\n', parent_str, fieldname, idx, length(alignment), ...
                min_min, min_max, ...
                max_min, max_max);
            end
          end
        end
      end

    end

    % Link error count field
    if ~isempty(regexpi(fieldname,'LinkErrorCount'))
      if iscell(logs.(fieldname))
        % Convert from cell to matrix
        logs.(fieldname) = cellfun(@(x) x(3:end), logs.(fieldname), 'UniformOutput', false);
        % Convert hex to decimal
        logs.(fieldname) = hex2dec(logs.(fieldname));
      end
      min_val = min(logs.(fieldname));
      max_val = max(logs.(fieldname));
      if max_val-min_val >= link_error_threshold
        for fid = fid_list(:).'
          fprintf(fid,'There should not be more than %d link errors that occur after the system starts. There were %d errors after the system started!!!\n', link_error_threshold,max_val-min_val);
          fprintf(fid,'%s.%s: max(%d) - min(%d) > %d\n', parent_str, fieldname, max_val, min_val, link_error_threshold);
        end
      end
    end

    % Notes field
    if ~isempty(regexpi(fieldname,'notes'))
      for fid = fid_list(:).'
        fprintf(fid,'Notes:\n');
        for idx=1:length(logs.(fieldname))
          fprintf(fid,'%s: %s\n\n', datestr(epoch_to_datenum(logs.nmeaTime(idx))), logs.(fieldname){idx});
        end
      end
    end

  end
end

end
