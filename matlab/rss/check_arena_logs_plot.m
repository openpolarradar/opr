function check_arena_logs_plot(param,logs,parent_str)
% check_arena_logs_plot(param,logs,parent_str)
%
% Helper function for check_arena_logs. Plots temperature fields and stores
% in .jpg and .fig formats in ct_tmp directory.
%
% This is a recursive function that traverses the "logs" structure read in
% from the arena XML logs and plots all temperature fields.
%
% See also: run_check_arena_logs.m, check_arena_logs.m,
% check_arena_logs_plot.m, read_arena_logs.m

if ~exist('parent_str','var')
  parent_str = '';
end

log_fieldnames = fieldnames(logs);

% Look through all fields in the structure array
for field_idx = 1:length(log_fieldnames)
  fieldname = log_fieldnames{field_idx};
  if isempty(parent_str)
    new_parent_str = fieldname;
  else
    new_parent_str = [parent_str '.' fieldname];
  end

  if 0
    % Debug
    for fid = fid_list(:).'
      fprintf(fid,'%s %s\n',new_parent_str,repmat('=', [1, 80-length(new_parent_str)]));
    end
  end

  if isstruct(logs.(fieldname))
    % If field is a structure, recurse on this field
    check_arena_logs_plot(param, logs.(fieldname), new_parent_str);

  else
    % If field is not a structure, check if it is a temperature field. If
    % it is, plot the field.
    if ~isempty(regexpi(fieldname,'Temp'))

      % Temperature plots
      new_parent_str_dashes = regexprep(new_parent_str,'\.','-');
      temperature_output_fn = ct_filename_ct_tmp(param,'','check_arena_logs',new_parent_str_dashes);
      temperature_output_fn_dir = fileparts(temperature_output_fn);
      if ~exist(temperature_output_fn_dir,'dir')
        mkdir(temperature_output_fn_dir);
      end

      h_fig = figure(1); clf(h_fig);
      h_axis = axes('parent',h_fig);
      plot(h_axis,logs.(fieldname));
      title(h_axis,sprintf('%s: %s', param.day_seg, new_parent_str),'interpreter','none')
      xlabel(h_axis,'Record');
      ylabel(h_axis,'Temperature (C)');
      grid(h_axis,'on');

      saveas(h_fig,[temperature_output_fn '.jpg'])
      saveas(h_fig,[temperature_output_fn '.fig'])
    end
  end

end

% Example fieldnames from GHOST radar:

% plot(logs.CTU.ctu.ZynqCoreTemp);
% plot(logs.CTU.ctu.ZynqCoreTemp);
%
% plot(logs.RDS_AWG_RX2.BoardTemp)
% plot(logs.RDS_AWG_RX2.LTM4639Temp)
% plot(logs.RDS_AWG_RX2.LTM4644ATemp)
% plot(logs.RDS_AWG_RX2.LTM4644BTemp)
% plot(logs.RDS_AWG_RX2.awg1.Board1Temp)
% plot(logs.RDS_AWG_RX2.awg1.Board2Temp)
% plot(logs.RDS_AWG_RX2.awg1.ZynqCoreTemp)
% plot(logs.RDS_AWG_RX2.digrx2.BoardADC0Temp)
% plot(logs.RDS_AWG_RX2.digrx2.BoardTemp)
% plot(logs.RDS_AWG_RX2.digrx2.ZynqCoreTemp)
%
% plot(logs.RDS_RX1.BoardTemp)
% plot(logs.RDS_RX1.LTM4639Temp)
% plot(logs.RDS_RX1.LTM4644ATemp)
% plot(logs.RDS_RX1.LTM4644BTemp)
% plot(logs.RDS_RX1.digrx1.BoardADC0Temp)
% plot(logs.RDS_RX1.digrx1.BoardTemp)
% plot(logs.RDS_RX1.digrx1.ZynqCoreTemp)
