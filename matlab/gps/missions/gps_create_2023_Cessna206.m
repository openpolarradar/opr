
% script gps_create_2023_Alaska_Cessna206-N7600N
%
% Makes the GPS files for 2023 Alaska Cessna206-N7600N field season

tic;

global gRadar;

support_path = '';
data_support_path = '';

if isempty(support_path)
  support_path = gRadar.support_path;
end

gps_path = fullfile(support_path,'gps','2023_Alaska_Cessna206');
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

% ======================================================================
% User Settings
% ======================================================================
debug_level = 1;

in_base_path = fullfile(data_support_path,'2023_Alaska_Cessna206-N7600N');

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};


season_name = '2023_Alaska_Cessna206';
% gps_source_to_use = 'NMEA';
gps_source_to_use = 'novatel';

if strcmpi(gps_source_to_use,'NMEA')
    year = 2023; month = 5; day = 23;
    file_idx = file_idx + 1;
    in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'nmea','','.gps');
    out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
    file_type{file_idx} = 'NMEA';
    params{file_idx} = struct('year',year,'month',month,'day',day,'format',3,'time_reference','utc');
    gps_source{file_idx} = 'nmea-field';
    sync_flag{file_idx} = 0;
    date_str{file_idx} = '20230523';
elseif strcmpi(gps_source_to_use,'novatel')
  % year = 2023; month = 5; day = 23;in_fns{file_idx}
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d_%02d_%02d',year,month,day)),'ie','','v2.txt');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  % file_type{file_idx} = 'General_ASCII';
  % params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  % params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  % params{file_idx}.textscan = {};
  % gps_source{file_idx} = 'cresis-final20230605';
  % sync_flag{file_idx} = 0;
  % date_str{file_idx} = '20230523';

  year = 2023; month = 5; day = 24;
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d_%02d_%02d',year,month,day)),'ie','','v2.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final20230605';
  sync_flag{file_idx} = 0;
  date_str{file_idx} = '20230524';
end

% ======================================================================
% Read and translate files according to user settings
% ======================================================================
gps_create;
