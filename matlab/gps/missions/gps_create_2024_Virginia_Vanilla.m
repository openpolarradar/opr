% script gps_create_2024_Antarctica_TOdtu
%
% Makes the GPS files for 2024_Antarctica_TOdtu field season

%% Setup
% =========================================================================
tic;

global gRadar;

support_path = '';

data_support_path = '/resfs/GROUPS/CRESIS/dataproducts/metadata/';

if isempty(support_path)
  support_path = gRadar.support_path;
end

season_name = '2024_Virginia_Vanilla';

gps_path = fullfile(support_path,'gps',season_name);
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

in_base_path = fullfile(data_support_path,season_name);

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};

%% <== CHOOSE WHICH GPS SOURCE TO PROCESS
gps_source_to_use = 'postprocessed';

if strcmpi(gps_source_to_use,'postprocessed')
  %% Postprocessed GPS SOURCE from Novatel Inertail Explorer
  % =======================================================================
  
  year = 2024; month = 11; day = 2; % Where the files got copied to
  date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','LOG00086'),'ie','','LOG00086.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final_20241113';
  sync_flag{file_idx} = 0;

end

%% gps_create
% Read and translate files according to user settings
% =========================================================================
gps_create;
