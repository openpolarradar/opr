% script gps_create_2016_Antarctica_LC130
%
% Makes the GPS files for 2016_Antarctica_LC130 field season

%% Setup
% =========================================================================
tic;

global gRadar;

support_path = '';
data_support_path = '';

if isempty(support_path)
  support_path = gRadar.support_path;
end

season_name = '2016_Antarctica_LC130';

gps_path = fullfile(support_path,'gps',season_name);
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

in_base_path = fullfile(data_support_path,season_name);

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};

%% <== CHOOSE WHICH GPS SOURCE TO PROCESS
gps_source_to_use = 'span';

if strcmpi(gps_source_to_use,'span')
  %% LDEO POST PROCESSED GPS SOURCE
  % =======================================================================

  % Generated the list from:
  % postprocessed_fns = get_filenames(in_base_path,'','','SPAN_FINAL_STD.txt',struct('recursive',true));
  % Combine files from the same day:
  postprocessed_fns = {};
  % postprocessed_fns{end+1} = {'20161122/GPS/AN03_F1001_20161122_SPAN_FINAL_STD.txt'};
  % postprocessed_fns{end+1} = {'20161123/GPS/AN03_F1002_20161123_SPAN_FINAL_STD.txt'};
  % postprocessed_fns{end+1} = {'20161124/GPS/AN03_F1003_20161124_SPAN_FINAL_STD.txt'};
  % postprocessed_fns{end+1} = {'20161127/GPS/AN03_F1004_20161127_SPAN_FINAL_STD.txt'};
  % postprocessed_fns{end+1} = {'20161129/GPS/AN03_F1005_20161129_SPAN_FINAL_STD.txt'};
  % postprocessed_fns{end}{end+1} = '20161129b/GPS/AN03_F1006_20161129_SPAN_FINAL_STD.txt';
  % postprocessed_fns{end+1} = {'20161130/GPS/AN03_F1007_20161130_SPAN_FINAL_STD.txt'};
  % postprocessed_fns{end+1} = {'20161201/GPS/AN03_F1008_20161201_SPAN_FINAL_STD.txt'};
  postprocessed_fns{end+1} = {'20161202/GPS/AN03_F1009_20161202_SPAN_FINAL_STD.txt'};
  postprocessed_fns{end}{end+1} = '20161202b/GPS/AN03_F1010_20161202_SPAN_FINAL_STD.txt';
  
  for postprocessed_fns_idx = 1:length(postprocessed_fns)
    postprocessed_fn = postprocessed_fns{postprocessed_fns_idx}{1};
    [postprocessed_fn_dir,postprocessed_fn_name,postprocessed_fn_ext] = fileparts(postprocessed_fn);
    datestr_year = str2double(postprocessed_fn_name(12:15));
    datestr_month = str2double(postprocessed_fn_name(16:17));
    datestr_day = str2double(postprocessed_fn_name(18:19));
    fprintf('%s: %04d%02d%02d %s\n', postprocessed_fn, datestr_year, datestr_month, datestr_day, datestr(now));
    file_idx = file_idx + 1;
    in_fns{file_idx} = postprocessed_fns{postprocessed_fns_idx};
    for in_fns_idx = 1:length(in_fns{file_idx})
      in_fns{file_idx}{in_fns_idx} = fullfile(in_base_path,in_fns{file_idx}{in_fns_idx});
    end
    out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
    file_type{file_idx} = 'General_ASCII';
    params{file_idx} = struct('time_reference','utc','headerlines',16,'format_str','%s%f%s%f%f%f%f%f%f%f%f%f');
    params{file_idx}.types = {'date_MDY','tmp_1','time_HMS','tmp_2','lat_deg','lon_deg','elev_m','pitch_deg','roll_deg','heading_deg','tmp_3','tmp_4'};
    params{file_idx}.textscan = {};
    gps_source{file_idx} = 'ldeo-final_20231024';
    date_str{file_idx} = sprintf('%04d%02d%02d',datestr_year,datestr_month,datestr_day);
  end

end

%% gps_create
% Read and translate files according to user settings
% =========================================================================
gps_create;

%% custom fixes
% =========================================================================
for idx = 1:length(file_type)
  out_fn = fullfile(gps_path,out_fns{idx});

  % There are many segments that have longitude errors caused by Novatel
  % Inertial Explorer's smoothing function was did not used to filter
  % across -180 to +180 transitions properly. Use gps_check.m to find the
  % indices that are bad. Commands for checking that:
  if ~isempty(regexpi(gps_source,'span'))
    fprintf('%s\n', out_fn);
    fprintf('  Removing bad speed records which indicate longitude -180 to +180 crossing smoothing error due to old Novatel smoothing implementation error.\n')
    gps = load(out_fn);

    speed = abs(diff(geodetic_to_along_track(gps)) ./ diff(gps.gps_time));
    speed_idxs = find(speed > 300);
    speed_idxs = speed_idxs(2:2:end);
    fprintf('    %d\n', speed_idxs);
    if ~isempty(speed_idxs)
      good_mask = true(size(gps.lon));
      good_mask(speed_idxs) = false;
      gps.gps_time = gps.gps_time(good_mask);
      gps.lat = gps.lat(good_mask);
      gps.lon = gps.lon(good_mask);
      gps.elev = gps.elev(good_mask);
      gps.roll = gps.roll(good_mask);
      gps.pitch = gps.pitch(good_mask);
      gps.heading = gps.heading(good_mask);

      speed = abs(diff(geodetic_to_along_track(gps)) ./ diff(gps.gps_time));
      speed_idxs = find(speed > 300);
      speed_idxs = speed_idxs(2:2:end);
      if isempty(speed_idxs)
        save(out_fn,'-v7.3','-struct','gps');
      else
        warning('GPS still has too high speed.')
      end
    end
  end
  
end
