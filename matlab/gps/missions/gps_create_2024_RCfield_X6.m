
% script gps_create_2024_RCfield_X6
%
% Makes the GPS files for 2024 RC field X6  test flights

tic;

global gRadar;

support_path = '';
data_support_path = '/resfs/GROUPS/CRESIS/General/projects/X6_Pro_RCFlightTest';

if isempty(support_path)
  support_path = gRadar.support_path;
end

gps_path = fullfile(support_path,'gps','2024_RCfield_X6');
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

% ======================================================================
% User Settings
% ======================================================================
debug_level = 1;

in_base_path = fullfile(data_support_path,'');

file_idx = 0; in_fns = {}; out_fns = {}; file_type = {}; params = {}; gps_source = {};
sync_fns = {}; sync_params = {};

season_name = '2024_RCfield_X6';
gps_source_to_use = 'novatel';

if strcmpi(gps_source_to_use,'novatel')
  % year = 2024; month = 2; day = 1;
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'Novatel_GPS'),'PPP','55','.txt');
  % file_type{file_idx} = 'General_ASCII';
  % params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  % params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  % params{file_idx}.textscan = {};
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  % gps_source{file_idx} = 'cresis-final20240306';
  % sync_flag{file_idx} = 0;
  % date_str{file_idx} = '20240201';

  year = 2024; month = 2; day = 21;
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'GPS','Novatel'),'LOG','59','.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final20240306';
  sync_flag{file_idx} = 0;
  date_str{file_idx} = '20240221';
end

% ======================================================================
% Read and translate files according to user settings
% ======================================================================
gps_create;

RCfield_Novatel_elev = 223; % from Data_20240201_01_001.mat over runway:mean(tmp.Elevation(600:1200)-tmp.Surface(600:1200)*150e6) 
RCfield_X6_alt = 265.7; % from static measurements at the calibration site: mean(alt(200:700))
% Merge gps,pitch,roll and heading from X6 DataFlash log files
for fn_idx = 1:length(out_fns)
    X6_GPS_fns = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'X6_GPS_AHR2'),'GPS','','.csv');
    X6_AHR2_fns = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'X6_GPS_AHR2'),'AHR2','','.csv');
    gps = load(fullfile(gps_path,out_fns{fn_idx}));
    
    [gps_time,lat,lon,alt,roll,pitch,heading] = X6_attitudes(X6_GPS_fns,X6_AHR2_fns,year);
    % remove data before GPS locking (alt~=0)
    good_idxs = find(alt>RCfield_X6_alt-20);
    gps_time = gps_time(good_idxs);
    lat = lat(good_idxs);
    lon = lon(good_idxs);
    alt = alt(good_idxs);    
    roll = roll(good_idxs);
    pitch = pitch(good_idxs);
    heading = heading(good_idxs);   
    
    % use Novatel GPS elevation to calibrate UAV's alt
    elev = alt - (RCfield_X6_alt - RCfield_Novatel_elev);

    % merger the pitch,roll and heading from UAV's AHR2
    roll = roll*pi/180;
    pitch = pitch*pi/180;
    heading = heading*pi/180;
    gps.roll = interp1(gps_time,roll,gps.gps_time,'linear',0);
    gps.pitch = interp1(gps_time,pitch,gps.gps_time,'linear',0);
    gps.heading = interp1(gps_time,heading,gps.gps_time,'linear',0);
    
    % append the UAV's gps and AHRS data beyond the Novatel GPS coverage
    append_idxs1 = find(gps_time<gps.gps_time(1));
    append_idxs2 = find(gps_time>gps.gps_time(end));
    gps.gps_time = [gps_time(append_idxs1),gps.gps_time,gps_time(append_idxs2)];
    gps.elev = [elev(append_idxs1),gps.elev,elev(append_idxs2)];
    gps.lat = [lat(append_idxs1),gps.lat,lat(append_idxs2)];
    gps.lon = [lon(append_idxs1),gps.lon,lon(append_idxs2)];
    gps.roll = [roll(append_idxs1),gps.roll,roll(append_idxs2)];
    gps.pitch = [pitch(append_idxs1),gps.pitch,pitch(append_idxs2)];
    gps.heading = [heading(append_idxs1),gps.heading,heading(append_idxs2)];
    save(fullfile(gps_path,out_fns{fn_idx}),'-struct','gps');
end