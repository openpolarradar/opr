% script gps_create_2022_Antarctica_BaslerMKB
%
% Makes the GPS files for 2022_Antarctica_BaslerMKB field season

%% Setup
% =========================================================================
tic;

global gRadar;

support_path = '';
data_support_path = '';

if isempty(support_path)
  support_path = gRadar.support_path;
end

season_name = '2022_Antarctica_BaslerMKB';

gps_path = fullfile(support_path,'gps',season_name);
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

in_base_path = fullfile(data_support_path,season_name);

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};


%% <== CHOOSE WHICH GPS SOURCE TO PROCESS
% gps_source_to_use = 'utig';
gps_source_to_use = 'utig_ppp';

if strcmpi(gps_source_to_use,'utig')
  %% "utig ELSA" GPS SOURCE
  % =======================================================================

  % year = 2022; month = 12; day = 10;
  % datestr_year = 2022; datestr_month = 12; datestr_day = 10; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;

  % year = 2022; month = 12; day = 12;
  % datestr_year = 2022; datestr_month = 12; datestr_day = 12; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2022; month = 12; day = 13;
  % datestr_year = 2022; datestr_month = 12; datestr_day = 13; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2022; month = 12; day = 28;
  % datestr_year = 2022; datestr_month = 12; datestr_day = 28; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2022; month = 12; day = 29;
  % datestr_year = 2022; datestr_month = 12; datestr_day = 29; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 6;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 6; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 7;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 7; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 8;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 8; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 9;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 9; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 10;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 10; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 12;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 12; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 13;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 13; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 16;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 16; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 17;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 17; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 20;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 20; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 24;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 24; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
%   year = 2023; month = 1; day = 25;
%   datestr_year = 2023; datestr_month = 1; datestr_day = 25; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 1; day = 26;
%   datestr_year = 2023; datestr_month = 1; datestr_day = 26; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 27;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 27; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
  % 
  % year = 2023; month = 1; day = 29;
  % datestr_year = 2023; datestr_month = 1; datestr_day = 29; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;

elseif strcmpi(gps_source_to_use,'utig_ppp')
  %% UTIG Post-processed GPS SOURCE
  % =======================================================================

  utig_ppp_dir = fullfile('/kucresis','scratch','data','UTIG','utigCXA1','targ','xtra','CXA1','POS','wpt1');
  utig_ppp_data = { ...
%     2022,12,10,	'F01';
%     2022,12,12,	'F02';
%     2022,12,13,	'F03';
%     2022,12,29,	'F05';
%     2023,01,07,	'F07';
%     2023,01,08,	'F08';
%     2023,01,09,	'F09';
%     2023,01,10,	'F10'; % File capitalization is wrong, just fix the filename
%     2023,01,12,	'F11';
%     2023,01,13,	'F12';
%     2023,01,16,	'F13';
%     2023,01,17,	'F14';
%     2023,01,20,	'F15';
%     2023,01,24,	'F16';
%     2023,01,25,	'F17';
%     2023,01,26,	'F18';
%     2023,01,27,	'F19'; % gunzip the file
%     2023,01,29,	'F20';
    };

  % NOTE F04 has no data and F06 is broken into three files. The latter is
  % dealt with after this for-loop.

  for day_idx = 1:size(utig_ppp_data,1)
    year = utig_ppp_data{day_idx,1}; month = utig_ppp_data{day_idx,2}; day = utig_ppp_data{day_idx,3};
    datestr_year = utig_ppp_data{day_idx}; datestr_month = utig_ppp_data{day_idx,2}; datestr_day = utig_ppp_data{day_idx,3}; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
    file_idx = file_idx + 1;
    in_fns{file_idx} = fullfile(utig_ppp_dir,utig_ppp_data{day_idx,4},sprintf('CXA1_%s_MKB14_LCPPP.wpt1',utig_ppp_data{day_idx,4}));
    out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig_ppp.mat', datestr_year, datestr_month, datestr_day);
    date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
    file_type{file_idx} = 'General_ASCII';
    params{file_idx} = struct('time_reference','utc','headerlines',21,'format_str','%f%f%f%f%f%f%f%f%f%f%f%f%f');
    params{file_idx}.types = {'year','day', 'sec','lon_deg','lat_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3','tmp_4'};
    params{file_idx}.textscan = {};
    gps_source{file_idx} = 'utig_postprocessed-final20231020';
    sync_flag{file_idx} = 1;
    % copyfile(fullfile('/data/UTIG/orig/xped/CXA1/acqn/ELSA/',utig_ppp_data{day_idx,4},'/serial*'), fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA/'))
    sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
    sync_file_type{file_idx} = 'utig';
    sync_params{file_idx} = struct('time_reference','utc');
  end

%   year = 2023; month = 1; day = 6;
%   datestr_year = 2023; datestr_month = 1; datestr_day = 6; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = {fullfile(utig_ppp_dir,'F06a','CXA1_F06a_MKB14_TCPPP.wpt1');
%     fullfile(utig_ppp_dir,'F06b','CXA1_F06b_MKB14_LCPPP.wpt1');
%     fullfile(utig_ppp_dir,'F06c','CXA1_F06c_MKB14_LCPPP.wpt1');};
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig_ppp.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'General_ASCII';
%   params{file_idx} = struct('time_reference','utc','headerlines',21,'format_str','%f%f%f%f%f%f%f%f%f%f%f%f%f');
%   params{file_idx}.types = {'year','day', 'sec','lon_deg','lat_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3','tmp_4'};
%   params{file_idx}.textscan = {};
%   gps_source{file_idx} = 'utig_postprocessed-final20231020';
%   sync_flag{file_idx} = 1;
%   % copyfile(fullfile('/data/UTIG/orig/xped/CXA1/acqn/ELSA/','F06','/serial*'), fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA/'))
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   sync_file_type{file_idx} = 'utig';
%   sync_params{file_idx} = struct('time_reference','utc');

end

%% gps_create
% Read and translate files according to user settings
% =========================================================================
gps_create;

%% custom fixes
% =========================================================================
for idx = 1:length(file_type)
  out_fn = fullfile(gps_path,out_fns{idx});
  
  [out_fn_dir,out_fn_name,out_fn_ext] = fileparts(out_fn);
  % PPP files need to have comp_time and radar_time interpolated to match
  % the gps.gps_time sample points output by PPP.
  if strcmp(out_fn_name(end-2:end),'ppp')

    fprintf('ppp: %s\n', out_fn);
    gps = load(out_fn);

    serial_out_fn_name = out_fn_name(1:end-4);
    serial_out_fn = fullfile(out_fn_dir,[serial_out_fn_name out_fn_ext]);
    fprintf('gps serial: %s\n', serial_out_fn);
    gps_serial = load(serial_out_fn);

    good_idxs = find(gps_serial.gps_time < min(gps.gps_time));

    gps.gps_time = [gps_serial.gps_time(good_idxs) gps.gps_time];
    gps.lat = [gps_serial.lat(good_idxs) gps.lat];
    gps.lon = [gps_serial.lon(good_idxs) gps.lon];
    gps.elev = [gps_serial.elev(good_idxs) gps.elev];
    gps.roll = [gps_serial.roll(good_idxs) gps.roll];
    gps.pitch = [gps_serial.pitch(good_idxs) gps.pitch];
    gps.heading = [gps_serial.heading(good_idxs) gps.heading];

    save(out_fn,'-v7.3','-struct','gps');
  end
end
