% script gps_create_2024_Antarctica_Ground2
%
% Makes the GPS files for 2024_Antarctica_Ground2 field season

%% Setup
% =========================================================================
tic;

global gRadar;

support_path = '';
data_support_path = '';

if isempty(support_path)
  support_path = gRadar.support_path;
end

season_name = '2024_Antarctica_Ground2';

gps_path = fullfile(support_path,'gps',season_name);
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

in_base_path = fullfile(data_support_path,season_name);

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};

%% <== CHOOSE WHICH GPS SOURCE TO PROCESS
gps_source_to_use = 'arena';
% gps_source_to_use = 'postprocessed';

if strcmpi(gps_source_to_use,'arena')
  %% FIELD/ARENA GPS SOURCE
  % =======================================================================
  
%   year = 2024; month = 12; day = 18; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = 2024; date_str_month = 12; date_str_day = 18; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');
    
%   year = 2024; month = 12; day = 30; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');
      
%  year = 2025; month = 1; day = 3; % Where the files got copied to
%  file_idx = file_idx + 1;
%  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%  %%%%%%%%%%%%% You may need to edit this line to a different date than the
%  %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%  date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%  date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%  file_type{file_idx} = 'arena';
%  params{file_idx} = struct('time_reference','utc');
%  gps_source{file_idx} = 'arena-field';
%  sync_flag{file_idx} = 1;
%  sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%  sync_file_type{file_idx} = 'arena';
%  sync_params{file_idx} = struct('time_reference','utc');
%   
%   year = 2025; month = 1; day = 4; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');
%   
%   year = 2025; month = 1; day = 6; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');
% 
%   year = 2025; month = 1; day = 7; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');

%   year = 2025; month = 1; day = 8; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');

%   year = 2025; month = 1; day = 11; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');
% 
%   year = 2025; month = 1; day = 12; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');
% 
%   year = 2025; month = 1; day = 15; % Where the files got copied to
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   %%%%%%%%%%%%% You may need to edit this line to a different date than the
%   %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
%   date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
%   file_type{file_idx} = 'arena';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'arena-field';
%   sync_flag{file_idx} = 1;
%   sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
%   sync_file_type{file_idx} = 'arena';
%   sync_params{file_idx} = struct('time_reference','utc');

  year = 2025; month = 1; day = 17; % Where the files got copied to
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
  %%%%%%%%%%%%% You may need to edit this line to a different date than the
  %%%%%%%%%%%%% listed one above. Do so if the date wrap screwed things up.
  date_str_year = year; date_str_month = month; date_str_day = day; % The day string that preprocess printed out
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', date_str_year, date_str_month, date_str_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', date_str_year, date_str_month, date_str_day);
  file_type{file_idx} = 'arena';
  params{file_idx} = struct('time_reference','utc');
  gps_source{file_idx} = 'arena-field';
  sync_flag{file_idx} = 1;
  sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','','gps.txt');
  sync_file_type{file_idx} = 'arena';
  sync_params{file_idx} = struct('time_reference','utc');
  
  
elseif strcmpi(gps_source_to_use,'postprocessed')
  %% POST PROCESSING GPS SOURCE
  % =======================================================================

  postprocessed_fns = get_filenames(in_base_path,'ie_','','_cat.txt');

  for postprocessed_fns_idx = 1:length(postprocessed_fns)
    postprocessed_fn = postprocessed_fns{postprocessed_fns_idx};
    [postprocessed_fn_dir,postprocessed_fn_name,postprocessed_fn_ext] = fileparts(postprocessed_fn);
    datestr_year = str2double(postprocessed_fn_name(4:7));
    datestr_month = str2double(postprocessed_fn_name(8:9));
    datestr_day = str2double(postprocessed_fn_name(10:11));
    date_time_stamp_str = postprocessed_fn_name(4:18);
    fprintf('%s: %04d%02d%02d %s\n', postprocessed_fn, datestr_year, datestr_month, datestr_day, date_time_stamp_str);
    file_idx = file_idx + 1;
    in_fns{file_idx} = postprocessed_fn;
    out_fns{file_idx} = fullfile('postprocessed', sprintf('gps_%s.mat', date_time_stamp_str));
    file_type{file_idx} = 'General_ASCII';
    params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
    params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
    params{file_idx}.textscan = {};
    gps_source{file_idx} = 'cresis-final_20240223';
    date_str{file_idx} = sprintf('%04d%02d%02d',datestr_year,datestr_month,datestr_day);
    sync_flag{file_idx} = 0;
  end

  arena_sync_fns = get_filenames(in_base_path,'202[34]','','-ctu-gps.txt',struct('recursive',true));

  for arena_sync_fns_idx = 1:length(arena_sync_fns)
    arena_sync_fn = arena_sync_fns{arena_sync_fns_idx};
    [arena_sync_fn_dir,arena_sync_fn_name,arena_sync_fn_ext] = fileparts(arena_sync_fn);
    datestr_year = str2double(arena_sync_fn_name(1:4));
    datestr_month = str2double(arena_sync_fn_name(5:6));
    datestr_day = str2double(arena_sync_fn_name(7:8));
    date_time_stamp_str = arena_sync_fn_name(1:15);
    fprintf('%s: %04d%02d%02d %s\n', arena_sync_fn, datestr_year, datestr_month, datestr_day, date_time_stamp_str);
    file_idx = file_idx + 1;
    in_fns{file_idx} = arena_sync_fn;
    out_fns{file_idx} = fullfile('sync', sprintf('gps_%s.mat', date_time_stamp_str));
    date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
    file_type{file_idx} = 'arena';
    params{file_idx} = struct('time_reference','utc');
    gps_source{file_idx} = 'arena-field';
    sync_flag{file_idx} = 1;
    sync_fns{file_idx} = arena_sync_fn;
    sync_file_type{file_idx} = 'arena';
    sync_params{file_idx} = struct('time_reference','utc');
  end

end

%% gps_create
% Read and translate files according to user settings
% =========================================================================
gps_create;

%% custom fixes
% =========================================================================
if strcmpi(gps_source_to_use,'arena')
  for file_idx = 1:length(out_fns)
    out_fn = fullfile(gps_path,out_fns{file_idx});

    load(out_fn,'gps_source');
    if ~isempty(regexpi(gps_source,'field'))
      % Extrapolation is necessary because GPS data starts after/stops before
      % the beginning/end of the radar data.
      warning('Extrapolating and filtering elevation for arena GPS data: %s', out_fn);
      gps = load(out_fn);

      if length(gps.lat) >= 2
        new_gps_time = [gps.gps_time(1)-20, gps.gps_time,gps.gps_time(end)+20];
        gps.lat = gps_interp1(gps.gps_time,gps.lat*pi/180,new_gps_time,'linear','extrap')*180/pi;
        gps.lon = gps_interp1(gps.gps_time,gps.lon*pi/180,new_gps_time,'linear','extrap')*180/pi;
        gps.elev = interp1(gps.gps_time,gps.elev,new_gps_time,'linear','extrap');
        gps.roll = gps_interp1(gps.gps_time,gps.roll,new_gps_time,'linear','extrap');
        gps.pitch = gps_interp1(gps.gps_time,gps.pitch,new_gps_time,'linear','extrap');
        gps.heading = gps_interp1(gps.gps_time,gps.heading,new_gps_time,'linear','extrap');
        gps.gps_time = new_gps_time;

        gps.elev = fir_dec(gps.elev,ones(1,101)/101,1);

        if 0
          % This may be necessary
          good_idxs = gps.radar_time~=0;
          gps.radar_time = gps.radar_time(good_idxs);
          gps.comp_time = gps.comp_time(:,good_idxs);
          gps.sync_gps_time = gps.sync_gps_time(good_idxs);
          gps.sync_lat = gps.sync_lat(good_idxs);
          gps.sync_lon = gps.sync_lon(good_idxs);
          gps.sync_elev = gps.sync_elev(good_idxs);
          save(out_fn,'-struct','gps');
        end

        save(out_fn,'-v7.3','-struct','gps');
      end
    end
  end

elseif strcmpi(gps_source_to_use,'postprocessed')

  out_postprocessed_fns = get_filenames(fullfile(gps_path, 'postprocessed'),'gps_','','.mat');
  out_postprocessed_fns_gps_start = [];
  out_postprocessed_fns_gps_stop = [];
  for out_postprocessed_fns_idx = 1:length(out_postprocessed_fns)
    out_postprocessed_fn = out_postprocessed_fns{out_postprocessed_fns_idx};
    gps = load(out_postprocessed_fn,'gps_time');
    out_postprocessed_fns_gps_start(end+1) = gps.gps_time(1);
    out_postprocessed_fns_gps_stop(end+1) = gps.gps_time(end);
  end

  out_heading_fns = get_filenames(fullfile(gps_path, 'heading'),'gps_','','.mat');
  out_heading_fns_gps_start = [];
  out_heading_fns_gps_stop = [];
  for out_heading_fns_idx = 1:length(out_heading_fns)
    out_heading_fn = out_heading_fns{out_heading_fns_idx};
    gps = load(out_heading_fn,'gps_time');
    out_heading_fns_gps_start(end+1) = gps.gps_time(1);
    out_heading_fns_gps_stop(end+1) = gps.gps_time(end);
  end

  out_sync_fns = get_filenames(fullfile(gps_path, 'sync'),'gps_','','.mat');
  out_sync_fns_gps_start = [];
  out_sync_fns_gps_stop = [];
  for out_sync_fns_idx = 1:length(out_sync_fns)
    out_sync_fn = out_sync_fns{out_sync_fns_idx};
    gps = load(out_sync_fn,'gps_time');
    if ~isempty(gps.gps_time)
      out_sync_fns_gps_start(end+1) = gps.gps_time(1);
      out_sync_fns_gps_stop(end+1) = gps.gps_time(end);
    else
      out_sync_fns_gps_start(end+1) = NaN;
      out_sync_fns_gps_stop(end+1) = NaN;
    end
  end

  gnss_out_list = {};
%   gnss_out_list{end+1} = '20231124';
  
  for gnss_out_list_idx = 1:length(gnss_out_list)
    date_str = gnss_out_list{gnss_out_list_idx};
    year = str2double(date_str(1:4));
    month = str2double(date_str(5:6));
    day = str2double(date_str(7:8));

    % Load all GNSS data from 1 day before the start of the current day to
    % 1 day after the end of the current day (2 days after the start of the
    % current day).
    
    % Desired start time
    start_time = datenum_to_epoch(datenum(year, month, day, 0, 0, 0)-1);
    
    % Desired stop time
    stop_time = datenum_to_epoch(datenum(year, month, day, 0, 0, 0)+2);

    % Post processed files to load
    out_postprocessed_idxs = find(out_postprocessed_fns_gps_start <= stop_time & out_postprocessed_fns_gps_stop >= start_time);

    % Heading files to load
    out_heading_idxs = find(out_heading_fns_gps_start <= stop_time & out_heading_fns_gps_stop >= start_time);

    % Sync files to load
    out_sync_idxs = find(out_sync_fns_gps_start <= stop_time & out_sync_fns_gps_stop >= start_time);

    % Load post-processed files
    for idx = 1:length(out_postprocessed_idxs)
      out_postprocessed_idx = out_postprocessed_idxs(idx);
      out_postprocessed_fn = out_postprocessed_fns{out_postprocessed_idx};

      tmp_gps = gps_load(out_postprocessed_fn);

      % Filter elevation data
      %tmp_gps.elev = fir_dec(tmp_gps.elev,ones(1,5)/5,1);

      if idx == 1
        gps = tmp_gps;
      else
        Nx = length(tmp_gps.gps_time);
        gps.gps_time(end+(1:Nx)) = tmp_gps.gps_time;
        gps.lat(end+(1:Nx)) = tmp_gps.lat;
        gps.lon(end+(1:Nx)) = tmp_gps.lon;
        gps.elev(end+(1:Nx)) = tmp_gps.elev;
        gps.roll(end+(1:Nx)) = tmp_gps.roll;
        gps.pitch(end+(1:Nx)) = tmp_gps.pitch;
        gps.heading(end+(1:Nx)) = tmp_gps.heading;
      end
    end

    % Load heading files
    for idx = 1:length(out_heading_idxs)
      out_heading_idx = out_heading_idxs(idx);
      out_heading_fn = out_heading_fns{out_heading_idx};

      % Enable gps_time_only so that records with lat/lon/elev equal to NaN
      % will still be kept (only using the heading and pitch).
      tmp_gps = gps_load(out_heading_fn,struct('gps_time_only',true));

      if idx == 1
        heading_gps = tmp_gps;
      else
        Nx = length(tmp_gps.gps_time);
        heading_gps.gps_time(end+(1:Nx)) = tmp_gps.gps_time;
        heading_gps.lat(end+(1:Nx)) = tmp_gps.lat;
        heading_gps.lon(end+(1:Nx)) = tmp_gps.lon;
        heading_gps.elev(end+(1:Nx)) = tmp_gps.elev;
        heading_gps.roll(end+(1:Nx)) = tmp_gps.roll;
        heading_gps.pitch(end+(1:Nx)) = tmp_gps.pitch;
        heading_gps.heading(end+(1:Nx)) = tmp_gps.heading;
      end
    end

    % Load sync files
    for idx = 1:length(out_sync_idxs)
      out_sync_idx = out_sync_idxs(idx);
      out_sync_fn = out_sync_fns{out_sync_idx};

      tmp_gps = gps_load(out_sync_fn);

      if idx == 1
        sync_gps = tmp_gps;
      else
        Nx = length(tmp_gps.gps_time);
        sync_gps.gps_time(end+(1:Nx)) = tmp_gps.gps_time;
        sync_gps.radar_time(end+(1:Nx)) = tmp_gps.radar_time;
        sync_gps.comp_time(:,end+(1:Nx)) = tmp_gps.comp_time;
        sync_gps.sync_elev(end+(1:Nx)) = tmp_gps.sync_elev;
        sync_gps.sync_gps_time(end+(1:Nx)) = tmp_gps.sync_gps_time;
        sync_gps.sync_lat(end+(1:Nx)) = tmp_gps.sync_lat;
        sync_gps.sync_lon(end+(1:Nx)) = tmp_gps.sync_lon;
        sync_gps.lat(end+(1:Nx)) = tmp_gps.lat;
        sync_gps.lon(end+(1:Nx)) = tmp_gps.lon;
        sync_gps.elev(end+(1:Nx)) = tmp_gps.elev;
        sync_gps.roll(end+(1:Nx)) = tmp_gps.roll;
        sync_gps.pitch(end+(1:Nx)) = tmp_gps.pitch;
        sync_gps.heading(end+(1:Nx)) = tmp_gps.heading;
      end
    end
    % Cleanup sync data
    good_idxs = sync_gps.radar_time~=0;
    sync_gps.radar_time = sync_gps.radar_time(good_idxs);
    sync_gps.comp_time = sync_gps.comp_time(:,good_idxs);
    sync_gps.sync_elev = sync_gps.sync_elev(good_idxs);
    sync_gps.sync_gps_time = sync_gps.sync_gps_time(good_idxs);
    sync_gps.sync_lat = sync_gps.sync_lat(good_idxs);
    sync_gps.sync_lon = sync_gps.sync_lon(good_idxs);

    % Add in sync data
    gps.comp_time = sync_gps.comp_time;
    gps.radar_time = sync_gps.radar_time;
    gps.sync_elev = sync_gps.sync_elev;
    gps.sync_gps_time = sync_gps.sync_gps_time;
    gps.sync_lat = sync_gps.sync_lat;
    gps.sync_lon = sync_gps.sync_lon;

    % Update param_gps
    gps.param_gps.in_fns = out_postprocessed_fns(out_postprocessed_idxs);

    gps.param_gps.sync_flag = sync_gps.param_gps.sync_flag;
    gps.param_gps.sync_file_type = sync_gps.param_gps.sync_file_type;
    gps.param_gps.sync_params = sync_gps.param_gps.sync_params;
    gps.param_gps.sync_fns = out_sync_fns(out_sync_idxs);

    % Add roll and heading information
    old_heading = gps.heading;
    heading_gps = gps_force_monotonic(heading_gps);
    bad_mask = heading_gps.heading==0 & heading_gps.pitch==0;
    heading_gps.pitch(bad_mask) = NaN;
    heading_gps.heading(bad_mask) = NaN;
    heading_gps.pitch = interp_finite(heading_gps.pitch,0,@gps_interp1);
    heading_gps.heading = interp_finite(heading_gps.heading,0,@gps_interp1);
    % Primary is in the center, secondary is in front. The heading vector
    % points from the primary to the secondary. Therefore the heading
    % vector for the dual receiver setup matches the heading of the radar.
    gps.heading = gps_interp1(heading_gps.gps_time,heading_gps.heading,gps.gps_time,'linear','extrap');
    % Radar pitch should match the dual receiver pitch as well.
    gps.pitch = gps_interp1(heading_gps.gps_time,heading_gps.pitch,gps.gps_time,'linear','extrap');

    % Save GPS file
    out_fn = fullfile(gps_path,sprintf('gps_%s.mat', date_str));
    if 0
      % Debugging
      if exist(out_fn,'file')
        dd = load(out_fn);
        figure(1); clf;
        plot(dd.lon, dd.lat,'LineWidth',2);
        hold on
        plot(gps.lon, gps.lat);
        keyboard
      end
    end
    fprintf('Saving final output %s\n', out_fn);
    ct_save(out_fn,'-v7.3','-STRUCT','gps');
  end
end
