% script gps_create_2023_Antarctica_BaslerMKB
%
% Makes the GPS files for 2023_Antarctica_BaslerMKB field season

%% Setup
% =========================================================================
tic;

global gRadar;

support_path = '';
data_support_path = '';

if isempty(support_path)
  support_path = gRadar.support_path;
end

season_name = '2023_Antarctica_BaslerMKB';

gps_path = fullfile(support_path,'gps',season_name);
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

in_base_path = fullfile(data_support_path,season_name);

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};


%% <== CHOOSE WHICH GPS SOURCE TO PROCESS
% gps_source_to_use = 'utig';
% gps_source_to_use = 'utig_ppp';
gps_source_to_use = 'novatelpostprocessed';

if strcmpi(gps_source_to_use,'utig')
  %% "utig ELSA" GPS SOURCE
  % =======================================================================

%   year = 2023; month = 12; day = 9;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;

%   year = 2023; month = 12; day = 11;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 12;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;

%   year = 2023; month = 12; day = 17;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;

%   year = 2023; month = 12; day = 21;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 24;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 25;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 26;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 27;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 28;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 29;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2024; month = 1; day = 4;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2024; month = 1; day = 5;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2024; month = 1; day = 7;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2024; month = 1; day = 8;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2024; month = 1; day = 9;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'utig';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'utig-field';
%   sync_flag{file_idx} = 0;
% 
  % year = 2024; month = 1; day = 11;
  % datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'utig';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'utig-field';
  % sync_flag{file_idx} = 0;
% 
  year = 2024; month = 1; day = 12;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'utig';
  params{file_idx} = struct('time_reference','utc');
  gps_source{file_idx} = 'utig-field';
  sync_flag{file_idx} = 0;

elseif strcmpi(gps_source_to_use,'utig_ppp')
  %% UTIG Post-processed GPS SOURCE
  % =======================================================================

  utig_ppp_dir = fullfile(gRadar.data_path,'UTIG','targ','xtra','CXA2','POS','wpt1');
  utig_ppp_data = { ...
    2023,12,11,	'F02';
    2023,12,12,	'F03';
    };

  % NOTE F04 has no data and F06 is broken into three files. The latter is
  % dealt with after this for-loop.

  for day_idx = 1:size(utig_ppp_data,1)
    year = utig_ppp_data{day_idx,1}; month = utig_ppp_data{day_idx,2}; day = utig_ppp_data{day_idx,3};
    datestr_year = utig_ppp_data{day_idx}; datestr_month = utig_ppp_data{day_idx,2}; datestr_day = utig_ppp_data{day_idx,3}; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
    file_idx = file_idx + 1;
    in_fns{file_idx} = fullfile(utig_ppp_dir,utig_ppp_data{day_idx,4},sprintf('CXA2_%s_MKB14_LCPPP.wpt1',utig_ppp_data{day_idx,4}));
    out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig_ppp.mat', datestr_year, datestr_month, datestr_day);
    date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
    file_type{file_idx} = 'General_ASCII';
    params{file_idx} = struct('time_reference','utc','headerlines',21,'format_str','%f%f%f%f%f%f%f%f%f%f%f%f%f');
    params{file_idx}.types = {'year','day', 'sec','lon_deg','lat_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3','tmp_4'};
    params{file_idx}.textscan = {};
    gps_source{file_idx} = 'utig_postprocessed-final20231020';
    sync_flag{file_idx} = 1;
    sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
    sync_file_type{file_idx} = 'utig';
    sync_params{file_idx} = struct('time_reference','utc');
  end

  % year = 2023; month = 1; day = 6;
  % datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = {fullfile(utig_ppp_dir,'F06a','CXA1_F06a_MKB14_TCPPP.wpt1');
  %   fullfile(utig_ppp_dir,'F06b','CXA1_F06b_MKB14_LCPPP.wpt1');
  %   fullfile(utig_ppp_dir,'F06c','CXA1_F06c_MKB14_LCPPP.wpt1');};
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig_ppp.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'General_ASCII';
  % params{file_idx} = struct('time_reference','utc','headerlines',21,'format_str','%f%f%f%f%f%f%f%f%f%f%f%f%f');
  % params{file_idx}.types = {'year','day', 'sec','lon_deg','lat_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3','tmp_4'};
  % params{file_idx}.textscan = {};
  % gps_source{file_idx} = 'utig_postprocessed-final20231020';
  % sync_flag{file_idx} = 1;
  % sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
  % sync_file_type{file_idx} = 'utig';
  % sync_params{file_idx} = struct('time_reference','utc');
  
elseif strcmpi(gps_source_to_use,'novatelpostprocessed')
  %% Novatel Post Processed GPS SOURCE
  % =======================================================================

    novatel_tc_data = { ...
      2023,12,11,	'F02';
      2023,12,12,	'F03';
      2023,12,21,	'F05';
      2023,12,24,	'F06';
      2023,12,25,	'F07';
      2023,12,26,	'F08'; % Needs to be saved as 20231226
      2023,12,27,	'F09';
      2023,12,28,	'F10';
      2023,12,29,	'F11_F12';
      2024,01,04,	'F13_F14';
      2024,01,05,	'F15_F16';
      2024,01,07,	'F17';
      2024,01,08,	'F18';
      2024,01,09,	'F19';
      2024,01,11,	'F20_F21';
      2024,01,12,	'F22';
    };

  for day_idx = 1:size(novatel_tc_data,1)
    year = novatel_tc_data{day_idx,1}; month = novatel_tc_data{day_idx,2}; day = novatel_tc_data{day_idx,3};
    datestr_year = novatel_tc_data{day_idx}; datestr_month = novatel_tc_data{day_idx,2}; datestr_day = novatel_tc_data{day_idx,3}; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
    file_idx = file_idx + 1;
    in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
      sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
    out_fns{file_idx} = sprintf('gps_%04d%02d%02d_utig_tc.mat', datestr_year, datestr_month, datestr_day);
    date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
    file_type{file_idx} = 'applanix';
    params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
    gps_source{file_idx} = 'novatelTC-final';
    sync_flag{file_idx} = 1;
    sync_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day),'ELSA'),'serial','','');
    sync_file_type{file_idx} = 'utig';
    sync_params{file_idx} = struct('time_reference','utc');
  end
  
end

%% gps_create
% Read and translate files according to user settings
% =========================================================================
gps_create;

%% custom fixes
% =========================================================================
for idx = 1:length(file_type)
  out_fn = fullfile(gps_path,out_fns{idx});
  
  [~,out_fn_name] = fileparts(out_fn);
  % PPP files need to have comp_time and radar_time interpolated to match
  % the gps.gps_time sample points output by PPP.
  if strcmp(out_fn_name(end-2:end),'ppp')
    fprintf('Syncing comp_time and radar_time to gps_time: %s\n', out_fn);
    gps = load(out_fn);
    gps.comp_time = interp1(gps.sync_gps_time,gps.comp_time,gps.gps_time);
    gps.radar_time = interp1(gps.sync_gps_time,gps.radar_time,gps.gps_time);
    save(out_fn,'-v7.3','-struct','gps');
  end
end
