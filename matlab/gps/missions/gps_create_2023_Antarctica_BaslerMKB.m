% script gps_create_2023_Antarctica_BaslerMKB
%
% Makes the GPS files for 2023_Antarctica_BaslerMKB field season

%% Setup
% =========================================================================
tic;

global gRadar;

support_path = '';
data_support_path = '';

if isempty(support_path)
  support_path = gRadar.support_path;
end

season_name = '2023_Antarctica_BaslerMKB';

gps_path = fullfile(support_path,'gps',season_name);
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

in_base_path = fullfile(data_support_path,season_name);

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};


%% <== CHOOSE WHICH GPS SOURCE TO PROCESS
% gps_source_to_use = 'novatelraw';
gps_source_to_use = 'novatelpostprocessed';

if strcmpi(gps_source_to_use,'novatelraw')
  %% sonntag_nav GPS SOURCE
  % =======================================================================

%   year = 2023; month = 12; day = 11;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 12;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 21;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 24;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 25;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 26;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;

%   year = 2023; month = 12; day = 27;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;

%   year = 2023; month = 12; day = 28;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2023; month = 12; day = 29;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;

%   year = 2024; month = 1; day = 4;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2024; month = 1; day = 5;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2024; month = 1; day = 7;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
% 
%   year = 2024; month = 1; day = 8;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
  % 
%   year = 2024; month = 1; day = 9;
%   datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
%   file_idx = file_idx + 1;
%   in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
%   out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
%   date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
%   file_type{file_idx} = 'novatelraw';
%   params{file_idx} = struct('time_reference','utc');
%   gps_source{file_idx} = 'novatelraw-field';
%   sync_flag{file_idx} = 0;
  % 
  % year = 2024; month = 1; day = 11;
  % datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  % file_idx = file_idx + 1;
  % in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
  % out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  % date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  % file_type{file_idx} = 'novatelraw';
  % params{file_idx} = struct('time_reference','utc');
  % gps_source{file_idx} = 'novatelraw-field';
  % sync_flag{file_idx} = 0;
  
  year = 2024; month = 1; day = 12;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'','aq-field22','gps');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'novatelraw';
  params{file_idx} = struct('time_reference','utc');
  gps_source{file_idx} = 'novatelraw-field';
  sync_flag{file_idx} = 0;
  
elseif strcmpi(gps_source_to_use,'novatelpostprocessed')
  %% Novatel Post Processed GPS SOURCE
  % =======================================================================

  year = 2023; month = 12; day = 11;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2023; month = 12; day = 12;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2023; month = 12; day = 21;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2023; month = 12; day = 24;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2023; month = 12; day = 25;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2023; month = 12; day = 26;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2023; month = 12; day = 27;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2023; month = 12; day = 28;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2023; month = 12; day = 29;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2024; month = 1; day = 4;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2024; month = 1; day = 5;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2024; month = 1; day = 7;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2024; month = 1; day = 8;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2024; month = 1; day = 9;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2024; month = 1; day = 11;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

  year = 2024; month = 1; day = 12;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'novatel','FINAL'),...
    sprintf('ie_field22_%04d%02d%02d',year,month,day),'TC','OUT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'applanix';
  params{file_idx} = struct('time_reference','gps','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'novatelTC-final';
  sync_flag{file_idx} = 0;

end

%% gps_create
% Read and translate files according to user settings
% =========================================================================
gps_create;

%% custom fixes
% =========================================================================
for idx = 1:length(file_type)
  out_fn = fullfile(gps_path,out_fns{idx});
  
  gps = load(out_fn);

%   if ~isempty(regexpi(gps_source,'novatelraw')) && ~isempty(regexpi(out_fn,'20230113'))
%     gps.roll(:) = 0;
%     gps.pitch(:) = 0;
%     [est_heading,along_track,speed] = trajectory_coord_system(gps);
%     gps.heading = est_heading;
%     
%     save(out_fn,'-append','-struct','gps','roll','pitch','heading');
%   end
  
  % Arena 500's radar_time is UTC time, fill in sync fields with this
  % information.
  gps.radar_time = gps.gps_time - utc_leap_seconds(gps.gps_time(1));
  gps.comp_time = gps.gps_time - utc_leap_seconds(gps.gps_time(1));
  gps.sync_gps_time = gps.gps_time;
  gps.sync_lat = gps.lat;
  gps.sync_lon = gps.lon;
  gps.sync_elev = gps.elev;
  
  fprintf('Saving sync fields into %s\n', out_fn);
  save(out_fn,'-append','-struct','gps','radar_time','comp_time','sync_gps_time','sync_lat','sync_lon','sync_elev');  
end
