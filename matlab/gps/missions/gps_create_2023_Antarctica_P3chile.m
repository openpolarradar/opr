% script gps_create_2023_Antarctica_P3chile
%
% Makes the GNSS files for 2023_Antarctica_P3chile field season

%% Setup
% =========================================================================
tic;

global gRadar;

support_path = '';
data_support_path = '';

if isempty(support_path)
  support_path = gRadar.support_path;
end

season_name = '2023_Antarctica_P3chile';

gps_path = fullfile(support_path,'gps',season_name);
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

in_base_path = fullfile(data_support_path,season_name);

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};


%% <== CHOOSE WHICH GNSS SOURCE TO PROCESS
% gps_source_to_use = 'cecsnmea'; % GNSS NMEA data stored by radar
gps_source_to_use = 'cecspostprocessed';

if strcmpi(gps_source_to_use,'cecsnmea')
  %% CECS NMEA GNSS SOURCE (created during preprocessing)
  % =======================================================================

  year = 2023; month = 11; day = 18;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('%04d%02d%02d',year,month,day)),'board_hdrs','','.mat');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'board_hdrs';
  params{file_idx} = struct('time_reference','utc','year',year,'month',month,'day',day);
  gps_source{file_idx} = 'cecs-field';
  sync_flag{file_idx} = 0;

elseif strcmpi(gps_source_to_use,'cecspostprocessed')
  %% CECS Post-processed GNSS SOURCE
  % =======================================================================

  % NOTE: Remove error messages in post processed files (search for the
  % string "E202" and at the end of the file)

  year = 2023; month = 11; day = 18;
  datestr_year = year; datestr_month = month; datestr_day = day; % <--- UPDATE TO MATCH WHAT PREPROCESS PRINTS OUT
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(in_base_path,sprintf('LMS_%04d%02d%02',year,month,day),'','.ALL');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', datestr_year, datestr_month, datestr_day);
  date_str{file_idx} = sprintf('%04d%02d%02d', datestr_year, datestr_month, datestr_day);
  file_type{file_idx} = 'general_ascii';

  params{file_idx} = struct('time_reference','utc','year',year,'month',month,'day',day, ...
    'headerlines',38,'format_str','%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'time_HMS','tmp_1','tmp_2','tmp_3','lon_deg','lat_deg','elev_m','roll_deg','pitch_deg','heading_deg'};
  params{file_idx}.textscan = {};
  
  gps_source{file_idx} = 'cecs-final_20240219';
  sync_flag{file_idx} = 0;
  
end

%% gps_create
% Read and translate files according to user settings
% =========================================================================
gps_create;

%% custom fixes
% =========================================================================
for idx = 1:length(file_type)
  out_fn = fullfile(gps_path,out_fns{idx});
  [~,out_fn_name] = fileparts(out_fn);

  load(out_fn,'gps_source');
  if ~isempty(regexpi(gps_source,'cecs-final'))
    % Extrapolation is necessary because GPS data starts after/stops before
    % the beginning/end of the radar data.
    warning('Forcing GNSS data to have monotonically increasing gps_time: %s', out_fn);
    gps = load(out_fn);

    % Making gps_time monotonic
    [gps,error_flag] = gps_force_monotonic(gps);

    save(out_fn,'-v7.3','-struct','gps');

  elseif ~isempty(regexpi(gps_source,'cecs-field'))
    % Extrapolation is necessary because GPS data starts after/stops before
    % the beginning/end of the radar data.
    warning('Removing repeated points, extrapolating, and filtering elevation for CECS NMEA GPS data: %s', out_fn);
    gps = load(out_fn);

    % Removing repeated GPS positions
    along_track = geodetic_to_along_track(gps.lat,gps.lon,gps.elev);
    good_idxs = [1 1+find(diff(along_track)>0)];
    gps.gps_time = gps.gps_time(good_idxs);
    gps.lat = gps.lat(good_idxs);
    gps.lon = gps.lon(good_idxs);
    gps.elev = gps.elev(good_idxs);
    gps.roll = gps.roll(good_idxs);
    gps.pitch = gps.pitch(good_idxs);
    gps.heading = gps.heading(good_idxs);

    % Extrapolating and filtering GPS positions
    if length(gps.lat) >= 2
      new_gps_time = [gps.gps_time(1)-10, gps.gps_time,gps.gps_time(end)+10];
      gps.lat = interp1(gps.gps_time,gps.lat,new_gps_time,'linear','extrap');
      gps.lon = interp1(gps.gps_time,gps.lon,new_gps_time,'linear','extrap');
      gps.elev = interp1(gps.gps_time,gps.elev,new_gps_time,'linear','extrap');
      gps.roll = interp1(gps.gps_time,gps.roll,new_gps_time,'linear','extrap');
      gps.pitch = interp1(gps.gps_time,gps.pitch,new_gps_time,'linear','extrap');
      gps.heading = interp1(gps.gps_time,gps.heading,new_gps_time,'linear','extrap');
      gps.gps_time = new_gps_time;
      
      gps.elev = fir_dec(gps.elev,ones(1,101)/101,1);
      
      save(out_fn,'-v7.3','-struct','gps');
    end
  end

end
