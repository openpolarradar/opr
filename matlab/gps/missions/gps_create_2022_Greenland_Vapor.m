% script make_gps_2022_Greenland_Vapor
%
% Makes the GPS files for 2022_Greenland_Vapor field season

%% Setup
% =========================================================================
tic;

global gRadar;

support_path = '';
data_support_path = '';

if isempty(support_path)
  support_path = gRadar.support_path;
end

season_name = '2022_Greenland_Vapor';

gps_path = fullfile(support_path,'gps',season_name);
if ~exist(gps_path,'dir')
  fprintf('Making directory %s\n', gps_path);
  fprintf('  Press a key to proceed\n');
  pause;
  mkdir(gps_path);
end

if isempty(data_support_path)
  data_support_path = gRadar.data_support_path;
end

in_base_path = fullfile(data_support_path,season_name);

file_idx = 0; in_fns = {}; out_fns = {}; date_str = {}; file_type = {}; params = {}; gps_source = {};
sync_flag = {}; sync_fns = {}; sync_file_type = {}; sync_params = {};


%% <== CHOOSE WHICH GPS SOURCE TO PROCESS
% gps_source_to_use = 'novatelraw';
gps_source_to_use = 'postprocess';

if strcmpi(gps_source_to_use,'novatelraw')
  %% NOVATEL RAW GPS SOURCE
  % =======================================================================
   
  % Raw data files stored from Novatel receiver onto open logger sd-card
  % (requires and uses BESTPOSB and TIMEB logs)

  year = 2022; month = 7; day = 27;
  out_fn = fullfile(gps_path,sprintf('gps_%04d%02d%02d.mat', year, month, day));
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'gps',sprintf('%04d%02d%02d',year,month,day)),'LOG','','.TXT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d_raw.mat', year, month, day);
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  file_type{file_idx} = 'novatelraw';
  params{file_idx} = struct('year',year,'time_reference','utc');
  gps_source{file_idx} = 'novatel-field';
  sync_flag{file_idx} = 0;

  year = 2022; month = 7; day = 28;
  out_fn = fullfile(gps_path,sprintf('gps_%04d%02d%02d.mat', year, month, day));
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'gps',sprintf('%04d%02d%02d',year,month,day)),'LOG','','.TXT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d_raw.mat', year, month, day);
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  file_type{file_idx} = 'novatelraw';
  params{file_idx} = struct('year',year,'time_reference','utc');
  gps_source{file_idx} = 'novatel-field';
  sync_flag{file_idx} = 0;

  year = 2022; month = 7; day = 29;
  out_fn = fullfile(gps_path,sprintf('gps_%04d%02d%02d.mat', year, month, day));
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'gps',sprintf('%04d%02d%02d',year,month,day)),'LOG','','.TXT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d_raw.mat', year, month, day);
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  file_type{file_idx} = 'novatelraw';
  params{file_idx} = struct('year',year,'time_reference','utc');
  gps_source{file_idx} = 'novatel-field';
  sync_flag{file_idx} = 0;

  year = 2022; month = 7; day = 30;
  out_fn = fullfile(gps_path,sprintf('gps_%04d%02d%02d.mat', year, month, day));
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'gps',sprintf('%04d%02d%02d',year,month,day)),'LOG','','.TXT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d_raw.mat', year, month, day);
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  file_type{file_idx} = 'novatelraw';
  params{file_idx} = struct('year',year,'time_reference','utc');
  gps_source{file_idx} = 'novatel-field';
  sync_flag{file_idx} = 0;

  year = 2022; month = 7; day = 31;
  out_fn = fullfile(gps_path,sprintf('gps_%04d%02d%02d.mat', year, month, day));
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'gps',sprintf('%04d%02d%02d',year,month,day)),'LOG','','.TXT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d_raw.mat', year, month, day);
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  file_type{file_idx} = 'novatelraw';
  params{file_idx} = struct('year',year,'time_reference','utc');
  gps_source{file_idx} = 'novatel-field';
  sync_flag{file_idx} = 0;

  year = 2022; month = 8; day = 1;
  out_fn = fullfile(gps_path,sprintf('gps_%04d%02d%02d.mat', year, month, day));
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,'gps',sprintf('%04d%02d%02d',year,month,day)),'LOG','','.TXT');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d_raw.mat', year, month, day);
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  file_type{file_idx} = 'novatelraw';
  params{file_idx} = struct('year',year,'time_reference','utc');
  gps_source{file_idx} = 'novatel-field';
  sync_flag{file_idx} = 0;
    
elseif strcmpi(gps_source_to_use,'postprocess')
  %% POST PROCESSED GPS SOURCE
  % =======================================================================
  
  fn_start='ie_';

  year = 2022; month = 7; day = 27;
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('ie_%04d%02d%02d',year,month,day)),fn_start,'','*.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final_20221005';
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  sync_flag{file_idx} = 0;
  
  year = 2022; month = 7; day = 28;
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('ie_%04d%02d%02d',year,month,day)),fn_start,'','*.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final_20221005';
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  sync_flag{file_idx} = 0;
  
  year = 2022; month = 7; day = 29;
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('ie_%04d%02d%02d',year,month,day)),fn_start,'','*.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final_20221005';
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  sync_flag{file_idx} = 0;  
  
  year = 2022; month = 7; day = 30;
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('ie_%04d%02d%02d',year,month,day)),fn_start,'','*.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final_20221005';
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  sync_flag{file_idx} = 0;
  
  year = 2022; month = 7; day = 31;
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('ie_%04d%02d%02d',year,month,day)),fn_start,'','*.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final_20221005';
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  sync_flag{file_idx} = 0;
  
  year = 2022; month = 8; day = 01;
  file_idx = file_idx + 1;
  in_fns{file_idx} = get_filenames(fullfile(in_base_path,sprintf('ie_%04d%02d%02d',year,month,day)),fn_start,'','*.txt');
  out_fns{file_idx} = sprintf('gps_%04d%02d%02d.mat', year, month, day);
  file_type{file_idx} = 'General_ASCII';
  params{file_idx} = struct('time_reference','gps','headerlines',16,'format_str','%s%s%f%f%f%f%f%f%f%f%f');
  params{file_idx}.types = {'date_MDY','time_HMS','lat_deg','lon_deg','elev_m','roll_deg','pitch_deg','heading_deg','tmp_1','tmp_2','tmp_3'};
  params{file_idx}.textscan = {};
  gps_source{file_idx} = 'cresis-final_20221005';
  date_str{file_idx} = sprintf('%04d%02d%02d',year,month,day);
  sync_flag{file_idx} = 0;

end

%% gps_create
% Read and translate files according to user settings
% =========================================================================
gps_create;

%% custom fixes
% =========================================================================
for idx = 1:length(file_type)
  out_fn = fullfile(gps_path,out_fns{idx});
  
  load(out_fn,'gps_source');

  if ~isempty(regexpi(out_fn,'201910XX'))
    % Fake GPS for testing
    warning('Faking GPS data: %s', out_fn);
    gps = load(out_fn);
    
    velocity = 4;
    gps.lat = -75.5 - (gps.gps_time-gps.gps_time(1))*velocity/111111;
    gps.lon(:) = -106.75;
    gps.elev(:) = 500;
    gps.heading(:) = -pi;
    
    save(out_fn,'-append','-struct','gps','gps_time','lat','lon','elev','roll','pitch','heading');
  end
  
end
