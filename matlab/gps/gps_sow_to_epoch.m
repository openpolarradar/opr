function time = gps_sow_to_epoch(sow,cur_gps_time)
% time = gps_sow_to_epoch(sow,cur_gps_time)
%
% Converts from GPS seconds of the week to seconds since the 
% Jan 1, 1970 epoch. The assumption is that sow DO NOT WRAP
% AROUND at 604800 seconds (the number of seconds in a week).
%
% Note that if the receiver provides raw GPS weeks, it will be mod 1024.
% Most GPS receivers correct this (i.e. it is not raw) and provide an
% absolute GPS weeks field with the gps_week_1024_cycles = 0.
%
% Inputs:
%
% sow: GPS seconds of the week
% 
% cur_gps_time: datenum containing a gps time in the same week OR struct
% that gives absolute reference to find which GPS week we are in. The
% struct fields should be:
%  .year
%  .month
%  .day
%
% Outputs:
%
% time: seconds since Jan 1, 1970 epoch
%
% Example:
%
% % See read_gps_applanix.m for an example
% % See read_gps_novatelraw.m for an example
%
% sow = 322626.5;
% gps_week = 2220;
% gps_week_1024_cycles = 0;
% time = gps_sow_to_epoch(sow,datenum(1980,1,06) + 7*gps_week + 1024*gps_week_1024_cycles);
%
% See also epoch_to_datenum.m, datenum_to_epoch.m, epoch_to_gps_sow.m,
% epoch_to_sod.m, sod_to_epoch.m, gps_sow_to_epoch.m, utc_leap_seconds.m,
% gps_to_utc.m, utc_to_gps.m

% GPS weeks started counting Jan 6, 1980
start_gps_time = datenum(1980,1,06);

% Current time is:
if isstruct(cur_gps_time)
  cur_gps_time = datenum(cur_gps_time.year,cur_gps_time.month,cur_gps_time.day);
else
  % cur_gps_time is a datenum already
end

% For debugging, the broadcasted GPS week is mod 1024
if 0
  % Determine the absolute GPS week
  gps_week = floor((cur_gps_time-start_gps_time)/7);

  gps_week_broadcast = mod(gps_week,1024);
end

% Find the beginning of the current GPS week
cur_gps_time_week = cur_gps_time - mod(cur_gps_time-start_gps_time,7);

% Add seconds of week converted to days to the ref_GPS
time = cur_gps_time_week + sow/86400;

% Convert from Matlab datenum to computer epoch
time = datenum_to_epoch(time);
