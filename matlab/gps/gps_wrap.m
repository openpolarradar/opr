function yq = gps_mean(yi,units_str)
% yq = gps_mean(yi,units_str)
%
% Wraps radians (degrees if 'deg') to -pi to +pi (-180 to +180 if 'deg')
%
% The data is assumed to be in radians unless second argument is "deg"
%
% yi: numeric matrix of radians or degrees
%
% units_str: optional. string. default is 'rad'. Other option is 'deg'.
%
% Examples:
% gps.lon = gps_mean(gps.lon,'deg');
% gps.heading = gps_mean(gps.heading);

if ~exist('units_str','var') || isempty(units_str)
  units_str = 'rad';
end

if units_str(1) == 'd'
  % Degrees
  yq = angle(exp(1i*yi/180*pi))*180/pi;
else
  % Radians
  yq = angle(exp(1i*yi));
end
