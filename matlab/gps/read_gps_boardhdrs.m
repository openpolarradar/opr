function gps = read_gps_boardhdrs(fn, param)
% gps = read_gps_boardhdrs(fn, param)
%
% Loads the boardhdrs file and extracts GNSS information.
%
% Example:
%
%   fn = 'C:\metadata\2023_Antarctica_P3chile\20231118\board_hdrs.mat'
%   gps = read_gps_boardhdrs(fn,struct('year',2023,'month',11,'day',18));
%   gps_plot(gps);
%   datestr(epoch_to_datenum(gps.gps_time(1)));
%   gps.utc_time = gps.gps_time - utc_leap_seconds(gps.gps_time(1))
% 
% Author: John Paden
%
% See also read_gps_*.m, gps_plot.m, gps_create.m

if ~exist('param','var')
  param = [];
end

if ~isfield(param,'year') || isempty(param.year)
  error('param.year is required')
end

if ~isfield(param,'month') || isempty(param.month)
  error('param.month is required')
end

if ~isfield(param,'day') || isempty(param.day)
  error('param.day is required')
end

if ~isfield(param,'time_reference') || isempty(param.time_reference)
  param.time_reference = 'utc';
end
if ~strcmpi(param.time_reference,'utc')
  warning('If board_hdrs comes from NMEA: NMEA is normally "utc" time, but param.time_reference = %s', param.time_reference);
end

%% Read in file

% Open file
tmp = load(fn,'board_hdrs');
tmp = tmp.board_hdrs{1};

gps = [];
gps.gps_time = datenum_to_epoch(datenum(param.year,param.month,param.day,0,0,tmp.utc_time_sod));
if strcmpi(param.time_reference,'utc')
  % Convert gps.gps_time from UTC to GPS time
  if ~isempty(gps.gps_time)
    gps.gps_time = gps.gps_time + utc_leap_seconds(gps.gps_time(1));
  end
end
gps.lat = tmp.lat;
gps.lon = tmp.lon;
gps.elev = tmp.elev;
gps.roll = zeros(size(gps.gps_time));
gps.pitch = zeros(size(gps.gps_time));
gps.heading = zeros(size(gps.gps_time));
