function yq = gps_mean(yi,varargin)
% yq = gps_mean(yi,varargin)
%
% Averages heading and longitude properly through 2*pi wraps.
%
% The data is assumed to be in radians!
%
% Examples:
% gps.lon = gps_mean(gps.lon/180*pi)*180/pi;
% gps.heading = gps_mean(gps.heading);

yi_x = cos(yi);
yi_y = sin(yi);
yq_x = mean(yi_x, varargin{:});
yq_y = mean(yi_y, varargin{:});
yq = atan2(yq_y,yq_x);
