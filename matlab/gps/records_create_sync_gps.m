function my_struct = records_create_sync_gps(param,my_struct,radar_time,comp_time)
% my_struct = records_create_sync_gps(param,my_struct,radar_time,comp_time)
%
% param = struct from read_param_xls
%  param.records.file.version
%  param.season_name
%  param.day_seg
%  param.records.gps.fn
%  param.records.gps.time_offset
% my_struct = vector or records struct to add GPS fields to
% radar_time = time recorded to raw data files by radar (contents depend on radar)
%   mcrds,accum2: radar_time is a free running 64 bit counter
%   others: radar_time is utc_time_sod or seconds of day
% comp_time = computer time recorded to raw data files by radar
%   only required for mcrds and accum2
%
% my_struct = Adds lat, lon, elev, roll, pitch, heading, gps_time, gps_source
%  to the my_struct struct
%
% Works for the following radars:
%  mcrds, accum2, mcords, mcords2, mcords3, fmcw, fmcw2, fmcw3, accum
% Important usage note for all radars besides mcrds,accum2:
%  The absolute radar time is determined by taking the date from the
%  segment name (e.g. 20111023), adding in the gps time offset from
%  the vectors worksheet and the utc_time_sod read from the radar data
%  files. Data in places like McMurdo with +12 hours UTC time often
%  require the vectors gps time offset to be -86400 because the UTC time
%  will be one day behind local time (which is what should be used for
%  the segment name).
%
% Called from records_create_sync.m

h_fig = get_figures(2,true);

%% Load the GPS data
gps_fn = ct_filename_support(param,param.records.gps.fn,'gps',true);
fprintf('Loading GPS data: %s (%s)\n', gps_fn, datestr(now));
gps = gps_load(gps_fn);

%% Check for non-monotonically increasing gps time
if any(diff(gps.gps_time) <= 0)
  error('GPS times are not monotonically increasing');
end

%% Check for repeat values
if length(unique(gps.gps_time)) ~= length(gps.gps_time)
  error('GPS file has repeat values in it');
end

%% Check for NaN in any of the fields
if any(isnan(gps.lat)) ...
    || any(isnan(gps.lon)) ...
    || any(isnan(gps.elev)) ...
    || any(isnan(gps.roll)) ...
    || any(isnan(gps.pitch)) ...
    || any(isnan(gps.heading)) ...
    || any(isnan(gps.gps_time))
  error('GPS file has NaN');
end

if any(param.records.file.version == [102 410])
  %% MCRDS and ACCUM2
  % Isolate the section of radar time from gps.radar_time that will be
  % used to interpolate with.
  if strcmpi(param.season_name,'2013_Antarctica_P3') && param.records.file.version == 102 && strcmpi(param.day_seg(1:8),'20131119')
    % no gps sync files, set radar_gps_time = comp_time(1) + radar_time -
    % radar_time(1) + comp_time_offset (-6*3600), the computer time offset
    % from gps time was 6 hours late
    radar_gps_time =  comp_time(1) + radar_time-radar_time(1) -6*3600 + max(param.records.gps.time_offset);
  else
    guard_time = 5;
    good_idxs = find(gps.comp_time >= comp_time(1)-guard_time ...
      & gps.comp_time <= comp_time(end)+guard_time);
    good_radar_time = gps.radar_time(good_idxs);
    good_sync_gps_time = gps.sync_gps_time(good_idxs);
    
    % From these good indexes, remove any repeat radar times (usually caused
    % by there being more than one NMEA string every 1 PPS
    good_idxs = 1+find(diff(good_radar_time) ~= 0);
    good_radar_time = good_radar_time(good_idxs);
    good_sync_gps_time = good_sync_gps_time(good_idxs);
    
    % Interpolate gps.sync_gps_time to radar gps_time using gps.radar_time
    % and radar_time
    radar_gps_time = interp1(good_radar_time, good_sync_gps_time, ...
      radar_time + max(param.records.gps.time_offset),'linear','extrap');
  end
elseif any(param.records.file.version == [405 406])
  %% ACORDS
  comp_time = radar_time;
  guard_time = 0;
  good_idxs = find(gps.comp_time >= comp_time(1)-guard_time ...
    & gps.comp_time <= comp_time(end)+guard_time);
  good_comp_time = gps.comp_time(good_idxs);
  good_sync_gps_time = gps.sync_gps_time(good_idxs);
  radar_gps_time = interp1(good_comp_time, good_sync_gps_time, ...
      comp_time + max(param.records.gps.time_offset),'linear','extrap');
  
elseif any(param.records.file.version == [409])
  %% ICARDS
  
  % there's a minor inacurracy (1e-7)of first
  % when read the csv file. This may cause radar_gps_time start earlier than gps.gps_time(first file)
  % or later than gps.gps_time(last file). This phenomenon will further
  % cause NaN when using interp1 to sync radar and gps!!This problem
  % cannot be solved in other scripts----qishi
  utc_time_sod = radar_time;
  
  % Check for seconds of day roll over and unwrap (assume jump backward
  % of more than 23 hours is a roll over)
  wrap_idxs = find(abs(diff(utc_time_sod) + 86400) < 3600);
  for wrap_idx = wrap_idxs
    utc_time_sod(wrap_idx+1:end) = utc_time_sod(wrap_idx+1:end) + 86400;
  end
  
  % Apply GPS sync correction to radar time
  utc_time_sod = utc_time_sod + max(param.records.gps.time_offset);
  
  % Determine absolute radar time and convert from UTC to GPS
  year = str2double(param.day_seg(1:4));
  month = str2double(param.day_seg(5:6));
  day = str2double(param.day_seg(7:8));
  radar_gps_time = datenum_to_epoch(datenum(year,month,day,0,0,utc_time_sod)) + utc_leap_seconds(gps.gps_time(1));
  if radar_gps_time(1)<gps.gps_time(1)
    radar_gps_time=radar_gps_time(find(radar_gps_time>=gps.gps_time(1)));
  end
  if radar_gps_time(end)>gps.gps_time(end)
    radar_gps_time=radar_gps_time(find(radar_gps_time<=gps.gps_time(end)));
  end
  
elseif any(param.records.file.version == [413 414])
  %% UTUA and BAS RDS based systems encode GPS time into the raw input files
  radar_gps_time = radar_time + max(param.records.gps.time_offset);
  
elseif any(param.records.file.version == [415])  
  %% UTIG RDS based systems
  good_mask = gps.comp_time >= comp_time(1) & gps.comp_time <= comp_time(end);
  radar_gps_time = interp1(gps.radar_time(good_mask), gps.gps_time(good_mask), ...
    radar_time + max(param.records.gps.time_offset),'linear','extrap');
  
elseif any(param.records.file.version == [12 417])
  %% LDEO RDS DICE and Snow SIR based systems
  radar_gps_time = radar_time;
  
elseif any(param.records.file.version == [9 10 103 412])
  %% Arena based systems
  % Interpolate GNSS data to the radar data.
  if strcmpi(param.records.arena.sync_gnss_field,'radar_time')
    % radar_time: use GPS file rel_time to sync with radar_time
    sync_gnss_field = gps.radar_time;
  elseif strcmpi(param.records.arena.sync_gnss_field,'comp_time(1,:)')
    % comp_time(1,:): use GPS file pps_cntr to sync with radar_time
    sync_gnss_field = gps.comp_time(1,:);
  elseif strcmpi(param.records.arena.sync_gnss_field,'comp_time(2,:)')
    % comp_time(2,:): use GPS file profile_cntr to sync with radar_time
    sync_gnss_field = gps.comp_time(2,:);
  elseif strcmpi(param.records.arena.sync_gnss_field,'sync_gps_time')
    % sync_gps_time: use GPS file NMEA-string time to sync with radar_time
    sync_gnss_field = gps.sync_gps_time;
  end

  % Note: radar_time is relative on older Arena systems
  % radar_time is UTC time from NMEA GPRMC message on newer Arena systems
  if ~isempty(param.records.arena.sync_mask_fn)
    sync_interp_mask = false(size(sync_gnss_field));
    for fn_idx = 1:length(gps.param_gps.sync_fns)
      if ~isempty(regexpi(gps.param_gps.sync_fns{fn_idx},param.records.arena.sync_mask_fn))
        sync_interp_mask(gps.sync_file_idx == fn_idx) = true;
      end
    end
    if all(~sync_interp_mask)
      error('param.records.arena.sync_mask_fn specified %s, but no sync entries match.', param.records.arena.sync_mask_fn);
    end
    radar_gps_time = interp1(sync_gnss_field(sync_interp_mask), gps.sync_gps_time(sync_interp_mask), ...
      radar_time + max(param.records.gps.time_offset),'linear','extrap');
  elseif param.records.arena.sync_mask_en
    % The sync mask is required when the sync_gnss_field is not unique (contains
    % values that repeat and is not monotonically increasing)
    sync_interp_mask = gps.radar_time >= my_struct.raw.rel_time_cntr_latch(1)/param.records.file.clk-10 ...
      & gps.radar_time <= my_struct.raw.rel_time_cntr_latch(end)/param.records.file.clk+10;
    radar_gps_time = interp1(sync_gnss_field(sync_interp_mask), gps.sync_gps_time(sync_interp_mask), ...
      radar_time + max(param.records.gps.time_offset),'linear','extrap');
  else
    radar_gps_time = interp1(sync_gnss_field, gps.sync_gps_time, ...
      radar_time + max(param.records.gps.time_offset),'linear','extrap');
  end

  % For each point in radar_time, find the closest time in sync_gnss_field.
  % This is done to ensure that the sync data overlaps the sync_gnss_field.
  closest_time = zeros(size(radar_time));
  % Add in the param.records.gps.time_offset
  radar_time_offsetted = radar_time + max(param.records.gps.time_offset);
  radar_time_offsetted_idx = 1;
  % Find the closest time for radar_time(1)
  [closest_time(radar_time_offsetted_idx),closest_idx] = min(abs(sync_gnss_field - radar_time_offsetted(radar_time_offsetted_idx)));
  if sync_gnss_field(closest_idx) <= radar_time_offsetted(radar_time_offsetted_idx)
    gps_idx_after_radar_time_offsetted_idx = min(closest_idx+1,length(sync_gnss_field));
  else
    gps_idx_after_radar_time_offsetted_idx = closest_idx;
  end
  % Find the closest time for radar_time(2:end), this algorithm uses the
  % fact that each vector should be sorted to reduce the computation time
  for radar_time_offsetted_idx = 2:length(radar_time)
    while gps_idx_after_radar_time_offsetted_idx <= length(sync_gnss_field) ...
      && sync_gnss_field(gps_idx_after_radar_time_offsetted_idx) < radar_time_offsetted(radar_time_offsetted_idx)
      gps_idx_after_radar_time_offsetted_idx = gps_idx_after_radar_time_offsetted_idx + 1;
    end
    closest_time(radar_time_offsetted_idx) = min(abs(radar_time_offsetted(radar_time_offsetted_idx) ...
      - sync_gnss_field([max(1,gps_idx_after_radar_time_offsetted_idx-1) min(gps_idx_after_radar_time_offsetted_idx,end)])));
  end

  % Create debug plots of GNSS synchronization fields

  % Plot clocks versus GNSS time
  figure(h_fig(1)); clf;
  h_axes = axes('parent',h_fig(1));
  h_plot= plot(h_axes(1),gps.sync_gps_time, gps.radar_time - gps.sync_gps_time,'b.');
  hold(h_axes(1),'on');
  grid(h_axes(1),'on');
  title(h_axes(1),sprintf('%s', [param.day_seg(1:8) '\' param.day_seg(9:end)]));
  h_plot(end+1) = plot(h_axes(1),gps.sync_gps_time, gps.comp_time(1,:) - gps.sync_gps_time,'r.');
  if param.records.arena.sync_mask_en
    h_plot(end+1)= plot(h_axes(1),gps.sync_gps_time(sync_interp_mask), gps.radar_time(sync_interp_mask) - gps.sync_gps_time(sync_interp_mask),'bx');
    h_plot(end+1) = plot(h_axes(1),gps.sync_gps_time(sync_interp_mask), gps.comp_time(1,sync_interp_mask) - gps.sync_gps_time(sync_interp_mask),'rx');
  end
  legend(h_axes(1),{'rel\_time','pps\_cntr'},'location','best');
  xlabel(h_axes(1), 'gps.sync\_gps\_time');
  ylabel(h_axes(1), 'Sync field time offset from GNSS time (sec)');

  % Save debug figures
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_gps_sync_gnss') '.jpg'];
  fprintf('Saving %s\n', fig_fn);
  fig_fn_dir = fileparts(fig_fn);
  if ~exist(fig_fn_dir,'dir')
    mkdir(fig_fn_dir);
  end
  ct_saveas(h_fig(1),fig_fn);
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_gps_sync_gnss') '.fig'];
  fprintf('Saving %s\n', fig_fn);
  ct_saveas(h_fig(1),fig_fn);

  if size(gps.comp_time,1) > 1
    % Plot clocks versus profile_cntr if it is available (gps.comp_time(2,:) is
    % profile_cntr from the Arena NMEA log files)
    figure(h_fig(2)); clf;
    h_axes(2) = axes('parent',h_fig(2));
    h_plot= plot(h_axes(2),gps.comp_time(2,:), gps.sync_gps_time, 'k.');
    hold(h_axes(2),'on');
    grid(h_axes(2),'on');
    title(h_axes(2),sprintf('%s', [param.day_seg(1:8) '\' param.day_seg(9:end)]));
    h_plot(end+1)= plot(h_axes(2),gps.comp_time(2,:), gps.radar_time,'b.');
    h_plot(end+1) = plot(h_axes(2),gps.comp_time(2,:), gps.comp_time(1,:),'r.');
    if param.records.arena.sync_mask_en
      h_plot(end+1)= plot(h_axes(2),gps.comp_time(2,sync_interp_mask), gps.sync_gps_time(sync_interp_mask), 'kx');
      h_plot(end+1)= plot(h_axes(2),gps.comp_time(2,sync_interp_mask), gps.radar_time(sync_interp_mask),'bx');
      h_plot(end+1) = plot(h_axes(2),gps.comp_time(2,sync_interp_mask), gps.comp_time(1,sync_interp_mask),'rx');
    end
    legend(h_axes(2),{'gps.sync\_gps\_time','rel\_time','pps\_cntr'},'location','best');
    xlabel(h_axes(2), 'profile\_cntr');
    ylabel(h_axes(2), 'GNSS sync field (sec)');

    % Save debug figures
    fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_gps_sync_profile') '.jpg'];
    fprintf('Saving %s\n', fig_fn);
    fig_fn_dir = fileparts(fig_fn);
    if ~exist(fig_fn_dir,'dir')
      mkdir(fig_fn_dir);
    end
    ct_saveas(h_fig(2),fig_fn);
    fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_gps_sync_profile') '.fig'];
    fprintf('Saving %s\n', fig_fn);
    ct_saveas(h_fig(2),fig_fn);
  end

  % Check if any of the radar_time points are too far from a sync_gnss_field
  % time stamp
  warning(['%s: %d records have sync times that are more than param.records.arena.sync_gap_threshold=%.0f sec ' ...
    'away from a valid record. The maximum gap found is %.1f sec. ' ...
    'If many records exceed, see gps_closest_time plot; there may be something wrong with ' ...
    'the raw data file radar_time or with the gps file''s sync_gnss_field. Investigate and ' ...
    'fix the issue and rerun records_create.'], param.day_seg, ...
    sum(abs(closest_time)>param.records.arena.sync_gap_threshold), param.records.arena.sync_gap_threshold, ...
    max(abs(closest_time)));

  clf(h_fig(1));
  h_axes = axes('parent',h_fig(1));
  h_plot = plot(h_axes, closest_time, '.-');
  grid(h_axes,'on');
  title(h_axes,sprintf('%s:\nValues should be small.', [param.day_seg(1:8) '\' param.day_seg(9:end)]));
  ylabel(h_axes,'Closest time to a valid sync\_gnss\_field time (sec)');
  xlabel(h_axes,'Record');
  hold(h_axes,'on');

  % Save debug figures
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','gps_closest_time') '.jpg'];
  fprintf('Saving %s\n', fig_fn);
  fig_fn_dir = fileparts(fig_fn);
  if ~exist(fig_fn_dir,'dir')
    mkdir(fig_fn_dir);
  end
  ct_saveas(h_fig(1),fig_fn);
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','gps_closest_time') '.fig'];
  fprintf('Saving %s\n', fig_fn);
  ct_saveas(h_fig(1),fig_fn);

else
  %% NI based, Ledford systems, CECS RDS (418), Leuschen (420,421,422)
  utc_time_sod = radar_time;
  
  % Check for seconds of day roll over and unwrap (assume jump backward
  % of more than 23 hours is a roll over)
  wrap_idxs = find(abs(diff(utc_time_sod) + 86400) < 3600);
  for wrap_idx = wrap_idxs
    utc_time_sod(wrap_idx+1:end) = utc_time_sod(wrap_idx+1:end) + 86400;
  end
  
  % Apply GPS sync correction to radar time
  utc_time_sod = utc_time_sod + max(param.records.gps.time_offset);
  
  % Determine absolute radar time and convert from UTC to GPS
  year = str2double(param.day_seg(1:4));
  month = str2double(param.day_seg(5:6));
  day = str2double(param.day_seg(7:8));
  radar_gps_time = datenum_to_epoch(datenum(year,month,day,0,0,utc_time_sod)) + utc_leap_seconds(gps.gps_time(1));
end

%% Synchronize times to get positions and absolute time
% NOTE: Extrapolation outside the time frame that the GPS data were
% collected is not allowed as it results in bad data, so the linear
% interpolation is set to NaN for extrapolation. Rather than rely on
% extrapolation, modify the make_gps_SEASON_NAME.m function to extend the
% GPS records as needed.
my_struct.lat = double(interp1(gps.gps_time,gps.lat,radar_gps_time,'spline',NaN));
my_struct.lon = double(gps_interp1(gps.gps_time,gps.lon/180*pi,radar_gps_time,'spline',NaN))*180/pi;
my_struct.elev = double(interp1(gps.gps_time,gps.elev,radar_gps_time,'spline',NaN));
my_struct.roll = double(interp1(gps.gps_time,gps.roll,radar_gps_time,'spline',NaN));
my_struct.pitch = double(interp1(gps.gps_time,gps.pitch,radar_gps_time,'spline',NaN));
my_struct.heading = double(gps_interp1(gps.gps_time,gps.heading,radar_gps_time,'spline',NaN));
my_struct.gps_time = radar_gps_time;
my_struct.gps_source = gps.gps_source;

%% Check for bad synchronization (NaN in synced GPS data)
nan_detected = false;
if any(isnan(my_struct.lat))
  fprintf('There are NaN in synced GPS lat.\n');
  nan_detected = true;
end
if any(isnan(my_struct.lon))
  fprintf('There are NaN in synced GPS lon.\n');
  nan_detected = true;
end
if any(isnan(my_struct.elev))
  fprintf('There are NaN in synced GPS elev.\n');
  nan_detected = true;
end
if any(isnan(my_struct.roll))
  fprintf('There are NaN in synced GPS roll.\n');
  nan_detected = true;
end
if any(isnan(my_struct.pitch))
  fprintf('There are NaN in synced GPS pitch.\n');
  nan_detected = true;
end
if any(isnan(my_struct.heading))
  fprintf('There are NaN in synced GPS heading.\n');
  nan_detected = true;
end
if any(isnan(my_struct.gps_time))
  fprintf('There are NaN in synced GPS time.\n');
  nan_detected = true;
end
if nan_detected
  warning('NaN found in GPS data for segment %s. Inspect gps.* and my_struct.* variables and fix my_struct GPS/Attitude fields (gps_time, lat, lon, elev, roll, pitch, heading) since NaN are not allowed in these fields. Usually the problem is with a max(param.records.gps.time_offset) problem or the GPS file not covering the time that the radar data were collected. In this case, fix the gps file or time_offset and rerun.', param.day_seg);
  if any(param.records.file.version == [102 405 406 410])
    % Accum2, ACORDS, MCRDS
    fprintf('GPS COMP TIME: %s to %s\n', datestr(epoch_to_datenum(gps.comp_time(1))), datestr(epoch_to_datenum(gps.comp_time(end))));
    fprintf('RADAR COMP TIME: %s to %s\n', datestr(epoch_to_datenum(comp_time(1))), datestr(epoch_to_datenum(comp_time(end))));
  else
    fprintf('GPS TIME: %s to %s\n', datestr(epoch_to_datenum(gps.gps_time(1))), datestr(epoch_to_datenum(gps.gps_time(end))));
    fprintf('RADAR GPS TIME: %s to %s\n', datestr(epoch_to_datenum(my_struct.gps_time(1))), datestr(epoch_to_datenum(my_struct.gps_time(end))));
  end
  keyboard
end

nonmonotonic_records = diff(my_struct.gps_time) <= 0;
clf(h_fig(1));
h_axes = axes('parent',h_fig(1));
h_plot = plot(h_axes, my_struct.gps_time);
grid(h_axes,'on');
title(h_axes,sprintf('%s:\nValues should monotonically increase at a constant rate.', [param.day_seg(1:8) '\' param.day_seg(9:end)]));
ylabel(h_axes,'my\_struct.gps\_time (sec)');
xlabel(h_axes,'Record');
hold(h_axes,'on');

% Save debug figures
fig_fn = [ct_filename_ct_tmp(param,'','records_create','gps_nonmonotonic') '.jpg'];
fprintf('Saving %s\n', fig_fn);
fig_fn_dir = fileparts(fig_fn);
if ~exist(fig_fn_dir,'dir')
  mkdir(fig_fn_dir);
end
ct_saveas(h_fig(1),fig_fn);
fig_fn = [ct_filename_ct_tmp(param,'','records_create','gps_nonmonotonic') '.fig'];
fprintf('Saving %s\n', fig_fn);
ct_saveas(h_fig(1),fig_fn);

clf(h_fig(2));
h_axes = axes('parent',h_fig(2));
h_plot = plot(h_axes, diff(my_struct.gps_time));
grid(h_axes,'on');
title(h_axes,sprintf('%s:\nValues should be positive and constant.', [param.day_seg(1:8) '\' param.day_seg(9:end)]));
ylabel(h_axes,'diff(my\_struct.gps\_time) (sec)');
xlabel(h_axes,'Record');
hold(h_axes,'on');

% Save debug figures
fig_fn = [ct_filename_ct_tmp(param,'','records_create','gps_nonmonotonic_dtime') '.jpg'];
fprintf('Saving %s\n', fig_fn);
fig_fn_dir = fileparts(fig_fn);
if ~exist(fig_fn_dir,'dir')
  mkdir(fig_fn_dir);
end
ct_saveas(h_fig(2),fig_fn);
fig_fn = [ct_filename_ct_tmp(param,'','records_create','gps_nonmonotonic_dtime') '.fig'];
fprintf('Saving %s\n', fig_fn);
ct_saveas(h_fig(2),fig_fn);

if any(nonmonotonic_records)
  if any(diff(my_struct.gps_time) < 0)
    warning('time not monotonically increasing: First non-monotonic record %d of %d total, %d total records.', ...
      find(nonmonotonic_records,1), sum(nonmonotonic_records), length(my_struct.gps_time));
    %keyboard
  else
    warning('time not monotonically increasing, but is monotonically non-decreasing: First non-monotonic record %d of %d total, %d total records.', ...
      find(nonmonotonic_records,1), sum(nonmonotonic_records), length(my_struct.gps_time));
  end
end
