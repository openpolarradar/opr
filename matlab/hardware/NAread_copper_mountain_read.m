function query_response = NAread_copper_mountain_read(vna_obj)
% query_response = NAread_copper_mountain_read(vna_obj)
%
%  Support function for NAread_copper_mountain.m
%
% Author: John Paden
%
% See also: NAread_copper_mountain, NAread_copper_mountain_read,
% NAread_copper_mountain_write, SXPParse, SXPWrite

% 10 is newline
nl = 10;

% read from VNA through TCP connection
% blocks until data read is not empty
% reads until new line character is received
query_response = '';
while true
  partial_query_response = read(vna_obj);
  if(isempty(partial_query_response)~=1)
    last_index = length(partial_query_response);
    query_response = strcat(query_response, partial_query_response);
    if (partial_query_response(last_index) == nl)
      break;
    end
  end
end
