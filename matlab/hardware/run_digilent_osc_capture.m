% Waveform capture for Digilent Discovery 3, USB occiliscope.
% 
% COMPUTER AND OCCILISCOPE SETUP
% =========================================================================
% -install Digilent waveforms on the computer running matlab
% -install the "Digilent Toolbox" from the matlab add-on menu
% -plug the oscilloscope into the computer using a USB-C cable
% -run the below program
% =========================================================================
% Note: at the time of writing, the download page for diligent adept, a 
% dependency of Waveforms on Linux is broken, so this program currently 
% only works on windows.
% 
% For additional documentation see: 
% "https://www.mathworks.com/matlabcentral/fileexchange/122817-digilent-toolbox"

% setup the paramiters for the capture
param.sampling_rate = 100e6;
param.channel_limits = [-2.5 2.5; -2.5 2.5];
sample_length = 1e-4;

dq = digilent_osc_setup(param); % open the osc object
data = digilent_osc_read(dq, sample_length); % collect data, store in "data"

data.time % the time vector for the data
data.ch1 % vector representing ch1 data
data.ch2 % vector representing ch2 data
data.start_time % datetime object representing the collection start time

% plotting and saving example:
clf;
figure(1)
plot(data.time, data.ch1)
hold on;
plot(data.time, data.ch2)

notes = "Notes about data";
save("name.mat", "data", "notes");



