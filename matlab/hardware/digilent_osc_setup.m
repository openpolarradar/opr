% Opens occiliscope object for waveform capture
% See "run_digilent_osc_capture.m" for setup information

function [dq_obj] = digilent_osc_setup(param)
  
  samp_rate = param.sampling_rate;
  ch_lims = param.channel_limits;
  
  daqreset;
  daqlist("digilent");
  dq_obj = daq("digilent");
  dq_obj.Rate = samp_rate;
  
  ch1 = addinput(dq_obj, "AD3_0", "ai0", "Voltage");
  ch2 = addinput(dq_obj, "AD3_0", "ai1", "Voltage");
  
  ch1.Range = ch_lims(1,:);
  ch2.Range = ch_lims(2,:);

end