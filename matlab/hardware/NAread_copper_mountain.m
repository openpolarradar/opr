function [hdr, data] = NAread_copper_mountain(ports,param)
% [hdr, data] = NAread_copper_mountain(ports,param)
%
% Records network analyzer (NA) measurements from Copper Mountain
% Technologies 4-port VNA.
%
% Note that server must be turned on in the S4VNA software though the menu
% System->Misc Setup->Network Remote Control Settings. This Matlab function
% connects to this S4VNA server running on the computer that is connected
% to the network analyzer (the Matlab function does not directly connect to
% the network analyzer).
%
% Help information on network analyzer TCP/IP interface:
% * https://coppermountaintech.com/help-s4/
% * https://coppermountaintech.com/webinar-how-to-automate-measurements-with-a-vna/
%
% INPUTS
% ports: list of ports to measure (a vector of unique numbers containing 1,
% 2, 3, and/or 4). E.g. [1 3] would use ports 1 and 3.
%
% param: structure controlling how the measurement is taken
%
%  .vna: structure of fields to set in the VNA object
%
%    .Timeout (default is 60 seconds)
%
%  .ip_addr: string containing IP Address or hostname of the computer that
%  is connected to the network analyzer and running the copper mountain
%  software.
%
%
%  .num_ave: Number of hardware averages. Default is 1. These averages are
%  done by the network analyzer itself.
%
%  .num_meas: Number of separate measurements to record. Default is 1. This
%  controls the dimension of data. More useful than num_ave since each
%  measurement is recorded so that standard deviations can be taken to
%  estimate the variance in the measurement.
%
%  .plot_en: Plot all S-parameters that are measured. Default is true.
%
%  .fn: String containing filename to store data in. A date-time string is
%  automatically appended to the filename. If field is left empty or
%  undefined, then the data are not stored.
%
% OUTPUTS
% hdr: struct containing fields about NA measurement
% data: double array of size (N,N,num_points,param.num_meas). For example:
%   data(2,1,:,1) represents first S21 measurement taken
%
% Examples:
%  [hdr, data] = NAread_copper_mountain([2 3],struct('ip_addr','129.237.165.26'));
%
% Author: John Paden
%
% See also: NAread_copper_mountain, NAread_copper_mountain_read,
% NAread_copper_mountain_write, SXPParse, SXPWrite

if 1
  % Hard code inputs for debugging
  ports = [2 3];
  param = struct('ip_addr','129.237.165.26');
  param.debug_level = 0;
end

%% Check user inputs

% param.debug_level: 0 runs normally, 1 runs in debug mode and only outputs
% the commands that will be sent to the network analyzer server. Default is
% 0.
if ~isfield(param,'debug_level')
  param.debug_level = 0;
end

if ~isfield(param,'ip_addr') || isempty(param.ip_addr)
  error('param.ip_addr must be set as a string such as ''129.237.165.26''');
end

if ~isfield(param,'num_ave')
  param.num_ave = 1; % Default No. of Averages = 1
end

if ~isfield(param,'num_meas')
  param.num_meas = 1; % Default No. of Measurements = 1
end

if ~isfield(param,'plot_en')
  param.plot_en = true;
end

if ~isfield(param,'vna')
  param.vna = struct();
end

if ~isfield(param.vna,'Timeout')
  param.vna.Timeout = 60;
end

%% Decode ports mask
ports = unique(ports);
if isempty(ports) || any(ports < 1 | ports > 4)
  error('ports must contain numbers from 1 to 4');
end
ports_str = [sprintf('%d',ports(1)) sprintf(',%d',ports(2:end))];
num_ports = length(ports);

%% Connect to Vector Network Analyzer (vna)
if param.debug_level == 0
  try
    global vna;
    clear('vna');
    global vna;
    vna = tcpclient("129.237.165.26", 5025, "Timeout", param.vna.Timeout, "ConnectTimeout", 5);
  catch ME
    disp('Error establishing TCP connection.');
    disp('Check that the TCP server is on.');
    return
  end
  param.vna = vna;
end

%% Query VNA
try
  hdr = [];
  hdr.ports = ports;
  hdr.param = param;
  hdr.datestr = datestr(now);

  % nl: new line character
  nl = 10;

  %% Query VNA settings
  NAread_copper_mountain_write(param, [uint8('*IDN?'),nl]);
  if param.debug_level == 0
    hdr.idn = char(NAread_copper_mountain_read(vna));
    hdr.idn = hdr.idn(1:end-1);
  end

  NAread_copper_mountain_write(param,[uint8('SENS:BAND:RES?'),nl]); % Resolution
  if param.debug_level == 0
    hdr.resolution = str2double(char(NAread_copper_mountain_read(vna)));
  end

  % Command not supported
  hdr.track = NaN;

  % Power (4-ports)
  hdr.power_dBm = NaN*zeros(1,4);
  for port = ports
    NAread_copper_mountain_write(param,[uint8(sprintf('SOURCE1:POWER:PORT%d:LEVEL?', port)),nl]);
    if param.debug_level == 0
      hdr.power_dBm(port) = str2double(char(NAread_copper_mountain_read(vna)));
    else
    end
  end

  NAread_copper_mountain_write(param,[uint8('SENS:FREQ:STAR?'),nl]); % READ Start Frequency
  if param.debug_level == 0
    hdr.start_freq_Hz = str2double(char(NAread_copper_mountain_read(vna)));
  else
  end

  NAread_copper_mountain_write(param,[uint8('SENS:FREQ:STOP?'),nl]); % READ Stop Frequency
  if param.debug_level == 0
    hdr.stop_freq_Hz = str2double(char(NAread_copper_mountain_read(vna)));
  else
  end

  NAread_copper_mountain_write(param,[uint8('SENS:SWE:POIN?'),nl]); %READ No. of Points
  if param.debug_level == 0
    hdr.num_points = str2double(char(NAread_copper_mountain_read(vna)));
  else
    hdr.num_points = 201;
  end

  % Command not supported
  hdr.sweep_time_sec = NaN;

  %IF Frequency
  NAread_copper_mountain_write(param,[uint8('SENSe:BWID?'),nl]);
  if param.debug_level == 0
    hdr.IF_bandwidth_Hz = str2double(char(NAread_copper_mountain_read(vna)));
  else
  end

  % Temperature
  NAread_copper_mountain_write(param,[uint8('SYST:TEMP:SENS?'),nl]); %FAHR or CELS
  if param.debug_level == 0
    hdr.temperature_celcius = str2double(char(NAread_copper_mountain_read(vna)));
  else
  end

  %% Set Parameters to Measure

  %% AVERAGES Section

  NAread_copper_mountain_write(param,[uint8('SENS:AVER ON'),nl]); %OFF or ON
  NAread_copper_mountain_write(param,[uint8(sprintf('SENS:AVER:COUN %d',param.num_ave)),nl]);% Sets the number of measurements to combine for an average.

  %% TRIG SECTION

  %TRIG MODEL 1 SOURCE
  % Set VNA trigger to BUS trigger
  NAread_copper_mountain_write(param, [uint8('TRIG:SOUR BUS'), nl]);

  % Set number of traces
  NAread_copper_mountain_write(param,[uint8(sprintf('CALC:PAR:COUN %d',num_ports^2)),nl]);

  for in_idx = 1:length(ports)
    for out_idx = 1:length(ports)
      out_port = ports(out_idx);
      in_port = ports(in_idx);
      data_idx = (in_idx-1)*num_ports+(out_idx-1) + 1;
      NAread_copper_mountain_write(param,[uint8(sprintf('DISP:WIND1:TRACe%d:STATe on',data_idx)),nl]);
      NAread_copper_mountain_write(param,[uint8(sprintf('CALC:PAR%d:DEF S%d%d',data_idx,out_port,in_port)),nl]);
    end
  end

  % Abort the current sweep
  NAread_copper_mountain_write(param,[uint8('ABORT'),nl]);

  % Set format to dB/angle in SNP file
  NAread_copper_mountain_write(param,[uint8('MMEMory:STORe:SNP:FORMat DB'),nl]);

  % Trigger all channels
  NAread_copper_mountain_write(param,[uint8('TRIG:SCOP ALL'),nl]);%Specifies whether a trigger signal is sent to all channels or only the current channel

  data = zeros(num_ports,num_ports,hdr.num_points,param.num_meas);
  for meas_idx = 1:param.num_meas
    S_param = zeros(num_ports,num_ports,hdr.num_points);

    % Trigger the VNA once
    NAread_copper_mountain_write(param, [uint8('TRIG:SEQUENCE:AVERAGE ON'), nl]);
    NAread_copper_mountain_write(param, [uint8('TRIG:SING'), nl]);
    NAread_copper_mountain_write(param, [uint8('*OPC?'), nl]); % use *OPC? command to wait for the sweep to finish
    if param.debug_level == 0
      opc_response = NAread_copper_mountain_read(vna);
    end

    % Read frequency data into array
    NAread_copper_mountain_write(param, [uint8('SENSe1:FREQuency:DATA?'), nl]);
    if param.debug_level == 0
      freq = NAread_copper_mountain_read(vna);
      NAread_copper_mountain_write(param, [uint8('*OPC?'), nl]); % use *OPC? command to wait for the sweep to finish
      opc_response = NAread_copper_mountain_read(vna);
      freq = char(freq);
      freq = str2num(freq);
      % disp(freq);
    else
      freq = linspace(500e6,1000e6,hdr.num_points);
    end
    hdr.freq = freq;

    for in_idx = 1:length(ports)
      for out_idx = 1:length(ports)
        out_port = ports(out_idx);
        in_port = ports(in_idx);
        data_idx = (in_idx-1)*num_ports+(out_idx-1) + 1;

        NAread_copper_mountain_write(param, [uint8(sprintf('CALCulate1:TRACe%d:DATA:SDATa?',data_idx)), nl]);
        if param.debug_level == 0
          data_single = NAread_copper_mountain_read(vna);
          data_single = char(data_single);
          data_single = str2num(data_single);
          data_single = data_single(1:2:end) + 1j*data_single(2:2:end);
          S_param(out_idx,in_idx,:) = data_single;
        end

        if param.plot_en
          h_fig = figure(data_idx); clf(h_fig);
          set(h_fig,'WindowStyle','docked');
          plot(hdr.freq/1e6, 20*log10(abs(squeeze(S_param(out_idx,in_idx,:)))));
          xlabel('Frequency in MHz');
          ylabel(sprintf('S_%d_%d',out_port,in_port));
          grid on;
          drawnow;
        end
      end
    end

    data(:,:,:,meas_idx) = S_param;
  end

  % return to internal trigger
  NAread_copper_mountain_write(param, [uint8('TRIG:SOUR INT'), nl]);

  %% VNA Disconnect
  clear('vna');

catch ME
  ME
  keyboard
  clear('vna');
  rethrow(ME);
end

if isfield(param,'fn')
  fn = param.fn;
  [fn_dir,fn_name] = fileparts(fn);
  if ~exist(fn_dir,'dir')
    mkdir(fn_dir);
  end
  fn_name = [fn_name sprintf('_%s.mat',datestr(now,'YYYYmmDD_HHMMSS'))];
  fn = fullfile(fn_dir,fn_name);
  save(fn,'hdr','data');
end
