function NAread_copper_mountain_write(param, data)
% NAread_copper_mountain_write(param, data)
%
%  Support function for NAread_copper_mountain.m
%
% Author: John Paden
%
% See also: NAread_copper_mountain, NAread_copper_mountain_read,
% NAread_copper_mountain_write, SXPParse, SXPWrite

if param.debug_level == 0
  write(param.vna, data);
else
  fprintf(char(data));
end
