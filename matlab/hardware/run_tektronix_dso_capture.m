% run_tektronix_dso_capture
%
% Example of running tektronix_dso_capture

param = [];
param.num_ave = 1;
param.num_pnts = 10000000;
param.fs = 5.0e9;
param.trigger = 'CH3';
% param.trigger = 'AUX';
% param.channels = {'CH1','CH2','CH3','CH4'};
param.channels = {'CH1','CH3'};
% 500 MHz, 2.5 GSPS DSO (see Utility->IO->USB Computer->USBTMC for address)
% param.visa_address = 'USB0::0x0699::0x0401::C000466::0::INSTR';
param.visa_address = 'TCPIP::172.16.0.185::INSTR';
% param.visa_address = 'TCPIP::172.16.0.186::INSTR';
% 1 GHz, 5 GSPS DSO (see Utility->IO->USB Computer->USBTMC for address)
% param.visa_address = 'USB0::0x0699::0x0401::C021536::0::INSTR';
param.time_out_sec = 60;
[instrument,time,data] = tektronix_dso_capture(param);

clf;
plot(time,data)
legend(param.channels);

notes = 'Some notes about the measurement';

save('Chan1_AWGpreAmpout.mat','instrument','time','data','notes')

if 0
  Chan1_AWGpreAmpout = load('Chan1_AWGpreAmpout.mat');
  clf;
  plot(Chan1_AWGpreAmpout.time*1e6,Chan1_AWGpreAmpout.data(:,1),'k')
  ylabel('Volt (V)')
  xlabel('Time (us)')
  grid on
end
