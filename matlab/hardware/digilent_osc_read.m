% Collects occiliscope data given
% See "run_digilent_osc_capture.m" for setup information

function [data] = digilent_osc_read(dq, len)
  [data_raw, startime] = read(dq, seconds(len));

  data.time = seconds(data_raw.Time);
  data.ch1 = data_raw.AD3_0_ai0;
  data.ch2 = data_raw.AD3_0_ai1;
  data.start_time = startime;
end