% function collate_mean(param,param_override)
%
% Collects statistics results from run_analysis.m specified in analysis sheet
% Requires param.analysis.cmd{1} to be analysis_mean
% Generates plots to verify mean power levels for specified/all wf-adc pairs
% Magnitude (dBm/Hz) vs Range_line for individual plots for wf-adc pair
% Magnitude (dBm/Hz) vs adc for combined plot for all waveforms
%
% Example:
%   See run_collate_mean for how to run.
%
% Authors: John Paden, Hara Madhav Talasila

%% General Setup
% =========================================================================

param = merge_structs(param, param_override);
physical_constants;

fprintf('=============================================================\n');
fprintf('%s: %s (%s)\n', param.season_name, param.day_seg, datestr(now));
fprintf('=============================================================\n');

%% Input Checks
% =========================================================================
if ~isfield(param.analysis, 'plot_individual')
  param.analysis.plot_individual = 1;
end

if ~isfield(param.analysis, 'enable_visible_plot_individual')
  param.analysis.enable_visible_plot_individual = 0;
end

if ~isfield(param.analysis, 'plot_combined')
  param.analysis.plot_combined = 0;
end

if ~isfield(param.analysis, 'enable_visible_plot_combined')
  param.analysis.enable_visible_plot_combined = 0;
end

if isfield(param.analysis.cmd{1},'out_path')
  stat_dir = ct_filename_out(param,param.analysis.cmd{1}.out_path,'',1);
else
  stat_dir = ct_filename_out(param,param.analysis.out_path,'',1);
end
if ~exist(stat_dir, 'dir')
  fprintf('Empty directory --> run_analysis results (%s)\n',stat_dir);
  return;
end

%% Collate the mean reults
% =========================================================================

% For figure handles
if param.analysis.plot_individual
  h_fig = get_figures(1,param.analysis.enable_visible_plot_combined);
  cur_fig = h_fig;
  clf(cur_fig);
  h_axes = axes('parent',cur_fig);
end

out_fn = ct_filename_ct_tmp(param,'','collate_mean','power_vs_slowtime');
out_fn_dir = fileparts(out_fn);
if ~exist(out_fn_dir, 'dir')
  mkdir(out_fn_dir);
end

% The big loop for each img-wf_adc pair
for img = 1:length(param.analysis.imgs)
  pow_spec_cmd = [];
  legend_str = {};
  for wf_adc = 1:size(param.analysis.imgs{img},1)
    wf = param.analysis.imgs{img}(wf_adc,1);
    adc = param.analysis.imgs{img}(wf_adc,2);
    %% Load the statistics file
    % =====================================================================
    stats = load(fullfile(stat_dir, sprintf('stats_%s_wf_%d_adc_%d.mat',param.day_seg,wf,adc)));
    % Initialization
    presums = stats.param_analysis.radar.wfs(wf).presums;
    rline_sum=0;
    rline_pow = [];
    num_blocks=0;
    
    % Collate
    for b_size = 1:length(stats.stats)
      rline_sum = rline_sum + sum(stats.stats{b_size}{1});
      rline_pow =[rline_pow stats.stats{b_size}{1}'];
      num_blocks = num_blocks + size(stats.stats{b_size}{1},1);
    end
    
    % Average the power spectrum over all the blocks
    rline_mean = rline_sum/num_blocks/50; %Check Z0=50 Ohm in param sheet 
    rline_pow = rline_pow/50;
    rline_pow_mean = sum(rline_pow)/num_blocks;

    % Calculate the expected Noise levels
    if isfield(param.radar.wfs(wf),'noise_fig')
      Noise_plot1 = 10*log10(BoltzmannConst*290*param.radar.wfs(wf).noise_fig(adc)) + 30 - 10*log10(presums);
    else
      Noise_plot1 = 10*log10(BoltzmannConst*290*1) + 30 - 10*log10(presums);
    end
    Noise_plot2 = 10*log10(10.^(-9.0)/30e6) + 30 - 10*log10(presums) - stats.param_analysis.radar.wfs(wf).adc_gains_dB(adc);
    Noise_plot = 10*log10(1*10.^(Noise_plot1/10) + 1*10.^(Noise_plot2/10));
    Noise_plots = Noise_plot *ones(1,length(rline_pow));
    
    % Calculate mean values for Measured power vs Expected noise power
    mean_power(img,wf_adc) = 10*log10(rline_mean/30e6) + 30;
    mean_noise_plot(img,wf_adc) = mean(Noise_plot);
    
    %% Generate the plots
    % =====================================================================
    if param.analysis.plot_individual
      if param.analysis.enable_visible_plot_individual
        figure(cur_fig); % Brings the current figure to the top
      end
      plot(h_axes, 10*log10(rline_pow/30e6)+30,'LineWidth',2);
      hold(h_axes, 'on');
      xlabel(h_axes,'Range lines');
      ylabel(h_axes,'Magnitude (dBm/Hz)');
      colors = get(h_axes,'ColorOrder');
      grid(h_axes, 'on');
      % leg=[];
      % for leg_idx = 1
      %   leg{leg_idx} = sprintf('( %.2f vs %.2f )',mean_power(img,wf_adc), mean_noise_plot(img,wf_adc)) ;
      %   plot(h_axes, Noise_plots,'Color',colors(leg_idx,:));
      % end
      axis(h_axes, 'tight');
      % title(h_axes,sprintf('For [ %d-%d ] Mean Power  (Measured[Thick] vs Expected[Line])',wf,adc));
      legend_str{wf_adc} = sprintf('%d-%d',img,wf_adc);
    end
    % To see the progress of current segment
    if wf_adc == 1
      fprintf('img-wfadc %d-%d',img,wf_adc);
    else
      fprintf(' %d-%d',img,wf_adc);
    end

  end
  fprintf('\n');

  if param.analysis.plot_individual
    %% Save the individual plots
    legend(h_axes,legend_str,'Location', 'bestoutside');

    ct_saveas(cur_fig, [out_fn '.fig']);
    ct_saveas(cur_fig, [out_fn '.jpg']);
  end
end

if param.analysis.plot_combined
  h_fig = get_figures(1,param.analysis.enable_visible_plot_combined);
  cur_fig = h_fig;
  clf(cur_fig);
  if param.analysis.enable_visible_plot_combined
    figure(cur_fig); % Brings the current figure to the top
  end
  h_axes = axes('parent',cur_fig);
  for img = 1:size(mean_power,1)
    plot(h_axes, mean_power(img,:),'LineWidth',3);
    hold(h_axes, 'on');
  end
  xlabel(h_axes,sprintf('wf_adc [1:%d]',wf_adc));
  ylabel(h_axes,'Magnitude (dBm/Hz)');
  colors = get(h_axes,'ColorOrder');
  grid(h_axes, 'on');
  % leg=[];
  % for leg_idx = 1:size(mean_power,1)
  %   leg{leg_idx} = sprintf('( %.2f vs %.2f ) img-%d',mean(mean_power(leg_idx,:)), mean(mean_noise_plot(leg_idx,:)), leg_idx ) ;
  %   plot(h_axes, mean_noise_plot(leg_idx,:),'--','Color',colors(leg_idx,:));
  % end
  axis(h_axes, 'tight');
  hold(h_axes, 'off');
  title(h_axes,sprintf('For %d imgs Mean Power (Measured[Thick] vs Expected[Thin])',img));
  % legend(h_axes,leg, 'Location', 'Best');

  out_fn = ct_filename_ct_tmp(param,'','collate_mean','power_mean');
  out_fn_dir = fileparts(out_fn);
  if ~exist(out_fn_dir, 'dir')
    mkdir(out_fn_dir);
  end

  ct_saveas(cur_fig, [out_fn '.fig']);
  ct_saveas(cur_fig, [out_fn '.jpg']);
end

  
fprintf('=============================================================\n');
fprintf('%s: %s (%s)\n', param.season_name, param.day_seg, datestr(now));
fprintf('Output directory -->\n %s\n',out_fn_dir);
fprintf('=============================================================\n');

