function records_create(param,param_override)
% records_create(param,param_override)
%
% Function for creating records file for all radar systems. The function is
% usually called from master.m but can also be called from
% run_records_create.m.
%
% Corrects jumps in utc time, typically one second jumps. This script
% obtains headers from data indicated in the vectors param spreadsheets.
% After loading the files using basic_load*.m, the records files are saved
% in the support directories for the specific radar in .mat form for
% quicker access.
%
% DOCUMENTATION:
% https://gitlab.com/openpolarradar/opr/-/wikis/OPR-Toolbox-Guide#records-worksheet
%
% Inputs:
% param: struct with processing parameters
%         -- OR --
%         function handle to script with processing parameters
% param_override: parameters in this struct will override parameters
%         in param.  This struct must also contain the gRadar fields.
%         Typically global gRadar; param_override = gRadar;
%
% Authors: John Paden
%
% See also: run_master.m, master.m, run_records_create.m, records_create.m,
%   records_create_sync.m, records_check.m

%% General Setup
% =====================================================================
param = merge_structs(param, param_override);

fprintf('=====================================================================\n');
fprintf('%s: %s (%s)\n', mfilename, param.day_seg, datestr(now));
fprintf('=====================================================================\n');

%% Input Checks
% =====================================================================

[output_dir,radar_type,radar_name] = ct_output_dir(param.radar_name);

% param.records.arena: structure controlling records creation of
% arena-based digital systems
if ~isfield(param.records,'arena')
  param.records.arena = [];
end

% param.records.arena.epri_alignment_guard: Double scalar. Default is 4.
% The EPRI values should be exact multiples of the total_presums and should
% be the same for all digital boards. However, if there are digital errors,
% the EPRI values can take on non-multiple values. When this happens, we
% need to assign headers to each EPRI based on which EPRI they are closest
% to. epri_alignment_guard sets how close they have to be to a particular
% EPRI to be considered to be part of that EPRI.
if ~isfield(param.records.arena,'epri_alignment_guard') || isempty(param.records.arena.epri_alignment_guard)
  param.records.arena.epri_alignment_guard = 4;
end

% param.records.arena.epri_mode: Double scalar. Default is 0. This is the
% arena pulse sequence generator mode that is the EPRI. This is used to
% determine the total_presums automatically. If empty, then the user is
% prompted to enter which mode is the epri_mode.
if ~isfield(param.records.arena,'epri_mode') || isempty(param.records.arena.epri_mode)
  param.records.arena.epri_mode = 0;
end

% param.records.arena.epri_time_error_threshold: Double scalar. Units are
% seconds. Default is 0.2 seconds. Sets the amount that radar_time can
% deviate from the expected time based on time generated from the EPRI.
% This is used to catch random errors in radar_time.
if ~isfield(param.records.arena,'epri_time_error_threshold') || isempty(param.records.arena.epri_time_error_threshold)
  param.records.arena.epri_time_error_threshold = 0.2;
end

% param.records.arena.mode_latch_fix: Scaler or Nx2 array. Default is true scaler.
% If true (or non-zero), assumes mode_latch should be even. Values of 1, 3, 5, ... get
% converted to 0, 2, 4, ... respectively.
% If an array, then the array defines a mode mapping. Each row represents
% one mode that will be mapped. The first column is the input mode into the
% mapping and the second column is the output mode. For example:
% [1 0; 2 0; 3 0] would map modes 1,2,3 to 0.
if ~isfield(param.records.arena,'mode_latch_fix')
  param.records.arena.mode_latch_fix = true;
end

% param.records.arena.radar_time_correction_en: Enable correction of
% radar_time vector that is used to sync with GNSS data. Correction uses
% the expected EPRI field.
if ~isfield(param.records.arena,'radar_time_correction_en') || isempty(param.records.arena.radar_time_correction_en)
  param.records.arena.radar_time_correction_en = false;
end

% param.records.arena.sync_gnss_field: string that may contain one of these
% three fields:
% * 'radar_time' (internal-radar-counter) [DEFAULT]
% * 'comp_time(1,:)' (pps counter time)
% * 'comp_time(2,:)' (profile counter time)
% * 'sync_gps_time' (GNSS time)
% Controls what GNSS data will be used to synchronize radar data to
% GNSS/GPS data. The default is 'rel_time'.
if ~isfield(param.records.arena,'sync_gnss_field') || isempty(param.records.arena.sync_gnss_field)
  param.records.arena.sync_gnss_field = 'radar_time';
end
if ~any(strcmpi(param.records.arena.sync_gnss_field,{'radar_time','comp_time(1,:)','comp_time(2,:)','sync_gps_time'}))
  error('Unsupported param.records.arena.sync_gnss_field value %s. Must be ''radar_time'', ''comp_time(1,:)'', ''comp_time(2,:)'', or ''sync_gps_time''.', param.records.arena.sync_gnss_field);
end

% param.records.arena.sync_mask_en: scalar logical (default is true) that
% controls whether or not rel_time will be used to mask off the GNSS file
% sync fields to just the region which matches the radar's rel_time values.
if ~isfield(param.records.arena,'sync_mask_en') || isempty(param.records.arena.sync_mask_en)
  param.records.arena.sync_mask_en = true;
end

% param.records.arena.sync_mask_fn: string (default is '') that controls
% whether or not the sync filename regular expression specified here will
% be used to mask off the GNSS file sync fields to just those that came
% from sync files that match the regular expression.
if ~isfield(param.records.arena,'sync_mask_fn') || isempty(param.records.arena.sync_mask_fn)
  param.records.arena.sync_mask_fn = '';
end

% param.records.arena.sync_radar_field: string that may contain one of these two
% fields:
% * 'rel_time' (internal-radar-counter) [DEFAULT]
% * 'pps' (UTC time)
% * 'profile' (internal-radar-prf-counter)
% Controls which radar data field will be used to synchronize radar data to
% GNSS/GPS data. The default is 'rel_time'.
if ~isfield(param.records.arena,'sync_radar_field') || isempty(param.records.arena.sync_radar_field)
  param.records.arena.sync_radar_field = 'rel_time';
end
if ~any(strcmpi(param.records.arena.sync_radar_field,{'rel_time','pps','profile'}))
  error('Unsupported param.records.arena.sync_radar_field value %s. Must be ''rel_time'', ''pps'', or ''profile''.', param.records.arena.sync_radar_field);
end
% Check that param.records.arena.sync_gnss_field and
% param.records.arena.sync_radar_field are a valid combination.
if strcmpi(param.records.arena.sync_gnss_field,'radar_time')
  if ~strcmpi(param.records.arena.sync_radar_field,'rel_time')
    error(['param.records.arena.sync_gnss_field and param.records.arena.sync_radar_field do not match. ' ...
      'param.records.arena.sync_gnss_field radar_time must be matched with param.records.arena.sync_radar_field equal to rel_time.'])
  end
elseif strcmpi(param.records.arena.sync_gnss_field,'comp_time(1,:)')
  if ~strcmpi(param.records.arena.sync_radar_field,'pps')
    error(['param.records.arena.sync_gnss_field and param.records.arena.sync_radar_field do not match. ' ...
      'param.records.arena.sync_gnss_field comp_time(1,:) must be matched with param.records.arena.sync_radar_field equal to pps.'])
  end
elseif strcmpi(param.records.arena.sync_gnss_field,'comp_time(2,:)')
  if ~strcmpi(param.records.arena.sync_radar_field,'profile')
    error(['param.records.arena.sync_gnss_field and param.records.arena.sync_radar_field do not match. ' ...
      'param.records.arena.sync_gnss_field comp_time(2,:) must be matched with param.records.arena.sync_radar_field equal to profile.'])
  end
elseif strcmpi(param.records.arena.sync_gnss_field,'sync_gps_time')
  if ~strcmpi(param.records.arena.sync_radar_field,'pps')
    error(['param.records.arena.sync_gnss_field and param.records.arena.sync_radar_field do not match. ' ...
      'param.records.arena.sync_gnss_field sync_gps_time must be matched with param.records.arena.sync_radar_field equal to pps.'])
  end
end

% param.records.arena.sync_radar_field: string that may contain one of these two
% fields:
% * 'rel_time' (internal-radar-counter) [DEFAULT]
% * 'pps' (UTC time)
% * 'profile' (internal-radar-prf-counter)
% Controls which radar data field will be used to synchronize radar data to
% GNSS/GPS data. The default is 'rel_time'.
if ~isfield(param.records.arena,'sync_gap_threshold') || isempty(param.records.arena.sync_gap_threshold)
  if strcmpi(param.records.arena.sync_gnss_field,'radar_time')
    % radar_time: use GPS file rel_time to sync with radar_time
    param.records.arena.sync_gap_threshold = 20;
  elseif strcmpi(param.records.arena.sync_gnss_field,'comp_time(1,:)')
    % comp_time(1,:): use GPS file pps_cntr to sync with radar_time
    param.records.arena.sync_gap_threshold = 20;
  elseif strcmpi(param.records.arena.sync_gnss_field,'comp_time(2,:)')
    % comp_time(2,:): use GPS file profile_cntr to sync with radar_time
    param.records.arena.sync_gap_threshold = 20 * param.radar.prf;
  elseif strcmpi(param.records.arena.sync_gnss_field,'sync_gps_time')
    % sync_gps_time: use GPS file NMEA-string time to sync with radar_time
    param.records.arena.sync_gap_threshold = 20;
  end
end

% param.records.arena.total_presums: override total_presums value that is
% normally read in from the config.xml file
if ~isfield(param.records.arena,'total_presums')
  param.records.arena.total_presums = [];
end

% boards: List of subdirectories containing the files for each board (a
% board is a data stream stored to disk and often contains the data stream
% from multiple ADCs)
if any(param.records.file.version == [1:5 8 11 13 14 101:102 405:406 409:411 413 414 415 416 418 420 421 422])
  if ~isfield(param.records.file,'boards') || isempty(param.records.file.boards)
    % Assume a single channel system
    param.records.file.boards = {''};
  end
elseif any(param.records.file.version == [6:7 9:10 12 103 401:404 407:408 412 417,423])
  if ~isfield(param.records.file,'boards') || isempty(param.records.file.boards)
    error('param.records.file.boards should be specified.');
  end
else
  error('Unsupported file version\n');
end
boards = param.records.file.boards;

if ~isfield(param.records,'epri_jump_threshold') || isempty(param.records.epri_jump_threshold)
  param.records.epri_jump_threshold = 10000;
end

if ~isfield(param.records,'file') || isempty(param.records.file)
  param.records.file = [];
end
if ~isfield(param.records.file,'version') || isempty(param.records.file.version)
  error('The param.records.file.version field must be specified.');
end

if ~isfield(param.records,'gps') || isempty(param.records.gps)
  param.records.gps = [];
end
if ~isfield(param.records.gps,'en') || isempty(param.records.gps.en)
  % Assume that GPS synchronization is enabled
  param.records.gps.en = true;
end
if ~isfield(param.records.gps,'fn') || isempty(param.records.gps.fn)
  param.records.gps.fn = '';
end
if ~isfield(param.records.gps,'time_offset') || isempty(param.records.gps.time_offset)
  param.records.gps.time_offset = 0;
end
if ~isfield(param.records.gps,'utc_time_halved') || isempty(param.records.gps.utc_time_halved)
  if param.records.file.version == 1
    error('The param.records.gps.utc_time_halved must be specified. Set to 0 if the UTC time in the data files is half of what it should have been.');
  else
    param.records.gps.utc_time_halved = 0;
  end
end

% param.records.mask_bad: Optional. Indicates bad records that should be
% ignored. Currently only works with arena. Should be a cell array with an
% entry for each board. The cell array should contain a N x 2 numeric
% matrix where each row specifies a start and stop index of bad records.
% Use inf in the stop index (column 2) to indicate the "last" record.
% Default value is N = 0 or {zeros(0,2),zeros(0,2),...,zeros(0,2)}.
if ~isfield(param.records,'mask_bad') || isempty(param.records.mask_bad)
  param.records.mask_bad = {};
end
for board_idx = 1:length(boards)
  if length(param.records.mask_bad) < board_idx
    % If mask_bad not defined for this board, set it to no bad records.
    param.records.mask_bad{board_idx} = zeros(0,2);
  end
end

% param.records.mask_good: Same as mask_bad, but forces the records to be
% considered good.
if ~isfield(param.records,'mask_good') || isempty(param.records.mask_good)
  param.records.mask_good = {};
end
for board_idx = 1:length(boards)
  if length(param.records.mask_good) < board_idx
    % If mask_good not defined for this board, set it to no bad records.
    param.records.mask_good{board_idx} = zeros(0,2);
  end
end

if ~isfield(param.records,'presum_mode') || isempty(param.records.presum_mode)
  if any(param.records.file.version == [402 403 404])
    % 8-channel DDS board had a waveform bug that required an extra waveform
    % to be sent and discarded (not used for presumming)
    param.records.presum_mode = 1;
  elseif any(param.records.file.version == [407 408])
    error('The param.records.presum_mode must be specified. If using the new waveform generator (Arena-based), this field should be 0. If using the old waveform generator, this field should be set to 1. The old waveform generator is an 8-channel 1 GSPS DDS by Ledford/Leuschen (not Arena-based).');
  else
    param.records.presum_mode = 0;
  end
end

if ~isfield(param.records,'use_ideal_epri') || isempty(param.records.use_ideal_epri)
  param.records.use_ideal_epri = false;
end

% records.frames: structure that controls the frame generation after the
% records file is created.
if ~isfield(param.records,'frames') || isempty(param.records.frames)
  param.records.frames = [];
end

% records.frames.mode: 0, 1, or 2. Default is 0. 0: do nothing with frames,
% 1: autogenerate frames if they do not exist and then manually edit the
% frames, 2: autogenerate the frames
if ~isfield(param.records.frames,'mode') || isempty(param.records.frames.mode)
  param.records.frames.mode = 0;
end

% records.start_record: Positive integer scaler. Optional. Default is 1.
% Controls the first record to be read in for the first file only.
if ~isfield(param.records,'start_record') || isempty(param.records.start_record)
  param.records.start_record = 1;
end

% records.stop_record: Positive integer scaler. Optional. Default is inf.
% Controls the last record to be read in for the last file only.
if ~isfield(param.records,'stop_record') || isempty(param.records.stop_record)
  param.records.stop_record = inf;
end

command_window_out_fn = ct_filename_ct_tmp(param,'','records_create', ['console.txt']);
command_window_out_fn_dir = fileparts(command_window_out_fn);
if ~exist(command_window_out_fn_dir,'dir')
  mkdir(command_window_out_fn_dir);
end
if exist(command_window_out_fn,'file')
  delete(command_window_out_fn);
end
diary(command_window_out_fn);


if any(param.records.file.version == [9 10 103 412])
  % Arena based systems
  h_fig = get_figures(3,true);
end

%% Load headers from each board
% =====================================================================
clear board_hdrs;
board_hdrs = {};
records = [];
for board_idx = 1:length(boards)
  board = boards{board_idx};
  
  fprintf('Getting files for board %s (%d of %d) (%s)\n', ...
    board, board_idx, length(boards), datestr(now));
  
  %% Load headers: get files
  % =====================================================================
  [base_dir,board_folder_name,board_fns{board_idx},file_idxs] = get_segment_file_list(param,board_idx);
  
  % Load header from radar data file
  fprintf('Loading raw files %i to %i\n',file_idxs([1 end]));
  
  % Initialize variables to store header fields
  if any(param.records.file.version == [9 10 103 412])
    % Arena based systems
    board_hdrs{board_idx}.file_size = zeros([0 0],'int32');
    board_hdrs{board_idx}.file_idx = zeros([0 0],'int32');
    board_hdrs{board_idx}.offset = zeros([0 0],'int32');
    board_hdrs{board_idx}.mode_latch = zeros([0 0],'int32');
    board_hdrs{board_idx}.subchannel = zeros([0 0],'int32');
    board_hdrs{board_idx}.pps_cntr_latch = zeros([0 0],'double'); % Time when PPS edge came in
    board_hdrs{board_idx}.pps_ftime_cntr_latch = zeros([0 0],'double'); % Fractional time since edge
    board_hdrs{board_idx}.profile_cntr_latch = zeros([0 0],'double'); % PRI counter
    board_hdrs{board_idx}.rel_time_cntr_latch = zeros([0 0],'double'); % 10 MHz counts counter
    cur_idx = 0;
    
  elseif any(param.records.file.version == [413 414])
    board_hdrs{board_idx}.gps_time = zeros([0 0],'double');
    board_hdrs{board_idx}.file_idx = zeros([0 0],'int32');
    board_hdrs{board_idx}.offset = zeros([0 0],'int32');
    
  elseif any(param.records.file.version == [12 415 417])
    board_hdrs{board_idx}.file_idx = zeros([0 0],'int32');
    board_hdrs{board_idx}.radar_time = zeros([0 0],'double');
    board_hdrs{board_idx}.comp_time = zeros([0 0],'double');
    board_hdrs{board_idx}.offset = zeros([0 0],'int32');
    
  else
    % NI, Rink, Paden, Leuschen, and Ledford systems
    board_hdrs{board_idx}.seconds = zeros([0 0],'uint32');
    board_hdrs{board_idx}.fraction = zeros([0 0],'uint32');
    board_hdrs{board_idx}.offset = zeros([0 0],'int32');
    board_hdrs{board_idx}.file_idx = zeros([0 0],'uint32');
    % records.relative_rec_num = This variable contains the first record
    % number of each file. After the loop runs there will always be one
    % to many elements (161 files will mean 162 elements in the array)
    % and the last entry is the record number that would have been next
    % so that length(hdr.utc_time_sod) = records.relative_rec_num(end)-1
    records.relative_rec_num{board_idx} = 1;
    
    if param.records.file.version ~= 101
      board_hdrs{board_idx}.epri = zeros([0 0],'uint32');
    end
    if param.records.file.version == 2
      board_hdrs{board_idx}.nyquist_zone = zeros([0 0],'uint8');
      board_hdrs{board_idx}.loopback_mode = zeros([0 0],'uint8');
    elseif param.records.file.version == 8
      board_hdrs{board_idx}.nyquist_zone = zeros([0 0],'uint8');
      board_hdrs{board_idx}.waveform_ID = zeros([0 0],'uint64');
    end
  end
  
  for file_idx = 1:length(file_idxs)
    % Get the temporary filename from the filename list
    file_num = file_idxs(file_idx);
    [~,fn_name] = fileparts(board_fns{board_idx}{file_num});
%     if any(param.records.file.version == [9 10 103 412])
%       % Update the filename to point to the packet stripped files
%       board_fns{board_idx}{file_num} = ct_filename_ct_tmp(rmfield(param,'day_seg'),'','headers', ...
%         fullfile(board_folder_name, [fn_name '.dat']));
%     end
    fn = board_fns{board_idx}{file_num};
    dir_info = dir(fn);
    
    fprintf('  %i/%i %s (%s)\n', ...
      file_idx,length(file_idxs), fn, datestr(now,'HH:MM:SS'));
    
    if any(param.records.file.version == [415])
      % Files may be empty, these are skipped in preprocessing
      if dir_info.bytes == 0
        continue
      end
    end
    
    % Load temporary files
    tmp_hdr_fn = ct_filename_ct_tmp(rmfield(param,'day_seg'),'','headers', ...
      fullfile(board_folder_name, [fn_name '.mat']));
    if any(param.records.file.version == [413 414])
      hdr_tmp = load(tmp_hdr_fn,'gps_time','wfs');
    else
      hdr_tmp = load(tmp_hdr_fn);
    end
    
    %% Concatenate all the fields together
    %  - Note that all fields from the file should have the same hdr_tmp
    %  length. Error in the file if not.
    if any(param.records.file.version == [9 10 103 412])
      % Arena based systems
      if hdr_tmp.offset > 1e9
        keyboard
      end

      board_hdrs{board_idx}.file_size(cur_idx + (1:length(hdr_tmp.mode_latch))) = dir_info.bytes;
      board_hdrs{board_idx}.file_idx(cur_idx + (1:length(hdr_tmp.mode_latch))) = file_num;

      if isfield(hdr_tmp,'profile')
        board_hdrs{board_idx}.profile(cur_idx + (1:length(hdr_tmp.mode_latch))) = hdr_tmp.profile;
      else
        board_hdrs{board_idx}.profile(cur_idx + (1:length(hdr_tmp.mode_latch))) = nan(size(hdr_tmp.mode_latch));
      end
      board_hdrs{board_idx}.mode_latch(cur_idx + (1:length(hdr_tmp.mode_latch))) = hdr_tmp.mode_latch;
      board_hdrs{board_idx}.offset(cur_idx + (1:length(hdr_tmp.mode_latch))) = hdr_tmp.offset;
      if isa(hdr_tmp.offset,'uint64')
        % Due to an error in arena_packet_strip_tcp_mex.cpp, bad records
        % are tagged as 4294967265 instead of -2^31. Fix this now.
        bad_recs = find(hdr_tmp.offset == 4294967265);
        new_idxs = cur_idx + (1:length(hdr_tmp.mode_latch));
        board_hdrs{board_idx}.offset(new_idxs(bad_recs)) = -2^31;
      end
      board_hdrs{board_idx}.subchannel(cur_idx + (1:length(hdr_tmp.mode_latch))) = hdr_tmp.subchannel;
      
      board_hdrs{board_idx}.pps_cntr_latch(cur_idx + (1:length(hdr_tmp.mode_latch))) = hdr_tmp.pps_cntr_latch;
      board_hdrs{board_idx}.pps_ftime_cntr_latch(cur_idx + (1:length(hdr_tmp.mode_latch))) = hdr_tmp.pps_ftime_cntr_latch;
      board_hdrs{board_idx}.profile_cntr_latch(cur_idx + (1:length(hdr_tmp.mode_latch))) = hdr_tmp.profile_cntr_latch;
      board_hdrs{board_idx}.rel_time_cntr_latch(cur_idx + (1:length(hdr_tmp.mode_latch))) = hdr_tmp.rel_time_cntr_latch;
      
      cur_idx = cur_idx + length(hdr_tmp.mode_latch);
      
    elseif any(param.records.file.version == [413 414])
      board_hdrs{board_idx}.gps_time(end+1:end+length(hdr_tmp.gps_time)) = hdr_tmp.gps_time;
      board_hdrs{board_idx}.file_idx(end+1:end+length(hdr_tmp.gps_time)) = file_num;
      board_hdrs{board_idx}.offset(end+1:end+length(hdr_tmp.gps_time)) = 1:length(hdr_tmp.gps_time);
      wfs = hdr_tmp.wfs;
      
    elseif any(param.records.file.version == [415])
      % UTIG RDS 60 MHZ MARFA/HICARS
      if file_idx == 1
        start_record = param.records.file.start_record;
      else
        start_record = 1;
      end
      if file_idx == length(file_idxs)
        stop_record = min(param.records.file.stop_record,length(hdr_tmp.radar_time));
      else
        stop_record = length(hdr_tmp.radar_time);
      end
      hdr_tmp.radar_time = hdr_tmp.radar_time(start_record:stop_record);
      hdr_tmp.comp_time = hdr_tmp.comp_time(start_record:stop_record);
      hdr_tmp.offset = hdr_tmp.offset(start_record:stop_record);
      board_hdrs{board_idx}.radar_time(end+1:end+length(hdr_tmp.radar_time)) = hdr_tmp.radar_time;
      board_hdrs{board_idx}.comp_time(end+1:end+length(hdr_tmp.radar_time)) = datenum_to_epoch(hdr_tmp.comp_time);
      board_hdrs{board_idx}.offset(end+1:end+length(hdr_tmp.radar_time)) = int32(hdr_tmp.offset);
      board_hdrs{board_idx}.file_idx(end+1:end+length(hdr_tmp.radar_time)) = file_num;
      wfs = [];
      
    elseif any(param.records.file.version == [417])
      % LDEO 188 MHz DICE
      if file_idx == 1
        start_record = param.records.file.start_record;
      else
        start_record = 1;
      end
      if file_idx == length(file_idxs)
        stop_record = min(param.records.file.stop_record,length(hdr_tmp.radar_time));
      else
        stop_record = length(hdr_tmp.radar_time);
      end
      hdr_tmp.radar_time = hdr_tmp.radar_time(start_record:stop_record);
      hdr_tmp.comp_time = hdr_tmp.comp_time(start_record:stop_record);
      hdr_tmp.offset = hdr_tmp.offset(start_record:stop_record);
      board_hdrs{board_idx}.radar_time(end+1:end+length(hdr_tmp.radar_time)) = hdr_tmp.radar_time;
      board_hdrs{board_idx}.comp_time(end+1:end+length(hdr_tmp.radar_time)) = datenum_to_epoch(hdr_tmp.comp_time);
      board_hdrs{board_idx}.offset(end+1:end+length(hdr_tmp.radar_time)) = int32(hdr_tmp.offset);
      board_hdrs{board_idx}.file_idx(end+1:end+length(hdr_tmp.radar_time)) = file_num;
      wfs = [];
      
    else
      % NI, Rink, Paden, Leuschen, and Ledford systems
      if isempty(hdr_tmp.seconds)
        fprintf('    File with no records.\n');
        records.relative_rec_num{board_idx}(file_idx+1) = records.relative_rec_num{board_idx}(file_idx);
        continue;
      end

      board_hdrs{board_idx}.seconds(end+1:end+length(hdr_tmp.seconds)) = hdr_tmp.seconds;
      board_hdrs{board_idx}.fraction(end+1:end+length(hdr_tmp.seconds)) = hdr_tmp.fraction;
      board_hdrs{board_idx}.file_idx(end+1:end+length(hdr_tmp.seconds)) = file_num;
      board_hdrs{board_idx}.offset(end+1:end+length(hdr_tmp.seconds)) = int32(hdr_tmp.offset);
      
      if any(param.records.file.version == [1:8 11 13 14 102 401:404 407:408 418 420 421 422 423])
        % Ledford, Rink and NI systems have EPRI field
        board_hdrs{board_idx}.epri(end+1:end+length(hdr_tmp.seconds)) = hdr_tmp.epri;
      end
      if param.records.file.version == 8
        board_hdrs{board_idx}.nyquist_zone(end+1:end+length(hdr_tmp.seconds)) = hdr_tmp.nyquist_zone;
        board_hdrs{board_idx}.waveform_ID(end+1:end+length(hdr_tmp.seconds)) = hdr_tmp.waveform_ID;
      end
      
      % Copy the waveform structure
      wfs = hdr_tmp.wfs;
      
      % Create records and file numbers
      records.relative_rec_num{board_idx}(file_idx+1) = length(hdr_tmp.seconds)+records.relative_rec_num{board_idx}(file_idx);
      [fn_dir fn_name fn_ext] = fileparts(fn);
      
      % Handle records that span two files
      if file_idx ~= length(file_idxs)
        % The last record in a file is generally incomplete and continues
        % in the next file. This incomplete record is marked as being
        % in the next file (so we increment file_idx) and we use a negative
        % index to indicate that it actually started in this file where the
        % negative index is relative to the end of this file.
        board_hdrs{board_idx}.file_idx(end) = board_hdrs{board_idx}.file_idx(end) + 1;
        file_size = dir(fn);
        board_hdrs{board_idx}.offset(end) = board_hdrs{board_idx}.offset(end) - file_size.bytes;
      else
        % Drop the last record of the last file since it is generally not a
        % complete record and there is no additional file to load which
        % contains the remainder of the record.
        if any(param.records.file.version == [1:8 11 13 14 102 401:404 407:408 418 420 421 422 423])
          board_hdrs{board_idx}.epri = board_hdrs{board_idx}.epri(1:end-1);
        end
        if param.records.file.version == 8
          board_hdrs{board_idx}.nyquist_zone = board_hdrs{board_idx}.nyquist_zone(1:end-1);
          board_hdrs{board_idx}.waveform_ID = board_hdrs{board_idx}.waveform_ID(1:end-1);
        end
        board_hdrs{board_idx}.seconds = board_hdrs{board_idx}.seconds(1:end-1);
        board_hdrs{board_idx}.fraction = board_hdrs{board_idx}.fraction(1:end-1);
        board_hdrs{board_idx}.file_idx = board_hdrs{board_idx}.file_idx(1:end-1);
        board_hdrs{board_idx}.offset = board_hdrs{board_idx}.offset(1:end-1);
      end
    end
  end
  
end

%% Correct EPRI for each board individually
% ======================================================================

if any(param.records.file.version == [9 10 103 412])
  % Arena based systems

  % Load XML configs file
  config_fn = ct_filename_ct_tmp(rmfield(param,'day_seg'),'','headers', ...
    fullfile(param.records.config_fn));
  configs = read_arena_xml(config_fn,[],[],[],param.records.arena);
  
  for board_idx = 1:length(boards)
    %% Correct EPRI/Arena: Find the PRIs associated with the EPRI profile
    % =====================================================================
    % Print out digital-system-mode distribution table setup
    fprintf('board_idx: %d =====================================================\n', board_idx);
    unique_modes = unique(board_hdrs{board_idx}.mode_latch);
    for unique_idx = 1:length(unique_modes)
      unique_modes_percentage(unique_idx) = sum(board_hdrs{board_idx}.mode_latch==unique_modes(unique_idx))/length(board_hdrs{board_idx}.mode_latch);
    end
    
    if size(param.records.data_map{board_idx},2) == 4
      % No Profile Processor Digital System (use mode_latch,subchannel instead)
      % Each row of param.records.data_map{board_idx} = [mode_latch subchannel wf adc]
      
      % Get the first row (which is always the EPRI row)
      epri_mode = param.records.data_map{board_idx}(1,1);
      epri_subchannel = param.records.data_map{board_idx}(1,2);
      
      if numel(param.records.arena.mode_latch_fix) == 1 && param.records.arena.mode_latch_fix
        warning('param.records.arena.mode_latch_fix is true: APPLYING HACK to fix mode_latch with zero-pi mode and digital errors that cause mode misalignment.');
        board_hdrs{board_idx}.mode_latch = floor(double(board_hdrs{board_idx}.mode_latch)/2)*2;
      elseif numel(param.records.arena.mode_latch_fix) > 1
        warning('param.records.arena.mode_latch_fix is a mode mapping: APPLYING HACK to map mode_latch values.');
        % param.records.arena.mode_latch_fix should be N by 2 array
        % Each row maps a particular mode
        % First column is the input mode, second column is the mode to map
        % it to.
        for mode_map_idx = 1:size(param.records.arena.mode_latch_fix,1)
          board_hdrs{board_idx}.mode_latch( ...
            board_hdrs{board_idx}.mode_latch == param.records.arena.mode_latch_fix(mode_map_idx,1)) ...
            = param.records.arena.mode_latch_fix(mode_map_idx,2);
        end
      end
      mask = board_hdrs{board_idx}.mode_latch == epri_mode & board_hdrs{board_idx}.subchannel == epri_subchannel;
    else
      % Profile Processor Digital System
      % Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
      
      % Get the first row (which is always the EPRI row)
      epri_profile = param.records.data_map{board_idx}(1,1);
      epri_mode = param.records.data_map{board_idx}(1,2);
      epri_subchannel = param.records.data_map{board_idx}(1,3);

      if numel(param.records.arena.mode_latch_fix) > 1
        warning('param.records.arena.mode_latch_fix is a mode mapping: APPLYING HACK to map mode_latch values.');
        % param.records.arena.mode_latch_fix should be N by 2 array
        % Each row maps a particular mode
        % First column is the input mode, second column is the mode to map
        % it to.
        for mode_map_idx = 1:size(param.records.arena.mode_latch_fix,1)
          board_hdrs{board_idx}.mode_latch( ...
            board_hdrs{board_idx}.mode_latch == param.records.arena.mode_latch_fix(mode_map_idx,1)) ...
            = param.records.arena.mode_latch_fix(mode_map_idx,2);
        end
      end

      mask = board_hdrs{board_idx}.profile == epri_profile;
    end
    % Data before first PPS reset may be incorrectly time tagged so we do not
    % use it
    first_reset = find(diff(board_hdrs{board_idx}.pps_cntr_latch)>0,1);
    if board_hdrs{board_idx}.pps_ftime_cntr_latch(first_reset) > 10e6
      mask(1:first_reset) = 0;
    end
    epri_pris = board_hdrs{board_idx}.profile_cntr_latch(mask);

    % Print out digital-system-mode distribution table
    new_unique_modes_percentage = [];
    fprintf('Mode - Before %% - After %%\n');
    for unique_idx = 1:length(unique_modes)
      new_unique_modes_percentage(unique_idx) = sum(board_hdrs{board_idx}.mode_latch==unique_modes(unique_idx))/length(board_hdrs{board_idx}.mode_latch);
      if new_unique_modes_percentage(unique_idx) > 0
        fprintf(' %3d   %5.1f%%     %5.1f%%\n', unique_modes(unique_idx), unique_modes_percentage(unique_idx)*100, new_unique_modes_percentage(unique_idx)*100)
      else
        fprintf(' %3d   %5.1f%%     %5.1f%%   None\n', unique_modes(unique_idx), unique_modes_percentage(unique_idx)*100, new_unique_modes_percentage(unique_idx)*100)
      end
    end
    new_unique_modes = setdiff(unique(board_hdrs{board_idx}.mode_latch), unique_modes);
    for unique_idx = 1:length(new_unique_modes)
      new_unique_modes_percentage(unique_idx) = sum(board_hdrs{board_idx}.mode_latch==new_unique_modes(unique_idx))/length(board_hdrs{board_idx}.mode_latch);
      if new_unique_modes_percentage(unique_idx) > 0
        fprintf(' %3d   %5.1f%%     %5.1f%%\n', new_unique_modes(unique_idx), 0, new_unique_modes_percentage(unique_idx)*100)
      else
        fprintf(' %3d   %5.1f%%     %5.1f%%   None\n', new_unique_modes(unique_idx), 0, new_unique_modes_percentage(unique_idx)*100)
      end
    end

    %% Correct EPRI/Arena: Find EPRI jumps and mask out
    % =====================================================================
    if isempty(param.records.arena.total_presums)
      total_presums = configs.total_presums;
    else
      total_presums = param.records.arena.total_presums;
    end
    jump_idxs = find( abs(diff(double(epri_pris))/total_presums - 1) > 0.1);
    
    bad_mask = zeros(size(epri_pris));
    num_errors_1_100 = 0;
    max_errors_1_100 = 5;
    num_small_neg_jump = 0;
    max_small_neg_jump = 5;
    for jump_idx_idx = 1:length(jump_idxs)
      jump_idx = jump_idxs(jump_idx_idx);
      jump = (epri_pris(jump_idx+1)-epri_pris(jump_idx))/total_presums - 1;
      if jump < -0.1
        if jump > -1
          num_small_neg_jump = num_small_neg_jump + 1;
        end
        if num_small_neg_jump <= max_small_neg_jump || jump < -1
          fprintf('%d: Record index where epri_pris jump occured: %d (skipped %.2f records)\n', jump_idx_idx, jump_idx, jump);
          fprintf('  board_hdrs{%d}.profile_cntr_latch jumped %d (expected %d): %d to %d\n', ...
            board_idx, epri_pris(jump_idx+1)-epri_pris(jump_idx), total_presums, epri_pris(jump_idx), epri_pris(jump_idx+1));
          fprintf('  Negative or zero time jump. Manually fix using debug prompt or ignore by running "dbcont".\n');
          if jump < -1
            bad_mask(jump_idx+1:end) = epri_pris(jump_idx+1:end) < epri_pris(jump_idx);
            %keyboard
          end
        end
      elseif jump > 0.1 && jump < 100
        num_errors_1_100 = num_errors_1_100 + 1;
        if num_errors_1_100 <= max_errors_1_100
          fprintf('%d: Record index where epri_pris jump occured: %d (skipped %.2f records)\n', jump_idx_idx, jump_idx, jump);
          fprintf('  board_hdrs{%d}.profile_cntr_latch jumped %d (expected %d): %d to %d\n', ...
            board_idx, epri_pris(jump_idx+1)-epri_pris(jump_idx), total_presums, epri_pris(jump_idx), epri_pris(jump_idx+1));
          fprintf('  Dropped 1 to 100 records\n');
        end
        %keyboard
      elseif jump > 50000
        fprintf('%d: Record index where epri_pris jump occured: %d (skipped %.2f records)\n', jump_idx_idx, jump_idx, jump);
        fprintf('  board_hdrs{%d}.profile_cntr_latch jumped %d (expected %d): %d to %d\n', ...
          board_idx, epri_pris(jump_idx+1)-epri_pris(jump_idx), total_presums, epri_pris(jump_idx), epri_pris(jump_idx+1));
        fprintf('  Record header error or dropped > 50000 records. Manually check and fix, split segment into two at this record, or ignore by running "dbcont".\n');
        keyboard
        epri_pris(jump_idx+1) = epri_pris(jump_idx);
        bad_mask(jump_idx+1) = 1;
      elseif jump > 100
        fprintf('%d: Record index where epri_pris jump occured: %d (skipped %.2f records)\n', jump_idx_idx, jump_idx, jump);
        fprintf('  board_hdrs{%d}.profile_cntr_latch jumped %d (expected %d): %d to %d\n', ...
          board_idx, epri_pris(jump_idx+1)-epri_pris(jump_idx), total_presums, epri_pris(jump_idx), epri_pris(jump_idx+1));
        fprintf('  Dropped 100 to 50000 records\n');
        %       keyboard
      end
    end
    if num_errors_1_100 > max_errors_1_100
      fprintf('Dropped 1 to 100 records %d times. Only printed first %d occurances.\n', num_errors_1_100, max_errors_1_100);
    end
    if num_small_neg_jump > max_small_neg_jump
      fprintf('%d small negative jumps. Only printed first %d occurances.\n', num_small_neg_jump, max_small_neg_jump);
    end
    fprintf('There were %d bad board_hdrs{%d}.profile_cntr_latch jumps out of %d records\n', length(jump_idxs), board_idx, length(epri_pris));
    fprintf('Marking %d records as bad\n', sum(bad_mask));
    
    if isempty(bad_mask)
      error('There are no records for board_hdrs{%d}. Either no data was collected or some aspect of preprocess has failed.', board_idx);
    end

    mask(mask) = ~bad_mask;
    
    %% Correct EPRI/Arena: Update epri_pri_idxs
    % Find the first subrecord in each epri_pri and update epri_pri_idxs
    % NOTE: This assumes that all data streams recorded during a particular
    % EPRI are tagged with the same profile_cntr_latch value. This will not
    % be the case if the integrations do not line up for different data
    % streams or if some data frames are lost at the hardware level due to
    % digital errors (the latter "should" not happen, but has occurred in
    % datasets affected by RF EMI or digital system bugs).
    %
    % Since we assume all data streams finish their integrations during the
    % same profile_cntr_latch value we should have one EPRI waveform for
    % each unique profile_cntr_latch value. The order that this EPRI
    % waveform occurs within the set of datastreams is arbitrary (since all
    % datastreams completed their integrations at the exact same time).
    % This code finds the datastream that is packed first in the data file
    % for each EPRI. We call this the start of the record for that EPRI.
    epri_pri_idxs = find(mask);
    for idx = 1:length(epri_pri_idxs)
      %if ~mod(idx-1,10000)
      %  fprintf('%d\n', idx);
      %end
      epri_pri_idx = epri_pri_idxs(idx);
      pri = board_hdrs{board_idx}.profile_cntr_latch(epri_pri_idx);
      if board_hdrs{board_idx}.profile_cntr_latch(1) == pri
        % Special case for first PRI
        epri_pri_idxs(idx) = 1;
      else
        % For second and later PRIs
        while board_hdrs{board_idx}.profile_cntr_latch(epri_pri_idx) == pri
          epri_pri_idx = epri_pri_idx-1; % Search backwards until we find the previous PRI
        end
        epri_pri_idxs(idx) = epri_pri_idx+1; % The subrecord after this is the first PRI in the current EPRI
      end
    end
    
    %% Correct EPRI/Arena: Ensure all records are valid in each EPRI
    % =====================================================================
    mask = zeros(size(board_hdrs{board_idx}.pps_cntr_latch));
    mask(epri_pri_idxs) = 1;
    found_board = false;
    num_bad_records = 0;
    for idx = 1:length(epri_pri_idxs)-1
      %if ~mod(idx-1,1000000)
      %  fprintf('%d\n', idx);
      %end
      for pri_idx = epri_pri_idxs(idx):epri_pri_idxs(idx+1)-1
        
        if ~found_board
          for configs_board_idx = 1:size(configs.adc,1)
            if isfield(configs.adc{configs_board_idx,board_hdrs{board_idx}.mode_latch(pri_idx)+1,board_hdrs{board_idx}.subchannel(pri_idx)+1},'name') ...
                && strcmpi(configs.adc{configs_board_idx,board_hdrs{board_idx}.mode_latch(pri_idx)+1,board_hdrs{board_idx}.subchannel(pri_idx)+1}.name, boards{board_idx})
              found_board = true;
              break;
            end
          end
          if ~found_board
            error('boards(%d)=%s not found in XML config file\n', board_idx, boards(board_idx));
          end
        end

        if board_hdrs{board_idx}.mode_latch(pri_idx) >= size(configs.adc,2) ...
            || board_hdrs{board_idx}.subchannel(pri_idx) >= size(configs.adc,3) ...
            || ~isfield(configs.adc{configs_board_idx,board_hdrs{board_idx}.mode_latch(pri_idx)+1,board_hdrs{board_idx}.subchannel(pri_idx)+1},'rg') ...
            || isempty(configs.adc{configs_board_idx,board_hdrs{board_idx}.mode_latch(pri_idx)+1,board_hdrs{board_idx}.subchannel(pri_idx)+1}.rg)
          fprintf('Record %d has bad header fields (mode_latch, subchannel, or XML file without rg/range-gate info)\n', pri_idx);
          mask(pri_idx) = 0;
          num_bad_records = num_bad_records+1;
          break;
        end
      end
    end
    fprintf('%d of %d records had bad header fields\n', num_bad_records, length(epri_pri_idxs)-1);
    mask(find(mask,1,'last')) = 0; % Remove last potentially incomplete record
    epri_pri_idxs = find(mask);
    
    %% Correct EPRI/Arena: Fix records split between two files
    % Find records that are split between two files and use a negative
    % offset to indicate this.
    for idx = 2:length(epri_pri_idxs)
      if board_hdrs{board_idx}.file_idx(epri_pri_idxs(idx)) > board_hdrs{board_idx}.file_idx(epri_pri_idxs(idx-1))
        % This record is split between files
        if board_hdrs{board_idx}.offset(epri_pri_idxs(idx)) >= 2^31
          % This split is already handled by arena_packet_strip, but we need
          % to convert uint32 number to negative int32.
          board_hdrs{board_idx}.offset(epri_pri_idxs(idx)) = board_hdrs{board_idx}.offset(epri_pri_idxs(idx)) - 2^32;
        else
          % Convert last record from previous file to negative offset in the
          % new file
          board_hdrs{board_idx}.offset(epri_pri_idxs(idx-1)) = board_hdrs{board_idx}.offset(epri_pri_idxs(idx-1)) - board_hdrs{board_idx}.file_size(epri_pri_idxs(idx-1));
          board_hdrs{board_idx}.file_idx(epri_pri_idxs(idx-1)) = board_hdrs{board_idx}.file_idx(epri_pri_idxs(idx-1)) + 1;
        end
      end
    end
    
    % Store new outputs
    board_hdrs{board_idx}.epri_pri_idxs = epri_pri_idxs;
    board_hdrs{board_idx}.mask = logical(mask);
  end

  %% Correct EPRI/Arena: Mask outputs
  for board_idx = 1:length(boards)
    board_hdrs{board_idx}.offset = board_hdrs{board_idx}.offset(board_hdrs{board_idx}.mask);
    board_hdrs{board_idx}.file_idx = board_hdrs{board_idx}.file_idx(board_hdrs{board_idx}.mask);
    board_hdrs{board_idx}.profile_cntr_latch = board_hdrs{board_idx}.profile_cntr_latch(board_hdrs{board_idx}.mask);
    board_hdrs{board_idx}.rel_time_cntr_latch = board_hdrs{board_idx}.rel_time_cntr_latch(board_hdrs{board_idx}.mask);
    board_hdrs{board_idx}.pps_cntr_latch = board_hdrs{board_idx}.pps_cntr_latch(board_hdrs{board_idx}.mask);
    board_hdrs{board_idx}.pps_ftime_cntr_latch = board_hdrs{board_idx}.pps_ftime_cntr_latch(board_hdrs{board_idx}.mask);
    board_hdrs{board_idx} = rmfield(board_hdrs{board_idx},{'file_size','mode_latch','subchannel','mask'});
  end
  
  %% Correct EPRI/Arena: Populate wfs structure
  for board_idx = 1:length(boards)
    board = boards(board_idx);
    
    for map_idx = 1:size(param.records.data_map{board_idx},1)
      if size(param.records.data_map{board_idx},2) == 4
        % No Profile Processor Digital System (use mode_latch,subchannel instead)
        % Each row of param.records.data_map{board_idx} = [mode_latch channel wf adc]
        
        wf = param.records.data_map{board_idx}(map_idx,3);
        % adc = param.records.data_map{board_idx}(map_idx,4);
        mode_latch = param.records.data_map{board_idx}(map_idx,1);
        subchannel = param.records.data_map{board_idx}(map_idx,2);
        
      else
        % Profile mode
        % Each row of param.records.data_map{board_idx} = [profile mode_latch channel wf adc]
        
        wf = param.records.data_map{board_idx}(map_idx,4);
        % adc = param.records.data_map{board_idx}(map_idx,5);
        mode_latch = param.records.data_map{board_idx}(map_idx,2);
        subchannel = param.records.data_map{board_idx}(map_idx,3);
        profile = param.records.data_map{board_idx}(map_idx,1);
      end
      
      found_board = false;
      for configs_board_idx = 1:size(configs.adc,1)
        if isfield(configs.adc{configs_board_idx,mode_latch+1,subchannel+1},'name') ...
            && strcmpi(configs.adc{configs_board_idx,mode_latch+1,subchannel+1}.name, boards{board_idx});
          found_board = true;
          break;
        end
      end
      if ~found_board
        error('boards(%d)=%s not found in configs file\n', board_idx, boards{board_idx});
      end
      
      wfs(wf).num_sam = configs.adc{configs_board_idx,mode_latch+1,subchannel+1}.num_sam;
      wfs(wf).bit_shifts = param.radar.wfs(wf).bit_shifts;
      wfs(wf).t0 = param.radar.wfs(wf).Tadc;
      wfs(wf).presums = configs.adc{configs_board_idx,mode_latch+1,subchannel+1}.presums;
    end
  end

elseif any(param.records.file.version == [413 414])
  % UTUA RDS systems
  % BAS RDS systems
  
elseif any(param.records.file.version == [415])
  % UTIG RDS systems
  
else
  % NI, Rink, Paden, Leuschen, and Ledford systems

end

%% Save workspace in case there is a failure
records_create_save_workspace;

%% Correct time, sync GPS data, and save records
records_create_sync;

%% Create reference trajectory file
if param.records.gps.en
  records_reference_trajectory(param);
end

%% Create frames
% param.records.frames.mode == 0: Do nothing
if param.records.frames.mode == 1 % Autogenerate if needed and then manually edit
  frames_fn = ct_filename_support(param,'','frames');
  if ~exist(frames_fn,'file')
    frames_autogenerate(param,param_override);
  end
  obj = frames_create(param,param_override);
elseif param.records.frames.mode >= 2 % Autogenerate the frames file
  frames_autogenerate(param,param_override);
end

diary off;
fprintf('Console output: %s\n', command_window_out_fn);

