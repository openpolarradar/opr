% records_create_sync
%
% Script called from records_create
%
% To run this in debug mode, you need to set the debug setup section in
% this file and then just run this as a script (must be run from ">>"
% prompt and not a debug "K>>" prompt).
%
% Author: John Paden

%% Debug Setup (for running records_create_sync directly)
% =====================================================================
dbstack_info = dbstack;
if ~exist('param','var') || isempty(param) || length(dbstack_info) == 1
  new_param = read_param_xls(ct_filename_param('rds_param_2018_Antarctica_Ground.xls'),'20181217_03');

  fn = ct_filename_ct_tmp(new_param,'','records','workspace');
  fn = [fn '.mat'];
  fprintf('Loading workspace %s (%s)\n', fn, datestr(now));
  if exist(fn,'file')
    load(fn);
  else
    error('Temporary records file does not exist');
  end
  
  % Update any parameters that the user changed in the spreadsheet
  param = merge_structs(param,new_param);
  clear new_param;
  
  clear('param_override');
  
  % Input checking
  if ~exist('param','var')
    error('A struct array of parameters must be passed in\n');
  end
  global gRadar;
  if exist('param_override','var')
    param_override = merge_structs(gRadar,param_override);
  else
    param_override = gRadar;
  end
  param = merge_structs(param, param_override);
  
  if any(param.records.file.version == [9 10 103 412])
    % Arena based systems
    h_fig = get_figures(3,true);
  end

end

fprintf('Running %s correction and gps sync (%s)\n', param.day_seg, datestr(now));

%% Input checks
% ======================================================================

if ~isfield(param.records,'manual_time_correct') || isempty(param.records.manual_time_correct)
  param.records.manual_time_correct = 0;
end

% Initialize notes string that will be stored in records file
radar_time_notes = '';
epri_notes = '';
clock_notes = '';

% Initialize records.settings
records.settings = [];

%% Align all boards using EPRI
% ======================================================================
if any(param.records.file.version == [9 10 103 412])
  % Arena based systems

  %% Align/Arena: Create output EPRI vector
  % 1. What do we expect from profile_cntr_latch and rel_time_cntr_latch?
  %    - Monotonically increasing
  %    - profile_cntr_latch increments by "presums" amount each time
  %    - rel_time_cntr_latch increments by "EPRI" clock counts each time
  %    - Consistent relationship between profile_cntr_latch and rel_time_cntr_latch
  % 2. What do we expect from pps_cntr_latch/pps_ftime_cntr_latch?
  %    - More likely to be bad or have problems than profile_cntr_latch and
  %    rel_time_cntr_latch
  %    - Monotonically increasing
  %    - pps_cntr_latch/pps_ftime_cntr_latch increments by "EPRI" time
  %    - Nearly consistent relationship with profile_cntr_latch and
  %    rel_time_cntr_latch, but can vary a little because it is not phase
  %    locked to these signals.
  figure(h_fig(1)); clf;
  h_axes = axes('parent',h_fig(1));
  figure(h_fig(2)); clf;
  h_axes(2) = axes('parent',h_fig(2));
  h_plot_legend = {};
  for board_idx = 1:length(boards)
    mask_nonmonotonic_pps = [false, diff(board_hdrs{board_idx}.pps_cntr_latch)<0];
    if any(mask_nonmonotonic_pps)
      warning('Board %d/%s: %d nonmonotonic records in board_hdrs{%d}.pps_cntr_latch', board_idx, boards{board_idx}, sum(mask_nonmonotonic_pps), board_idx);
    end
    
    mask_nonmonotonic = [false, diff(board_hdrs{board_idx}.profile_cntr_latch)<0];
    if any(mask_nonmonotonic)
      warning('Board %d/%s: %d nonmonotonic records in board_hdrs{%d}.profile_cntr_latch', board_idx, boards{board_idx}, sum(mask_nonmonotonic), board_idx);
    end
    
    mask_nonmonotonic = [false, diff(board_hdrs{board_idx}.rel_time_cntr_latch)<0];
    if any(mask_nonmonotonic)
      warning('Board %d/%s: %d nonmonotonic records in board_hdrs{%d}.rel_time_cntr_latch', board_idx, boards{board_idx}, sum(mask_nonmonotonic), board_idx);
    end

    day_seg_epoch = datenum_to_epoch(datenum(str2double(param.day_seg(1:4)),str2double(param.day_seg(5:6)),str2double(param.day_seg(7:8))));
    mask_day_seg_epoch = board_hdrs{board_idx}.pps_cntr_latch > day_seg_epoch + 2*86400 ...
      | board_hdrs{board_idx}.pps_cntr_latch < day_seg_epoch - 86400;
    if any(mask_day_seg_epoch)
      warning('Board %d/%s: %d bad timestamp records in board_hdrs{%d}.pps_cntr_latch', board_idx, boards{board_idx}, sum(mask_day_seg_epoch), board_idx);
    end

    % Estimate board param.records.file.clk from pps_ftime_cntr_latch
    clock_notes = [clock_notes sprintf('Max board_hdrs{%d}.pps_ftime_cntr_latch = %.12f (gives erroneous values if PPS does not reset ftime)\n', board_idx, max(board_hdrs{board_idx}.pps_ftime_cntr_latch))];
    clock_notes = [clock_notes sprintf('Robust mean of max_filt1 board_hdrs{%d}.pps_ftime_cntr_latch = %.12f\n', board_idx, mean_without_outliers(max_filt1(board_hdrs{board_idx}.pps_ftime_cntr_latch,501)))];

    h_plot(board_idx) = plot(h_axes(1),board_hdrs{board_idx}.pps_ftime_cntr_latch);
    h_plot_legend{board_idx} = sprintf('%d',board_idx);
    hold(h_axes(1),'on');
    grid(h_axes(1),'on');
    title(h_axes(1),sprintf('%s', [param.day_seg(1:8) '\' param.day_seg(9:end)]));
    xlabel(h_axes(1), 'Records');
    ylabel(h_axes(1), 'pps\_ftime\_cntr\_latch (counts)');
    ylim(h_axes(1),[0 param.records.file.clk*1.1]);
    zoom(h_axes(1), 'reset');

    plot(h_axes(2),board_hdrs{board_idx}.profile_cntr_latch,board_hdrs{board_idx}.rel_time_cntr_latch/param.records.file.clk,'b');
    hold(h_axes(2),'on');
    grid(h_axes(2),'on');
    title(h_axes(2),sprintf('%s', [param.day_seg(1:8) '\' param.day_seg(9:end)]));
    pps_cntr_latch_new = board_hdrs{board_idx}.pps_cntr_latch + board_hdrs{board_idx}.pps_ftime_cntr_latch/param.records.file.clk;
    plot(h_axes(2),board_hdrs{board_idx}.profile_cntr_latch,pps_cntr_latch_new,'r');
    xlabel(h_axes(2), 'profile\_cntr\_latch');
    ylabel(h_axes(2), 'Time (sec)');
    legend_boards_str = sprintf(' %d', 1:board_idx);
    legend(h_axes(2),{['rel\_time\_cntr\_latch boards' legend_boards_str],['pps\_cntr\_latch' legend_boards_str]},'location','best');

    % Fix pps_cntr_latch and pps_ftime_cntr_latch using rel_time (requires
    % that rel_time is ~perfect).
    if any(mask_nonmonotonic_pps) || any(mask_day_seg_epoch)
      warning('Board %d/%s: fixing board_hdrs{%d}.pps_cntr_latch', board_idx, boards{board_idx}, board_idx);
      clock_notes = [clock_notes sprintf('Board %d/%s: fixing board_hdrs{%d}.pps_cntr_latch\n', board_idx, boards{board_idx}, board_idx)];
      pps_cntr_latch_new = board_hdrs{board_idx}.pps_cntr_latch + board_hdrs{board_idx}.pps_ftime_cntr_latch/param.records.file.clk;
      pps_cntr_latch_new(mask_day_seg_epoch) = NaN;
      if 0
        % Debug plots
        figure(8); clf;
        plot(pps_cntr_latch_new)
        hold on
        plot(board_hdrs{board_idx}.rel_time_cntr_latch/param.records.file.clk)
      end
      rel_time_offset = nanmedian(board_hdrs{board_idx}.rel_time_cntr_latch/10e6 - pps_cntr_latch_new);
      rel_time = board_hdrs{board_idx}.rel_time_cntr_latch/param.records.file.clk - rel_time_offset;

      bad_mask = abs(rel_time - pps_cntr_latch_new) > 1;
      pps_cntr_latch_new(bad_mask) = NaN;
      pps_cntr_latch_new = merge_vectors(pps_cntr_latch_new, rel_time);
      if 0
        figure(8); clf;
        plot(board_hdrs{board_idx}.pps_cntr_latch,'.')
        hold on;
        plot(pps_cntr_latch_new)
        ylim([min(pps_cntr_latch_new) max(pps_cntr_latch_new)])
      end

      board_hdrs{board_idx}.pps_cntr_latch = floor(pps_cntr_latch_new);
      board_hdrs{board_idx}.pps_ftime_cntr_latch = (pps_cntr_latch_new - board_hdrs{board_idx}.pps_cntr_latch)*param.records.file.clk;
    end
  end

  % Save debug figures
  legend(h_axes(1), h_plot_legend)
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_pps_ftime_cntr_latch') '.jpg'];
  fprintf('Saving %s\n', fig_fn);
  fig_fn_dir = fileparts(fig_fn);
  if ~exist(fig_fn_dir,'dir')
    mkdir(fig_fn_dir);
  end
  ct_saveas(h_fig(1),fig_fn);
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_pps_ftime_cntr_latch') '.fig'];
  fprintf('Saving %s\n', fig_fn);
  ct_saveas(h_fig(1),fig_fn);

  fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_radar_clocks') '.jpg'];
  fprintf('Saving %s\n', fig_fn);
  ct_saveas(h_fig(2),fig_fn);
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_radar_clocks') '.fig'];
  fprintf('Saving %s\n', fig_fn);
  ct_saveas(h_fig(2),fig_fn);

  %% Align/Arena: Create output EPRI vector
  % When all boards are working properly, then every board will have the
  % same EPRI values (board_hdrs{board_idx}.profile_cntr_latch). When
  % digital errors occur, each board can have a different EPRI. The code
  % below tries to sync the boards as close as possible including detection
  % of missing records in some, but not all, of the boards.
  
  min_epri = inf;
  max_epri = -inf;
  epri_list = [];
  good_offsets = {};
  offset_scores = {};
  % epri_list: this will be the list of all epri values that do not get
  % removed by param.records.epri_jump_threshold in the following loop
  epri_list = [];
  % best_epri_list: epri_list elements which matched the "best" offset
  best_epri_list = [];
  % diff_epri: the median PRI number (profile_cntr_latch) difference
  % between consecutive EPRI headers. For most radar settings, the
  % difference should be constant, but with header errors or digital system
  % problems, the PRI number difference between consecutive EPRI headers
  % can take on any value. The median is used to avoid infrequency errors
  % from causing the diff_epri estimate from being incorrect.
  diff_epri = zeros(size(boards));
  special_epri_generation = false;
  for board_idx = 1:length(boards)
    % Cluster EPRI values
    
    % board_hdrs{board_idx}.profile_cntr_latch: vector of PRI numbers (one
    % for each header read in). The PRI number refers to the pulse number
    % which starts at 0 and increments by 1 for each transmitted pulse
    % (i.e. each step in the pulse sequence generator). With hardware
    % averaging, the PRI number in each header should skip by the amount of
    % pulses for all modes.
    epri_pri_idxs = board_hdrs{board_idx}.profile_cntr_latch;
    
    % epri_pri_idxs should be sorted. In case there are header errors, sort
    % the vector.
    [A,B] = sort(epri_pri_idxs);
    
    % Reference EPRI to the middle/median point in case there are header
    % errors.
    
    % Find the median PRI-number
    med = median(A);
    % Find the index into A associated with this median PRI-number
    [~,med_idx] = min(abs(A-med));
    % A: PRI-number relative to median
    A = A-med;
    dA = diff(A);
    % Look for first too large jump relative to the median in the second half of the segment
    % Mark as bad all records after this first too large jump
    bad_mask = false(size(B));
    bad_start_idx = find(dA(med_idx:end) > param.records.epri_jump_threshold,1);
    if ~isempty(bad_start_idx)
      bad_mask(med_idx+bad_start_idx:end) = true;
    end
    % Look for first too large jump relative to the median  in the first half of the segment
    % Mark as bad all records before this first too large jump
    bad_start_idx = find(dA(med_idx-1:-1:1) > param.records.epri_jump_threshold,1);
    if ~isempty(bad_start_idx)
      bad_mask(med_idx-bad_start_idx:-1:1) = true;
    end

    % Apply user manual mask_bad and mask_good from param.records.mask_good
    for mask_idx = 1:size(param.records.mask_bad{board_idx},1)
      param.records.mask_bad{board_idx}(~isfinite(param.records.mask_bad{board_idx})) = length(bad_mask);
      bad_mask(param.records.mask_bad{board_idx}(mask_idx,1):param.records.mask_bad{board_idx}(mask_idx,2)) = true;
    end
    for mask_idx = 1:size(param.records.mask_good{board_idx},1)
      param.records.mask_good{board_idx}(~isfinite(param.records.mask_good{board_idx})) = length(bad_mask);
      bad_mask(param.records.mask_good{board_idx}(mask_idx,1):param.records.mask_good{board_idx}(mask_idx,2)) = false;
    end
    
    figure(h_fig(1)); clf;
    h_axes(1) = axes('parent',h_fig(1));
    bad_idxs = find(bad_mask([1:med_idx-1,med_idx+1:end]));
    good_idxs = find(~bad_mask([1:med_idx-1,med_idx+1:end]));
    plot(h_axes(1),dA,'x');
    hold(h_axes(1),'on');
    if isempty(bad_idxs)
      plot(h_axes(1),NaN,'r.');
    else
      plot(h_axes(1),bad_idxs,dA(bad_idxs),'r.');
    end
    if isempty(good_idxs)
      plot(h_axes(1),NaN,'g.');
    else
    plot(h_axes(1),good_idxs,dA(good_idxs),'g.');
    end
    xlabel(h_axes(1),'Records');
    ylabel(h_axes(1),'EPRI jump')
    legend(h_axes(1),{'all','excluded','included'},'location','best')
    title(h_axes(1),sprintf('%s: EPRI jumps board %d; color indicates if the record\n is included or excluded from this segment', param.day_seg, board_idx),'interpreter','none')
    
    figure(h_fig(2)); clf;
    h_axes(2) = axes('parent',h_fig(2));
    plot(h_axes(2),A);
    hold(h_axes(2),'on');
    plot(h_axes(2),find(~bad_mask),A(~bad_mask),'.');
    xlabel(h_axes(2),'Records');
    ylabel(h_axes(2),'EPRI')
    title(h_axes(2),sprintf('%s: EPRI board %d', param.day_seg, board_idx),'interpreter','none')

    epri_notes = sprintf('Board %d dropped %d of %d records due to param.records.epri_jump_threshold. max jump is %d.\n', ...
      board_idx, sum(bad_mask), length(epri_pri_idxs), max(dA));
    
    warning('board %d: %d of %d records show bad out of range EPRI values. max jump is %d, param.records.epri_jump_threshold is %d', ...
      board_idx, sum(bad_mask), length(epri_pri_idxs), max(dA), param.records.epri_jump_threshold);

    fig_fn = [ct_filename_ct_tmp(param,'','records_create',sprintf('arena_epri_jump_dA_%d',board_idx)) '.jpg'];
    fprintf('Saving %s\n', fig_fn);
    fig_fn_dir = fileparts(fig_fn);
    if ~exist(fig_fn_dir,'dir')
      mkdir(fig_fn_dir);
    end
    ct_saveas(h_fig(1),fig_fn);
    fig_fn = [ct_filename_ct_tmp(param,'','records_create',sprintf('arena_epri_jump_dA_%d',board_idx)) '.fig'];
    fprintf('Saving %s\n', fig_fn);
    ct_saveas(h_fig(1),fig_fn);

    fig_fn = [ct_filename_ct_tmp(param,'','records_create',sprintf('arena_epri_jump_A_%d',board_idx)) '.jpg'];
    fprintf('Saving %s\n', fig_fn);
    fig_fn_dir = fileparts(fig_fn);
    if ~exist(fig_fn_dir,'dir')
      mkdir(fig_fn_dir);
    end
    ct_saveas(h_fig(2),fig_fn);
    fig_fn = [ct_filename_ct_tmp(param,'','records_create',sprintf('arena_epri_jump_A_%d',board_idx)) '.fig'];
    fprintf('Saving %s\n', fig_fn);
    ct_saveas(h_fig(2),fig_fn);

    if param.records.gps.en
      % Just for debugging to see the trajectory associated with the EPRI
      if strcmpi(param.records.arena.sync_radar_field,'pps')
        % If using pps_cntr to synchronize with GNSS, then use this setting
        radar_time = double(board_hdrs{board_idx}.pps_cntr_latch) ...
          + double(board_hdrs{board_idx}.pps_ftime_cntr_latch)/param.records.file.clk;
      elseif strcmpi(param.records.arena.sync_radar_field,'rel_time')
        % If using rel_time to synchronize with GNSS, then use this setting
        radar_time = double(board_hdrs{board_idx}.rel_time_cntr_latch)/param.records.file.clk;
      elseif strcmpi(param.records.arena.sync_radar_field,'profile')
        % If using profile_cntr to synchronize with GNSS, then use this setting
        radar_time = double(board_hdrs{board_idx}.profile_cntr_latch);
      end
      comp_time = [];
      tmp_records = [];
      tmp_records.raw.pps_cntr_latch = board_hdrs{board_idx}.pps_cntr_latch;
      tmp_records.raw.pps_ftime_cntr_latch = board_hdrs{board_idx}.pps_ftime_cntr_latch;
      tmp_records.raw.profile_cntr_latch = board_hdrs{board_idx}.profile_cntr_latch;
      tmp_records.raw.rel_time_cntr_latch = board_hdrs{board_idx}.rel_time_cntr_latch;
      tmp_records = records_create_sync_gps(param,tmp_records,radar_time,comp_time);

      figure(h_fig(3)); clf;
      h_axes(3) = axes('parent',h_fig(3));
      plot(h_axes(3),tmp_records.lon,tmp_records.lat,'.');
      hold(h_axes(3),'on');
      xlabel(h_axes(3),'Longitude (deg)');
      ylabel(h_axes(3),'Latitude (deg)')
      title(h_axes(3),sprintf('%s: Trajectory board %d', param.day_seg, board_idx),'interpreter','none')
      plot(h_axes(3),tmp_records.lon(~bad_mask),tmp_records.lat(~bad_mask),'.');

      fig_fn = [ct_filename_ct_tmp(param,'','records_create',sprintf('arena_epri_jump_gps_%d',board_idx)) '.jpg'];
      fprintf('Saving %s\n', fig_fn);
      fig_fn_dir = fileparts(fig_fn);
      if ~exist(fig_fn_dir,'dir')
        mkdir(fig_fn_dir);
      end
      ct_saveas(h_fig(3),fig_fn);
      fig_fn = [ct_filename_ct_tmp(param,'','records_create',sprintf('arena_epri_jump_gps_%d',board_idx)) '.fig'];
      fprintf('Saving %s\n', fig_fn);
      ct_saveas(h_fig(3),fig_fn);

      figure(h_fig(3)); clf;
      h_axes(3) = axes('parent',h_fig(3));
      tmp_along_track = geodetic_to_along_track(tmp_records.lon, tmp_records.lat);
      tmp_dx = diff(tmp_along_track);
      plot(h_axes(3),tmp_dx,'.');
      hold(h_axes(3),'on');
      xlabel(h_axes(3),'Records');
      ylabel(h_axes(3),'dx (m)')
      title(h_axes(3),sprintf('%s: Trajectory along-track stepsize board %d', param.day_seg, board_idx),'interpreter','none')
      plot(h_axes(3),good_idxs,tmp_dx(good_idxs),'.');

      fig_fn = [ct_filename_ct_tmp(param,'','records_create',sprintf('arena_epri_jump_dx_%d',board_idx)) '.jpg'];
      fprintf('Saving %s\n', fig_fn);
      fig_fn_dir = fileparts(fig_fn);
      if ~exist(fig_fn_dir,'dir')
        mkdir(fig_fn_dir);
      end
      ct_saveas(h_fig(3),fig_fn);
      fig_fn = [ct_filename_ct_tmp(param,'','records_create',sprintf('arena_epri_jump_dx_%d',board_idx)) '.fig'];
      fprintf('Saving %s\n', fig_fn);
      ct_saveas(h_fig(3),fig_fn);
    end

    % Drop all records before/after the first jump that occurs relative to
    % the median PRI-number. Account for the sorting that occurred.
    back_idxs = 1:length(B);
    back_idxs = back_idxs(B);
    epri_pri_idxs = epri_pri_idxs(back_idxs(logical(~bad_mask)));
    
    % Keep track of the smallest PRI-number and largest PRI-number
    min_epri = min(min_epri,min(epri_pri_idxs));
    max_epri = max(max_epri,max(epri_pri_idxs));
    
    % Store PRI-numbers captured by this board into list of all PRI-numbers
    epri_list(end+(1:length(epri_pri_idxs))) = epri_pri_idxs;
    
    % Find the median PRI-number jump between recorded records.
    diff_epri_pri_idxs = diff(epri_pri_idxs);
    diff_epri(board_idx) = median(diff_epri_pri_idxs);

    % Assume that each board records data at a constant rate so that when
    % no errors occur, the PRI number in epri_pri_idxs, skips by diff_epri
    % in every record. Digital system errors can cause a consistent non-PRI
    % number offset. For example, the following has a diff_epri of 20, but
    % midway a "phase-offset" occurs at 60 to 70.
    %   [0 20 40 60 70 90 110 130]
    % The following code tries to detect if this occurs in a segment by
    % aligning each possible phase-offset. In the above example,
    % phase-offsets of 0 (implying 20*N) and 10 (implying 10+20*N) would
    % match many records which would flag this segment as having a phase
    % offset that varies throughout the segment.
    %
    % The worst possible score is length(epri_pri_idxs). This implies that
    % not a single PRI number in epri_pri_idxs lined up.
    min_score = inf;
    best_offset = NaN;
    offsets = 0:diff_epri(board_idx);
    offset_scores{board_idx} = zeros(size(offsets));
    for offset_idx = 1:length(offsets)
      offset = offsets(offset_idx);
      offset_scores{board_idx}(offset_idx) = sum(abs(mod(epri_pri_idxs - offset,diff_epri(board_idx))) > 0);
      if offset_scores{board_idx}(offset_idx) < min_score
        min_score = offset_scores{board_idx}(offset_idx);
        best_offset = offset;
        best_epri_list = [best_epri_list epri_pri_idxs(mod(epri_pri_idxs-offset,diff_epri(board_idx))==0)];
      end
    end
    if 0
      % For debugging, plot this to find which PRI-numbers do not fall into
      % this pattern:
      plot(mod((epri_pri_idxs - best_offset)/diff_epri(board_idx),1) ~= 0);
      keyboard
    end
    % Keep track of any offsets that cause more than 1% of records to align
    % with the expected PRI numbers.
    good_offsets{board_idx} = find(offset_scores{board_idx} < length(epri_pri_idxs)*(1-0.01));
    if min_score > 0
      warning('For the best phase-offset of profile_cntr_latch, there are %d of %d records that do not align with the expected EPRI values.', min_score, length(epri_pri_idxs));
      if length(good_offsets{board_idx}) > 1
        warning('There is more than one phase-offset for profile_cntr_latch that matches at least 1% of the records. An ADC link error probably occured or incorrect pulse sequence radar settings were used since PRI-numbers do not follow a regular interval for the whole segment. Special generation of the EPRI vector is now enabled to help track changes in the phase-offset.')
        % special_epri_generation: this enables some code below that tries
        % to track the phase offset of the EPRI
        special_epri_generation = true;
      end
    end
  end
  % * Hopefully at this point, very large or very small EPRI values that are
  % in error have been removed.
  %
  % * epri_list contains a list of all PRI numbers that occurred in any
  % header
  %
  % * diff_epri contains the estimated EPRI jump for each board. This
  % number should be the same for each board. If not, there are too many
  % digital errors or the radar settings are not currently supported by the
  % processing.

  % Find the first EPRI with the most valid boards (if more than one EPRI
  % has the most valid boards, mode returns the smallest or first EPRI;
  % most of the EPRIs should have occured in each board and so most will
  % have occured in all the boards)
  master_epri = mode(best_epri_list);
  % Assume that each board records data at the EPRI/EPRF rate (i.e. all the
  % PRI-numbers align between boards and occur at a regular interval)
  if any(diff_epri ~= diff_epri(1))
    error('Inconsistent EPRI step size between boards. Should all be the same: %s', mat2str_generic(diff_epri));
  end
  epri = [fliplr(master_epri:-diff_epri(1):min_epri), master_epri+diff_epri(1):diff_epri(1):max_epri];
  %% Special generation of the EPRI vector
  if special_epri_generation
    % This code runs when there was more than one phase offset of the PRI
    % numbers that matched the profile_cntr_latch usually indicating there
    % was a digital system glitch. This special_epri_generation attempts to
    % track the phase offset changes. NOTE: This code does not work when
    % there are many digital system errors and/or the digital boards do not
    % have the same PRI numbers stored in profile_cntr_latch. If this is
    % the case, this code probably does not make things worse, but it does
    % not achieve the objective of being able to precisely estimate the PRI
    % numbers in profile_cntr_latch for the whole segment. In this case,
    % param.records.arena.epri_alignment_guard should generally be set to
    % the total_presums.
    epri_list = sort(epri_list);
    epri_list_idx = 1;
    epri_offset = 0;
    for epri_idx = 1:length(epri)
      total = 0;
      match = 0;
      epri(epri_idx) = epri(epri_idx) + epri_offset;
      if epri(epri_idx) >= max_epri
        % Break and truncate remaining epri values
        break;
      end
      % Find all records from all boards that occur within this EPRI period
      % Find any that exactly match the expected EPRI
      start_epri_list_idx = epri_list_idx;
      while epri_list(epri_list_idx) <= epri(epri_idx)
        total = total + 1;
        if epri_list(epri_list_idx) == epri(epri_idx)
          match = match + 1;
        end
        epri_list_idx = epri_list_idx + 1;
      end
      % An exact match for the EPRI was not found. If a consistent value is
      % found for another EPRI, then adjust the phase-offset. NOTE:
      % sometimes there are just a bunch of bad PRI number jumps and this
      % code just hops around a bunch trying to track the EPRI without
      % helping or hurting anything.
      if total > 0 && match == 0
        if all(epri_list(start_epri_list_idx:epri_list_idx-1) == epri_list(start_epri_list_idx))
          warning('Special generation of EPRI: EPRI offset changed at record %d.', epri_idx);
          epri_offset_correction = epri_list(start_epri_list_idx) - epri(epri_idx) + diff_epri(1);
          epri_offset = epri_offset + epri_offset_correction;
          epri(epri_idx) = epri(epri_idx) + epri_offset_correction;
        end
      end
    end
    % Truncate to the last valid entry
    epri = epri(1:epri_idx);
  end
  
  %% Align/Arena: Fill in missing records from each board
  records_pps_tmp = nan(size(epri));
  records.raw.profile_cntr_latch = nan(size(epri));
  records.raw.rel_time_cntr_latch = nan(size(epri));
  % param.records.arena.epri_alignment_guard: this is the number of pulses 
  % The EPRI alignment guard should never be larger than the
  % total_presums/2 since that would imply overlapping EPRI windows.
  param.records.arena.epri_alignment_guard = min(param.records.arena.epri_alignment_guard,total_presums/2);
  for board_idx = 1:length(boards)
    [profile_cntr_latch_sorted profile_cntr_latch_sorted_idxs] = sort(board_hdrs{board_idx}.profile_cntr_latch);
    if param.records.arena.epri_alignment_guard == 0
      % If the profile_cntr_latch values are perfectly aligned to multiples
      % of the EPRI, then this will work
      [~,out_idxs,in_idxs] = intersect(epri,board_hdrs{board_idx}.profile_cntr_latch);
    else
      % If the profile_cntr_latch values are not perfectly aligned, this
      % code tries to guess which records from each board are associated
      % with a particular EPRI.
      out_idxs = nan(size(epri));
      in_idxs = nan(size(epri));
      out_idx = 0;
      cur_idx = 2;
      for idx = 1:length(epri)
        while cur_idx < length(profile_cntr_latch_sorted) && profile_cntr_latch_sorted(cur_idx) < epri(idx)
          cur_idx = cur_idx+1;
        end
        if idx == 1
          d_epri_pre = min(param.records.arena.epri_alignment_guard, 0.5*(epri(idx+1)-epri(idx)));
        else
          d_epri_pre = min(param.records.arena.epri_alignment_guard, 0.5*(epri(idx)-epri(idx-1)));
        end
        if idx == length(epri)
          d_epri_post = min(param.records.arena.epri_alignment_guard, 0.5*(epri(idx)-epri(idx-1)));
        else
          d_epri_post = min(param.records.arena.epri_alignment_guard, 0.5*(epri(idx+1)-epri(idx)));
        end
        if profile_cntr_latch_sorted(cur_idx-1) > epri(idx)-d_epri_pre ...
            && profile_cntr_latch_sorted(cur_idx-1) <= epri(idx)+d_epri_post
          out_idx = out_idx + 1;
          out_idxs(out_idx) = idx;
          in_idxs(out_idx) = profile_cntr_latch_sorted_idxs(cur_idx-1);
        elseif profile_cntr_latch_sorted(cur_idx) > epri(idx)-d_epri_pre ...
            && profile_cntr_latch_sorted(cur_idx) <= epri(idx)+d_epri_post
          out_idx = out_idx + 1;
          out_idxs(out_idx) = idx;
          in_idxs(out_idx) = profile_cntr_latch_sorted_idxs(cur_idx);
          if cur_idx < length(profile_cntr_latch_sorted)
            cur_idx = cur_idx+1;
          end
        end
      end
      out_idxs = out_idxs(1:out_idx);
      in_idxs = in_idxs(1:out_idx);
    end
    
    if length(epri)-length(out_idxs) >= 0.01*length(epri)
      warning('Board %d is missing %d of %d records. Since this is more than 1%% of the records it could imply digital errors and that param.records.arena.epri_alignment_guard should probably be set to %d if it is not already.', board_idx, length(epri)-length(out_idxs), length(epri), total_presums/2);
    else
      fprintf('Board %d is missing %d of %d records.\n', board_idx, length(epri)-length(out_idxs), length(epri));
    end

    % offset: Missing records filled in with -2^31
    offset = zeros(size(epri),'int32');
    offset(:) = -2^31;
    offset(out_idxs) = board_hdrs{board_idx}.offset(in_idxs);
    board_hdrs{board_idx}.offset = offset;
    
    % file_idx: Missing records filled in with NaN
    file_idx = nan(size(epri));
    file_idx(out_idxs) = board_hdrs{board_idx}.file_idx(in_idxs);
    board_hdrs{board_idx}.file_idx = interp_finite(file_idx,[],'nearest');
    
    % Time stamps should be the same for each board. Each board should
    % have outputs for every EPRI. However, the whole point of this section
    % is to handle the case when each board worked for only a subset of
    % EPRIs due to digital errors. Since board_idxs == 1 writes to the
    % outputs first and subsequent boards only fill in EPRIs that have not
    % been filled in yet from lower board_idxs, the first board will
    % generally have the greatest effect on the actual time values used and
    % the last board will have the least effect on the actual time values
    % used. All the boards should produce the SAME values... however, it is
    % common for one board to have many more digital errors in its time fields than other boards. It
    % is best to list boards in param.records from best to worst because of this.
    if 0
      % 1. Determine which output time stamps have not been filled yet
      empty_output_idxs = find(isnan(records.raw.pps_cntr_latch));
      [new_out_idxs,new_out_idxs_idxs] = intersect(out_idxs,empty_output_idxs);
      new_in_idxs = in_idxs(new_out_idxs_idxs);
      % 2. Update the output time stamps that have not been filled yet for
      % which this board_idx has outputs
      records.raw.pps_cntr_latch(new_out_idxs) = board_hdrs{board_idx}.pps_cntr_latch(new_in_idxs);
      records.raw.pps_ftime_cntr_latch(new_out_idxs) = board_hdrs{board_idx}.pps_ftime_cntr_latch(new_in_idxs);
      records.raw.profile_cntr_latch(new_out_idxs) = board_hdrs{board_idx}.profile_cntr_latch(new_in_idxs);
      records.raw.rel_time_cntr_latch(new_out_idxs) = board_hdrs{board_idx}.rel_time_cntr_latch(new_in_idxs);
    else
      new_pps = nan(size(epri));
      new_pps(out_idxs) = board_hdrs{board_idx}.pps_cntr_latch(in_idxs) ...
        + board_hdrs{board_idx}.pps_ftime_cntr_latch(in_idxs) / param.records.file.clk;
      new_pps = merge_vectors(records_pps_tmp, new_pps, false);
      records_pps_tmp = new_pps;

      new_profile_cntr_latch = nan(size(epri));
      new_profile_cntr_latch(out_idxs) = board_hdrs{board_idx}.profile_cntr_latch(in_idxs);
      new_profile_cntr_latch = merge_vectors(records.raw.profile_cntr_latch, new_profile_cntr_latch, false);
      records.raw.profile_cntr_latch = new_profile_cntr_latch;

      new_rel_time_cntr_latch = nan(size(epri));
      new_rel_time_cntr_latch(out_idxs) = board_hdrs{board_idx}.rel_time_cntr_latch(in_idxs);
      new_rel_time_cntr_latch = merge_vectors(records.raw.rel_time_cntr_latch, new_rel_time_cntr_latch, false);
      records.raw.rel_time_cntr_latch = new_rel_time_cntr_latch;
    end
  end
  records.raw.pps_cntr_latch = floor(records_pps_tmp);
  records.raw.pps_ftime_cntr_latch = (records_pps_tmp - records.raw.pps_cntr_latch) * param.records.file.clk;
  
  records.raw.pps_cntr_latch = interp_finite(records.raw.pps_cntr_latch);
  records.raw.pps_ftime_cntr_latch = interp_finite(records.raw.pps_ftime_cntr_latch,[],@(xi,yi,xq) interp1(xi,yi,xq,'linear','extrap'),[],'interp');
  records.raw.profile_cntr_latch = interp_finite(records.raw.profile_cntr_latch,[],@(xi,yi,xq) interp1(xi,yi,xq,'linear','extrap'),[],'interp');
  records.raw.rel_time_cntr_latch = interp_finite(records.raw.rel_time_cntr_latch,[],@(xi,yi,xq) interp1(xi,yi,xq,'linear','extrap'),[],'interp');
  
  if strcmpi(param.records.arena.sync_radar_field,'pps')
    % 'pps'
    % If using pps_cntr to synchronize with GNSS, then use this setting
    radar_time = double(records.raw.pps_cntr_latch) ...
      + double(records.raw.pps_ftime_cntr_latch)/param.records.file.clk;
  elseif strcmpi(param.records.arena.sync_radar_field,'rel_time')
    % 'rel_time'
    % If using rel_time to synchronize with GNSS, then use this setting
    radar_time = double(records.raw.rel_time_cntr_latch)/param.records.file.clk;
  elseif strcmpi(param.records.arena.sync_radar_field,'profile')
    % 'profile'
    % If using profile_cntr to synchronize with GNSS, then use this setting
    radar_time = double(records.raw.profile_cntr_latch);
  end
  comp_time = [];

  clf(h_fig(3));
  h_axes = axes('parent',h_fig(3));
  h_plot = plot(h_axes, diff(radar_time));
  grid(h_axes,'on');
  title(h_axes,sprintf('%s:\nAll values should be greater than 0.', [param.day_seg(1:8) '\' param.day_seg(9:end)]));
  ylabel(h_axes,'diff(radar\_time)');
  xlabel(h_axes,'Record');
  hold(h_axes,'on');
  if any(diff(radar_time) < 0)
    warning('radar_time should be monotonically increasing, but there are %d records that are not. Applying simple HACK fix that linearly interpolates through any values that would cause time to go backwards.', sum(diff(radar_time) < 0));
    % Initial point before decrease is often the problem, so remove this
    % first
    bad_mask = diff(radar_time) < 0;
    bas_mask(1) = false;
    radar_time(bad_mask) = NaN;
    radar_time = interp_finite(radar_time,NaN);
    % Now remove any remaining negative time jumps
    last_idx = 1;
    for idx = 2:length(radar_time)
      if radar_time(idx) < radar_time(last_idx)
        radar_time(idx) = NaN;
      else
        last_idx = idx;
      end
    end
    radar_time = interp_finite(radar_time,NaN);
    % Plot corrected time
    h_plot(2) = plot(h_axes, diff(radar_time));
    legend(h_plot,{'Bad','Corrected'});
  end
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_nonmonotonic_dtime') '.jpg'];
  fprintf('Saving %s\n', fig_fn);
  fig_fn_dir = fileparts(fig_fn);
  if ~exist(fig_fn_dir,'dir')
    mkdir(fig_fn_dir);
  end
  ct_saveas(h_fig(3),fig_fn);
  fig_fn = [ct_filename_ct_tmp(param,'','records_create','arena_nonmonotonic_dtime') '.fig'];
  fprintf('Saving %s\n', fig_fn);
  ct_saveas(h_fig(3),fig_fn);

elseif any(param.records.file.version == [413 414])
  %% Align/UTUA RDS: Nothing required
  %% Align/BAS RDS: Nothing required
  
elseif any(param.records.file.version == [415])
  %% Align/UTIG RDS MARFA/HICARS: Nothing required
  board_idx = 1;
  board_hdrs{board_idx}.radar_time = medfilt1(board_hdrs{board_idx}.radar_time,5);
  board_hdrs{board_idx}.comp_time = medfilt1(board_hdrs{board_idx}.comp_time,5);
  
elseif any(param.records.file.version == [12 417])
  %% Align LDEO RDS DICE, SIR Snow: Nothing required
  board_idx = 1;
  board_hdrs{board_idx}.radar_time = medfilt1(board_hdrs{board_idx}.radar_time,5);
  board_hdrs{board_idx}.comp_time = medfilt1(board_hdrs{board_idx}.comp_time,5);
  
else
  %% Align/CReSIS: Create output EPRI vector
  min_epri = inf;
  max_epri = -inf;
  epri_list = [];
  for board_idx = 1:length(boards)
    % Cluster EPRI values
    
    epri_raw = double(board_hdrs{board_idx}.epri);
    
    [A,B] = sort(epri_raw);
    med = median(A);
    [~,med_idx] = min(abs(A-med));
    A = A-med;
    dA = diff(A);
    bad_mask = zeros(size(B));
    bad_start_idx = find(dA(med_idx:end) > param.records.epri_jump_threshold,1);
    if ~isempty(bad_start_idx)
      bad_mask(med_idx+bad_start_idx:end) = true;
    end
    bad_start_idx = find(dA(med_idx-1:-1:1) > param.records.epri_jump_threshold,1);
    if ~isempty(bad_start_idx)
      bad_mask(med_idx-bad_start_idx:-1:1) = true;
    end
    back_idxs = 1:length(B);
    back_idxs = back_idxs(B);
    if sum(bad_mask) > 0
      warning('%d of %d records show bad out of range EPRI values. max jump is %d, param.records.epri_jump_threshold is %d', sum(bad_mask), length(epri_raw), max(dA), param.records.epri_jump_threshold);
    end
    epri_raw = epri_raw(back_idxs(logical(~bad_mask)));
    
    % Remove isolated EPRI values
    min_epri = min(min_epri,min(epri_raw));
    max_epri = max(max_epri,max(epri_raw));
    epri_list(end+(1:length(epri_raw))) = epri_raw;
    diff_epri_raw = diff(epri_raw);
    diff_epri(board_idx) = median(diff_epri_raw);
    min_score = inf;
    for offset = 0:diff_epri(board_idx)
      score = sum(mod((epri_raw - offset)/diff_epri(board_idx),1) ~= 0);
      if score < min_score
        min_score = score;
      end
    end
    if min_score > 0
      warning('%d of %d records show slipped EPRI values.', min_score, length(epri_raw));
    end
  end
  master_epri = mode(epri_list);
  if any(diff_epri ~= diff_epri(1))
    error('Inconsistent EPRI step size between boards. Should all be the same: %s', mat2str_generic(diff_epri));
  end
  epri = [fliplr(master_epri:-diff_epri(1):min_epri), master_epri+diff_epri:diff_epri:max_epri];

  %% Align/CReSIS: Fill in missing records from each board
  records.raw.epri = nan(size(epri));
  records.raw.seconds = nan(size(epri));
  records.raw.fraction = nan(size(epri));
  if param.records.file.version == 8
    records.settings.nyquist_zone_sig = nan(size(epri));
    records.settings.waveform_ID = nan(size(epri));
  end
  for board_idx = 1:length(boards)
    [~,out_idxs,in_idxs] = intersect(epri,board_hdrs{board_idx}.epri);
    fprintf('Board %d is missing %d of %d records.\n', board_idx, length(epri)-length(out_idxs), length(epri));
    
    % offset: Missing records filled in with -2^31
    offset = zeros(size(epri),'int32');
    offset(:) = -2^31;
    offset(out_idxs) = board_hdrs{board_idx}.offset(in_idxs);
    board_hdrs{board_idx}.offset = offset;
    
    % file_idx: Missing records filled in with NaN
    file_idx = nan(size(epri));
    file_idx(out_idxs) = board_hdrs{board_idx}.file_idx(in_idxs);
    board_hdrs{board_idx}.file_idx = interp_finite(file_idx,[],'nearest');
    
    % Time stamps are assumed to be the same from each board so each board
    % just writes all of its time stamps to the output records fields.
    records.raw.epri(out_idxs) = board_hdrs{board_idx}.epri(in_idxs);
    if board_idx > 1 && length(param.records.gps.time_offset) == 1
      % Old parameter spreadsheet format only contained a single entry for
      % all boards in param.records.gps.time_offset
      records.raw.seconds(out_idxs) = board_hdrs{board_idx}.seconds(in_idxs) ...
        + max(param.records.gps.time_offset) - param.records.gps.time_offset;
    else
      records.raw.seconds(out_idxs) = board_hdrs{board_idx}.seconds(in_idxs) ...
        + max(param.records.gps.time_offset) - param.records.gps.time_offset(board_idx);
    end
    records.raw.fraction(out_idxs) = board_hdrs{board_idx}.fraction(in_idxs);
    if param.records.file.version == 8
      records.settings.nyquist_zone_sig(out_idxs) = board_hdrs{board_idx}.nyquist_zone(in_idxs);
      records.settings.waveform_ID(out_idxs) = board_hdrs{board_idx}.waveform_ID(in_idxs);
    end
  end
  records.raw.epri = interp_finite(records.raw.epri);
  records.raw.seconds = interp_finite(records.raw.seconds);
  records.raw.fraction = interp_finite(records.raw.fraction);

  utc_time_sod = double(records.raw.seconds) + double(records.raw.fraction) / param.records.file.clk;
  comp_time = [];
end

%% Correct radar time with EPRI
% ===================================================================
if any(param.records.file.version == [9 10 103 412])
  if param.records.arena.radar_time_correction_en
    %% Radar time: Arena
    epri_time = epri/param.radar.prf;
    radar_time_error = epri_time - radar_time;
    epri_time = epri_time - median(radar_time_error);
    radar_time_error = radar_time_error - mean_without_outliers(diff(radar_time_error)) * (0:length(radar_time_error)-1);
    radar_time_error = radar_time_error - median(radar_time_error);

    % Debug code
    figure(h_fig(1)); clf;
    plot(radar_time_error);
    title(sprintf('%s: radar_time_error: trends should be ignored\ndiscontinuities should be fixed', param.day_seg),'interpreter','none')

    % This code is mostly looking for outliers of radar_time based on the
    % expected time generated from the EPRI sequence. If radar_time deviates
    % a lot, then we assume there could be an error in radar_time.
    % epri_time_error_threshold: units of seconds
    bad_idxs = find(abs(radar_time_error) > param.records.arena.epri_time_error_threshold);

    % Replace all values that are marked as bad (that deviated too much)
    old_radar_time = radar_time;
    mask = false(size(radar_time));
    mask(bad_idxs) = true;
    first_good_idx = find(mask==false,1);
    if ~isempty(first_good_idx)
      % Correct start
      mask(1:first_good_idx-1) = false;
      radar_time(1:first_good_idx-1) = radar_time(first_good_idx) - epri_time(first_good_idx) + epri_time(1:first_good_idx-1);
      % Correct End
      last_good_idx = find(mask==false,1,'last');
      mask(last_good_idx+1:end) = false;
      radar_time(last_good_idx+1:end) = radar_time(last_good_idx) - epri_time(last_good_idx) + epri_time(last_good_idx+1:end);
      % Correct Middle
      radar_time(mask) = NaN;
      radar_time = interp_finite(radar_time);
    end

    % Debug code
    figure(h_fig(2)); clf;
    plot(radar_time);
    ylims = ylim;
    hold on;
    plot(bad_idxs,radar_time(bad_idxs),'x');
    plot(old_radar_time)
    ylim(ylims)
    ylabel('radar\_time');
    legend({'corrected','corrected points','original'},'location','best')
    title(sprintf('%s: radar_time corrected and original', param.day_seg),'interpreter','none')

    figure(h_fig(3)); clf;
    plot(diff(radar_time));
    title(sprintf('%s: diff(radar_time)', param.day_seg),'interpreter','none')

    if (first_good_idx > 100 || last_good_idx < length(epri_time)-99)
      % Many bad records at start or end often indicate there is a trend in
      % radar_time that should be kept and NOT corrected and so the
      % param.records.arena.epri_time_error_threshold value should be
      % increased and records_create run again so that the correction does
      % not occur.
      warning('param.records.arena.epri_time_error_threshold is %g. Inspect plots, if radar_time_error is just a trend, then consider increasing param.records.arena.epri_time_error_threshold to avoid triggering the correction. If radar_time_error has large discontinuities, then the correction should probably be triggered.', param.records.arena.epri_time_error_threshold);
      keyboard
    end
  end
  
elseif any(param.records.file.version == [413 414])
  %% Radar time: UTUA RDS
  % Nothing to be done
  board_idx = 1;
  radar_time = board_hdrs{board_idx}.gps_time;
  comp_time = [];
  
elseif any(param.records.file.version == [416])
  %% Radar time: St. Olaf HF RDS
  % Nothing to be done
  board_idx = 1;
  radar_time = board_hdrs{board_idx}.radar_time;
  comp_time = board_hdrs{board_idx}.comp_time;
  
elseif any(param.records.file.version == [12 415 417])
  %% Radar time: UTIG RDS
  % Nothing to be done
  board_idx = 1;
  radar_time = board_hdrs{board_idx}.radar_time;
  comp_time = board_hdrs{board_idx}.comp_time;
  
elseif any(param.records.file.version == [1 2 3 4 5 6 7 8 11 13 14 101 403 407 408 418 420 421 422 423])
  %% Radar time: CReSIS or CECS
 
  if 0
    % Test sequences
    utc_time_sod = [0 1 2 3 10000 5 6 7 8 9 10 11 12 13 24 25 26 27 28 19 20 21 22 19 20 21 22]
    utc_time_sod = utc_time_sod + 0.0001*randn(size(utc_time_sod))
    epri = 100 + [1:23, 20:23]
    epri(15) = 5000;
  end
  
  % Estimate the pulse repetition interval, PRI
  PRI = median(diff(utc_time_sod));
  
  % Create an EPRI sequence from the time record
  time_epri = utc_time_sod / PRI;
  [~,good_time_idx] = min(abs(utc_time_sod - median(utc_time_sod)));
  time_epri = time_epri - time_epri(good_time_idx);
  
  % Find the difference of the time-generated epri and the recorded epri
  dtime_epri = diff(time_epri);
  depri = diff(epri);
  
  % Find good/bad differences. Mask values are:
  %  0: both differences are bad
  %  1: EPRI good
  %  2: Time-generated EPRI good
  %  3: EPRI and time-generated EPRI good
  dtime_epri_threshold = 0.1; % Allow for 10% PRI error
  mask = (depri == 1) + (2*(abs(dtime_epri-1) < dtime_epri_threshold));
  % If the EPRI's both indicate the same number of skipped records,
  % consider it a good difference.
  mask(mask ~= 3 & depri == round(dtime_epri)) = 3;
  
  % Fix differenced time-generated EPRIs using differenced EPRIs
  dtime_epri(mask==1) = depri(mask==1);
  % Fix differenced EPRIs using differenced time-generated EPRIs
  depri(mask==2) = round(dtime_epri(mask==2));
  
  % Find sequences of good records (where mask > 0) and deal with each
  % segment separately.
  good_out_mask = false(size(utc_time_sod));
  start_idx = find(mask ~= 0,1);
  while ~isempty(start_idx)
    stop_idx = start_idx-1 + find(mask(start_idx+1:end) == 0,1);
    if isempty(stop_idx)
      stop_idx = numel(mask);
    end
    
    % Find a median point in each segment and assume this value is good
    [~,good_time_idx] = min(abs(utc_time_sod(start_idx:stop_idx+1) - median(utc_time_sod(start_idx:stop_idx+1))));
    [~,good_epri_idx] = min(abs(epri(start_idx:stop_idx+1) - median(epri(start_idx:stop_idx+1))));
    
    % Reconstruct epri
    tmp = [0 cumsum(depri(start_idx:stop_idx))];
    tmp = tmp - tmp(good_epri_idx) + epri(start_idx-1+good_epri_idx);
    epri_new(start_idx:stop_idx+1) = tmp;
    
    % Reconstruct time from time-generated EPRIs
    tmp = [0 cumsum(dtime_epri(start_idx:stop_idx))*PRI];
    tmp = tmp - tmp(good_time_idx) + utc_time_sod(start_idx-1+good_time_idx);
    utc_time_sod_new(start_idx:stop_idx+1) = tmp;
    
    % Mark these records as good outputs
    good_out_mask(start_idx:stop_idx+1) = true;
    
    % Find the next sequence
    start_idx = stop_idx + find(mask(stop_idx+1:end) ~= 0,1);
  end
  
  utc_time_sod = utc_time_sod_new;
  
  % Check for day wraps in the UTC time seconds of day
  day_wrap_idxs = find(diff(utc_time_sod) < -50000);
  day_wrap_offset = zeros(size(utc_time_sod));
  for day_wrap_idx = day_wrap_idxs
    day_wrap_offset(day_wrap_idx+1:end) = day_wrap_offset(day_wrap_idx+1:end) + 86400;
  end
  utc_time_sod = utc_time_sod + day_wrap_offset;
  
  radar_time = utc_time_sod;
end

%% Correlate GPS with radar data
% ===================================================================
if param.records.gps.en
  records = records_create_sync_gps(param,records,radar_time,comp_time);
  
else
  records.gps_time = NaN*zeros(size(radar_time));
  records.lat = NaN*zeros(size(radar_time));
  records.lon = NaN*zeros(size(radar_time));
  records.elev = NaN*zeros(size(radar_time));
  records.roll = NaN*zeros(size(radar_time));
  records.pitch = NaN*zeros(size(radar_time));
  records.heading = NaN*zeros(size(radar_time));
  records.gps_source = 'NA';
end

%% Save records files
% =====================================================================

% Save concatenated files in records directories after time fix
records_fn = ct_filename_support(param,'','records');
[records_fn_dir records_fn_name] = fileparts(records_fn);
if ~exist(records_fn_dir,'dir')
  fprintf('Output directory %s does not exist, creating...\n', records_fn_dir);
  mkdir(records_fn_dir);
end

% Standard Fields
% records
%  .lat
%  .lon
%  .elev
%  .roll
%  .pitch
%  .heading
%  .gps_time
%  .gps_source
records.relative_filename = [];
records.relative_rec_num = [];
records.offset = [];
for board_idx = 1:length(board_hdrs)
  unique_file_idxs = unique(board_hdrs{board_idx}.file_idx);
  base_dir = get_segment_file_list(param,board_idx);
  for file_idx = 1:length(unique_file_idxs)
    records.relative_rec_num{board_idx}(file_idx) = find(board_hdrs{board_idx}.file_idx == unique_file_idxs(file_idx),1);
    [fn_dir fn_name fn_ext] = fileparts(board_fns{board_idx}{unique_file_idxs(file_idx)});
    relative_path = fn_dir(length(fullfile([base_dir filesep])):end);
    records.relative_filename{board_idx}{file_idx} = fullfile(relative_path,[fn_name fn_ext]);
  end
  records.offset(board_idx,:) = board_hdrs{board_idx}.offset;
end
records.bit_mask = zeros(size(records.offset),'uint8');
records.radar_name = param.radar_name;
if param.ct_file_lock
  records.file_version = '1L';
else
  records.file_version = '1';
end
records.file_type = 'records';

% Create the first entry in the records.settings field
records.settings.wfs_records = 1;
records.settings.wfs = wfs;

records.notes = cat(2,sprintf('\nEPRI NOTES\n%s',epri_notes), ...
  sprintf('\nCLOCK NOTES\n%s',clock_notes), ...
  sprintf('\nRADAR TIME NOTES\n%s',radar_time_notes));
records.param_records = param;

fprintf('Saving records file %s (%s)\n',records_fn,datestr(now));
ct_file_lock_check(records_fn,3);
ct_save(records_fn,'-v7.3','-struct','records');
