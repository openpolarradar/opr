function [hdr,data] = basic_load_vapor(fn,param)
% [hdr,data] = basic_load_vapor(fn,param)
%
% fn = '/cresis/snfs1/data/HF_Sounder/2022_Greenland_Vapor/data/07272022/test98_00000.dat';
% [hdr,data] = hfrds.basic_load_vapor(fn,param);

stop_idx = 8192; start_idx = 9;     %Define start and stop indices for data, header is first 8 lines

%% Open file little-endian for reading
[fid,msg] = fopen(fn,'rb','ieee-le');
if fid < 1
  fprintf('Could not open file %s\n', fn);
  error(msg);
end

x =  fread(fid,[8192,1000],'int16');

fclose(fid);

data = x(start_idx:stop_idx,:);
header = x(1:start_idx-1,:);

%convert int16 to uint16
mask = header < 0;
header = header + mask*65536;
%convert two uint16s to uint32;
header = header(1:2:7,:) + 65536*header(2:2:8,:);

% Reset/clear hdr struct
hdr = [];

hdr.frame_sync = header(1,:); %should always be 1212568132 = 0x48465244
hdr.epri = header(2,:); %increments one per record
time = header(3,:); %time of day in binary coded decimal
hour = bitand(bitshift(time,-20),15)*10 + bitand(bitshift(time,-16),15);
min =  bitand(bitshift(time,-12),15)*10 + bitand(bitshift(time,-8),15);
sec =  bitand(bitshift(time,-4),15)*10 + bitand(bitshift(time,-0),15);
hdr.fractions = header(4,:);
hdr.seconds = 3600*hour + 60*min + sec;
hdr.utc_time_sod = hdr.seconds + hdr.fractions/100e6; %seconds of day including fraction

hdr.stop_idx = stop_idx;
hdr.start_idx = start_idx;
hdr.presums = 64;

% Raw data
hdr.wfs(1).num_sam = hdr.stop_idx - hdr.start_idx;
