function success = preprocess_task_stolaf(param)
% success = preprocess_task_stolaf(param)
%
% Reads St. Olaf radar data files. Support function for preprocess_task.m
%
% Author: John Paden
%
% See also: storead_script_v36.m

% =========================================================================
%% Get Data File list for this board
board_folder_name = fullfile(param.preprocess.board_folder_names);

get_filenames_param = struct('regexp',param.preprocess.file.regexp);
fns = get_filenames(fullfile(param.preprocess.base_dir,board_folder_name), ...
  param.preprocess.file.prefix, param.preprocess.file.midfix, ...
  param.preprocess.file.suffix, get_filenames_param);

%% Iterate packet_strip through file list
out = [];
out.fns = {};
out.fns_hdrs = {};
out.file_idxs = [];
out.gps_time = [];
out.lat = [];
out.lon = [];
out.elev = [];
out.offset = [];
out_file_idx = 0;
for fn_idx = 1:length(fns)
  fn = fullfile(fns{fn_idx});
  fn_info = dir(fn);

  [fn_dir,fn_name,fn_ext] = fileparts(fn);

  % Print status
  fprintf('preprocess_task_stolaf %d/%d %s\n    %s\n', fn_idx, length(fns), fn, datestr(now));

  pathname = {[fn_dir filesep]};
  filename = {[fn_name fn_ext]};
  ii = 1;
  try
    [s, s_info] = storead_script_v36(pathname,filename,ii);

    % Concatenate outputs
    out_file_idx = out_file_idx + 1;
    out.fns{out_file_idx} = fn;
    out.fns_hdrs{out_file_idx} = s_info;

    out.file_idxs = cat(2,out.file_idxs,fn_idx*ones([1 length(s(1).Time)]));
    out.gps_time...
      = cat(2,out.gps_time,reshape(s(1).Time,[1 length(s(1).Time)]));
    out.lat...
      = cat(2,out.lat,reshape(s(1).Latitude,[1 length(s(1).Latitude)]));
    out.lon...
      = cat(2,out.lon,reshape(s(1).Longitude,[1 length(s(1).Longitude)]));
    out.elev...
      = cat(2,out.elev,reshape(s(1).Altitude,[1 length(s(1).Altitude)]));
    out.offset ...
      = cat(2,out.offset,reshape(s(1).offset,[1 length(s(1).offset)]));
  catch ME
    fprintf('File reading failed with the following info and error:\n')
    fprintf('%s\n','='*ones(1,80))
    fn_info
    ME.getReport
    fprintf('%s\n','='*ones(1,80))
  end

end

%% Sort outputs by gps_time
%  -- Assumes gps_time has no errors --
[out.gps_time,sort_idxs] = sort(out.gps_time);
out.file_idxs = out.file_idxs(sort_idxs);
out.lat = out.lat(sort_idxs);
out.lon = out.lon(sort_idxs);
out.elev = out.elev(sort_idxs);
out.offset = out.offset(sort_idxs);

%% Save outputs
% Create output filename
fn = out.fns{1};
fn_info = dir(fn);
out_fn = ct_filename_ct_tmp(param,'','headers', ...
  fullfile(board_folder_name, fn_name));
[out_fn_dir,out_fn_name] = fileparts(out_fn);
out_hdr_fn = fullfile(out_fn_dir,[out_fn_name,'.mat']);

% Look for along-track gaps in the recording
out.along_track = geodetic_to_along_track(out.lat,out.lon);
diff_along_track = diff(out.along_track);

% Create segments for gaps larger than some threshold
along_track_gaps = find(diff_along_track > 250);

% Record information about each segment
segs = [];
start_rec = 1;
if 1
  clf;
  plot(out.lon, out.lat, 'b.');
  hold on;
  grid on;
end
for gap_idx = 1:length(along_track_gaps)
  end_rec = along_track_gaps(gap_idx);
  % Record a new segment
  seg_idx = length(segs)+1;
  segs(seg_idx).day_wrap_offset = 0;
  segs(seg_idx).start_idxs = start_rec;
  segs(seg_idx).stop_idxs = end_rec;
  stats.on_time(seg_idx) = out.gps_time(end_rec) - out.gps_time(start_rec);
  if 1
    plot(out.lon(start_rec), out.lat(start_rec), 'go');
    plot(out.lon(end_rec), out.lat(end_rec), 'r+');
  end
  start_rec = along_track_gaps(gap_idx)+1;
end
end_rec = length(out.along_track);
% Record a new segment
seg_idx = length(segs)+1;
segs(seg_idx).day_wrap_offset = 0;
segs(seg_idx).start_idxs = start_rec;
segs(seg_idx).stop_idxs = end_rec;
stats.on_time(seg_idx) = out.gps_time(end_rec) - out.gps_time(start_rec);
if 1
  plot(out.lon(start_rec), out.lat(start_rec), 'go');
  plot(out.lon(end_rec), out.lat(end_rec), 'r+');
end

if 1
  % Debug: Test Code
  for seg_idx = 1:length(segs)
    fprintf('Segment %d\n', seg_idx);
    disp(segs(seg_idx))
  end

  fprintf('On time: %g\n', sum(stats.on_time));
  fprintf('Seg\tOn%%\tOn');
  fprintf('\n');

  for seg_idx = 1:length(segs)
    fprintf('%d\t%.0f%%\t%.1g', seg_idx, stats.on_time(seg_idx)/sum(stats.on_time)*100, stats.on_time(seg_idx));
    fprintf('\n');
  end
end

% Create the parameters to output
oparams = {};
[~,defaults] = param.preprocess.default();
for seg_idx = 1:length(segs)
  segment = segs(seg_idx);

  % Determine which default parameters to use: since there is no
  % configuration data stored with the radar data, the "defaults" parameter
  % structure index match, match_idx, is hard coded.
  % =======================================================================
  match_idx = 1;

  oparams{end+1} = defaults{match_idx};
  oparams{end} = rmfield(oparams{end},'config_regexp');
  oparams{end} = rmfield(oparams{end},'name');

  % Parameter spreadsheet
  % =======================================================================
  oparams{end}.day_seg = sprintf('%s_%02d',param.preprocess.date_str,seg_idx);
  oparams{end}.cmd.notes = defaults{match_idx}.name;

  oparams{end}.records.file.idxs = param;

  oparams{end}.records.gps.time_offset = param.records.gps.time_offset + segment.day_wrap_offset;

  oparams{end}.records.file.base_dir = param.preprocess.base_dir;
  oparams{end}.records.file.board_folder_name = param.preprocess.board_folder_names;
  if ~isempty(oparams{end}.records.file.board_folder_name) ...
      && oparams{end}.records.file.board_folder_name(1) ~= filesep
    % Ensures that board_folder_name is not a text number which Excel
    % will misinterpret as a numeric type
    oparams{end}.records.file.board_folder_name = ['/' oparams{end}.records.file.board_folder_name];
  end
  if ~isnan(str2double(oparams{end}.records.file.board_folder_name))
    oparams{end}.records.file.board_folder_name = ['/' oparams{end}.records.file.board_folder_name];
  end
  oparams{end}.records.file.boards = param.records.file.boards;
  oparams{end}.records.file.version = param.records.file.version;
  oparams{end}.records.file.prefix = param.records.file.prefix;
  oparams{end}.records.file.clk = param.records.file.clk;
end

%% Print out segments
% =========================================================================
if ~exist(param.preprocess.param_fn,'file')
  warning('Could not find parameter spreadsheet file so not printing parameter values.\n  param.preprocess.param_fn = ''%s''', param.preprocess.param_fn);
else
  % Print parameter spreadsheet values to stdout and param_txt_fn
  % =========================================================================
  fid = 1;
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  cmd\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'cmd',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  records\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'records',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  qlook\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'qlook',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  sar\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'sar',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  array\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'array',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  radar\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'radar',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  post\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'post',oparams,fid);
  % Other sheets
  warning off MATLAB:xlsfinfo:ActiveX
  [status, sheets] = xlsfinfo(param.preprocess.param_fn);
  warning on MATLAB:xlsfinfo:ActiveX
  for sheet_idx = 1:length(sheets)
    if ~any(strcmpi(sheets{sheet_idx},{'cmd','records','qlook','sar','array','radar','post'}))
      fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  %s\n', sheets{sheet_idx}); fprintf(fid,'%s</strong>\n','='*ones(1,80));
      read_param_xls_print(param.preprocess.param_fn,sheets{sheet_idx},oparams,fid);
    end
  end
  fprintf(fid,'\n');
  
  param_txt_fn = ct_filename_ct_tmp(param,'','param', [param.preprocess.date_str,'.txt']);
  fprintf('Writing %s\n\n', param_txt_fn);
  param_txt_fn_dir = fileparts(param_txt_fn);
  if ~exist(param_txt_fn_dir,'dir')
    mkdir(param_txt_fn_dir);
  end
  [fid,msg] = fopen(param_txt_fn,'wb');
  if fid<0
    error('Could not write to %s: %s\n', param_txt_fn, msg);
  end
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  cmd\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'cmd',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  records\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'records',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  qlook\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'qlook',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  sar\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'sar',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  array\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'array',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  radar\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'radar',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  post\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'post',oparams,fid);
  fprintf(fid,'\n');
  % Other sheets
  warning off MATLAB:xlsfinfo:ActiveX
  [status, sheets] = xlsfinfo(param.preprocess.param_fn);
  warning on MATLAB:xlsfinfo:ActiveX
  for sheet_idx = 1:length(sheets)
    if ~any(strcmpi(sheets{sheet_idx},{'cmd','records','qlook','sar','array','radar','post'}))
      fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  %s\n', sheets{sheet_idx}); fprintf(fid,'%s</strong>\n','='*ones(1,80));
      read_param_xls_print(param.preprocess.param_fn,sheets{sheet_idx},oparams,fid);
    end
  end
  fprintf(fid,'\n');
  fclose(fid);
end

%% Exit task
% =========================================================================
fprintf('%s done %s\n', mfilename, datestr(now));

success = true;
