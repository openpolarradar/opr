function ct_saveas(fig,fn,varargin)
% ct_saveas(fig,fn,varargin)
%
% Function calls saveas, but adds a creation function callback to force the
% "Visible" property to "on" when the figure is opened. This is useful when
% creating invisible figures and saving them.

% Input checks
global gRadar;
if ~isfield(gRadar,'min_disk_space') || isempty(gRadar.min_disk_space)
  min_disk_space = 10e9;
else
  min_disk_space = gRadar.min_disk_space;
end

% Get the free space where the file is going to be stored
free = get_disk_space(fn);

% Assume free == 0 is an error in get_disk_space and so ignore it because
% the save will fail anyway if this is the case.
if free > 0 && free < min_disk_space
  error('Insufficient disk space (%g MB free, minimum allowed % MB).', free/1e6, min_disk_space/1e6);
end

% Check that output directory exists
fn_dir = fileparts(fn);
if ~isempty(fn_dir) && ~exist(fn_dir,'dir')
  mkdir(fn_dir);
end

% Set the create creation function to force the visibility to on when
% opening.
try
  set(fig, 'CreateFcn', 'set(gcbo,''Visible'',''on'')');
end
saveas(fig,fn,varargin{:});
