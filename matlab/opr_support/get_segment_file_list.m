function [base_dir,board_folder_name,fns,file_idxs] = get_segment_file_list(param,board_idx)
% [base_dir,board_folder_name,fns,file_idxs] = get_segment_file_list(param,board_idx)
%
% Support function for records_create.m.
% Can also be used to get all the file information for every segment
% using run_get_segment_file_list.m.
%
% param: struct from param spreadsheet read in by read_param_xls.
% See input checks section for description of each of the inputs that are required.
%
% board_idx: Index into 
%
% Author: John Paden
%
% See also: get_frame_id, get_raw_files.m, get_segment_file_list.m,
%   run_get_segment_file_list.m

%% Input checks

%% Input checks: param.records.file

if ~exist('param','var') || ~isfield(param,'records') || ~isfield(param.records,'file')
  error('MATLAB:get_segment_file_list:missingField','Input argument param.records.file must exist.')
end

%  .base_dir: String. Default ''. Base directory for all data files.
if ~isfield(param.records.file,'base_dir') || isempty(param.records.file.base_dir)
  param.records.file.base_dir = '';
end

%  .board_folder_name: String. Default ''. board folder name (%b in the
%  filename will be replaced by the corresponding string for the current
%  board from the cell array of strings param.records.file.boards)
if ~isfield(param.records.file,'board_folder_name') || isempty(param.records.file.board_folder_name)
  param.records.file.board_folder_name = '';
end

%    .boards: cell array of strings containing the board folder names (e.g.
%    {'board0','board1',...} or {'chan1','chan2',...})
if ~isfield(param.records.file,'boards') || isempty(param.records.file.boards)
  param.records.file.boards = {''};
end
%    .recursive: logic scaler indicating whether or not the file search
%    should be recursive (search subfolders). Default is true.
if ~isfield(param.records.file,'recursive') || isempty(param.records.file.recursive)
  param.records.file.recursive = true;
end
%    .version: integer scaler containing the raw file version (see "raw
%    file guide" on wiki)
if ~isfield(param.records.file,'version') || numel(param.records.file.version)~=1 || ~isnumeric(param.records.file.version)
  error('MATLAB:get_segment_file_list:missingVersion','param.records.file.version must be a scalar integer.')
end
% Determine raw filename extension and check file version
% NOTE: default_regexp and default_suffix are not used, but are here for
% reference param.records.file.regexp and param.records.file.suffix.
if any(param.records.file.version == [1 9:10 13 14 101 103 401 412 415 418 420 421 422 423])
  default_regexp = '';
  default_suffix = '.dat';
elseif any(param.records.file.version == [416])
  default_regexp = '';
  default_suffix = '.gtd';
elseif any(param.records.file.version == [2:8 11 102 402:408 411])
  default_regexp = '';
  default_suffix = '.bin';
elseif any(param.records.file.version == [410])
  default_regexp = '';
  default_suffix = '.raw';
elseif any(param.records.file.version == [405 406])
  default_regexp = '\.[0-9]*$';
  default_suffix = '';
elseif any(param.records.file.version == [409])
  default_regexp = '\.[0-9][0-9][0-9]$';
  default_suffix = '';
elseif any(param.records.file.version == [413 414])
  default_regexp = '';
  default_suffix = '.mat';
elseif any(param.records.file.version == [12])
  default_regexp = '';
  default_suffix = '.sir';
elseif any(param.records.file.version == [417])
  default_regexp = '';
  default_suffix = '.dice';
else 
  error('Unsupported file version\n');
end

% param.records.file.prefix: Cell array equal in size to
% param.records.file.boards. Default value in each cell is an empty string.
% If this field is a string instead of a cell array, then it will be
% converted into a cell array where the contents of each cell are the
% string that was passed in. Data files must start with this prefix string.

% param.records.file.midfix: Same as prefix except data filenames must
% contain this string between the prefix and the suffix.

% param.records.file.regexp: Same as prefix except data filenames must
% match this regular expression.

% param.records.file.suffix: Same as prefix except data filenames must
% contain this string at the end and after the midfix.

% See get_filenames.m for details on how these four fields are used.

for file_fieldname = {'prefix','midfix','suffix','regexp'}
  file_fieldname = file_fieldname{1};
  if ~isfield(param.records.file,file_fieldname) || isempty(param.records.file.(file_fieldname))
    param.records.file.(file_fieldname) = '';
  end
  % Convert strings into cell arrays corresponding in size to boards.
  if ischar(param.records.file.(file_fieldname))
    param.records.file.(file_fieldname) = cellstr(repmat(param.records.file.(file_fieldname)(:).',[length(param.records.file.boards) 1]));
  end
  for idx=1:length(param.records.file.boards)
    if length(param.records.file.(file_fieldname)) < idx || isempty(param.records.file.(file_fieldname){idx})
      param.records.file.(file_fieldname){idx} = '';
    elseif ~ischar(param.records.file.(file_fieldname){idx})
      error('MATLAB:get_segment_file_list:incorrect_type','param.records.file.%s{} cell elements must be strings.',file_fieldname);
    end
  end
end

%  .start_idx: Array of positive integers. Default is an array of ones
%  equal in size to param.records.file.boards. Represents the first file to
%  return out of the list of files returned by the file search.
if ~isfield(param.records.file,'start_idx') || isempty(param.records.file.start_idx)
  param.records.file.start_idx = ones(size(param.records.file.boards));
end
if numel(param.records.file.start_idx) == 1
  param.records.file.start_idx = param.records.file.start_idx * ones(size(param.records.file.boards));
end
if numel(param.records.file.start_idx) ~= numel(param.records.file.boards)
  error('MATLAB:get_segment_file_list:incorrect_type','param.records.file.start_idx must be a numeric array equal in size to param.records.file.boards.');
end

%  .stop_idx: Array of positive integers. Default is an array of infs equal
%  in size to param.records.file.boards. Represents the first file to
%  return out of the list of files returned by the file search. Setting to
%  inf causes stop_idx to be set to the last file.
if ~isfield(param.records.file,'stop_idx') || isempty(param.records.file.stop_idx)
  param.records.file.stop_idx = inf*ones(size(param.records.file.boards));
end
if numel(param.records.file.stop_idx) == 1
  param.records.file.stop_idx = param.records.file.stop_idx * ones(size(param.records.file.boards));
end
if numel(param.records.file.stop_idx) ~= numel(param.records.file.boards)
  error('MATLAB:get_segment_file_list:incorrect_type','param.records.file.stop_idx must be a numeric array equal in size to param.records.file.boards.');
end

%% Input checks: board_idx

% board_idx: Scalar positive integer. Default is 1. Index into
% param.records.file.boards{board_idx} to generate the file list for.
if ~exist('board_idx','var') || isempty(board_idx)
  board_idx = 1;
end

%% Setup board_folder_name and base_dir
board_folder_name = param.records.file.board_folder_name;
board_folder_name = regexprep(board_folder_name,'%b',param.records.file.boards{board_idx});

base_dir = fullfile(ct_filename_data(param,param.records.file.base_dir),board_folder_name);
base_dir(base_dir == '/' | base_dir == '\') = filesep;

%% Return file list
if nargout > 2
  if 0
    fprintf('Getting files for %s (%s)\n', base_dir, datestr(now));
  end
  
  get_filenames_param = struct('regexp',param.records.file.regexp{board_idx},'recursive',param.records.file.recursive);
  fns = get_filenames(base_dir, ...
    param.records.file.prefix{board_idx}, param.records.file.midfix{board_idx}, ...
    param.records.file.suffix{board_idx}, get_filenames_param);

  % Special sort for ACORDS filenames because the extensions are not a standard length
  if any(param.records.file.version == [405 406])
    basenames = {};
    file_idxs = [];
    new_fns = {};
    for fidx = 1:length(fns)
      fname = fname_info_acords(fns{fidx},struct('hnum',1,'file_version',param.records.file.version));
      new_fns{fidx} = [fname.basename sprintf('.%03d',fname.file_idx)];
    end
    [new_fns,sorted_idxs] = sort(new_fns);
    fns = fns(sorted_idxs);
  end

  if 0
    fprintf('  Found %d files in %s\n', length(fns), base_dir);
  end
  
  if isempty(fns)
    fprintf('No files match the mask:\n');
    fprintf('  path: %s\n', base_dir);
    fprintf('  mask: %s*%s*%s\n', param.records.file.prefix{board_idx}, param.records.file.midfix{board_idx}, param.records.file.suffix{board_idx});
    fprintf('  regexp: %s*%s*%s\n', param.records.file.regexp{board_idx});
    error('No files found');
  end
  
  if param.records.file.stop_idx == inf
    % A stop index of infinity says to include all files
    stop_idx = length(fns);
  elseif param.records.file.stop_idx > length(fns)
    warning('Stop index (%d) is larger than number of files available (%d). This can be caused by an error in the stop index or missing files. dbcont to continue.',param.records.file.stop_idx,length(fns));
    keyboard
    stop_idx = length(fns);
  elseif param.records.file.stop_idx < 0
    % A stop index of -N says to include all but the last N files
    stop_idx = length(fns) + param.records.file.stop_idx;
  else
    if board_idx > 1 && length(param.records.file.stop_idx) == 1
      % Old parameter spreadsheet format only contained a single entry for
      % all boards in param.records.file.stop_idx
      stop_idx = param.records.file.stop_idx;
    else
      stop_idx = param.records.file.stop_idx(board_idx);
    end
  end
  if board_idx > 1 && length(param.records.file.start_idx) == 1
    % Old parameter spreadsheet format only contained a single entry for
    % all boards in param.records.file.start_idx
    file_idxs = param.records.file.start_idx:stop_idx;
  else
    file_idxs = param.records.file.start_idx(board_idx):stop_idx;
  end
  
  if isempty(file_idxs)
    error('No files selected to load out of %i files', length(fns));
  end
end
