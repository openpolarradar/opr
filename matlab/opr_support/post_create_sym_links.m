% script post_create_sym_links
%
% Script for creating the CSARP_post symbolic links
%
% E.g.
% CSARP_post -> ../../../public/data/rds/2010_Greenland_DC8/

%% User Settings
% =========================================================================

debug_enabled = true;

sys = {'accum','kuband','rds','snow'};

post_base_dir = '/kucresis/scratch/dataproducts/public/data/';
post_base_dir_sym_link = '../../../public/data/';

season_post_dir_name = 'CSARP_post';

%% Automated Section
% =========================================================================

if ~isunix
  error('post_create_sym_links is for unix platforms only')
end

% Loop through each system type directory
for sys_idx = 1:length(sys)
  sys_dir = fullfile(gRadar.out_path,sys{sys_idx})

  % Get season directories
  fns = get_filenames(sys_dir,'[0-9][0-9][0-9][0-9]_','','',struct('type','d'))

  % Loop through each season
  for fns_idx = 1:length(fns)
    [~,fns_name] = fileparts(fns{fns_idx});

    post_dir = fullfile(post_base_dir,sys{sys_idx},fns_name)

    post_dir_exists = isdir(post_dir)

    if post_dir_exists
      season_post_dir = fullfile(fns{fns_idx},season_post_dir_name)

      dir_info = dir(season_post_dir);
      season_post_dir_exists = ~isempty(dir_info);
      season_post_dir_isdir = isdir(season_post_dir);
      if ~season_post_dir_exists
        % Create link
        cmd = sprintf('ln -s %s %s\n', fullfile(post_base_dir_sym_link,sys{sys_idx},fns_name), season_post_dir);
        fprintf('%s\n', cmd);
        [status,cmdout] = unix(cmd);
        if status ~= 0
          status
          cmdout
          keyboard
        end
      else
        season_post_dir_islink = ~unix(sprintf('test -L %s', season_post_dir))

        if season_post_dir_islink
          % Remove link
          cmd = sprintf('rm -f %s', season_post_dir);
          fprintf('%s\n', cmd);
          [status,cmdout] = unix(cmd);
          if status ~= 0
            status
            cmdout
            keyboard
          end

          % Create link
          cmd = sprintf('ln -s %s %s\n', fullfile(post_base_dir_sym_link,sys{sys_idx},fns_name), season_post_dir);
          fprintf('%s\n', cmd);
          [status,cmdout] = unix(cmd);
          if status ~= 0
            status
            cmdout
            keyboard
          end
        else
          % Move dir
          cmd = sprintf('mv %s %s', season_post_dir, [season_post_dir '_new']);
          fprintf('%s\n', cmd);
          [status,cmdout] = unix(cmd);
          if status ~= 0
            status
            cmdout
            keyboard
          end
          % Create link
          cmd = sprintf('ln -s %s %s\n', fullfile(post_base_dir_sym_link,sys{sys_idx},fns_name), season_post_dir);
          fprintf('%s\n', cmd);
          [status,cmdout] = unix(cmd);
          if status ~= 0
            status
            cmdout
            keyboard
          end
        end
      end

      %fprintf('%s\t%d\n', post_dir, post_dir_exists)
    else
      %fprintf('%s\t%d\n', post_dir, post_dir_exists)
    end
    if debug_enabled
      keyboard
    end
  end
end









