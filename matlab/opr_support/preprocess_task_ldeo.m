function success = preprocess_task_ldeo(param)
% success = preprocess_task_ldeo(param)
%
% Reads all headers from LDEO raw radar data files, stores into separate
% files, and generates segment information for the parameter spreadsheet.
%
% Example:
% Called from preprocess_task_ldeo.m
%
% Author: John Paden
%
% See also: basic_load_ldeo.m

% Pull in the inputs from param struct
reuse_tmp_files = param.preprocess.reuse_tmp_files;

[~,defaults] = param.preprocess.default();

%% Read each config/system XML file pair into a configs structure
% =========================================================================

% Not applicable yet

% =========================================================================
%% Get Data File list for this board
board_idx = 1;
[base_dir,board_folder_name,fns] = get_segment_file_list(param,board_idx);

fns_datenum = zeros(size(fns));
for fn_idx = 1:length(fns)
  config_fname_info = fname_info_ldeo(fns{fn_idx});
  fns_datenum(fn_idx) = config_fname_info.datenum;
end

%% Iterate packet_strip through file list
old_fn_dir = [];
board_hdrs{1}.radar_time = [];
board_hdrs{1}.comp_time = [];
board_hdrs{1}.file_idxs = [];
for fn_idx = 1:length(fns)
  fn = fullfile(fns{fn_idx});
  
  [fn_dir,fn_name] = fileparts(fn);
  if ~strcmpi(fn_dir,old_fn_dir)
    % New data directory: assume that this is from a different ldeo
    % board and state vectors should be reset.
    last_bytes_m = [];
    last_bytes = zeros(64,1,'uint8');
    last_bytes_len = int32(0);
    num_expected = int32(-1);
    pkt_counter = int32(-1);
    old_fn_dir = fn_dir;
  end
  
  % Create output filenames
  out_fn = ct_filename_ct_tmp(param,'','headers', ...
    fullfile(board_folder_name, fn_name));
  [out_fn_dir,out_fn_name] = fileparts(out_fn);
  out_fn = fullfile(out_fn_dir,[out_fn_name,'.dat']);
  out_hdr_fn = fullfile(out_fn_dir,[out_fn_name,'.mat']);
  
  % Print status
  fprintf('%s %d/%d %s (%s)\n    %s\n', mfilename, fn_idx, ...
    length(fns), fn, datestr(now), out_fn);
  
  % Check to make sure output directory exists
  if ~exist(out_fn_dir,'dir')
    mkdir(out_fn_dir);
  end
  
  % Copy GPS (LDEO SPAN) Files
  if fn_idx == 1
    out_config_fn_dir = fullfile(fn_dir,param.records.file.config_folder_name);
    gps_files = fullfile(out_config_fn_dir,'*SPAN*.txt');
    out_log_dir = fullfile(param.data_support_path, param.season_name, param.preprocess.date_str, 'GPS');
    try
      fprintf('Copy %s\n  %s\n', gps_files, out_log_dir);
      if ~exist(out_log_dir,'dir')
        mkdir(out_log_dir)
      end
      copyfile(gps_files, out_log_dir);
    catch ME
      warning('Error while copying log files:\n%s\n', ME.getReport);
    end
  end
  
  finfo = fname_info_ldeo(fn);
  
  % Check to see if outputs already exist
  if reuse_tmp_files && exist(out_hdr_fn,'file')
    load(out_hdr_fn,'radar_time','comp_time');
    board_hdrs{1}.radar_time(end+(1:length(radar_time))) = radar_time;
    board_hdrs{1}.comp_time(end+(1:length(radar_time))) = comp_time;
    %board_hdrs{1}.file_idxs(end+(1:length(radar_time))) = finfo.file_idx*ones([1 length(radar_time)]);
    board_hdrs{1}.file_idxs(end+(1:length(radar_time))) = fn_idx*ones([1 length(radar_time)]);
    continue;
  end
  
  finfo_dir = dir(fn);
  if finfo_dir.bytes < 96160
    continue;
  end
  hdr = basic_load_ldeo(fn);
  if isempty(hdr)
    continue;
  end
  
  %% Write header output file
  radar_time = hdr{1}.filename_utc_time;
  comp_time = hdr{1}.filename_utc_time;
  offset = hdr{1}.offset;
  
  save(out_hdr_fn, 'offset', 'radar_time', 'comp_time');
  
  board_hdrs{1}.radar_time(end+(1:length(radar_time))) = radar_time;
  board_hdrs{1}.comp_time(end+(1:length(radar_time))) = comp_time;
  %board_hdrs{1}.file_idxs(end+(1:length(radar_time))) = finfo.file_idx*ones([1 length(radar_time)]);
  board_hdrs{1}.file_idxs(end+(1:length(radar_time))) = fn_idx*ones([1 length(radar_time)]);

  if 0
    %% Debug outputs
    % load(out_tmp_fn);
  end
  
end

if 1
  % diff(radar_time), 0.0124
  figure(1); clf;
  subplot(2,1,1);
  plot(diff(board_hdrs{1}.radar_time),'.')
  subplot(2,1,2);
  plot(diff(board_hdrs{1}.radar_time),'.')
  ylim([0 0.04])
end

t_PRI = 160e-6;           % Pulse Repitition Interval (s)
presums = 16;             % Number of hardware stacks
t_EPRI = t_PRI*4*presums; % Effective PRI, 10.24 ms (seems like factor of 4 should be 2 for the 2 different pulses that are transmitted, 3 us and then 1 us)

new_radar_time = nan(size(board_hdrs{1}.file_idxs));
cur_file_idx = 0;
for idx = 1:length(board_hdrs{1}.file_idxs)
  if cur_file_idx < board_hdrs{1}.file_idxs(idx)
    cur_file_idx = board_hdrs{1}.file_idxs(idx);
    new_radar_time(idx) = board_hdrs{1}.radar_time(idx);
  end
end
new_radar_time(end+1) = board_hdrs{1}.radar_time(end) + t_EPRI;
file_start_idxs = find(~isnan(new_radar_time));
num_records_per_file = diff(file_start_idxs);
if any(num_records_per_file > 1050)
  warning('This preprocessing run has files with too many records. %d files. median %d max %d.', ...
    length(num_records_per_file), median(num_records_per_file), max(num_records_per_file))
  num_records_per_file_error_fn = fullfile(out_fn_dir,'num_records_per_file_error.mat');
  save(num_records_per_file_error_fn,'file_start_idxs','board_hdrs','param','fns','num_records_per_file','new_radar_time');
end

% No heading information, break segments based on time, epri, or radar
% counter information (param.preprocess.field_time_gap and
% param.preprocess.max_time_gap determine which field and gap size to use).
counters = {};
file_idxs = {};
param.preprocess.field_time_gap = 'radar_time';
for board_idx = 1:numel(param.records.file.boards)
  counters{board_idx} = double(board_hdrs{board_idx}.(param.preprocess.field_time_gap));
  file_idxs{board_idx} = board_hdrs{board_idx}.file_idxs;
  day_wrap_offset{board_idx} = zeros(size(board_hdrs{board_idx}.file_idxs));
end

[segs,stats] = preprocess_create_segments(counters,file_idxs,day_wrap_offset,param.preprocess.max_time_gap);

if 1
  % Debug: Test Code
  for seg_idx = 1:length(segs)
    fprintf('Segment %d\n', seg_idx);
    disp(segs(seg_idx))
  end
  
  fprintf('On time: %g\n', sum(stats.on_time));
  fprintf('Seg\tOn%%\tOn\tGap');
  for board_idx = 1:size(stats.board_time,2)
    fprintf('\tBrd%d%%\tBrd%d\tFile0\tFileN\tIndex0\tIndexN', board_idx, board_idx);
  end
  fprintf('\n');
  
  for seg_idx = 1:length(segs)
    if seg_idx == 1
      gap = 0;
    else
      gap = stats.start_time(seg_idx)-stats.stop_time(seg_idx-1);
    end
    fprintf('%d\t%.0f%%\t%.1g\t%.1f', seg_idx, stats.on_time(seg_idx)/sum(stats.on_time)*100, stats.on_time(seg_idx), gap);
    for board_idx = 1:size(stats.board_time,2)
      fprintf('\t%.0f%%\t%.1g\t%d\t%d\t%d\t%d', stats.board_time(seg_idx,board_idx)/stats.on_time(seg_idx)*100, ...
        stats.board_time(seg_idx,board_idx), stats.start_file(seg_idx,board_idx), ...
        stats.stop_file(seg_idx,board_idx), stats.start_record(seg_idx,board_idx), ...
        stats.stop_record(seg_idx,board_idx));
    end
    fprintf('\n');
  end
end

% Create the parameters to output
oparams = {};
for segment_idx = 1:length(segs)
  segment = segs(segment_idx);
  
  % Determine which default parameters to use
  % =======================================================================
  match_idx = 1;
  oparams{end+1} = merge_structs(param,defaults{match_idx});
  oparams{end} = rmfield(oparams{end},'config_regexp');
  oparams{end} = rmfield(oparams{end},'name');
  
  % Parameter spreadsheet
  % =======================================================================
  oparams{end}.day_seg = sprintf('%s_%02d',param.preprocess.date_str,segment_idx);
  oparams{end}.cmd.notes = defaults{match_idx}.name;
  
  oparams{end}.records.file.start_idx = segment.start_idxs;
  oparams{end}.records.file.stop_idx = segment.stop_idxs;
  oparams{end}.records.file.start_record = stats.start_record(segment_idx,:);
  oparams{end}.records.file.stop_record = stats.stop_record(segment_idx,:);
  defaults{match_idx}.records.gps.time_offset = 0;
  oparams{end}.records.gps.time_offset = defaults{match_idx}.records.gps.time_offset + segment.day_wrap_offset;
  
  oparams{end}.records.file.base_dir = param.records.file.base_dir;
  oparams{end}.records.file.board_folder_name = param.records.file.board_folder_name;
  if ~isempty(oparams{end}.records.file.board_folder_name) ...
      && oparams{end}.records.file.board_folder_name(1) ~= filesep
    % Ensures that board_folder_name is not a text number which Excel
    % will misinterpret as a numeric type
    oparams{end}.records.file.board_folder_name = ['/' oparams{end}.records.file.board_folder_name];
  end
  if ~isnan(str2double(oparams{end}.records.file.board_folder_name))
    oparams{end}.records.file.board_folder_name = ['/' oparams{end}.records.file.board_folder_name];
  end
  oparams{end}.records.file.boards = param.records.file.boards;
  oparams{end}.records.file.version = param.records.file.version;
  if isfield(param.records.file,'prefix')
    oparams{end}.records.file.prefix = param.records.file.prefix;
  end
  oparams{end}.records.file.clk = param.records.file.clk;
end

%% Print out segments
% =========================================================================
if ~isempty(param.preprocess.param_fn)
  % Print parameter spreadsheet values to stdout and param_txt_fn
  % =========================================================================
  fid = 1;
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  cmd\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'cmd',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  records\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'records',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  qlook\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'qlook',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  sar\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'sar',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  array\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'array',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  radar\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'radar',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  post\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'post',oparams,fid);
  % Other sheets
  try
    warning off MATLAB:xlsfinfo:ActiveX
    [status, sheets] = xlsfinfo(param.preprocess.param_fn);
    warning on MATLAB:xlsfinfo:ActiveX
    for sheet_idx = 1:length(sheets)
      if ~any(strcmpi(sheets{sheet_idx},{'cmd','records','qlook','sar','array','radar','post'}))
        fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  %s\n', sheets{sheet_idx}); fprintf(fid,'%s</strong>\n','='*ones(1,80));
        read_param_xls_print(param.preprocess.param_fn,sheets{sheet_idx},oparams,fid);
      end
    end
  catch ME
    ME.getReport
  end
  fprintf(fid,'\n');
  
  param_txt_fn = ct_filename_ct_tmp(param,'','param', [param.preprocess.date_str,'.txt']);
  fprintf('Writing %s\n\n', param_txt_fn);
  param_txt_fn_dir = fileparts(param_txt_fn);
  if ~exist(param_txt_fn_dir,'dir')
    mkdir(param_txt_fn_dir);
  end
  [fid,msg] = fopen(param_txt_fn,'wb');
  if fid<0
    error('Could not write to %s: %s\n', param_txt_fn, msg);
  end
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  cmd\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'cmd',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  records\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'records',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  qlook\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'qlook',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  sar\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'sar',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  array\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'array',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  radar\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'radar',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  post\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'post',oparams,fid);
  fprintf(fid,'\n');
  % Other sheets
  try
    warning off MATLAB:xlsfinfo:ActiveX
    [status, sheets] = xlsfinfo(param.preprocess.param_fn);
    warning on MATLAB:xlsfinfo:ActiveX
    for sheet_idx = 1:length(sheets)
      if ~any(strcmpi(sheets{sheet_idx},{'cmd','records','qlook','sar','array','radar','post'}))
        fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  %s\n', sheets{sheet_idx}); fprintf(fid,'%s</strong>\n','='*ones(1,80));
        read_param_xls_print(param.preprocess.param_fn,sheets{sheet_idx},oparams,fid);
      end
    end
  catch ME
    ME.getReport
  end
  fprintf(fid,'\n');
  fclose(fid);
end

%% Exit task
% =========================================================================
fprintf('%s done %s\n', mfilename, datestr(now));

success = true;
