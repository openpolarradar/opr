% script post_create_sym_links
%
% Script for creating the CSARP_layer symbolic links
%
% Creates a list of commands that should be copied and pasted into a shell
% script and run as root.
%
% E.g.
% CSARP_post -> ../../../public/data/rds/2010_Greenland_DC8/

%% User Settings
% =========================================================================

debug_enabled = true;

sys = {'accum','kuband','rds','snow'};

season_post_dir_name = 'CSARP_post';

layer_dir_name = 'CSARP_layer';

%% Automated Section
% =========================================================================

% Loop through each system type directory
for sys_idx = 1:length(sys)
  sys_dir = fullfile(gRadar.out_path,sys{sys_idx});

  % Get season directories
  fns = get_filenames(sys_dir,'[0-9][0-9][0-9][0-9]_','','',struct('type','d'));

  % Loop through each season
  for fns_idx = 1:length(fns)

    post_dir = fullfile(fns{fns_idx},season_post_dir_name);

    post_layer_dir = fullfile(fns{fns_idx},season_post_dir_name,layer_dir_name);

    layer_dir = fullfile(fns{fns_idx},layer_dir_name);

    % If CSARP_layer symbolic link exists, remove it
    layer_dir_sym_link_exists = ~unix(sprintf('test -L %s', layer_dir));
    if layer_dir_sym_link_exists
      cmd = sprintf('rm -f %s', layer_dir);
      fprintf('# %s\n', cmd);
      if debug_enabled
        keyboard
      end
      [status,cmdout] = unix(cmd);
      if status ~= 0
        status
        cmdout
        keyboard
      end
    end

    post_dir_exists = isdir(post_dir);

    post_layer_dir_exists = isdir(post_layer_dir);

    layer_dir_exists = isdir(layer_dir);

    % If CSARP_post/CSARP_layer exists, but CSARP_layer does not, make a
    % sym link to CSARP_post/CSARP_layer
    if post_layer_dir_exists && ~layer_dir_exists
      cmd = sprintf('ln -s %s %s', 'CSARP_post/CSARP_layer', layer_dir);
      fprintf('%s\n', cmd);
      % keyboard
      % [status,cmdout] = unix(cmd);
      % if status ~= 0
      %   status
      %   cmdout
      %   keyboard
      % end
    end

    % fprintf('%d\t%d\t%s\n', post_layer_dir_exists, layer_dir_exists, fns{fns_idx})

    % If CSARP_post/CSARP_layer does not exist, and CSARP_layer and
    % CSARP_post exist, then move CSARP_layer to CSARP_post/CSARP_layer and
    % make a symbolic link to CSARP_post/CSARP_layer
    if layer_dir_exists && post_dir_exists && ~post_layer_dir_exists
      cmd = sprintf('mv %s %s', layer_dir, post_layer_dir);
      fprintf('%s\n', cmd);
      % keyboard
      % [status,cmdout] = unix(cmd);
      % if status ~= 0
      %   status
      %   cmdout
      %   keyboard
      % end
      cmd = sprintf('ln -s %s %s', 'CSARP_post/CSARP_layer', layer_dir);
      fprintf('%s\n', cmd);
      % keyboard
      % [status,cmdout] = unix(cmd);
      % if status ~= 0
      %   status
      %   cmdout
      %   keyboard
      % end
    end
  end
end









