% script run_get_raw_files
%
% Script for running get_raw_files.
%
% Creates an output file containing the tape locations of
% all the files comprising the given frames.
%
% Authors: Reece Mathews
%
% See also: get_raw_files.m

%% USER INPUT

clear params;

if 1
  seasons = {'rds_param_2014_Antarctica_DC8.xls', 'rds_param_2011_Antarctica_DC8.xls', 'rds_param_2018_Antarctica_DC8.xls'};
  flight_lines = {{'20141103_04_014', '20141121_06_015'}, {'20111104_02_024', '20111104_02_023'}, {'20181115_01_002', '20181115_01_001'}};

else
  % Example using the parameter spreadsheet to specify which files to
  % load for a single season
  seasons = {'snow_param_2019_Greenland_P3.xls'};
  params = read_param_xls(ct_filename_param('snow_param_2019_Greenland_P3.xls'));
  segments = {'20190512_01'};
  flight_lines = {{}};
  for param_idx = 1:length(params)
    param = params(param_idx);
    if ismember(param.day_seg, segments)
      flight_lines{end} = [flight_lines{end} ct_get_frame_list(param)];
    end
  end
end

tape_list_path = 'C:/Users/r354m259/RAS_files_tapes_table.txt';
output_file = 'tapes_miguel_july_2024_missing_frame.txt';

%% AUTOMATED PART

% Produce tape_list sorted by filenames
tape_list = readmatrix(tape_list_path, 'Delimiter', ' ', 'OutputType', 'string');
[~, file, ext] = fileparts(tape_list(:, 2));
tape_list = [tape_list file + ext];
tape_list = sortrows(tape_list, 3);

% Map directories to the corresponding small file archives
small_file_archives = string();
for file_idx = 1:size(tape_list, 1)
    filepath = tape_list{file_idx, 2};
    tapes = tape_list(file_idx, 1);
    if endsWith(filepath, "small_file_archive.tar")
        [parent, file_name, ext] = fileparts(filepath);
        file_name = [file_name ext];
        archive_idx = size(small_file_archives, 1) + 1;
        small_file_archives(archive_idx, 1) = tapes;
        small_file_archives(archive_idx, 2) = convertCharsToStrings(parent);
        small_file_archives(archive_idx, 3) = convertCharsToStrings(file_name);
    end
end
small_file_archives = sortrows(small_file_archives, 2);


output_fid = fopen(output_file,'w');

for season_idx=1:length(seasons)
    season_name = ct_filename_param(seasons{season_idx});

    frame_idxs = flight_lines{season_idx};

    [load_info,gps_time,recs] = get_raw_files(season_name, frame_idxs, {}, {}, {}, {}, tape_list, small_file_archives);

    fprintf(output_fid, '%s\n', seasons{season_idx});
    for file_list=1:length(load_info.filenames)
        fprintf(output_fid, 'Filelist: %d\n', file_list);
        fprintf(output_fid, 'tapes filename stored_filename\n');
        for file_idx=1:length(load_info.filenames{file_list})
          fprintf(output_fid, '%s %s %s\n', load_info.tapes{file_list}{file_idx}, load_info.filenames{file_list}{file_idx}, load_info.stored_filenames{file_list}{file_idx});
        end
    end
    fprintf(output_fid, '\n');
end

fclose(output_fid);