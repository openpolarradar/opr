function ctrl_chain = preprocess(param,param_override)
% ctrl_chain = preprocess(param,param_override)
%
% Script for generating preprocess_task.m tasks to run on a cluster.
%
% Each preprocess task does the following on one dataset (one dataset per
% task):
%
% 1. Copy metadata files if applicable 2. Load raw data files:
%   a. Create temporary header files for records_create
%   b. Create network header stripped raw data files if applicable
% 3. Break data into segments
% 4. Create parameter spreadsheet entries
% 5. Load GPS data and create maps of each segment and file if applicable
%
% INPUTS
% =========================================================================
% The input arguments are a set of cell arrays where each cell contains the
% information for one dataset (one cluster task per dataset).
%
% param.preprocess.default: Cell array of function handles to
% default_radar_params_SEASON.m functions.
%
% See input checks section to see further details on each input.
%
% Author: John Paden
%
% See also: run_preprocess.m, preprocess.m, preprocess_task.m

%% General Setup
% =====================================================================
param = merge_structs(param, param_override);

fprintf('=====================================================================\n');
fprintf('%s: %s (%s)\n', mfilename, '', datestr(now));
fprintf('=====================================================================\n');

%% Create each task
% =========================================================================

fun = 'preprocess_task';

ctrl = cluster_new_batch(param);

cluster_compile(fun,[],0,ctrl);

%% Dataset Loop
sparam = [];
sparam.task_function = fun;
sparam.argsin{1} = param; % Static parameters
sparam.num_args_out = 1;
for dataset_idx = 1:numel(param.preprocess.default)

  % cparam: structure controlling the operation of this specific
  % preprocess_task.m task. This will be loaded in the tasks's dynamic
  % parameters, dparam.

  % Load the default parameter structure:
  cparam = param.preprocess.default{dataset_idx}();
  cparam = merge_structs(param,cparam);
  cparam = merge_structs(cparam,param_override);

  %% Dataset Loop: Input checks
  % =========================================================================

  % .preprocess.cpu_time_per_file: Positive double with units of seconds.
  % Default is 10 seconds. Cluster cpu_time allotment per raw data file
  if ~isfield(cparam.preprocess,'cpu_time_per_file') || isempty(cparam.preprocess.cpu_time_per_file)
    cparam.preprocess.cpu_time_per_file = 10;
  end
  % .preprocess.default: The function handle for creating the default
  % parameters for this task. It will be used to return the default values
  % for the particular settings used by each segment.
  cparam.preprocess.default = param.preprocess.default{dataset_idx};
  % .preprocess.date_str: Optional. Cell array of strings. Default is
  % current date. Not used by all radars. Used by preprocess_task_cresis
  % and preprocess_task_bas to create the param.day_seg field in the output
  % segments and to generate temporary file output paths for the log files
  % and the files used by records_create.
  if isfield(param.preprocess,'date_str') && iscell(param.preprocess.date_str) ...
      && numel(param.preprocess.date_str) == numel(param.preprocess.default) ...
      && ischar(param.preprocess.date_str{dataset_idx})
    cparam.preprocess.date_str = param.preprocess.date_str{dataset_idx};
  elseif any(strcmpi(cparam.preprocess.digital_system_type,{'cecs','cresis','utig'}))
    error('For this digital_system_type, the param.preprocess.date_str field must be defined and be a cell array of strings equal in size to param.preprocess.default.')
  end
  % param.preprocess.digital_system_type: String containing the digital
  % system type.
  if ~isfield(cparam.preprocess,'digital_system_type') ...
      || isempty(cparam.preprocess.digital_system_type) ...
      || ~any(strcmpi(cparam.preprocess.digital_system_type,{'arena','bas','cecs','cresis','ldeo','stolaf','utig','utua'}))
    error('MATLAB:preprocess:digital_system_type','param.preprocess.digital_system_type must be defined and be a string containing arena, bas, cresis, stolaf, utig, or utua.');
  end
  % param.preprocess.file: a cell array of structures that describe
  % where the files are for each dataset. The cell array should be the
  % same size as param.preprocess.default and the entries in the cells
  % should correspond to the entries in param.preprocess.default.
  % * The structures will be merged with the param.records.file structure
  % returned by the corresponding param.preprocess.default{} function call.
  % Usually fields that do not change are defined in the default parameter
  % structure defined in the param.preprocess.default{} function. Only fields
  % that change for each dataset need to be defined in the
  % param.preprocess.file{} structure.
  % * To understand what the param.preprocess.file{} fields are and how they
  % are used, see records_create.m documentation. Note that the segment
  % specific fields, file.start_idx and file.stop_idx, are not used since one
  % of the purposes of preprocessing is to determine these values.
  % * Default values for the fields will follow get_segment_file_list.m
  if isfield(param.preprocess,'file')
    cparam.records.file = merge_structs(cparam.records.file,param.preprocess.file{dataset_idx});
  end
  % param.records.file.boards: cell array of strings containing an entry
  % for each simultaneous file stream being recorded to disk. For example,
  % common values are {'board0','board1',...} or {'chan1','chan2',...}.
  % These strings will be substituted in board_folder_name whereever a '%b'
  % string appears. Default value is a single element cell array with an
  % empty string in it (i.e. assumes only a single file stream). If
  % multiple boards are present, but they are all in the same directory,
  % then pass in a cell array of size equal to the number of separate file
  % streams with the elements all set to ''. The individual files in each
  % file stream can then be separated using the param.records.file search
  % parameters.
  if ~isfield(cparam.records.file,'boards') || isempty(cparam.records.file.boards)
    cparam.records.file.boards = {''};
  end
  % .preprocess.field_time_gap: String that describes the fieldname to use when
  % breaking datasets into segments. Default is 'gps_time'. Only used by
  % preprocess_task_cresis.m (CReSIS National Instruments based digital
  % system). Potential values are:
  % * "gps_time"
  % * "utc_time_sod"
  if ~isfield(cparam.preprocess,'field_time_gap') || isempty(param.preprocess.field_time_gap)
    if any(strcmpi(cparam.preprocess.digital_system_type,{'CECS','CRESIS'}))
      cparam.preprocess.field_time_gap = 'utc_time_sod';
    else
      cparam.preprocess.field_time_gap = 'gps_time';
    end
  end
  % .preprocess.gps_file_mask: String. Default is an empty string. Only
  % used by preprocess_task_cresis.m (CReSIS National Instruments based
  % digital system) and preprocess_task_arena.m. File mask relative to
  % param.preprocess.config_folder_name for GPS files. Leave empty if there
  % are no GPS files to copy.
  if ~isfield(cparam.preprocess,'gps_file_mask') || isempty(cparam.preprocess.gps_file_mask)
    if any(strcmpi(cparam.preprocess.digital_system_type,{'ARENA'}))
      cparam.preprocess.gps_file_mask = 'logs/*';
    else
      cparam.preprocess.gps_file_mask = '';
    end
  end
  % param.preprocess.header_load_func: Function handle to the function
  % which will load the header information for the data files.
  if ~isfield(cparam.preprocess,'header_load_func') ...
      || isempty(cparam.preprocess.header_load_func) ...
      || ~isa(cparam.preprocess.header_load_func,'function_handle')
    error('MATLAB:preprocess:header_load_func','param.preprocess.header_load_func must be function handle.');
  end
  % .preprocess.max_time_gap: Scalar double. Default is 10. Maximum time gap
  % between data records in a segment. Gaps larger than this cause the data
  % stream to be broken into different segments at the gap.
    % Maximum time in seconds between two data records before forcing a segment break
  if ~isfield(cparam.preprocess,'max_time_gap') || isempty(cparam.preprocess.max_time_gap)
    cparam.preprocess.max_time_gap = 10;
  end
  % .preprocess.min_seg_size: Scalar integer. Default is 2. Minimum number of files in
  % a segment (segments with less files will be discarded)
  if ~isfield(cparam.preprocess,'min_seg_size') || isempty(cparam.preprocess.min_seg_size)
    cparam.preprocess.min_seg_size = 2;
  end
  % .preprocess.online_mode: Not supported for all radars. Scaler integer.
  % Default is 0. Online modes perform a subset of operations that are
  % designed to just ensure that the files being recorded do not have
  % header errors. Allowed values:
  % * 0: not online mode
  % * 1: online mode (process all files)
  % * 2: online mode (process only the most recent file)
  if ~isfield(cparam.preprocess,'online_mode') || isempty(cparam.preprocess.online_mode)
    cparam.preprocess.online_mode = 0;
  end
  % .preprocess.param_fn: String containing the parameter spreadsheet filename.
  % Default is RADARNAME_param_SEASONNAME.xls.
  if ~isfield(cparam.preprocess,'param_fn') || isempty(cparam.preprocess.param_fn)
    cparam.preprocess.param_fn ...
      = ct_filename_param(sprintf('%s_param_%s.xlsx',ct_output_dir(cparam.radar_name),cparam.season_name));
  end
  % .preprocess.param_worksheets: Cell array of worksheet names to modify
  % in param_fn (beyond the standard ones). Corresponding
  % param.(param.preprocess.param_worksheets{}) fields should be set in the
  % function pointed to by param.preprocess.default{}.
  if ~isfield(cparam.preprocess,'param_worksheets') || isempty(cparam.preprocess.param_worksheets)
    cparam.preprocess.param_worksheets = {};
  end
  % .preprocess.plots_visible: Logical scaler. Default true. If true, then
  % plots generated during preprocessing will display rather than being
  % generated as hidden figures.
  if ~isfield(cparam.preprocess,'plots_visible') || isempty(cparam.preprocess.plots_visible)
    cparam.preprocess.plots_visible = 1;
  end
  % .preprocess.reuse_tmp_files: Logical scaler. Default is true. If true, the
  % function will use any existing header files in ct_tmp and not recreate
  % them.
  if ~isfield(cparam.preprocess,'reuse_tmp_files') || isempty(cparam.preprocess.reuse_tmp_files)
    cparam.preprocess.reuse_tmp_files = true;
  end
  % .preprocess.segment_end_file_trim: Non-negative scaler. Default is 1.
  % Only used with cresis digital_system_type. This many files will be
  % ignored at the end of each segment. This is useful since the file at
  % the end often contains corrupt data.
  if ~isfield(cparam.preprocess,'segment_end_file_trim') || isempty(cparam.preprocess.segment_end_file_trim)
    cparam.preprocess.segment_end_file_trim = 1;
  end
  % .preprocess.skip_files_end: Scaler integer. Default is 0. Number of files
  % to ignore at the end of the segment
  if ~isfield(cparam.preprocess,'skip_files_end') || isempty(cparam.preprocess.skip_files_end)
    cparam.preprocess.skip_files_end = 0;
  end
  % .preprocess.skip_files_start: Scaler integer. Default is 0. Number of files
  % to ignore at the beginning of the segment.
  if ~isfield(cparam.preprocess,'skip_files_start') || isempty(cparam.preprocess.skip_files_start)
    cparam.preprocess.skip_files_start = 0;
  end

  %% Setup preprocess task
  
  dparam = [];
  dparam.argsin{1} = cparam;
  
  num_fns = 0;
  for board_idx = 1:length(cparam.records.file.boards)
    [base_dir,board_folder_name,fns,file_idxs] = get_segment_file_list(cparam,board_idx);
    num_fns = num_fns + length(fns);
  end

  dparam.cpu_time = 60 + cparam.preprocess.cpu_time_per_file*num_fns;
  dparam.mem = 4e9;
  dparam.notes = sprintf('%s:%s:%s %d files', ...
    mfilename, base_dir, board_folder_name, num_fns);
  ctrl = cluster_new_task(ctrl,sparam,dparam);
end

ctrl_chain = {{ctrl}};

fprintf('%s: Done %s\n', mfilename, datestr(now));
