function success = preprocess_task_bas(param)
% success = preprocess_task_bas(param)
%
% Support function for preprocess.m
%
% Example:
% Called from preprocess_task.m
%
% Author: John Paden
%
% See also: run_preprocess.m, preprocess.m, preprocess_task.m,
% preprocess_task_arena.m, preprocess_task_cresis.m

%% Read headers
% =========================================================================

fns = {};
for board_idx = 1:length(param.records.file.boards)
  [base_dir,board_folder_name,fns{board_idx},file_idxs] = get_segment_file_list(param,board_idx);
  board_hdrs{board_idx}.gps_time = [];
  board_hdrs{board_idx}.file_idxs = [];
  num_sam = [];
  for fn_idx = 1:length(fns{board_idx})
    fn = fns{board_idx}{fn_idx};
    fprintf('%d of %d: %s (%s)\n', fn_idx, ...
      length(fns{board_idx}), fn, datestr(now,'HH:MM:SS'));

    % Open the PRI file to get the pri pulse number and number of records
    [fn_dir,fn_name,fn_ext] = fileparts(fn);
    pri_fn = fullfile(fn_dir,[fn_name '_pri' fn_ext]);
    pri = load(pri_fn,'pri');
    % Open the data file to get the number of samples per range line, Nt
    if isempty(num_sam)
      data = load(fn,'s');
      num_sam = size(data.s,1);
      clear data;
    end

    % Create fname and update with other fields
    tmp_hdr = fname_info_bas(fn);
    tmp_hdr.pri = pri.pri;
    tmp_hdr.wfs.num_sam = num_sam;
    tmp_hdr.gps_time = datenum_to_epoch(tmp_hdr.datenum);
    tmp_hdr.gps_time = tmp_hdr.gps_time + utc_leap_seconds(tmp_hdr.gps_time);

    Nx = length(tmp_hdr.pri);
    if Nx > 1
      PRI = median(diff(tmp_hdr.pri)) / param.radar.prf;
      tmp_hdr.gps_time = tmp_hdr.gps_time + [0 PRI*(1:Nx-1)];
    elseif Mx ~= 1
      error('File with no records not handled.');
    end
    tmp_hdr.file_idxs = fn_idx*ones(1,Nx);

    tmp_hdr_fn = ct_filename_ct_tmp(param,'','headers', ...
      fullfile(char(board_folder_name), ''));

    % Create temporary filename to load the settings
    fn = fns{board_idx}{fn_idx};
    [~,fn_name] = fileparts(fn);
    tmp_hdr_fn = ct_filename_ct_tmp(param,'','headers', ...
      fullfile(board_folder_name, [fn_name '.mat']));

    tmp_hdr_fn_dir = fileparts(tmp_hdr_fn);
    if ~exist(tmp_hdr_fn_dir,'dir')
      mkdir(tmp_hdr_fn_dir);
    end

    fprintf('  Saving %s\n', tmp_hdr_fn);
    save(tmp_hdr_fn,'-v7.3','-struct','tmp_hdr');

    board_hdrs{board_idx}.gps_time(end+(1:Nx)) = tmp_hdr.gps_time;
    board_hdrs{board_idx}.file_idxs(end+(1:Nx)) = tmp_hdr.file_idxs;
  end
  board_hdrs{board_idx}.day_wrap_offset = zeros(size(board_hdrs{board_idx}.file_idxs));
end

%% Create Segments
% =========================================================================

% Break segments based on settings files and header information
% Each temporary file is its own segment and contains full settings
% information.

% No heading information, break segments based on time, epri, or radar
% counter information (param.preprocess.field_time_gap and
% param.preprocess.max_time_gap determine which field and gap size to use).
counters = {};
file_idxs = {};
for board_idx = 1
  counters{board_idx} = double(board_hdrs{board_idx}.gps_time);
  file_idxs{board_idx} = board_hdrs{board_idx}.file_idxs;
  day_wrap_offset{board_idx} = board_hdrs{board_idx}.day_wrap_offset;
end
[segs,stats] = preprocess_create_segments(counters,file_idxs,day_wrap_offset,param.preprocess.max_time_gap);

if 1
  % Debug: Test Code
  for seg_idx = 1:length(segs)
    fprintf('Segment %d\n', seg_idx);
    disp(segs(seg_idx))
  end
  
  fprintf('On time: %g\n', sum(stats.on_time));
  fprintf('Seg\tOn%%\tOn');
  for board_idx = 1:size(stats.board_time,2)
    fprintf('\t%d%%\t%d', board_idx, board_idx);
  end
  fprintf('\n');
  
  for seg_idx = 1:length(segs)
    fprintf('%d\t%.0f%%\t%.1g', seg_idx, stats.on_time(seg_idx)/sum(stats.on_time)*100, stats.on_time(seg_idx));
    for board_idx = 1:size(stats.board_time,2)
      fprintf('\t%.0f%%\t%.1g', stats.board_time(seg_idx,board_idx)/stats.on_time(seg_idx)*100, stats.board_time(seg_idx,board_idx));
    end
    fprintf('\n');
  end
end

% Create the parameters to output
oparams = {};
[~,defaults] = param.preprocess.default();
for segment_idx = 1:length(segs)
  segment = segs(segment_idx);
  
  % Determine which default parameters to use
  % =======================================================================
  match_idx = 1;
  default = defaults{match_idx};
  default = merge_structs(param,default);
  oparams{end+1} = default;
  try
    oparams{end} = rmfield(oparams{end},'config_regexp');
  end
  oparams{end} = rmfield(oparams{end},'name');
  
  % Parameter spreadsheet
  % =======================================================================
  oparams{end}.day_seg = sprintf('%s_%02d',param.preprocess.date_str,segment_idx);
  oparams{end}.cmd.notes = default.name;
  
  board_idx = 1;
  fn = fns{board_idx}{segment.start_idxs};
  [~,fn_name] = fileparts(fn);
  [~,board_folder_name] = get_segment_file_list(param,board_idx);
  tmp_hdr_fn = ct_filename_ct_tmp(param,'','headers', ...
    fullfile(board_folder_name, [fn_name '.mat']));
  fname = load(tmp_hdr_fn,'name');
  oparams{end}.cmd.mission_names = fname.name;
  
  oparams{end}.records.file = param.records.file;
  oparams{end}.records.file.start_idx = segment.start_idxs;
  oparams{end}.records.file.stop_idx = segment.stop_idxs;
  oparams{end}.records.gps.time_offset = default.records.gps.time_offset + segment.day_wrap_offset;
  if ~isempty(oparams{end}.records.file.board_folder_name) ...
      && oparams{end}.records.file.board_folder_name(1) ~= filesep
    % By adding a file separator to board_folder_name, we ensure that Excel
    % does not interpret the field as a number which will not load properly
    oparams{end}.records.file.board_folder_name = [filesep oparams{end}.records.file.board_folder_name];
  end
end

%% Print out segments
% =========================================================================
if ~isempty(param.preprocess.param_fn)
  % Print parameter spreadsheet values to stdout and param_txt_fn
  % =========================================================================
  fid = 1;
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  cmd\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'cmd',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  records\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'records',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  qlook\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'qlook',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  sar\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'sar',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  array\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'array',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  radar\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'radar',oparams,fid);
  fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  post\n'); fprintf(fid,'%s</strong>\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'post',oparams,fid);
  for idx = 1:length(param.preprocess.param_worksheets)
    worksheet_name = param.preprocess.param_worksheets{idx};
    fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  %s\n',worksheet_name); fprintf(fid,'%s</strong>\n','='*ones(1,80));
    read_param_xls_print(param.preprocess.param_fn,worksheet_name,oparams,fid);
  end
  fprintf(fid,'\n');
  
  param_txt_fn = ct_filename_ct_tmp(param,'','param', [param.preprocess.date_str,'.txt']);
  fprintf('Writing %s\n\n', param_txt_fn);
  param_txt_fn_dir = fileparts(param_txt_fn);
  if ~exist(param_txt_fn_dir,'dir')
    mkdir(param_txt_fn_dir);
  end
  [fid,msg] = fopen(param_txt_fn,'wb');
  if fid<0
    error('Could not write to %s: %s\n', param_txt_fn, msg);
  end
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  cmd\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'cmd',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  records\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'records',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  qlook\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'qlook',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  sar\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'sar',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  array\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'array',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  radar\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'radar',oparams,fid);
  fprintf(fid,'\n');
  fprintf(fid,'%s\n','='*ones(1,80)); fprintf(fid,'  post\n'); fprintf(fid,'%s\n','='*ones(1,80));
  read_param_xls_print(param.preprocess.param_fn,'post',oparams,fid);
  fprintf(fid,'\n');
  for idx = 1:length(param.preprocess.param_worksheets)
    worksheet_name = param.preprocess.param_worksheets{idx};
    fprintf(fid,'<strong>%s\n','='*ones(1,80)); fprintf(fid,'  %s\n',worksheet_name); fprintf(fid,'%s</strong>\n','='*ones(1,80));
    read_param_xls_print(param.preprocess.param_fn,worksheet_name,oparams,fid);
    fprintf(fid,'\n');
  end
  fclose(fid);
end

%% Exit task
% =========================================================================
fprintf('%s done %s\n', mfilename, datestr(now));

success = true;
