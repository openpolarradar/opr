

params = read_param_xls(ct_filename_param('accum_param_2023_Antarctica_BaslerMKB.xlsx'),'',{'analysis_noise','analysis'});

params = ct_set_params(params,'cmd.generic',0);
% params = ct_set_params(params,'cmd.generic',1,'day_seg','20231227_02');



%% Automated Section
% =====================================================================

% Input checking
global gRadar;
if exist('param_override','var')
  param_override = merge_structs(gRadar,param_override);
else
  param_override = gRadar;
end

% Process each of the segments
for param_idx = 1:length(params)
  param = params(param_idx);
  if ~isfield(param.cmd,'generic') || iscell(param.cmd.generic) || ischar(param.cmd.generic) || ~param.cmd.generic
    continue;
  end
  
  
  fns = get_filenames('/scratch/accum/2023_Antarctica_BaslerMKB/CSARP_analysis/','coh_noise_simp',param.day_seg,'.mat');
  
  for fn_idx = 1:length(fns)
    
    fn = fns{fn_idx};
    fprintf('%s\n', fn);
    
    generic = load(fn,'param_records','gps_time','firdec_gps_time');
    
    %     generic.gps_time = generic.gps_time + offset;
    
    
    delta_offset = param.records.gps.time_offset - generic.param_records.records.gps.time_offset;
    if delta_offset ~= 0
      fprintf('    Delta GPS time offset is %.1f sec\n', delta_offset);
      
      generic.param_records.records.gps.time_offset = param.records.gps.time_offset;
      generic.gps_time = generic.gps_time + delta_offset;
      generic.firdec_gps_time = generic.firdec_gps_time + delta_offset;
      % Check for gps_time dependent fields (lat, lon, elev, roll, pitch,
      % heading)
      save(fn,'-struct','generic','param_records','gps_time','firdec_gps_time','-append');
    end
    
  end
  
  
end




