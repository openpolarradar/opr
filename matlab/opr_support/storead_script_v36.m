function [s, s_info] = storead_script_v36(pathname,filename,ii)
% -------------------------------------------------------------------------
% SYNTAX:
% [s, s_info] = storead_script_v36(pathname,filename,ii)
% NOTE: FIRST FIVE LINES MODIFIED FROM ORIGINAL SCRIPT + s_info==>s_info
%
% DESCRIPTION:
% Reads any data file created by the St. Olaf radar systems
%
% INPUT:
%   filename    String containing the filename that contain the data
%
% OUTPUT:
%   raw             An n x 1 array of structure containing the data, where
%                   n is the number of channels
%   s_info         A structure containing information about the data 
%                   in the file
%
% REMARKS:
%
% EXAMPLES:
%
% AUTHOR:
% I. Campbell 6/21/05
% Department of Physics, St. Olaf College, MN, USA
%
% REVISION HISTORY:
% Renamed fields and fixed bugs, R. Pettersson  Nov. 2005
% At some point this was de-functionalized into a script (perhaps
% 2006-2007). Re-adapted to a new version of gekko (3.6). B. Youngblood and
% K. Lapo June 2010
% Change in type for metadata.nominal frequency added by RWJ for
% Version 3.8 in September 2013.
% 
% COMPABILITY:
%
% DEPENDENCIES:
%
% REFERENCES:
% -------------------------------------------------------------------------

full_filename = [pathname{ii} filename{ii}];

% Open the file
fid = fopen(full_filename,'r'); 
if(fid < 0)
    msg = sprintf('Error reading the file: %s', full_filename);
    error(msg);
end

% ==================== File Header ==================== %

% Version number divided by 100
s_info.Version = fread(fid, 1, 'int16')/100;    
% Ends in NULL character
s_info.Filename = char(fread(fid, 64, 'char'));   
% Decimal day from 1 Jan 1970 We add an offset to 1 Jan 1970 to get
% MATLAB date numbers
s_info.Serialtime = fread(fid,1,'float64') + datenum(1970,1,1);   
% Time zone the time is stored in minutes and transformed to decimal days
s_info.Timezone = fread(fid,1,'int16')/1440; 
% Number of channels recorded
s_info.nChannels = fread(fid,1,'uint8');     
% Acquistition method (0 = Odometer, 1 = Stacks, 2 = Time)
s_info.RecordMode = fread(fid,1,'uint8'); 
switch(s_info.RecordMode)
    case 0
        s_info.RecordModeString = 'Odometer';
    case 1
        s_info.RecordModeString = 'Stacks';
    case 2
        s_info.RecordModeString = 'Time';
    otherwise
        warning('Unknown Record Mode');
end 
% Recording interval, when the recording method is stacks this is 0,
% otherwise is is the distance/time settings triggering a new trace
s_info.RecordInterval = fread(fid,1,'int16');     
% Number of stacks per trace
s_info.NumberOfStacks = fread(fid,1,'int16');  
% Sampling Frequency in MHz
s_info.SampFreq = fread(fid,1,'int16') * 1e6;
% Pretrigger depth
s_info.PreTriggerDepth = fread(fid,1,'int16'); 
% Postrigger depth
s_info.PostTriggerDepth = fread(fid,1,'int16'); 
% Trigger source (1 = Chan A, 2 = Chan B, -1 = External)
s_info.TriggerSource = fread(fid,1,'int8');   
switch(s_info.TriggerSource)
    case 1
        s_info.triggersourceString = 'Channel A';
    case 2
        s_info.triggersourceString = 'Channel B';
    case -1
        s_info.triggersourceString = 'External';
    otherwise
        warning('Unknown in Trigger Source');
end
% Trigger slope (0 = positive, 1 = negative)
s_info.TriggerSlope = fread(fid,1,'uint8');    
switch(s_info.TriggerSlope)
    case 0
        s_info.TriggerSlopeString = 'Negative';
    case 1
        s_info.TriggerSlopeStrong = 'Positive';
    otherwise
        warning('Unknown Trigger Slope');
end
% External Trigger range (Full range in in mV)
s_info.ExtTriggerRange = fread(fid,1,'int16');
% External trigger coupling (0 = DC, 1 = AC)
s_info.ExtTriggerCoupling = fread(fid,1,'uint8'); 
switch(s_info.ExtTriggerCoupling)
    case 0
        s_info.ExtTriggerCouplingString = 'DC';
    case 1
        s_info.ExtTriggerCouplingString = 'AC';
    otherwise
        warning('Unknown External Trigger Coupling');
end

% Odometer calibration constant(meters per trigger)
% (Only available for pre 3.21 version)
if(s_info.Version < 3.21)
    s_info.OdometerCalibration = fread(fid,1,'int16');
end


% Nominal Freqiency (MHz)
% (Change of type in Version 3.8, N.B. this adds 2 bytes to header length)
if(s_info.Version < 3.8)
    s_info.NominalFrequency = fread(fid,1,'int16');
else
    s_info.NominalFrequency = fread(fid,1,'float32');
end

% Antenna separation (m)
s_info.AntennaSeparation = fread(fid,1,'float32');

% Read and toss extra blank spaceor 
% Only needed for pre 3.6 version
if(s_info.Version < 3.6) 
    buffer = fread(fid,27,'int8');  
end

% ==================== Channel Headers ==================== %
  
for(nn = 1:s_info.nChannels)
    % Channel number
    nChan = fread(fid,1,'uint8');
		
	if(nChan ~= nn)
        error('Corrupt Channel header');
    end
    % Construct channel name
    ChName = sprintf('Channel%d', nChan);
    % Full voltage range in mV
    s_info.(ChName).VoltRange = fread(fid,1,'int16');
    %Channel Impedance (0 = 50 Ohm, 1 = 1 MOhm)
    s_info.(ChName).Impedance = fread(fid,1,'uint8');
	
    switch(s_info.(ChName).Impedance)
        case 0
            s_info.(ChName).ImpedanceString = '1 MOhm';
        case 1
            s_info.(ChName).ImpedanceString = '50 Ohm';
        otherwise
            msg = sprintf('Unknown Impedance for Channel %d', nChan);
            warning(msg);
    end
    % Channel coupling (0 = DC, 1 = AC)
    s_info.(ChName).Coupling = fread(fid,1,'uint8');
    switch(s_info.(ChName).Coupling)
        case 0
            s_info.(ChName).CouplingString = 'DC';
        case 1
            s_info.(ChName).CouplingString = 'AC';
        otherwise
            msg = sprintf('Unknown Coupling for Channel %d', nChan);
            warning(msg);
    end
    
    
    % Read and toss extra blank space
   % (Only needed for pre 3.6 version)
    if(s_info.Version < 3.6)
        buffer = fread(fid,27,'int8');  
    end
    
    % Travel time
    s(nn).TravelTime = [-s_info.PreTriggerDepth:...
                        (s_info.PostTriggerDepth - 1)]'...
                         * 1/s_info.SampFreq;
end

% ==================== Trace Headers and data ==================== %

% Set trace counter
nTrc = 1;
while 1
%     try
        for(nn = 1:s_info.nChannels)
            % Read Trace header type (0 = Trace, 1 = Marker, 2 = Comment)
            nHeaderType = fread(fid,1,'uint8');
            % Recording channel number
            nChannel = fread(fid,1,'uint8');
			
            if(nChannel ~= nn)
                error('Corrupt Trace header');
            end
            % Trace number in file set
            s(nn).nTrace(nTrc) = fread(fid,1,'int32'); 
            % Decimal day from 1 Jan 1970. 
            nTime = fread(fid,1,'float64');
            % We add an offset to 1 Jan 1970 to get MATLAB date numbers
            s(nn).Time(nTrc) = nTime + datenum(1970,1,1);
            % Stacks/trace unless record mode is stacks, when it is
            % time/trace
            s(nn).TraceInterval(nTrc) = fread(fid,1,'float32');
            % Trigger level in percentage of input range in mV
            s(nn).TriggerLevel(nTrc) = fread(fid,1,'int16'); 
            % This is a bug that was fixed since no versions have ever used
            % an odometer or pressure reading. 3.6 and beyond do not use
            % these fields.
            if(s_info.Version < 3.21)   
                % Odometer readings (0 if no Odometer is used)
                s(nn).Odometer(nTrc) = fread(fid,1,'float32');

                % Pressure gauge (0 if no pressure gauge is used)
                s(nn).Pressure(nTrc) = fread(fid,1,'float32');  
            end
            % GPS Latitude
            s(nn).Latitude(nTrc) = fread(fid,1,'float64');
            % GPS longitude
            s(nn).Longitude(nTrc) = fread(fid,1,'float64'); 
            % GPS altitude
            s(nn).Altitude(nTrc) = fread(fid,1,'float32');
            % GPS accuracy
            s(nn).GPSResolution(nTrc) = fread(fid,1,'float32');
            
            % Read and toss last blank bytes
                % (Only needed for pre 3.6 version)
                if(s_info.Version < 3.6)
                    if(s_info.Version < 3.2)  
                        buffer = fread(fid,12,'int8'); 
                    else
                        buffer = fread(fid,14,'int8'); 
                    end
                end

            % If it is actual radar data, not a comment or marker
           if(nHeaderType == 0)  
               % Total number of data points 
               nMaxPts = s_info.PreTriggerDepth ...
                        + s_info.PostTriggerDepth;
                % Read the trace data
                cur_pos = ftell(fid); % cur_pos: Get the byte offset to the start of the radar data waveform
                [newdata, count] = fread(fid, nMaxPts, '*int16');
                % Store data
                s(nn).Data(1:nMaxPts, nTrc) = newdata;   
                s(nn).offset(nTrc) = cur_pos;
            elseif(nHeaderType == 1)
                % Read marker number
                nNumber = fread(fid,1,'int16');   
                % Read trace number
                marker(nNumber).TraceNumber = fread(fid,1,'int32');
                % Read TIme 
                nTime = fread(fid,1,'float64');
                % We add an offset to 1 Jan 1970 to get MATLAB date numbers
                marker(nNumber).Time = nTime + datenum(1970,1,1);
                % GPS Latitude
                marker(nNumber).Latitude = fread(fid,1,'float64');
                % GPS longitude
                marker(nNumber).Longitude = fread(fid,1,'float64'); 
                % GPS altitude
                marker(nNumber).Altitude = fread(fid,1,'float32');
                % GPS altitude
                marker(nNumber).GPSResolution = fread(fid,1,'float32');
                % Break the channel loop and restart
                break;  
           end
        end
        %Move forward one byte to test for end of file. If the EOF is not
        % reached, step back one byte, otherwise break the while statement
        status = fseek(fid,1,'cof');
        if(status == 0)
            fseek(fid,-1,'cof');
            % Increment number of traces
            nTrc = nTrc + 1;
%             buffer = fread(fid,14,'int8');
        else
            break;
        end
%     catch
%          % Display error message
%          msg = sprintf('[storead_script.m]: Error reading the file: %s',full_filename);
%          error(msg);
%     end
end

% Close the file
fclose(fid);

%clear unnecessary variables.  This is necessary in the script version of
%this routine since the variables are in the Matlab workspace (B. Welch
%5/2007).

clear newdata
