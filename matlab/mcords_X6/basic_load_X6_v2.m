function [hdr,data] = basic_load_X6_v2(fn,param)
% [hdr,data] = basic_load_X6_v2(fn,param)
%
% Raw data loader for Vapor HF RDS. Based on the X6 VHF radar digital
% system.
%
% Data collected outside with no transmit:
% fn = '/resfs/GROUPS/CRESIS/General/projects/Arnold_CAREER/Data/06142024/rec2400001.dat';
% [hdr,data] = basic_load_X6_v2(fn,param);
%
% Data collected in the lab:???
% fn = '/resfs/GROUPS/CRESIS/General/projects/Arnold_CAREER/Data/06102024/rec1600001.dat';
% [hdr,data] = basic_load_X6_v2(fn,param);
%
% Data collected in the lab:???
% fn = '/resfs/GROUPS/CRESIS/General/projects/Arnold_CAREER/Data/06102024/rec2000001.dat';
% [hdr,data] = basic_load_X6_v2(fn,param);

param = [];
param.clk = 100e6;
record_size = 8192;
num_sam = 4085;

% Open file little-endian for reading
[fid,msg] = fopen(fn,'rb','ieee-le');
if fid < 1
  fprintf('Could not open file %s\n', fn);
  error(msg);
end

raw_data = fread(fid,[record_size,4096],'int16=>int16');
fclose(fid);

Nx = size(raw_data,2);

hdr = [];
hdr.frame_sync = typecast(reshape(raw_data(1:2,:),[1 2*Nx]),'uint32');
hdr.epri = typecast(reshape(raw_data(3:4,:),[1 2*Nx]),'uint32');
hdr.unknown1 = double(typecast(reshape(raw_data(5:6,:),[1 2*Nx]),'uint32'));
raw_seconds = double(typecast(reshape(raw_data(7:8,:),[1 2*Nx]),'uint32'));
hour = bitand(bitshift(raw_seconds,-20),15)*10 + bitand(bitshift(raw_seconds,-16),15);
min =  bitand(bitshift(raw_seconds,-12),15)*10 + bitand(bitshift(raw_seconds,-8),15);
sec =  bitand(bitshift(raw_seconds,-4),15)*10 + bitand(bitshift(raw_seconds,-0),15);
hdr.seconds = hour*3600 + min*60 + sec;
hdr.fractions = double(typecast(reshape(raw_data(9:10,:),[1 2*Nx]),'uint32'));
hdr.utc_time_sod = hdr.seconds + hdr.fractions/param.clk;
wf_hdr = double(typecast(reshape(raw_data(11,:),[1 Nx]),'uint8'));
hdr.wfs(1).unknown2 = wf_hdr(1:2:end);
hdr.wfs(1).unknown3 = wf_hdr(2:2:end);
hdr.wfs(1).num_sam = num_sam;
hdr.wfs(2).num_wfs = 2*ones(1,Nx);
hdr.wfs(2).wf_idx = 0*ones(1,Nx); % Zero-indexed

wf_offset = 4096;
hdr.wfs(2).sync = typecast(reshape(raw_data(wf_offset+(1:2),:),[1 2*Nx]),'uint32');
hdr.wfs(2).epri = typecast(reshape(raw_data(wf_offset+(3:4),:),[1 2*Nx]),'uint32');
hdr.wfs(2).unknown1 = double(typecast(reshape(raw_data(wf_offset+(5:6),:),[1 2*Nx]),'uint32'));% In param.radar_clk clock cycles
raw_seconds = double(typecast(reshape(raw_data(wf_offset+(7:8),:),[1 2*Nx]),'uint32'));
hour = bitand(bitshift(raw_seconds,-20),15)*10 + bitand(bitshift(raw_seconds,-16),15);
min =  bitand(bitshift(raw_seconds,-12),15)*10 + bitand(bitshift(raw_seconds,-8),15);
sec =  bitand(bitshift(raw_seconds,-4),15)*10 + bitand(bitshift(raw_seconds,-0),15);
hdr.wfs(2).seconds = hour*3600 + min*60 + sec;
hdr.wfs(2).fractions = double(typecast(reshape(raw_data(wf_offset+(9:10),:),[1 2*Nx]),'uint32'));
hdr.wfs(2).utc_time_sod = hdr.seconds + hdr.fractions/param.clk;
wf_hdr = double(typecast(reshape(raw_data(wf_offset+(11),:),[1 Nx]),'uint8'));
hdr.wfs(2).unknown2 = wf_hdr(1:2:end);
hdr.wfs(2).unknown3 = wf_hdr(2:2:end);
hdr.wfs(2).num_sam = num_sam;
hdr.wfs(2).num_wfs = 2*ones(1,Nx);
hdr.wfs(2).wf_idx = 1*ones(1,Nx); % Zero-indexed

data = {};
data{1} = raw_data(12+(0:num_sam-1),:);
data{2} = raw_data(wf_offset+12+(0:num_sam-1),:);

%% Plot debug figures
if 0 % Enable for debugging
  h_fig = figure(1);
  set(h_fig,'WindowStyle','docked');
  clf;
  plot(hdr.frame_sync);
  xlabel('Record')
  
  h_fig = figure(2);
  set(h_fig,'WindowStyle','docked');
  clf;
  plot(diff(hdr.epri));
  xlabel('Record')
  ylabel('diff(hdr.epri)')

  h_fig = figure(3);
  set(h_fig,'WindowStyle','docked');
  clf;
  plot(hdr.seconds);
  xlabel('Record')
  ylabel('Floor UTC time (seconds of day)')
  
  h_fig = figure(4);
  set(h_fig,'WindowStyle','docked');
  clf;
  plot(hdr.fractions);
  xlabel('Record')
  ylabel('PPS fraction (10 MHz clock cycles)')

  h_fig = figure(5);
  set(h_fig,'WindowStyle','docked');
  clf;
  plot(diff(hdr.utc_time_sod));
  xlabel('Record');
  ylabel('diff(hdr.utc\_time\_sod)');
  
  pri = mean(diff(hdr.utc_time_sod))
  prf = 1/pri

  % link_figures([1 2 3 4 5],'x');

  h_fig = figure(10);
  set(h_fig,'WindowStyle','docked');
  clf;
  plot(data{1}(:,1));
  hold on
  plot(data{2}(:,1));
  legend('wf1','wf2')

  h_fig = figure(11);
  set(h_fig,'WindowStyle','docked');
  clf;
  imagesc(data{1});
  colorbar;

  h_fig = figure(12);
  set(h_fig,'WindowStyle','docked');
  clf;
  imagesc(data{2});
  colorbar;
end
