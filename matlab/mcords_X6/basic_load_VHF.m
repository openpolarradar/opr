%read in data
% fn='P:\ARISS\HTG_ZRF8\scratch_data\test00001.dat';
fn='P:\ARISS\HTG_ZRF8\scratch_data\short00000.dat';
fid=fopen(fn,'r','ieee-le');
x =  fread(fid,[8192,4096],'int16');
fclose(fid);
header = x(1:11,:);
% convert int16 to uint16
mask = header < 0;
header = header + mask*65536;
header6 = header(11,:);
% convert two uint16s to uint32 for the first 10 uint16s;
header = header(1:2:10,:) + 65536*header(2:2:10,:);
header = [header;header6];

% waveforms
data = x(12:8192,:);
sync = header(1,:);                   % hex2dec('1ACFFC1D') = 449838109
epri = header(2,:);                   % increments one per record
pri_count = header(3,:);              % PRI count in terms of 122.88 MHz clock corresponding to 10kHz (12288)
time = header(4,:);                   % binary coded decimal for 00hhmmss
hour = bitand(bitshift(time,-20),15)*10 + bitand(bitshift(time,-16),15);
min =  bitand(bitshift(time,-12),15)*10 + bitand(bitshift(time,-8),15);
sec =  bitand(bitshift(time,-4),15)*10 + bitand(bitshift(time,-0),15);
seconds = hour*3600 + min*60 + sec;
frac = header(5,:);                   % fraction (100 MHz clock)
wf_hexstr = dec2hex(header(6,:));
wf_num = str2num(wf_hexstr(:,1))+1;            % number of waveforms
wf_idx = str2num(wf_hexstr(:,2))+1;            % waveform index
figure(1);clf;plot(sync);
figure(2);clf;plot(diff(epri));
figure(3);clf;plot(seconds);
figure(4);clf;plot(frac);
figure(5);clf;plot(data(:,1));


