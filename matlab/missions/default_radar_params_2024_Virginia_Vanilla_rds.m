function [param,defaults] = default_radar_params_2024_Virginia_Vanilla_rds
% [param,defaults] = default_radar_params_2024_Virginia_Vanilla_rds
%
% rds: 2024_Virginia_Vanilla
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: Jilu Li, John Paden
%
% See also: default_radar_params_*.m

%% Preprocess parameters
param.season_name = '2024_Virginia_Vanilla';
param.radar_name = 'vhfrds';

% Reading in files
param.preprocess.digital_system_type = 'cresis';
param.preprocess.header_load_func = @basic_load_VHF_v2;

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

param.records.file.xml_version = -1; % No XML file available

param.preprocess.tx_enable = [1];

%% CReSIS preprocess parameters
param.preprocess.cresis.expected_rec_sizes = [16384];
param.preprocess.gps_file_mask = 'LOG*';

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.file.boards = {''};
param.records.file.version = 423;
param.records.file.prefix = '';
param.records.file.suffix = '.dat';
param.records.file.clk = 245.76e6;
param.records.frames.geotiff_fn = fullfile('greenland','Landsat-7','Greenland_natural_90m.tif');
param.records.frames.mode = 2;
param.records.frames.length = 2000;
param.records.gps.en = 1;
param.records.gps.time_offset = 1;

%% Qlook worksheet
if 0
  param.qlook.img_comb = [];
  param.qlook.imgs = {[1 1]};
else
  param.qlook.img_comb = [10e-6 -inf 1e-6];
  param.qlook.imgs = {[ones([4 1]),(1:4).'], [2*ones([4 1]),(1:4).']};
end
param.qlook.out_path = '';
param.qlook.block_size = 2000;
param.qlook.motion_comp = 0;
param.qlook.dec = 40;
%param.qlook.B_filter = hanning(801); % Add this in by hand
param.qlook.inc_dec = 5;
param.qlook.surf.en = 1;
param.qlook.surf.profile = 'RDS';


%% SAR worksheet
param.sar.out_path = '';
param.sar.imgs = param.qlook.imgs;
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 2000;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = 4e-6;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 8.1;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.imgs = param.qlook.imgs;
param.array.img_comb = param.qlook.img_comb;
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -2:2;
param.array.dbin = 1;
param.array.dline = 5;

%% Radar worksheet
param.radar.prf = 1e4;
param.radar.fs = 245.76e6;
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 2;
param.radar.lever_arm_fh = @lever_arm;
chan_equal_Tsys = [0 0 0 0]/1e9;
chan_equal_dB = [0 0 0 0];
chan_equal_deg = [0 0 0 0];
for wf = 1:4
  param.radar.wfs(wf).Tadc_adjust = 0.0;
  param.radar.wfs(wf).tx_weights = [0.3,0,0,0];
  param.radar.wfs(wf).ft_dec = [1 1];
  param.radar.wfs(wf).adc_gains_dB = 23.; 
  param.radar.wfs(wf).rx_paths = [1:4]; % ADC to rx path mapping
  param.radar.wfs(wf).ref_fn = '';
  param.radar.wfs(wf).chan_equal_Tsys = chan_equal_Tsys;
  param.radar.wfs(wf).chan_equal_dB = chan_equal_dB;
  param.radar.wfs(wf).chan_equal_deg = chan_equal_deg;
  param.radar.wfs(wf).coh_noise_method = '';
  param.radar.wfs(wf).adcs = [];
  param.radar.wfs(wf).nz_trim = {[0 0]};
  param.radar.wfs(wf).nz_valid = [0];
end

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Greenland';
param.post.map.type = 'combined';
% param.post.echo.elev_comp = 2;
% param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+1000]';
param.post.echo.elev_comp = 3;
param.post.echo.depth = '[publish_echogram_switch(Bbad,0.25,Surface_Elev,-1000,DBottom,-100),max(Surface_Elev+100)]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'arctic';

%% Analysis worksheet
param.analysis_noise.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_noise.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_noise.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average


%% Radar Settings

% This is usually read from an XLM file, which the VHF does not have.
% Ptx ~50 dBm (100W), Gtx, Grx ~2.2dBi,
defaults = {};

default = param;

if 1
  wf = 1;
  default.radar.wfs(wf).f0 = 75e6;
  default.radar.wfs(wf).f1 = 80e6;
  default.radar.wfs(wf).tukey = 0.2;
  default.radar.wfs(wf).Tpd = 1e-9;
  fc = (default.radar.wfs(wf).f0+default.radar.wfs(wf).f1)/2;
  default.radar.wfs(wf).BW_window = fc+[-2.5e6 +2.5e6];
  default.radar.wfs(wf).t_ref = 0;
  default.radar.wfs(wf).system_dB = 10*log10(100)+0+0+20*log10(300000000/(8*pi*fc))+10*log10(50);%Pt+Gr+Gt+term+Z0

  wf = 2;
  default.radar.wfs(wf).f0 = 75e6;
  default.radar.wfs(wf).f1 = 80e6;
  default.radar.wfs(wf).tukey = 0.2;
  default.radar.wfs(wf).Tpd = 10e-6;
  fc = (default.radar.wfs(wf).f0+default.radar.wfs(wf).f1)/2;
  default.radar.wfs(wf).BW_window = fc+[-2.5e6/2 +2.5e6/2];
  default.radar.wfs(wf).t_ref = 0;
  default.radar.wfs(wf).system_dB = 10*log10(100)+0+0+20*log10(300000000/(8*pi*fc))+10*log10(50);%Pt+Gr+Gt+term+Z0

end

default.name='default_params';
default.config_regexp = '.*';

defaults{end+1} = default;
