function [param,defaults] = default_radar_params_2024_Antarctica_GroundGHOST_rds
% param = default_radar_params_2024_Antarctica_GroundGHOST_rds
%
% rds: 2024_Antarctica_GroundGHOST and 2024_Antarctica_GroundGHOST2
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: John Paden

%% Preprocess parameters
param.season_name = '2024_Antarctica_GroundGHOST';
param.radar_name = 'rds';

% Reading in files
param.preprocess.digital_system_type = 'arena';
param.preprocess.wg_type = 'arena';
param.preprocess.header_load_func = @basic_load_arena;
param.preprocess.tx_map = {'awg1'};

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

% Creating settings files
param.preprocess.max_data_rate = 60;
param.preprocess.max_duty_cycle = 0.1;
param.preprocess.prf_multiple = [10e6 10e6/20]; % Power supply sync signal that PRF must be a factor of these numbers
param.preprocess.PRI_guard = 1e-6;
param.preprocess.PRI_guard_percentage = 450e6/500e6;
param.preprocess.tx_enable = [1 1];
param.preprocess.max_tx = 1.0;
param.preprocess.max_tx_voltage = sqrt(400*50)*10^(-2/20); % voltage at max_tx

%% GHOST 1 rds Arena Parameters
arena = [];
arena.clk = 10e6;
fs = 480e6;
fs_snow = 1000e6;
fs_dac = 480e6;
subsystem_idx = 0;
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'CTU';
arena.subsystem(subsystem_idx).subSystem{1} = 'ctu';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'SNOW';
arena.subsystem(subsystem_idx).subSystem{1} = 'awg0';
arena.subsystem(subsystem_idx).subSystem{2} = 'digrx0';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'RDS_RX1';
arena.subsystem(subsystem_idx).subSystem{1} = 'digrx1';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'RDS_AWG_RX2';
arena.subsystem(subsystem_idx).subSystem{1} = 'awg1';
arena.subsystem(subsystem_idx).subSystem{2} = 'digrx2';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'Data Server';

dac_idx = 0;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'awg0';
arena.dac(dac_idx).type = 'dac-ad9129_0012';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = -10;
arena.dac(dac_idx).desiredAlignMax = 0;
arena.dac(dac_idx).dcoPhase = 80;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'awg1';
arena.dac(dac_idx).type = 'dac-ad9783_002C';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = -4;
arena.dac(dac_idx).desiredAlignMax = 6;
arena.dac(dac_idx).dcoPhase = 80;

adc_idx = 0;
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx0';
arena.adc(adc_idx).type = 'adc-ad9680_0017';
arena.adc(adc_idx).sampFreq = fs_snow;
arena.adc(adc_idx).adcMode = 1;
arena.adc(adc_idx).desiredAlignMin = -8;
arena.adc(adc_idx).desiredAlignMax = 2;
arena.adc(adc_idx).stream = 'socket';
arena.adc(adc_idx).ip = '172.16.0.121';
arena.adc(adc_idx).outputSelect = 1;
arena.adc(adc_idx).wf_set = 1;
arena.adc(adc_idx).gain_dB = [0 0];
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx1';
arena.adc(adc_idx).type = 'adc-ad9684_002D';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 1;
arena.adc(adc_idx).desiredAlignMin = NaN;
arena.adc(adc_idx).desiredAlignMax = NaN;
arena.adc(adc_idx).stream = 'socket';
arena.adc(adc_idx).ip = '172.16.0.18';
arena.adc(adc_idx).outputSelect = 1;
arena.adc(adc_idx).wf_set = 2;
arena.adc(adc_idx).gain_dB = [0 0];
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx2';
arena.adc(adc_idx).type = 'adc-ad9684_002D';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 1;
arena.adc(adc_idx).desiredAlignMin = NaN;
arena.adc(adc_idx).desiredAlignMax = NaN;
arena.adc(adc_idx).stream = 'socket';
arena.adc(adc_idx).ip = '172.16.0.121';
arena.adc(adc_idx).outputSelect = 1;
arena.adc(adc_idx).wf_set = 2;
arena.adc(adc_idx).gain_dB = [0 0];

daq_idx = 0;
daq_idx = daq_idx + 1;
arena.daq(daq_idx).name = 'daq0';
arena.daq(daq_idx).type = 'daq_0001';
arena.daq(daq_idx).auxDir = '/data/';
arena.daq(daq_idx).fileStripe = '/data/%b/';
arena.daq(daq_idx).fileName = 'rds_%b';
arena.daq(daq_idx).udp_packet_headers = false;

arena.system.name = 'ku0001';

arena.param.tx_max = [1 1];
arena.param.PA_setup_time = 2e-6; % Time required to enable PA before transmit
arena.param.TTL_time_delay = 0.0; % TTL time delay relative to transmit start
arena.param.ADC_time_delay = -2e-6; % ADC time delay relative to transmit start

arena.psc.type = 'psc_0003';

arena.daq.type = 'daq_0001';

arena.ctu.name = 'ctu';
arena.ctu.type = 'ctu_001D';
if 1
  % External GPS
  arena.ctu.nmea = 31;
  arena.ctu.nmea_baud = 115200;
  arena.ctu.pps = 10;
  arena.ctu.pps_polarity = 1;
else
  % Internal GPS
  arena.ctu.nmea = 60;
  arena.ctu.nmea_baud = 115200;
  arena.ctu.pps = 63;
  arena.ctu.pps_polarity = 1;
end
idx = 0;
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'EPRI';
arena.ctu.out.bit_group(idx).bits = 0;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [0 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'PRI';
arena.ctu.out.bit_group(idx).bits = 1;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'PA'; % 1 enables the transmitter
arena.ctu.out.bit_group(idx).bits = 2;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'TR'; % 1 enables tx-ant path
arena.ctu.out.bit_group(idx).bits = 3;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'Blank'; % 1 enables rx blank mode
arena.ctu.out.bit_group(idx).bits = 4;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];

arena.ctu.out.time_cmd = {'2e-6+param.wfs(wf).Tpd+0.1e-6' '2e-6+param.wfs(wf).Tpd+0.1e-6' '2e-6+param.wfs(wf).Tpd+0.1e-6' '2/param.prf'};

param.arena = arena;

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.gps.time_offset = 0;
param.records.frames.geotiff_fn = 'antarctica\Landsat-7\Antarctica_LIMA.tif';
param.records.frames.mode = 1;
param.records.file.version = 103;
param.records.file.midfix = param.radar_name;
param.records.file.suffix = '.dat';
param.records.file.boards = {'digrx1','digrx2'};
param.records.file.board_folder_name = '%b';
param.records.file.clk = 10e6;
param.records.arena.epri_time_error_threshold	= 1;
param.records.arena.mode_latch_fix = [(0:15)' floor((0:15)'/2)*2];

%% Qlook worksheet
param.qlook.out_path = '';
param.qlook.block_size = 5000;
param.qlook.motion_comp = 0;
param.qlook.dec = 20;
param.qlook.inc_dec = 10;
param.qlook.surf.en = 1;
param.qlook.surf.method = 'fixed';
param.qlook.surf.fixed_value = 0;
param.qlook.resample = [2 1];

%% SAR worksheet
param.sar.out_path = '';
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 5000;
param.sar.mocomp.en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 1;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.bin_rng = 0;
param.array.line_rng = -5:5;
param.array.dbin = 1;
param.array.dline = 6;
param.array.ft_over_sample = 2;

%% Radar worksheet
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 1.5;
param.radar.lever_arm_fh = @lever_arm;

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.img = 0;
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 2;
param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+500]';
% param.post.echo.elev_comp = 3;
% param.post.echo.depth = '[min(Surface_Elev)-1500 max(Surface_Elev)+100]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'antarctic';

%% Analysis worksheet
param.analysis_noise.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_noise.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_noise.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average

%% Radar Settings

defaults = {};

%% Radar Settings: Survey Mode Thin Ice Single Polarization
% =========================================================================
default = [];
default.records.arena.total_presums = 76*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  ],[...
  0  0 0 1 5
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  2  0 2 1 6 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  4  2 0 2 5
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  6  2 2 2 6 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  ]};
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1; 1 2; 1 3; 1 4; 1 5; 1 7; 2 1; 2 2; 2 3; 2 4; 2 5; 2 7]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 3 3 4 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+1500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_config_thin_pingpong$';
default.name = '140-190 MHz Thin Pingpong w/ Snow';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thin Ice Single Polarization (no ANT5/ANT6)
% =========================================================================
default = [];
default.records.arena.total_presums = 78*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  ],[...
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  ]};
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1; 1 2; 1 3; 1 4; 1 7; 1 8; 2 1; 2 2; 2 3; 2 4; 2 7; 2 8]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 NaN NaN 3 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+1500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_config_thin_pingpong_noANT5_noANT6$';
default.name = '140-190 MHz Thin Pingpong (no ANT5/ANT6) w/ Snow';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization
% =========================================================================
default = [];
default.records.arena.total_presums = 62*4;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  8  4 0 3 1
  9  4 1 3 2
  10 4 2 3 3
  11 4 3 3 4
  12 6 0 4 1
  13 6 1 4 2
  14 6 2 4 3
  15 6 3 4 4
  ],[...
  0  0 0 1 5
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  2  0 2 1 6 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  4  2 0 2 5
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  6  2 2 2 6 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  8  4 0 3 5
  9  4 1 3 7 % ANT6 and ANT7 are out of order
  10 4 2 3 6 % ANT6 and ANT7 are out of order
  11 4 3 3 8
  12 6 0 4 5
  13 6 1 4 7 % ANT6 and ANT7 are out of order
  14 6 2 4 6 % ANT6 and ANT7 are out of order
  15 6 3 4 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[3 1; 3 2; 3 3; 3 4; 3 5; 3 7; 4 1; 4 2; 4 3; 4 4; 4 5; 4 7],[1 1; 1 2; 1 3; 1 4; 1 5; 1 7; 2 1; 2 2; 2 3; 2 4; 2 5; 2 7]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:4
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 3 3 4 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(3).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(4).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+3500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_config_thick_pingpong$';
default.name = '140-190 MHz Thick Pingpong';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization
% =========================================================================
default = [];
default.records.arena.total_presums = 62*4;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  8  4 0 3 1
  9  4 1 3 2
  10 4 2 3 3
  11 4 3 3 4
  12 6 0 4 1
  13 6 1 4 2
  14 6 2 4 3
  15 6 3 4 4
  ],[...
  0  0 0 1 5
  1  0 1 1 6
  2  0 2 1 7
  3  0 3 1 8
  4  2 0 2 5
  5  2 1 2 6
  6  2 2 2 7
  7  2 3 2 8
  8  4 0 3 5
  9  4 1 3 6
  10 4 2 3 7
  11 4 3 3 8
  12 6 0 4 5
  13 6 1 4 6
  14 6 2 4 7
  15 6 3 4 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[3 1; 3 2; 3 3; 3 4; 3 5; 3 6; 4 1; 4 2; 4 3; 4 4; 4 5; 4 6],[1 1; 1 2; 1 3; 1 4; 1 5; 1 6; 2 1; 2 2; 2 3; 2 4; 2 5; 2 6]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:4
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 3 4 4 3]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = -1.745e-6;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
default.radar.wfs(3).tx_weights = [1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
default.radar.wfs(4).tx_weights = [0 0 0 0 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+3000]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_rds_config_thick_pingpong2024$';
default.name = '140-190 MHz Thick Pingpong';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization (no ANT5/ANT6)
% =========================================================================
default = [];
default.records.arena.total_presums = 62*4;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  8  4 0 3 1
  9  4 1 3 2
  10 4 2 3 3
  11 4 3 3 4
  12 6 0 4 1
  13 6 1 4 2
  14 6 2 4 3
  15 6 3 4 4
  ],[...
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  9  4 1 3 7 % ANT6 and ANT7 are out of order
  11 4 3 3 8
  13 6 1 4 7 % ANT6 and ANT7 are out of order
  15 6 3 4 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[3 1; 3 2; 3 3; 3 4; 3 7; 3 8; 4 1; 4 2; 4 3; 4 4; 4 7; 4 8],[1 1; 1 2; 1 3; 1 4; 1 7; 1 8; 2 1; 2 2; 2 3; 2 4; 2 7; 2 8]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:4
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 NaN NaN 3 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(3).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(4).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+3500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_config_thick_pingpong_noANT5_noANT6$';
default.name = '140-190 MHz Thick Pingpong (no ANT5/ANT6)';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thin Ice Single Polarization (no snow radar)
% =========================================================================
default = [];
default.records.arena.total_presums = 254*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  ],[...
  0  0 0 1 5
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  2  0 2 1 6 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  4  2 0 2 5
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  6  2 2 2 6 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  ]};
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1; 1 2; 1 3; 1 4; 1 5; 1 7; 2 1; 2 2; 2 3; 2 4; 2 5; 2 7]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 3 3 4 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+1500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_rds_config_thin_pingpong$';
default.name = '140-190 MHz Thin Pingpong';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thin Ice Single Polarization (no ANT5/ANT6) (no snow radar)
% =========================================================================
default = [];
default.records.arena.total_presums = 254*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  ],[...
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  ]};
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1; 1 2; 1 3; 1 4; 1 7; 1 8; 2 1; 2 2; 2 3; 2 4; 2 7; 2 8]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 NaN NaN 3 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+1500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_rds_config_thin_pingpong_noANT5_noANT6$';
default.name = '140-190 MHz Thin Pingpong (no ANT5/ANT6)';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization (no snow radar)
% =========================================================================
default = [];
default.records.arena.total_presums = 102*4;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  8  4 0 3 1
  9  4 1 3 2
  10 4 2 3 3
  11 4 3 3 4
  12 6 0 4 1
  13 6 1 4 2
  14 6 2 4 3
  15 6 3 4 4
  ],[...
  0  0 0 1 5
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  2  0 2 1 6 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  4  2 0 2 5
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  6  2 2 2 6 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  8  4 0 3 5
  9  4 1 3 7 % ANT6 and ANT7 are out of order
  10 4 2 3 6 % ANT6 and ANT7 are out of order
  11 4 3 3 8
  12 6 0 4 5
  13 6 1 4 7 % ANT6 and ANT7 are out of order
  14 6 2 4 6 % ANT6 and ANT7 are out of order
  15 6 3 4 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[3 1; 3 2; 3 3; 3 4; 3 5; 3 7; 4 1; 4 2; 4 3; 4 4; 4 5; 4 7],[1 1; 1 2; 1 3; 1 4; 1 5; 1 7; 2 1; 2 2; 2 3; 2 4; 2 5; 2 7]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:4
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 3 3 4 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(3).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(4).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+3500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_rds_config_thick_pingpong$';
default.name = '140-190 MHz Thick Pingpong';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization (no ANT5/ANT6) (no snow radar)
% =========================================================================
default = [];
default.records.arena.total_presums = 102*4;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  8  4 0 3 1
  9  4 1 3 2
  10 4 2 3 3
  11 4 3 3 4
  12 6 0 4 1
  13 6 1 4 2
  14 6 2 4 3
  15 6 3 4 4
  ],[...
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  9  4 1 3 7 % ANT6 and ANT7 are out of order
  11 4 3 3 8
  13 6 1 4 7 % ANT6 and ANT7 are out of order
  15 6 3 4 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[3 1; 3 2; 3 3; 3 4; 3 7; 3 8; 4 1; 4 2; 4 3; 4 4; 4 7; 4 8],[1 1; 1 2; 1 3; 1 4; 1 7; 1 8; 2 1; 2 2; 2 3; 2 4; 2 7; 2 8]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:4
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 NaN NaN 3 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(3).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(4).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+3500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_rds_config_thick_pingpong_noANT5_noANT6$';
default.name = '140-190 MHz Thick Pingpong (no ANT5/ANT6)';
defaults{end+1} = default;

%% Radar Settings: LAB TEST 2024
% =========================================================================
% This lab test was recorded with ghost_rds_config_thick and the config
% file needs to have the PSC settings need to be updated:
%      <config type="psc_0003">
%         <name>ghost_rds_config_thick</name>
% -->
%      <config type="psc_0003">
%         <name>lab_test</name>
default = [];
default.records.arena.total_presums = 204*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  ],[...
  0  0 0 1 5
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  2  0 2 1 6 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  ]};
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1; 1 2; 1 3; 1 4; 1 5; 1 7]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:1
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 3 3 4 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+4500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^lab_test$';
default.name = 'Lab test';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization (no snow radar, no ping pong)
% =========================================================================
default = [];
default.records.arena.total_presums = 204*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  4 0 2 1
  5  4 1 2 2
  6  4 2 2 3
  7  4 3 2 4
  ],[...
  0  0 0 1 5
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  2  0 2 1 6 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  4  4 0 2 5
  5  4 1 2 7 % ANT6 and ANT7 are out of order
  6  4 2 2 6 % ANT6 and ANT7 are out of order
  7  4 3 2 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[1 1; 1 2; 1 3; 1 4; 1 5; 1 7],[2 1; 2 2; 2 3; 2 4; 2 5; 2 7]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 3 3 4 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+4500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_rds_config_thick$';
default.name = '140-190 MHz Thick';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization (no snow radar, no ping pong)
% =========================================================================
default = [];
default.records.arena.total_presums = 204*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  4 0 2 1
  5  4 1 2 2
  6  4 2 2 3
  7  4 3 2 4
  ],[...
  0  0 0 1 5
  1  0 1 1 6
  2  0 2 1 7
  3  0 3 1 8
  4  4 0 2 5
  5  4 1 2 6
  6  4 2 2 7
  7  4 3 2 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[2 1;2 2;2 3;2 4;2 5;2 6],[1 1;1 2;1 3;1 4;1 5;1 6]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 3 4 5 6 5 6]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = -1.745e-6;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 3 4]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [-3.8 123 0 117.3 154.2 -165.9 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+4500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_rds_config_thick2024$';
default.name = '140-190 MHz Thick';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization (no ANT5/ANT6)  (no snow radar, no ping pong)
% =========================================================================
default = [];
default.records.arena.total_presums = 204*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  ],[...
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[1 1; 1 2; 1 3; 1 4; 1 7; 1 8],[2 1; 2 2; 2 3; 2 4; 2 7; 2 8]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 NaN NaN 3 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+4500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^ghost_rds_config_thick_noANT5_noANT6$';
default.name = '140-190 MHz Thick';
defaults{end+1} = default;

%% Radar Settings: Test (unknown setup)
% =========================================================================
default = [];
default.records.arena.total_presums = 204*2;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1 1
  1  0 1 1 2
  2  0 2 1 3
  3  0 3 1 4
  4  2 0 2 1
  5  2 1 2 2
  6  2 2 2 3
  7  2 3 2 4
  ],[...
  0  0 0 1 5
  1  0 1 1 7 % ANT6 and ANT7 are out of order
  2  0 2 1 6 % ANT6 and ANT7 are out of order
  3  0 3 1 8
  4  2 0 2 5
  5  2 1 2 7 % ANT6 and ANT7 are out of order
  6  2 2 2 6 % ANT6 and ANT7 are out of order
  7  2 3 2 8
  ]};
default.qlook.img_comb = [10e-06 -inf 1e-06];
default.qlook.imgs = {[1 1; 1 2; 1 3; 1 4; 1 5; 1 7],[2 1; 2 2; 2 3; 2 4; 2 5; 2 7]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = 35.64*ones(1,8); % ADC gain
  default.radar.wfs(wf).rx_paths = [1 2 5 6 3 3 4 4]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0 0 0 0 0 0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).tx_paths = {[1 2 5 6]}; % awg1-CH0-->ANT1, awg1-CH1-->ANT2, awg1-CH2-->ANT5, awg1-CH3-->ANT6
  default.radar.wfs(wf).DDC_dec = 8;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0];
end
default.radar.wfs(1).tx_weights = [1 1 0 0 0 0 0 0];
default.radar.wfs(2).tx_weights = [1 1 0 0 0 0 0 0];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+4500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '.*';
default.name = 'Unknown Setup';
defaults{end+1} = default;
