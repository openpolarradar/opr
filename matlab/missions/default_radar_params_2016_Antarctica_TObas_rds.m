function [param,defaults] = default_radar_params_2016_Antarctica_TObas_rds
% [param,defaults] = default_radar_params_2016_Antarctica_TObas_rds
%
% RDS: 2016_Antarctica_TObas
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Author: John Paden

%% Preprocess parameters
param.season_name = '2016_Antarctica_TObas';
param.radar_name = 'rds';

param.preprocess.digital_system_type = 'bas';
param.preprocess.header_load_func = @load;

param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.file.version = 414;
param.records.file.regexp = {};
param.records.data_map = {};
param.records.file.boards = {};
adc_names = {'RxP1','RxP2','RxP3','RxP4','RxB5','RxB6','RxB7','RxB8','RxS9','RxSA','RxSB','RxSC'};
for adc = 1:12
  adc_name = adc_names{adc};
  wf_names = {'TxP1234_%s_C13L1','TxP1234_%s_C13L4','TxP1234_%s_J13L4','TxS9ABC_%s_C13L4','TxS9ABC_%s_J13L4'};
  for wf = 1:length(wf_names)
    wf_name = wf_names{wf};
    param.records.file.regexp{end+1} = sprintf(['.*' wf_name '.*[0-9].mat'], adc_name);
    param.records.data_map{end+1} = [NaN NaN wf adc];
    if adc>=1 && adc<=4
        param.records.file.boards{end+1} = 'Port';
    elseif adc>=5 && adc<=8
        param.records.file.boards{end+1} = 'Belly';
    elseif adc>=9 && adc<=12
        param.records.file.boards{end+1} = 'Star';
    end
  end
end
param.records.frames.geotiff_fn = 'antarctica/Landsat-7/Antarctica_LIMA_480m.tif';
param.records.frames.mode = 1;
param.records.gps.time_offset = 0;

%% Qlook worksheet
param.qlook.out_path = '';
param.qlook.block_size = 10000;
param.qlook.motion_comp = 0;
param.qlook.dec = 20;
param.qlook.inc_dec = 10;
param.qlook.surf.en = 1;
param.qlook.surf.profile = 'RDS_OIB';
param.qlook.img_comb = [4e-06 -inf 1e-06];
param.qlook.imgs = {[1*ones(4,1),(1:4).'],[2*ones(4,1),(1:4).']};

%% SAR worksheet
param.sar.out_path = '';
param.sar.chunk_len = 5000;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = inf;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 2.5;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;
param.sar.imgs = {[ones([12 1]),(1:12).'],[2*ones([12 1]),(1:12).'],[4*ones([12 1]),(1:12).']};

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -5:5;
param.array.dbin = 1;
param.array.dline = 6;
param.array.DCM = [];
param.array.Nsv = 1;
param.array.theta_rng = [0 0];
param.array.sv_fh = @array_proc_sv;
param.array.diag_load = 0;
param.array.Nsig = 2;
param.array.imgs = param.qlook.imgs;
param.array.img_comb = param.qlook.img_comb;

%% Radar worksheet
param.radar.fs = 120e6;
param.radar.Tadc = []; % normally leave empty to use value in file header
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 2;
param.radar.lever_arm_fh = @lever_arm;
param.radar.adc_bits = 14;
param.radar.fs = 120e6;
param.radar.prf = 15625;
param.radar.Vpp_scale = 2;
param.radar.Tadc_adjust = 0; % System time delay: leave this empty or set it to zero at first, determine this value later using data over surface with known height or from surface multiple
param.radar.lever_arm_fh = @lever_arm;
for wf = 1:5
  param.radar.wfs(wf).tukey = 0.1;
  param.radar.wfs(wf).f0 = 143.5e6;
  param.radar.wfs(wf).f1 = 156.5e6;
  param.radar.wfs(wf).rx_paths = [1:12]; % ADC to rx path mapping
  param.radar.wfs(wf).presums = 25;
  param.radar.wfs(wf).Tadc_adjust = 0; % System time delay: leave this empty or set it to zero at first, determine this value later using data over surface with known height or from surface multiple
end
param.radar.wfs(2).next = [3*ones(12,1) (1:12).'];
param.radar.wfs(4).next = [5*ones(12,1) (1:12).'];
param.radar.wfs(2).weight = 0.5*ones(1,12);
param.radar.wfs(3).weight = -0.5*ones(1,12);
param.radar.wfs(4).weight = 0.5*ones(1,12);
param.radar.wfs(5).weight = -0.5*ones(1,12);
param.radar.wfs(1).Tpd = 1e-6;
param.radar.wfs(2).Tpd = 4e-6;
param.radar.wfs(3).Tpd = 4e-6;
param.radar.wfs(4).Tpd = 4e-6;
param.radar.wfs(5).Tpd = 4e-6;
param.radar.wfs(1).adc_gains_dB = 27*ones(1,12); % Gain from the first LNA to the ADC
param.radar.wfs(2).adc_gains_dB = 45*ones(1,12); % Gain from the first LNA to the ADC
param.radar.wfs(3).adc_gains_dB = 45*ones(1,12); % Gain from the first LNA to the ADC
param.radar.wfs(4).adc_gains_dB = 45*ones(1,12); % Gain from the first LNA to the ADC
param.radar.wfs(5).adc_gains_dB = 45*ones(1,12); % Gain from the first LNA to the ADC
param.radar.wfs(1).tx_weights = [2000 2000 2000 2000 0 0 0 0 0 0 0 0];
param.radar.wfs(2).tx_weights = [2000 2000 2000 2000 0 0 0 0 0 0 0 0];
param.radar.wfs(3).tx_weights = [2000 2000 2000 2000 0 0 0 0 0 0 0 0];
param.radar.wfs(4).tx_weights = [0 0 0 0 0 0 0 0 2000 2000 2000 2000];
param.radar.wfs(5).tx_weights = [0 0 0 0 0 0 0 0 2000 2000 2000 2000];

Tsys = [0 0 0 0 0 0 0 0 0 0 0 0]/1e9;
chan_equal_dB = [0 0 0 0 0 0 0 0 0 0 0 0];
chan_equal_deg = [0 0 0 0 0 0 0 0 0 0 0 0];
for wf = 1:5
  param.radar.wfs(wf).Tsys = Tsys;
  param.radar.wfs(wf).chan_equal_dB = chan_equal_dB;
  param.radar.wfs(wf).chan_equal_deg = chan_equal_deg;
end

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.layer_dir = 'layerData';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 2;
param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+1500]';
% param.post.echo.elev_comp = 3;
% param.post.echo.depth = '[min(Surface_Elev)-1500 max(Surface_Elev)+100]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'antarctic';

param.preprocess.param_worksheets = {};
%% analysis_noise worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_noise';
param.analysis_noise.block_size = 10000;
param.analysis_noise.imgs = param.sar.imgs;
param.analysis_noise.cmd{1}.method = 'coh_noise';

%% analysis_equal worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_equal';
param.analysis_equal.block_size = 10000;
param.analysis_equal.imgs = param.sar.imgs;
param.analysis_equal.cmd{1}.method = 'waveform';
param.analysis_equal.cmd{1}.start_time = struct('eval',struct('cmd','s=s-1.5e-6;'));
param.analysis_equal.cmd{1}.Nt = 128;
param.analysis_equal.cmd{1}.dec = 10;

%% analysis_spec worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_spec';
param.analysis_spec.block_size = 10000;
param.analysis_spec.imgs = param.sar.imgs;
param.analysis_spec.cmd{1}.method = 'specular';
param.analysis_spec.cmd{1}.rlines = 128;
param.analysis_spec.cmd{1}.threshold = 40;
param.analysis_spec.cmd{1}.signal_doppler_bins = [1:4 125:128];
param.analysis_spec.cmd{1}.noise_doppler_bins = [12:117];

%% Radar Settings

defaults = {};

% Survey Mode
default.name = 'Survey Mode 143.5-156.5 MHz';
defaults{end+1} = default;
