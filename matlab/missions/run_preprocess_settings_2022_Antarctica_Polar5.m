% script run_preprocess_settings_AWI.m
%
% Support script for run_preprocess.m

param.preprocess = [];

%% MCORDS5 AWI UWB - SINGLE DAY
% cur_idx = length(param.preprocess.default)+1;
% param.preprocess.default{cur_idx} = @default_radar_params_2022_Greenland_Polar5_rds;
% param.preprocess.base_dir{cur_idx} = 'G:\';
% param.preprocess.config_folder_names{cur_idx} = '20220603';
% param.preprocess.board_folder_names{cur_idx} = fullfile('20220603','%b');
% param.preprocess.date_str{cur_idx} = '20220603';

%% SNOW8 SINGLE DAY
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = @default_radar_params_2022_Antarctica_Polar5_snow;
param.preprocess.base_dir{cur_idx} = '/albedo/work/user/ajutila/Data/';
param.preprocess.config_folder_names{cur_idx} = fullfile('2212030901');
param.preprocess.board_folder_names{cur_idx} = fullfile('2212030901','%b');
param.preprocess.date_str{cur_idx} = '20221203';

