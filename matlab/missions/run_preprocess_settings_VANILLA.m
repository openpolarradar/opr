% script run_preprocess_VANILLA.m
%
% Support script for run_preprocess.m
%
% Preprocess setup script for 2021_Arctic_Vanilla.

param.preprocess = [];

% Snow9 SINGLE DAY
% cur_idx = length(param.preprocess.default)+1;
% param.preprocess.default{cur_idx} = default_radar_params_2021_Arctic_Vanilla_snow();
% param.preprocess.base_dir{cur_idx} = '/cresis/snfs1/data/SnowRadar/2021_Arctic_Vanilla/';
% param.preprocess.config_folder_names{cur_idx} = '/20210414';
% param.preprocess.board_folder_names{cur_idx} = '/20210414';
% param.preprocess.date_strs{cur_idx} = '20210414';

cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = default_radar_params_2021_Arctic_Vanilla_snow();
param.preprocess.base_dir{cur_idx} = '/cresis/snfs1/data/SnowRadar/2021_Arctic_Vanilla/';
param.preprocess.config_folder_names{cur_idx} = '/20210819/sysconfig';
param.preprocess.board_folder_names{cur_idx} = '/20210819/data';
param.preprocess.date_strs{cur_idx} = '20210819';