% script run_preprocess_settings_SIR.m
%
% Support script for run_preprocess.m

param.preprocess = [];
param.preprocess.default = {};

if ispc
  base_dir = fullfile();
else
  base_dir = fullfile('/kucresis/scratch/data/SIR/20162017/radar/sir/raw');
end

% preprocess_list is an N by 4 cell array with the columns representing in
% order:
%   base_dir, config_folder_name, board_folder_name, date_str
preprocess_list = { ...
  '','../../../../pnt/pod-ins/trajectory/F1001/txt/','F1001','20161122'; ...
  '','../../../../pnt/pod-ins/trajectory/F1002/txt/','F1002','20161123'; ...
  '','../../../../pnt/pod-ins/trajectory/F1003/txt/','F1003','20161124'; ...
  '','../../../../pnt/pod-ins/trajectory/F1004/txt/','F1004','20161127'; ...
  '','../../../../pnt/pod-ins/trajectory/F1005/txt/','F1005','20161129'; ...
  '','../../../../pnt/pod-ins/trajectory/F1006/txt/','F1006','20161129b'; ...
  '','../../../../pnt/pod-ins/trajectory/F1007/txt/','F1007','20161130'; ...
  '','../../../../pnt/pod-ins/trajectory/F1008/txt/','F1008','20161201'; ...
  '','../../../../pnt/pod-ins/trajectory/F1009/txt/','F1009','20161202'; ...
  '','../../../../pnt/pod-ins/trajectory/F1010/txt/','F1010','20161202b'; ...
  };

default_function_handle = @default_radar_params_2016_Antarctica_LC130_snow;

if 0
  %% SINGLE DAY

  preprocess_list_idx = 9;

  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = default_function_handle;
  param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
  param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
  param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
  param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};

elseif 1
  %% MULTIPLE DAYS

  for preprocess_list_idx = 1:size(preprocess_list,1)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = default_function_handle;
    param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
    param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
    param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
    param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};
  end
end
