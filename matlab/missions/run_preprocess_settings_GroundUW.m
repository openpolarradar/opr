% script run_preprocess_settings_GroundUW.m
%
% Support script for run_preprocess.m

param.preprocess = [];

%% HF RDS - SINGLE DAY
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = @default_radar_params_2019_Antarctica_GroundUW_rds;
param.preprocess.base_dir{cur_idx} = 'C:\rds\2019_Antarctica_GroundUW\';
param.preprocess.config_folder_names{cur_idx} = '29Dec2019';
param.preprocess.board_folder_names{cur_idx} = '29Dec2019';
param.preprocess.date_str{cur_idx} = '20191229';
