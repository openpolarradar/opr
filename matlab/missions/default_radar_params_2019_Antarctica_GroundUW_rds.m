function [param,defaults] = default_radar_params_2019_Antarctica_GroundUW
% [param,defaults] = default_radar_params_2019_Antarctica_GroundUW
%
% rds: 2019_Antarctica_GroundUW
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: John Paden

%% Preprocess parameters
param.season_name = '2019_Antarctica_GroundUW';
param.radar_name = 'rds';

% Reading in files
param.preprocess.daq_type = 'stolaf';

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

%% StoDeep RDS Parameters
fs = 200e6;

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.gps.time_offset = 0;
param.records.frames.geotiff_fn = 'antarctica\Landsat-7\Antarctica_LIMA.tif';
param.records.frames.mode = 1;
param.records.file.version = 416;
param.records.file.prefix = ''; % No required character prefix
param.records.file.suffix = '.gtd'; % All data files have this extension
param.records.file.boards = {''};
param.records.file.board_folder_name = '';
param.records.file.clk = 200e6;

%% Qlook worksheet
param.qlook.out_path = '';
param.qlook.block_size = 5000;
param.qlook.motion_comp = 0;
param.qlook.dec = 20;
param.qlook.inc_dec = 10;
param.qlook.surf.en = 1;
param.qlook.surf.method = 'fixed';
param.qlook.surf.fixed_value = 0;
param.qlook.resample = [2 1];
param.qlook.img_comb = [];
param.qlook.imgs = {[1 1],[1 2]};

%% SAR worksheet
param.sar.out_path = '';
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 5000;
param.sar.chunk_overlap = 10;
param.sar.frm_overlap = 0;
param.sar.coh_noise_removal = 0;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = inf;
param.sar.pulse_rfi.en = [];
param.sar.pulse_rfi.inc_ave= [];
param.sar.pulse_rfi.thresh_scale = [];
param.sar.trim_vals = [];
param.sar.pulse_comp = 1;
param.sar.ft_dec = 1;
param.sar.ft_wind = @hanning;
param.sar.ft_wind_time = 0;
param.sar.lever_arm_fh = @lever_arm;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 2.5;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;
param.sar.imgs = param.qlook.imgs;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -5:5;
param.array.dbin = 1;
param.array.dline = 6;
param.array.DCM = [];
param.array.Nsv = 1;
param.array.theta_rng = [0 0];
param.array.sv_fh = @array_proc_sv;
param.array.diag_load = 0;
param.array.Nsig = 2;
param.array.imgs = param.qlook.imgs;
param.array.img_comb = param.qlook.img_comb;

%% Radar worksheet
param.radar.fs = 200e6;
param.radar.adc_bits = 14; % NEEDS TO BE UPDATED
param.radar.Vpp_scale = 1.5; % NEEDS TO BE UPDATED
param.radar.lever_arm_fh = @lever_arm;
for wf = 1:1
  param.radar.wfs(wf).Tpd = 0.2e-6; % NEEDS TO BE UPDATED
  param.radar.wfs(wf).f0 = 3e6; % NEEDS TO BE UPDATED
  param.radar.wfs(wf).f1 = 3e6; % NEEDS TO BE UPDATED
  param.radar.wfs(wf).BW = [1e6 5e6]; % NEEDS TO BE UPDATED
  %param.radar.wfs(wf).zero_pad; % NEEDS TO BE UPDATED
  %param.radar.wfs(wf).tukey = 0; % NEEDS TO BE UPDATED
  param.radar.wfs(wf).tx_weights = 1;
  param.radar.wfs(wf).adc_gains_dB = [38 38]; % ADC gain
  param.radar.wfs(wf).adcs = [1 2]; % ADC to rx path mapping
  param.radar.wfs(wf).rx_paths = [1 2]; % ADC to rx path mapping
  param.radar.wfs(wf).gain_en = [0 0]; % Disable fast-time gain correction
  param.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  param.radar.wfs(wf).Tadc_adjust = 0;
  param.radar.wfs(wf).bit_shifts = [6 8];
  param.radar.wfs(wf).Tsys = Tsys;
  param.radar.wfs(wf).chan_equal_dB = chan_equal_dB;
  param.radar.wfs(wf).chan_equal_deg = chan_equal_deg;
  param.radar.wfs(wf).bit_shifts = [6 8];
end
Tsys = [0 0]/1e9;
chan_equal_dB = [0 0];
chan_equal_deg = [0 0];
param.radar.ref_fn = '';

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.img = 1;
param.post.layer_dir = 'layerData';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 2;
param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+2900]';
% param.post.echo.elev_comp = 3;
% param.post.echo.depth = '[min(Surface_Elev)-1500 max(Surface_Elev)+100]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'antarctic';

%% Analysis worksheet
param.analysis_noise.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_noise.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_noise.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average
param.analysis_noise.imgs = param.qlook.imgs;

%% Radar Settings

defaults = {};

% Survey Mode
default.name = 'Survey Mode 1-5 MHz';
defaults{end+1} = default;
