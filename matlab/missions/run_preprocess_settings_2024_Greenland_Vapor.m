% script run_preprocess_settings_2024_Greenland_Vapor.m
%
% Support script for run_preprocess_Vapor.m

param.preprocess = [];
param.preprocess.default = {};

if ispc
  base_dir = fullfile('P:\','Arnold_CAREER','Data');
  %base_dir = fullfile('Z:\','data','HF_Sounder','2024_Greenland_Vapor');
else
  base_dir = fullfile('/resfs','GROUPS','CRESIS','General','projects','Arnold_CAREER','Data');
  %base_dir = fullfile('/cresis','snfs1','data','HF_Sounder','2024_Greenland_Vapor');
end

default_function_handle = @default_radar_params_2024_Greenland_Vapor_rds;

% preprocess_list is an N by 4 cell array with the columns representing in
% order:
%   base_dir, config_folder_name, board_folder_name, date_str
preprocess_list = { ...
  '','06102024/GPS','06102024','20240610'; ...
  '','06142024/GPS','06142024','20240614'; ... % Bad seconds field that requires hack to be enabled in preprocess_task_cresis.m line ~783
  '','06212024/GPS','06212024','20240621'; ...
  };

if 1
  %% SINGLE DAY

  preprocess_list_idx = 3;

  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = default_function_handle;
  param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
  param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
  param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
  param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};

elseif 1
  %% MULTIPLE DAYS

  for preprocess_list_idx = 1:size(preprocess_list,1)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = default_function_handle;
    param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
    param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
    param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
    param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};
  end
end
