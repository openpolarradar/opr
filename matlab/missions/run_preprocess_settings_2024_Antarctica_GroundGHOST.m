% script run_preprocess_settings_2024_Antarctica_GroundGHOST.m
%
% Support script for run_preprocess_GHOST.m
%
% Preprocess setup script for 2024_Antarctica_GroundGHOST and
% 2024_Antarctica_GroundGHOST2.

param.preprocess = [];
param.preprocess.default = [];

if 0
  %% RDS GHOST1
  if ispc
    base_dir = fullfile('C:\','data');
  else
    base_dir = fullfile('/mnt/data39/data32/MCoRDS/2024_Antarctica_GroundGHOST/');
    %base_dir = fullfile('/kucresis','scratch','data','MCoRDS','2024_Antarctica_GroundGHOST');
    %base_dir = fullfile('/mnt','raid_nvmeblue','data','MCoRDS','2024_Antarctica_GroundGHOST';
  end
  
  % preprocess_list is an N by 4 cell array with the columns representing in
  % order:
  %   base_dir, config_folder_name, board_folder_name, date_str
  preprocess_list = { ...
    '','20241010','20241010/%b','20241010'; ...
    '','20241023','20241023/%b','20241023'; ...
    '','20241024','20241024/%b','20241024'; ...
    '','20241026','20241026/%b','20241026'; ...
    };

  default_function_handle = @default_radar_params_2024_Antarctica_GroundGHOST_rds;
  param_override.season_name = '2024_Antarctica_GroundGHOST';

elseif 1
  %% RDS GHOST2
  if ispc
    base_dir = fullfile('C:\','data');
  else
    %base_dir = fullfile('/kucresis/scratch/data/MCoRDS/2024_Antarctica_GroundGHOST2/');
    base_dir = fullfile('/mnt/data36/');
  end
  
  % preprocess_list is an N by 4 cell array with the columns representing in
  % order:
  %   base_dir, config_folder_name, board_folder_name, date_str
  preprocess_list = { ...
%     '','20241020','20241020/%b','20241020'; ...
%     '','20241021','20241021/%b','20241021'; ...
%     '','20241023','20241023/%b','20241023'; ...
%     '','20241024','20241024/%b','20241024'; ...
%     '','20250102','20250102/%b','20250102'; ...
    '','20250103','20250103/%b','20250103'; ...
    '','20250104','20250104/%b','20250104'; ...
    '','20250106','20250106/%b','20250106'; ...
    '','20250107','20250107/%b','20250107'; ...
    '','20250108','20250108/%b','20250108'; ...
    '','20250111','20250111/%b','20250111'; ...
    '','20250112','20250112/%b','20250112'; ...
    '','20250115','20250115/%b','20250115'; ...
    '','20250117','20250117/%b','20250117'; ...
    '','20250118','20250118/%b','20250118'; ...

};

  default_function_handle = @default_radar_params_2024_Antarctica_GroundGHOST_rds;
  param_override.season_name = '2024_Antarctica_GroundGHOST2';
end

if 0
  %% SINGLE DAY

  preprocess_list_idx = 1;

  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = default_function_handle;
  param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
  param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
  param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
  param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};

elseif 1
  %% MULTIPLE DAYS

  for preprocess_list_idx = 1:size(preprocess_list,1)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = default_function_handle;
    param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
    param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
    param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
    param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};
  end
end
