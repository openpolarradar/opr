% script run_preprocess_settings_PASIN.m
%
% Support script for run_preprocess_PASIN.m

param.preprocess = [];
param.preprocess.default = {};

base_dir = fullfile('/cresis','snfs1','data','PASIN','2016_Antarctica_TObas');

fns_dir_list = { ...
  '','','f11a%b','20170101'; ...
  '','','f30a%b','20170122'; ...
  '','','f31a%b','20170122'; ...
  };

if 1
  %% PASIN RDS SINGLE DAY

  fns_dir_list_idx = 1;

  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = @default_radar_params_2016_Antarctica_TObas_rds;
  param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,fns_dir_list{fns_dir_list_idx,1});
  param.preprocess.file{cur_idx}.config_folder_name = fns_dir_list{fns_dir_list_idx,2};
  param.preprocess.file{cur_idx}.board_folder_name = fns_dir_list{fns_dir_list_idx,3};
  param.preprocess.date_str{cur_idx} = fns_dir_list{fns_dir_list_idx,4};

elseif 0
  %% PASIN RDS MULTIPLE DAYS

  for fns_dir_list_idx = 1:size(fns_dir_list,1)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = @default_radar_params_2016_Antarctica_TObas_rds;
    param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,fns_dir_list{fns_dir_list_idx,1});
    param.preprocess.file{cur_idx}.config_folder_name = fns_dir_list{fns_dir_list_idx,2};
    param.preprocess.file{cur_idx}.board_folder_name = fns_dir_list{fns_dir_list_idx,3};
    param.preprocess.date_str{cur_idx} = fns_dir_list{fns_dir_list_idx,4};
  end
end
