% script run_preprocess_BAS.m
%
% Support script for run_preprocess.m
%
% Preprocess setup script for 2018_Antarctica_TObas and
% 2019_Antarctica_TObas.

param.preprocess = [];

%% ACCUM3 SINGLE DAY
% cur_idx = length(param.preprocess.default)+1;
% param.preprocess.default{cur_idx} = @default_radar_params_2018_Antarctica_TObas_accum;
% param.preprocess.base_dir{cur_idx} = '/cresis/snfs1/data/Accum_Data/2019_Antarctica_TObas/';
% param.preprocess.config_folder_name{cur_idx} = '20191225';
% param.preprocess.board_folder_name{cur_idx} = '20191225/%b';
% param.preprocess.date_str{cur_idx} = '20191225';


%% ACCUM3 MULTIPLE DAYS
% date_str = {'20191215','20191222','20191225','20191226','20191229','20191230'};
date_str = {'20200125','20200126','20200127','20200128'};

for idx = 1:length(date_str)
  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = @default_radar_params_2018_Antarctica_TObas_accum;
  param.preprocess.base_dir{cur_idx} = '/cresis/snfs1/data/Accum_Data/2019_Antarctica_TObas/;
  param.preprocess.config_folder_name{cur_idx} = sprintf('%s/',date_str{idx});
  param.preprocess.board_folder_name{cur_idx} = sprintf('%s/%%b',date_str{idx});
end
