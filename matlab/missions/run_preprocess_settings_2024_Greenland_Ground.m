% script run_preprocess_settings_2024_Greenland_Ground.m
%
% Support script for run_preprocess_EAGER.m
%
% Preprocess setup script for 2024_Greenland_Ground.

param.preprocess = [];
param.preprocess.default = {};

if ispc
  base_dir = fullfile('C:\','data');
else
  base_dir = fullfile('/data');
  %base_dir = fullfile('/kucresis','scratch','data','Accum_Data','2024_Greenland_Ground');
end

% preprocess_list is an N by 4 cell array with the columns representing in
% order:
%   base_dir, config_folder_name, board_folder_name, date_str
preprocess_list = { ...
  '','20240605','20240605/%b','20240605'; ...
  };

default_function_handle = @default_radar_params_2024_Greenland_Ground_accum;

if 1
  %% SINGLE DAY

  preprocess_list_idx = 1;

  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = default_function_handle;
  param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
  param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
  param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
  param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};

elseif 0
  %% MULTIPLE DAYS

  for preprocess_list_idx = 1:size(preprocess_list,1)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = default_function_handle;
    param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
    param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
    param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
    param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};
  end
end
