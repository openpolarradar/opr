function [param,defaults] = default_radar_params_2023_Antarctica_BaslerMKB_accum
% param = default_radar_params_2023_Antarctica_BaslerMKB_accum
%
% accum: 2023_Antarctica_BaslerMKB
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: John Paden
%
% See also: default_radar_params_2023_Antarctica_BaslerMKB_accum_data_map

%% Preprocess parameters
param.season_name = '2023_Antarctica_BaslerMKB';
param.radar_name = 'accum';

% Reading in files
param.preprocess.digital_system_type = 'arena';
param.preprocess.wg_type = 'arena';
param.preprocess.header_load_func = @basic_load_arena;
param.preprocess.tx_map = {'awg0','awg1'};

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

% Creating settings files
param.preprocess.max_data_rate = 60;
param.preprocess.max_duty_cycle = 0.1;
param.preprocess.prf_multiple = [10e6 10e6/20]; % Power supply sync signal that PRF must be a factor of these numbers
param.preprocess.PRI_guard = 1e-6;
param.preprocess.PRI_guard_percentage = 450e6/500e6;
param.preprocess.tx_enable = [1 1];
param.preprocess.max_tx = 1.0;
param.preprocess.max_tx_voltage = sqrt(400*50)*10^(-2/20); % voltage at max_tx

%% COLDEX accum Arena Parameters
arena = [];
arena.clk = 10e6;
fs = 280e6*16;
fs_dac = 280e6*32;
subsystem_idx = 0;
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA5xxRF';
arena.subsystem(subsystem_idx).subSystem{1} = 'dac1';
arena.subsystem(subsystem_idx).subSystem{2} = 'adc';
arena.subsystem(subsystem_idx).subSystem{3} = 'ctu1';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA5xxRF_2';
arena.subsystem(subsystem_idx).subSystem{1} = 'dac2';
arena.subsystem(subsystem_idx).subSystem{2} = 'adc';
arena.subsystem(subsystem_idx).subSystem{3} = 'ctu1';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'Data Server';

dac_idx = 0;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'dac1';
arena.dac(dac_idx).type = 'rfsoc_dac_1002';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = -10;
arena.dac(dac_idx).desiredAlignMax = 0;
arena.dac(dac_idx).dcoPhase = 80;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'dac2';
arena.dac(dac_idx).type = 'rfsoc_dac_1002';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = -4;
arena.dac(dac_idx).desiredAlignMax = 6;
arena.dac(dac_idx).dcoPhase = 80;

adc_idx = 0;
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx0';
arena.adc(adc_idx).type = 'rfsoc_adc_2002';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 1;
arena.adc(adc_idx).desiredAlignMin = -8;
arena.adc(adc_idx).desiredAlignMax = 2;
arena.adc(adc_idx).stream = 'socket';
arena.adc(adc_idx).ip = '172.16.0.122';
arena.adc(adc_idx).outputSelect = 1;
arena.adc(adc_idx).wf_set = 1;
arena.adc(adc_idx).gain_dB = [0 0];
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx1';
arena.adc(adc_idx).type = 'rfsoc_adc_2002';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 1;
arena.adc(adc_idx).desiredAlignMin = NaN;
arena.adc(adc_idx).desiredAlignMax = NaN;
arena.adc(adc_idx).stream = 'socket';
arena.adc(adc_idx).ip = '172.16.0.123';
arena.adc(adc_idx).outputSelect = 1;
arena.adc(adc_idx).wf_set = 2;
arena.adc(adc_idx).gain_dB = [0 0];

daq_idx = 0;
daq_idx = daq_idx + 1;
arena.daq(daq_idx).name = 'daq0';
arena.daq(daq_idx).type = 'daq_0001';
arena.daq(daq_idx).auxDir = '/data/';
arena.daq(daq_idx).fileStripe = '/data/%b/';
arena.daq(daq_idx).fileName = 'accum_%b';
arena.daq.udp_packet_headers = false;
% arena.daq.udp_packet_headers = true; % HACK FOR TEST DATASET

arena.system.name = 'extsyncarena5xx';

arena.param.tx_max = [1 1];
arena.param.PA_setup_time = 2e-6; % Time required to enable PA before transmit
arena.param.TTL_time_delay = 0.0; % TTL time delay relative to transmit start
arena.param.ADC_time_delay = -2e-6; % ADC time delay relative to transmit start

arena.psc.type = 'psc_0008';

arena.daq.type = 'daq_0001';

arena.ctu.name = 'ctu';
arena.ctu.type = 'ctu_001D';
if 1
  % External GPS
  arena.ctu.nmea = 31;
  arena.ctu.nmea_baud = 115200;
  arena.ctu.pps = 10;
  arena.ctu.pps_polarity = 1;
else
  % Internal GPS
  arena.ctu.nmea = 60;
  arena.ctu.nmea_baud = 115200;
  arena.ctu.pps = 63;
  arena.ctu.pps_polarity = 1;
end
idx = 0;
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'EPRI';
arena.ctu.out.bit_group(idx).bits = 0;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [0 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'PRI';
arena.ctu.out.bit_group(idx).bits = 1;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'PA'; % 1 enables the transmitter
arena.ctu.out.bit_group(idx).bits = 2;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'TR'; % 1 enables tx-ant path
arena.ctu.out.bit_group(idx).bits = 3;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'Blank'; % 1 enables rx blank mode
arena.ctu.out.bit_group(idx).bits = 4;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];

arena.ctu.out.time_cmd = {'2e-6+param.wfs(wf).Tpd+0.1e-6' '2e-6+param.wfs(wf).Tpd+0.1e-6' '2e-6+param.wfs(wf).Tpd+0.1e-6' '2/param.prf'};

param.arena = arena;

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.gps.time_offset = 16.839;
param.records.frames.geotiff_fn = fullfile('antarctica','Landsat-7','Antarctica_LIMA.tif');
param.records.frames.mode = 1;
param.records.file.version = 103;
param.records.file.midfix = param.radar_name;
param.records.file.suffix = '.dat';
param.records.file.boards = {'digrx0','digrx1','digrx2'};
param.records.file.board_folder_name = '%b';
param.records.file.clk = 10e6;
param.records.arena.mode_latch_fix = [(0:15)' floor((0:15)'/2)*2];
param.records.arena.sync_time = 'pps';

%% Qlook worksheet
param.qlook.out_path = '';
param.qlook.block_size = 5000;
param.qlook.motion_comp = 0;
param.qlook.dec = 20;
param.qlook.inc_dec = 10;
param.qlook.surf.en = 1;
param.qlook.surf.method = 'accum';
param.qlook.resample = [2 1];

%% SAR worksheet
param.sar.out_path = '';
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 5000;
param.sar.chunk_overlap = 10;
param.sar.frm_overlap = 0;
param.sar.coh_noise_removal = 0;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = inf;
param.sar.pulse_rfi.en = [];
param.sar.pulse_rfi.inc_ave= [];
param.sar.pulse_rfi.thresh_scale = [];
param.sar.trim_vals = [];
param.sar.pulse_comp = 1;
param.sar.ft_dec = 1;
param.sar.ft_wind = @hanning;
param.sar.ft_wind_time = 0;
param.sar.lever_arm_fh = @lever_arm;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 0.5;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -25:25;
param.array.dbin = 1;
param.array.dline = 12;
param.array.DCM = [];
param.array.Nsv = 1;
param.array.theta_rng = [0 0];
param.array.sv_fh = @array_proc_sv;
param.array.diag_load = 0;
param.array.Nsig = 2;
param.array.ft_over_sample = 2;

%% Radar worksheet
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 1.5;
param.radar.lever_arm_fh = @lever_arm;

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.img = 0;
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 3;
param.post.echo.depth = '[publish_echogram_switch(Bbad,0.25,Surface_Elev,-3500,DBottom,-100),max(Surface_Elev+100)]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'antarctic';

%% Analysis noise worksheet
param.analysis_noise.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_noise.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_noise.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average
param.analysis_noise.cmd{cmd_idx}.block_ave = 1000;

%% Analysis equal worksheet
param.analysis_equal.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_equal.cmd{cmd_idx}.method = 'waveform';
param.analysis_equal.cmd{cmd_idx}.start_time = struct('eval',struct('cmd','s=s-1.5e-6;'));
param.analysis_equal.cmd{cmd_idx}.Nt = 128;
param.analysis_equal.cmd{cmd_idx}.dec = 10;


%% Radar Settings

defaults = {};

%% Survey Mode Bad Repeat Count
clear default;
default.records.arena.total_presums = 40;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1  5
  1  0 1 1  6
  2  0 2 1  7
  3  0 3 1  8
  4  0 4 1  1
  5  0 5 1  2
  6  0 6 1  3
  7  0 7 1  4
  8  4 0 2  5
  9  4 1 2  6
  10 4 2 2  7
  11 4 3 2  8
  12 4 4 2  1
  13 4 5 2  2
  14 4 6 2  3
  15 4 7 2  4
  ],[...
  0  0 0 1  9
  1  0 1 1  10
  2  0 2 1  11
  3  0 3 1  12
  4  0 4 1  13
  5  0 5 1  14
  6  0 6 1  15
  7  0 7 1  16
  8  4 0 2  9
  9  4 1 2  10
  10 4 2 2  11
  11 4 3 2  12
  12 4 4 2  13
  13 4 5 2  14
  14 4 6 2  15
  15 4 7 2  16
  ],[...
  0  0 0 1  17
  1  0 1 1  18
  2  0 2 1  19
  3  0 3 1  20
  4  0 4 1  21
  5  0 5 1  22
  6  0 6 1  23
  7  0 7 1  24
  8  4 0 2  17
  9  4 1 2  18
  10 4 2 2  19
  11 4 3 2  20
  12 4 4 2  21
  13 4 5 2  22
  14 4 6 2  23
  15 4 7 2  24
  ]};
default.qlook.img_comb = [8e-06 -inf 2e-06];
default.qlook.imgs = {[ones(16,1) (1:16).'],[2*ones(16,1) (1:16).']};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = {[ones(22,1) (1:22).'],[2*ones(22,1) (1:22).']};
default.analysis_equal.imgs = {[ones(22,1) (1:22).']};
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).tx_weights = zeros(1,22);
  default.radar.wfs(wf).conjugate = 1;
  default.radar.wfs(wf).tx_paths = {[4 3 2 1 inf inf inf inf],[16 15 14 13 inf inf inf inf]}; % dac1-ch1->Ant4, dac1-ch2->Ant3, dac1-ch3->Ant2, dac1-ch4->Ant1, dac2-ch1->Ant16, dac2-ch2->Ant15, dac2-ch3->Ant14, dac2-ch4->Ant13
  default.radar.wfs(wf).DDC_dec = 8; % 280 MHz / 8 = 35 MHz
  default.radar.wfs(wf).bit_shifts = zeros(1,24);
  default.radar.wfs(wf).adc_gains_dB = [38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38]; % ADC gain
  default.radar.wfs(wf).rx_paths = [2 1 4 3 6 5 7 8 10 9 12 11 14 13 16 15 17 18 19 20 21 22 NaN NaN]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = zeros(1,24); % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = -3.5e-6 - 89.909e-9;
  default.radar.wfs(wf).Tsys = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
end

% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = 'psc_bad';
default.name = 'Do not process';
defaults{end+1} = default;


%% Survey Mode Thick Ice Single Polarization
clear default;
default.records.arena.total_presums = 40;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0 0 1  5
  1  0 1 1  6
  2  0 2 1  7
  3  0 3 1  8
  4  0 4 1  1
  5  0 5 1  2
  6  0 6 1  3
  7  0 7 1  4
  8  4 0 2  5
  9  4 1 2  6
  10 4 2 2  7
  11 4 3 2  8
  12 4 4 2  1
  13 4 5 2  2
  14 4 6 2  3
  15 4 7 2  4
  ],[...
  0  0 0 1  9
  1  0 1 1  10
  2  0 2 1  11
  3  0 3 1  12
  4  0 4 1  13
  5  0 5 1  14
  6  0 6 1  15
  7  0 7 1  16
  8  4 0 2  9
  9  4 1 2  10
  10 4 2 2  11
  11 4 3 2  12
  12 4 4 2  13
  13 4 5 2  14
  14 4 6 2  15
  15 4 7 2  16
  ],[...
  0  0 0 1  17
  1  0 1 1  18
  2  0 2 1  19
  3  0 3 1  20
  4  0 4 1  21
  5  0 5 1  22
  6  0 6 1  23
  7  0 7 1  24
  8  4 0 2  17
  9  4 1 2  18
  10 4 2 2  19
  11 4 3 2  20
  12 4 4 2  21
  13 4 5 2  22
  14 4 6 2  23
  15 4 7 2  24
  ]};
default.qlook.img_comb = [8e-06 -inf 2e-06];
default.qlook.imgs = {[ones(16,1) (1:16).'],[2*ones(16,1) (1:16).']};
default.sar.imgs = {[ones(22,1) (1:22).'],[2*ones(22,1) (1:22).']};
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = {[ones(22,1) (1:22).'],[2*ones(22,1) (1:22).']};
default.analysis_equal.imgs = {[ones(22,1) (1:22).']};
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).tx_weights = zeros(1,22);
  default.radar.wfs(wf).conjugate = 1;
  default.radar.wfs(wf).tx_paths = {[3 4 1 2 inf inf inf inf],[15 16 13 14 inf inf inf inf]}; % dac1-ch1->Ant4, dac1-ch2->Ant3, dac1-ch3->Ant2, dac1-ch4->Ant1, dac2-ch1->Ant16, dac2-ch2->Ant15, dac2-ch3->Ant14, dac2-ch4->Ant13
  default.radar.wfs(wf).DDC_dec = 4; % 280 MHz / 8 = 35 MHz
  default.radar.wfs(wf).bit_shifts = zeros(1,24);

  default.radar.wfs(wf).adc_gains_dB = [38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38]; % ADC gain
  default.radar.wfs(wf).rx_paths = [2 1 4 3 6 5 7 8 10 9 12 11 14 13 16 15 17 18 19 20 21 22 NaN NaN]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = zeros(1,24); % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 4.100571428571500e-07;
  default.radar.wfs(wf).Tsys = [16.29 18.2 14.07 17.28 -1.94 -2.13 0.03 -2.79 -10.36 -7 -9.34 -9.87 10.79 11.46 6.92 8.5 50.04 49.4 49.83 49.76 49.26 49.52]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [-96 -163 -162.9 -126.5 -51.4 83.1 166.8 0 -59.6 -109.7 -151.5 -117.8 -67.4 -97.1 91.4 -66.7 147.3 5.3 58.9 -179.3 178 137.3];
end
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = 'psc_survey_leftonly';
default.name = 'Survey Mode 687.5-747.5 MHz Thick Ice';
defaults{end+1} = default;


%% Transmit Equalization 70 MHz sampling frequency
clear default;
default.records.arena.total_presums = 160;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0  0 1  5
  1  0  1 1  6
  2  0  2 1  7
  3  0  3 1  8
  4  0  4 1  1
  5  0  5 1  2
  6  0  6 1  3
  7  0  7 1  4
  8  2  0 2  5
  9  4  0 3  5
  10 6  0 4  5
  11 8  0 5  5
  12 10 0 6  5
  13 12 0 7  5
  14 14 0 8  5
  ],[...
  0  0  0 1  9
  1  0  1 1  10
  2  0  2 1  11
  3  0  3 1  12
  4  0  4 1  13
  5  0  5 1  14
  6  0  6 1  15
  7  0  7 1  16
  8  2  0 2  9
  9  4  0 3  9
  10 6  0 4  9
  11 8  0 5  9
  12 10 0 6  9
  13 12 0 7  9
  14 14 0 8  9
  ],[...
  0  0  0 1  17
  1  0  1 1  18
  2  0  2 1  19
  3  0  3 1  20
  4  0  4 1  21
  5  0  5 1  22
  6  0  6 1  23
  7  0  7 1  24
  8  2  0 2  17
  9  4  0 3  17
  10 6  0 4  17
  11 8  0 5  17
  12 10 0 6  17
  13 12 0 7  17
  14 14 0 8  17
  ]};
default.qlook.img_comb = [8e-06 -inf 2e-06];
default.qlook.imgs = {[ones(16,1) (1:16).'],[2*ones(16,1) (1:16).']};
default.sar.imgs = {[ones(22,1) (1:22).'],[2*ones(22,1) (1:22).']};
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = {[ones(22,1) (1:22).'],[2*ones(22,1) (1:22).']};
default.analysis_equal.imgs = {[ones(22,1) (1:22).']};
default.radar.ref_fn = '';
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
default.analysis_equal.cmd{cmd_idx}.method = 'waveform';
default.analysis_equal.cmd{cmd_idx}.start_time = struct('eval',struct('cmd','s=s-1.5e-6;'));
default.analysis_equal.cmd{cmd_idx}.Nt = 128;
default.analysis_equal.cmd{cmd_idx}.dec = 2; % Counteract slow EPRI
for wf = 1:8
  default.radar.wfs(wf).tx_weights = zeros(1,22);
  default.radar.wfs(wf).conjugate = 1;
  default.radar.wfs(wf).tx_paths = {[3 4 1 2 inf inf inf inf],[15 16 13 14 inf inf inf inf]}; % dac1-ch1->Ant4, dac1-ch2->Ant3, dac1-ch3->Ant2, dac1-ch4->Ant1, dac2-ch1->Ant16, dac2-ch2->Ant15, dac2-ch3->Ant14, dac2-ch4->Ant13
  default.radar.wfs(wf).DDC_dec = 4; % 280 MHz / 8 = 35 MHz
  default.radar.wfs(wf).bit_shifts = zeros(1,24);

  default.radar.wfs(wf).adc_gains_dB = [38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38]; % ADC gain
  default.radar.wfs(wf).rx_paths = [2 1 4 3 6 5 7 8 10 9 12 11 14 13 16 15 17 18 19 20 21 22 NaN NaN]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = zeros(1,24); % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 4.100571428571500e-07;
  default.radar.wfs(wf).Tsys = [16.29 18.2 14.07 17.28 -1.94 -2.13 0.03 -2.79 -10.36 -7 -9.34 -9.87 10.79 11.46 6.92 8.5 50.04 49.4 49.83 49.76 49.26 49.52]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [-96 -163 -162.9 -126.5 -51.4 83.1 166.8 0 -59.6 -109.7 -151.5 -117.8 -67.4 -97.1 91.4 -66.7 147.3 5.3 58.9 -179.3 178 137.3];
end
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^psc_txequal$';
default.name = 'Transmit Equalization 687.5-747.5 MHz';
defaults{end+1} = default;


%% Transmit Equalization 35 MHz sampling frequency
clear default;
default.records.arena.total_presums = 160;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
% Profile Processor Digital System
% Each row of param.records.data_map{board_idx} = [profile mode_latch subchannel wf adc]
% adc's setup to map to ANT1 through ANT8 as labeled on chassis
default.records.data_map = {...
  [...
  0  0  0 1  5
  1  0  1 1  6
  2  0  2 1  7
  3  0  3 1  8
  4  0  4 1  1
  5  0  5 1  2
  6  0  6 1  3
  7  0  7 1  4
  8  2  0 2  5
  9  4  0 3  5
  10 6  0 4  5
  11 8  0 5  5
  12 10 0 6  5
  13 12 0 7  5
  14 14 0 8  5
  ],[...
  0  0  0 1  9
  1  0  1 1  10
  2  0  2 1  11
  3  0  3 1  12
  4  0  4 1  13
  5  0  5 1  14
  6  0  6 1  15
  7  0  7 1  16
  8  2  0 2  9
  9  4  0 3  9
  10 6  0 4  9
  11 8  0 5  9
  12 10 0 6  9
  13 12 0 7  9
  14 14 0 8  9
  ],[...
  0  0  0 1  17
  1  0  1 1  18
  2  0  2 1  19
  3  0  3 1  20
  4  0  4 1  21
  5  0  5 1  22
  6  0  6 1  23
  7  0  7 1  24
  8  2  0 2  17
  9  4  0 3  17
  10 6  0 4  17
  11 8  0 5  17
  12 10 0 6  17
  13 12 0 7  17
  14 14 0 8  17
  ]};
default.qlook.img_comb = [8e-06 -inf 2e-06];
default.qlook.imgs = {[ones(16,1) (1:16).'],[2*ones(16,1) (1:16).']};
default.sar.imgs = {[ones(22,1) (1:22).'],[2*ones(22,1) (1:22).']};
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = {[ones(22,1) (1:22).'],[2*ones(22,1) (1:22).']};
default.analysis_equal.imgs = {[ones(22,1) (1:22).']};
default.radar.ref_fn = '';
default.analysis_equal.cmd{1}.dec = 1; % Counteract slow EPRI
for wf = 1:8
  default.radar.wfs(wf).tx_weights = zeros(1,22);
  default.radar.wfs(wf).conjugate = 1;
  default.radar.wfs(wf).tx_paths = {[3 4 1 2 inf inf inf inf],[15 16 13 14 inf inf inf inf]}; % dac1-ch1->Ant4, dac1-ch2->Ant3, dac1-ch3->Ant2, dac1-ch4->Ant1, dac2-ch1->Ant16, dac2-ch2->Ant15, dac2-ch3->Ant14, dac2-ch4->Ant13
  default.radar.wfs(wf).DDC_dec = 8; % 280 MHz / 8 = 35 MHz
  default.radar.wfs(wf).bit_shifts = zeros(1,24);

  default.radar.wfs(wf).adc_gains_dB = [38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38 38]; % ADC gain
  %default.radar.wfs(wf).adcs = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22]; % ADCs
  default.radar.wfs(wf).rx_paths = [2 1 4 3 6 5 7 8 10 9 12 11 14 13 16 15 17 18 19 20 21 22 NaN NaN]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = zeros(1,24); % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = -2e-6;
  default.radar.wfs(wf).Tsys = [16.29 18.2 14.07 17.28 -1.94 -2.13 0.03 -2.79 -10.36 -7 -9.34 -9.87 10.79 11.46 6.92 8.5 50.04 49.4 49.83 49.76 49.26 49.52]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0];
  default.radar.wfs(wf).chan_equal_deg = [-61.7 -127.8 -121.2 -98.5 -40.6 98.6 0 -152 174.6 113.2 89.4 117.9 -18.1 -37.8 -40.4 -169 1.3 179.9 -127.8 -117.3 -120.5 -145.6];
end
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '^psc_txequal_narrow$';
default.name = 'Transmit Equalization 687.5-712.5 MHz';
defaults{end+1} = default;
