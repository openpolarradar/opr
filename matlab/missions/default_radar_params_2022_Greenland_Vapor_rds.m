function [param,defaults] = default_radar_params_2022_Greenland_Vapor_rds
% [param,defaults] = default_radar_params_2022_Greenland_Vapor_rds
%
% rds: 2022_Greenland_Vapor
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: Emily Arnold, John Paden, Gabe Rose
%
% See also: default_radar_params_*.m

%% Preprocess parameters
param.season_name = '2022_Greenland_Vapor';
param.radar_name = 'rds';

% Reading in files
param.preprocess.digital_system_type = 'cresis';
param.preprocess.header_load_func = @hfrds.basic_load_vapor;

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

% param.preprocess.daq_type = 'cresis';
% param.preprocess.wg_type = 'cresis';
%param.preprocess.header_load_func = @basic_load;
% param.preprocess.header_load_func = @load_data_vapor; %This file was in the folder with other files Bailey made. I believe it to be the Vapor specific basic_load function.
% param.preprocess.board_map = {''};
% param.preprocess.tx_map = {''};

param.records.file.xml_version = -1; % No XML file available

param.preprocess.tx_enable = [1];

%% CReSIS preprocess parameters
param.preprocess.cresis.expected_rec_sizes = [250448];
param.preprocess.cresis.gps_file_mask = 'LOG*';

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.data_map = {[1 1 1 1; 1 2 1 2; 1 3 1 3; 1 4 1 4]};
param.records.file.boards = {''};
param.records.file.version = 420;
param.records.file.prefix = '';
param.records.file.suffix = '.dat';
param.records.file.clk = 100e6;
param.records.frames.geotiff_fn = fullfile('greenland','Landsat-7','Greenland_natural_90m.tif');
param.records.frames.mode = 2;
param.records.frames.length = 2000;
param.records.gps.en = 1;
param.records.gps.time_offset = 1;

%% Qlook worksheet
param.qlook.img_comb = [];
param.qlook.imgs = {[1 1]};
param.qlook.out_path = '';
param.qlook.block_size = 2000;
param.qlook.motion_comp = 0;
param.qlook.dec = 400;
%param.qlook.B_filter = hanning(801); % Add this in by hand
param.qlook.inc_dec = 5;
param.qlook.surf.en = 1;
param.qlook.surf.profile = 'RDS';


%% SAR worksheet
param.sar.out_path = '';
param.sar.imgs = param.qlook.imgs;
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 2000;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = 4e-6;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 8.1;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.imgs = param.qlook.imgs;
param.array.img_comb = param.qlook.img_comb;
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -2:2;
param.array.dbin = 1;
param.array.dline = 5;

%% Radar worksheet
param.radar.prf = 1e4;
param.radar.fs = 100e6;
param.radar.adc_bits = 12;
param.radar.Vpp_scale = 1; % Digital receiver gain is 5, full scale Vpp is 2
param.radar.lever_arm_fh = @lever_arm;
chan_equal_Tsys = [0]/1e9;
chan_equal_dB = [0];
chan_equal_deg = [0];
for wf = 1
  param.radar.wfs(wf).Tadc_adjust = 0.0;
  param.radar.wfs(wf).tx_weights = 1;
  param.radar.wfs(wf).adc_gains_dB = [59]; % Radiometric calibration to 1/R^2
  param.radar.wfs(wf).rx_paths = [1]; % ADC to rx path mapping
  param.radar.wfs(wf).ref_fn = '';
  param.radar.wfs(wf).chan_equal_Tsys = chan_equal_Tsys;
  param.radar.wfs(wf).chan_equal_dB = chan_equal_dB;
  param.radar.wfs(wf).chan_equal_deg = chan_equal_deg;
  param.radar.wfs(wf).coh_noise_method = '';
  param.radar.wfs(wf).adcs = [1];
  param.radar.wfs(wf).nz_trim = {[0 0]};
  param.radar.wfs(wf).nz_valid = [0];
end

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Greenland';
param.post.map.type = 'combined';
% param.post.echo.elev_comp = 2;
% param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+1000]';
param.post.echo.elev_comp = 3;
param.post.echo.depth = '[publish_echogram_switch(Bbad,0.25,Surface_Elev,-1000,DBottom,-100),max(Surface_Elev+100)]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'arctic';

  
%% Radar Settings

%This is usually read from an XLM file, which the HF does not have.
defaults = {};

default = param;

wf = 1;
default.radar.wfs(wf).f0 = 3.3725e7;
default.radar.wfs(wf).f1 = 4.0925e7;
default.radar.wfs(wf).tukey = 1;
default.radar.wfs(wf).Tpd = 0.00000020833;
% default.radar.wfs(wf).Tpd = 0.000005; % Long pulse duration mode
default.radar.wfs(wf).BW_window = [33725000 40925000];
default.radar.wfs(wf).t_ref = 0;
fc = (default.radar.wfs(wf).f0+default.radar.wfs(wf).f1)/2;
default.radar.wfs(wf).system_dB = 10*log10(100)+0+0+20*log10(300000000/(8*pi*fc))+10*log10(50);%Pt+Gr+Gt+term+Z0
default.name='default_params';
default.config_regexp = '.*';

defaults{end+1} = default;
