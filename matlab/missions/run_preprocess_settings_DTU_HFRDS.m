% script run_preprocess_settings_DTU_HFRDS.m
%
% Support script for run_preprocess_DTU_HFRDS.m

param.preprocess = [];
param.preprocess.default = {};

if ispc
  base_dir = fullfile('Y:\','data','MCoRDS','2024_Antarctica_TOdtu');
else
  base_dir = fullfile('/kucresis','scratch','data','MCoRDS','2024_Antarctica_TOdtu');
end

% preprocess_list is an N by 4 cell array with the columns representing in
% order:
%   base_dir, config_folder_name, board_folder_name, date_str
preprocess_list = { ...
  '',fullfile('HFS_05302023','config'),fullfile('HFS_05302023','digrx0'),'20240530'; ...
  };

default_function_handle = @default_radar_params_2024_Antarctica_TOdtu_rds;

if 1
  %% SINGLE DAY

  preprocess_list_idx = 1;

  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = default_function_handle
  param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
  param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
  param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
  param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};

elseif 0
  %% MULTIPLE DAYS

  for preprocess_list_idx = 1:size(preprocess_list,1)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = default_function_handle
    param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
    param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
    param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
    param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};
  end
end
