% script run_preprocess_settings_2022_Antarctica_GroundGHOST.m
%
% Support script for run_preprocess_UTIG.m
%
% Preprocess setup script for 2022_Antarctica_GroundGHOST.

param.preprocess = [];

%% RDS SINGLE DAY
% cur_idx = length(param.preprocess.default)+1;
% param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_BaslerMKB_rds;
% param.preprocess.file{cur_idx}.base_dir = '/data/utigCXA2/orig/xped/CXA2/acqn/MARFA/';
% param.preprocess.file{cur_idx}.config_folder_name = '../../ELSA/F12/';
% param.preprocess.file{cur_idx}.board_folder_name = 'F12';
% param.preprocess.date_str{cur_idx} = '20231229';

%% RDS EVERY DAY

% for flight = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22]
for flight = [22]
  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_BaslerMKB_rds;
  param.preprocess.file{cur_idx}.base_dir = '/cresis/snfs1/data/UTIG/utigCXA2/orig/xped/CXA2/acqn/MARFA/';
  param.preprocess.file{cur_idx}.config_folder_name = sprintf('../../ELSA/F%02d/', flight);
  param.preprocess.file{cur_idx}.board_folder_name = sprintf('F%02d', flight);
  if flight == 1
    param.preprocess.date_str{cur_idx} = '20231209';
  elseif flight == 2
    param.preprocess.date_str{cur_idx} = '20231211';
  elseif flight == 3
    param.preprocess.date_str{cur_idx} = '20231212';
  elseif flight == 4
    param.preprocess.date_str{cur_idx} = '20231217';
  elseif flight == 5
    param.preprocess.date_str{cur_idx} = '20231221';
  elseif flight == 6
    param.preprocess.date_str{cur_idx} = '20231224';
  elseif flight == 7
    param.preprocess.date_str{cur_idx} = '20231225';
  elseif flight == 8
    param.preprocess.date_str{cur_idx} = '20231226';
  elseif flight == 9
    param.preprocess.date_str{cur_idx} = '20231227';
  elseif flight == 10
    param.preprocess.date_str{cur_idx} = '20231228';
  elseif flight == 11
    param.preprocess.date_str{cur_idx} = '20231229';
  elseif flight == 12
    param.preprocess.date_str{cur_idx} = '20231229';
  elseif flight == 13
    param.preprocess.date_str{cur_idx} = '20240104a';
  elseif flight == 14
    param.preprocess.date_str{cur_idx} = '20240104b';
  elseif flight == 15
    param.preprocess.date_str{cur_idx} = '20240105a';
  elseif flight == 16
    param.preprocess.date_str{cur_idx} = '20240105b';
  elseif flight == 17
    param.preprocess.date_str{cur_idx} = '20240107';
  elseif flight == 18
    param.preprocess.date_str{cur_idx} = '20240108';
  elseif flight == 19
    param.preprocess.date_str{cur_idx} = '20240109';
  elseif flight == 20
    param.preprocess.date_str{cur_idx} = '20240111';
  elseif flight == 21
    param.preprocess.date_str{cur_idx} = '20240111b';
  elseif flight == 22
    param.preprocess.date_str{cur_idx} = '20240112';
  end
end
