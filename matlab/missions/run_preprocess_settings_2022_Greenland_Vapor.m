% script run_preprocess_settings_2022_Greenland_Vapor.m
%
% Support script for run_preprocess_Vapor.m

param.preprocess = [];
param.preprocess.default = {};

if ispc
  base_dir = fullfile('Z:\','data','HF_Sounder','2022_Greenland_Vapor');
else
  base_dir = fullfile('/cresis','snfs1','data','HF_Sounder','2022_Greenland_Vapor');
end

default_function_handle = @default_radar_params_2022_Greenland_Vapor_rds;

% preprocess_list is an N by 4 cell array with the columns representing in
% order:
%   base_dir, config_folder_name, board_folder_name, date_str
preprocess_list = { ...
  '','gps/07272022','data/07272022','20220727'; ...
  '','gps/07282022','data/07282022','20220728'; ...
  '','gps/07292022','data/07292022','20220729'; ...
  '','gps/07302022','data/07302022','20220730'; ...
  '','gps/07312022','data/07312022','20220731'; ...
  '','gps/08012022','data/08012022','20220801'; ...
  };

if 1
  %% SINGLE DAY

  preprocess_list_idx = 1;

  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = default_function_handle;
  param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
  param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
  param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
  param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};

elseif 0
  %% MULTIPLE DAYS

  for preprocess_list_idx = 1:size(preprocess_list,1)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = default_function_handle;
    param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
    param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
    param.preprocess.file{cur_idx}.board_folder_name = preprocess_list{preprocess_list_idx,3};
    param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};
  end
end
