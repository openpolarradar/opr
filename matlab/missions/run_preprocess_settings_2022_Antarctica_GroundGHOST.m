% script run_preprocess_settings_2022_Antarctica_GroundGHOST.m
%
% Support script for run_preprocess.m
%
% Preprocess setup script for 2022_Antarctica_GroundGHOST.

param.preprocess = [];

%% RDS SINGLE DAY
% cur_idx = length(param.preprocess.default)+1;
% param.preprocess.default{cur_idx} = @default_radar_params_2022_Antarctica_GroundGHOST_rds;
% param.preprocess.file{cur_idx}.base_dir = '/data/';
% param.preprocess.file{cur_idx}.config_folder_names = '20230202d';
% param.preprocess.file{cur_idx}.board_folder_names = '20230202d/%b';
% param.preprocess.date_str{cur_idx} = '20230202d';

%% RDS MULTIPLE DAYS
date_str = {'20230202', '20230203', '20230207'}; % 20230206 is in 20230207
% date_str = {'20230202'};
for idx = 1:length(date_str)
  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = @default_radar_params_2022_Antarctica_GroundGHOST_rds;
  param.preprocess.file{cur_idx}.base_dir = '/cresis/snfs1/data/MCoRDS/2022_Antarctica_GroundGHOST/';
  param.preprocess.file{cur_idx}.config_folder_name = date_str{idx};
  param.preprocess.file{cur_idx}.board_folder_name = [date_str{idx} '/%b'];
  param.preprocess.date_str{cur_idx} = date_str{idx};
end
