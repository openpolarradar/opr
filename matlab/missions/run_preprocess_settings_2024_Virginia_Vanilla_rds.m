% script run_preprocess_settings_2024_Virginia_Vanilla_rds.m
%
% Preprocess setup for 2024_Virginia_Vanilla VHF radar

param.preprocess = [];
param.preprocess.default = {};

if ispc
  base_dir = fullfile('Y:\','data','MCoRDS','2024_Virginia_Vanilla');
else
  base_dir = fullfile('/kucresis','scratch','data','MCoRDS','2024_Virginia_Vanilla');
end

% preprocess_list is an N by 4 cell array with the columns representing in
% order:
%   base_dir, config_folder_name, board_folder_name, date_str
preprocess_list = { ...
  '',fullfile('test_flight_20241102','gps'),fullfile('test_flight_20241102','rds'),'20241102'; ...
  };

default_function_handle = @default_radar_params_2024_Virginia_Vanilla_rds;

if 1
  %% SINGLE DAY

  preprocess_list_idx = 1;

  for cur_idx = 1:4
      param.preprocess.default{cur_idx} = default_function_handle
      param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
      param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
      param.preprocess.file{cur_idx}.board_folder_name = [preprocess_list{preprocess_list_idx,3} sprintf('/ch%d',cur_idx)];
      param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};
  end

elseif 0
  %% MULTIPLE DAYS

  for preprocess_list_idx = 1:size(preprocess_list,1)
      for cur_idx = 1:4;
          param.preprocess.default{cur_idx} = default_function_handle
          param.preprocess.file{cur_idx}.base_dir = fullfile(base_dir,preprocess_list{preprocess_list_idx,1});
          param.preprocess.file{cur_idx}.config_folder_name = preprocess_list{preprocess_list_idx,2};
          param.preprocess.file{cur_idx}.board_folder_name = [preprocess_list{preprocess_list_idx,3},sprintf('/ch%d',cur_idx)];
          param.preprocess.date_str{cur_idx} = preprocess_list{preprocess_list_idx,4};
      end
  end
end
