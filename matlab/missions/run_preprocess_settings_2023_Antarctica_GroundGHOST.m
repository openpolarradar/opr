% script run_preprocess_settings_2023_Antarctica_GroundGHOST.m
%
% Support script for run_preprocess_GHOST.m
%
% Preprocess setup script for 2023_Antarctica_GroundGHOST.

param.preprocess = [];
param.preprocess.default = [];

if 0
  %% RDS SINGLE DAY
  date_str = '20240108';
  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_GroundGHOST_rds;
  param.preprocess.file{cur_idx}.base_dir = '/kucresis/scratch/data/MCoRDS/2023_Antarctica_GroundGHOST/';
  param.preprocess.file{cur_idx}.config_folder_name = date_str;
  param.preprocess.file{cur_idx}.board_folder_name = [date_str '/%b'];
  param.preprocess.date_str{cur_idx} = date_str;

else
  %% RDS MULTIPLE DAYS
  date_str = {'20231122', '20231123', '20231124', '20231125', '20231129', '20231203', ...
    '20231205', '20231215', '20231216', '20231220', '20231227', '20231230', '20231231', ...
    '20240101', '20240102', '20240103', '20240104', '20240105', '20240106', '20240107', '20240108'};
  for idx = 1:length(date_str)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_GroundGHOST_rds;
    param.preprocess.file{cur_idx}.base_dir = '/kucresis/scratch/data/MCoRDS/2023_Antarctica_GroundGHOST/';
    param.preprocess.file{cur_idx}.config_folder_name = date_str{idx};
    param.preprocess.file{cur_idx}.board_folder_name = [date_str{idx} '/%b'];
    param.preprocess.date_str{cur_idx} = date_str{idx};
  end
end
