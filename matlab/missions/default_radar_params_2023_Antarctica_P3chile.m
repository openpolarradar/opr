function [param,defaults] = default_radar_params_2023_Antarctica_P3chile
% [param,defaults] = default_radar_params_2023_Antarctica_P3chile
%
% HF Sounder: 2023_Antarctica_P3chile
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Author: John Paden

%% Preprocess parameters
param.season_name = '2023_Antarctica_P3chile';
param.radar_name = 'rds';

param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

param.preprocess.digital_system_type = 'cecs';
param.preprocess.header_load_func = @basic_load_cecs;
% param.preprocess.board_map = {''};
% param.preprocess.tx_map = {''};

% param.preprocess.daq.xml_version = -1; % No XML file available

% param.preprocess.tx_enable = [1];

%% CReSIS parameters
param.preprocess.cresis.clk = 200e6;
param.preprocess.cresis.expected_rec_sizes = [32820];
% param.preprocess.cresis.gps_file_mask = 'LOG*';

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.sar = 1;
param.cmd.array = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.file.boards = {''};
param.records.file.version = 418;
param.records.file.prefix = 'P3';
param.records.file.suffix = '.dat';
param.records.file.clk=200e6;
param.records.frames.geotiff_fn = fullfile('antarctica','Landsat-7','Antarctica_LIMA.tif');
param.records.frames.mode = 2;
param.records.frames.length = 50000;
param.records.gps.en = 1;
param.records.gps.time_offset = 1;

%% Qlook worksheet
param.qlook.img_comb = [10e-06 -Inf 1e-06];
param.qlook.imgs = {[2 1],[1 1]};
param.qlook.out_path = '';
param.qlook.block_size = 5000;
param.qlook.motion_comp = 0;
param.qlook.dec = 20;
param.qlook.inc_dec = 5;
param.qlook.surf.en = 1;
param.qlook.surf.profile = 'RDS';

%% SAR worksheet
param.sar.out_path = '';
param.sar.imgs = param.qlook.imgs;
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 4000;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = 4e-6;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 2.5;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.imgs = param.qlook.imgs;
param.array.img_comb = param.qlook.img_comb;
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -5:5;
param.array.dbin = 1;
param.array.dline = 6;

%% Radar worksheet
param.radar.prf = 10e3;
param.radar.fs = 800e6;
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 1; % Digital receiver gain is 5, full scale Vpp is 2
param.radar.lever_arm_fh = @lever_arm;
chan_equal_Tsys = [0]/1e9;
chan_equal_dB = [0];
chan_equal_deg = [0];
for wf = 1:2
  param.radar.wfs(wf).Tadc_adjust = -1.0033e-6;
  param.radar.wfs(wf).tx_weights = 1;
  param.radar.wfs(wf).rx_paths = [1]; % ADC to rx path mapping
  param.radar.wfs(wf).chan_equal_Tsys = chan_equal_Tsys;
  param.radar.wfs(wf).chan_equal_dB = chan_equal_dB;
  param.radar.wfs(wf).chan_equal_deg = chan_equal_deg;
  param.radar.wfs(wf).f0 = 140e6;
  param.radar.wfs(wf).f1 = 160e6;
  param.radar.wfs(wf).tukey = 0.4;
  param.radar.wfs(wf).BW_window = [140e6 160e6];
  param.radar.wfs(wf).t_ref = 0;
  fc = (param.radar.wfs(wf).f0+param.radar.wfs(wf).f1)/2;
  param.radar.wfs(wf).system_dB = 10*log10(500)+0+0+20*log10(300000000/(8*pi*fc))+10*log10(50);%Pt+Gr+Gt+term+Z0
  param.radar.wfs(wf).DDC_dec = 16; % 800 MHz / 16 = 50 MHz
  param.radar.wfs(wf).DDC_freq = -150e6;
end

%% Post worksheet
param.post.data_dirs = {'standard'};
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
param.post.img_dpi = 600;
% param.post.echo.elev_comp = 2;
% param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+3000]';
param.post.echo.elev_comp = 3;
param.post.echo.depth = '[publish_echogram_switch(Bbad,0.25,Surface_Elev,-3000,DBottom,-100),max(Surface_Elev+100)]';
param.post.echo.er_ice = 3.15;
param.post.echo.plot_params = {'PaperPosition',[0.25 2.5 3 6]};
param.post.ops.location = 'antarctic';
  
%% Radar Settings

%This is usually read from an XLM file, which the HF does not have.
defaults = {};

default = param;

wf = 1;
default.radar.wfs(wf).Tpd = 10e-6;
default.radar.wfs(wf).adc_gains_dB = [76];

wf = 2;
default.radar.wfs(wf).Tpd = 1e-6;
default.radar.wfs(wf).adc_gains_dB = [34];

default.name='default_params';
default.config_regexp = '.*';

defaults{end+1} = default;
