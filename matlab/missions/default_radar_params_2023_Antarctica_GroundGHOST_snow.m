function [param,defaults] = default_radar_params_2023_Antarctica_GroundGHOST_snow
% [param,defaults] = default_radar_params_2023_Antarctica_GroundGHOST_snow
%
% Snow: 2023_Antarctica_GroundGHOST
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Author: John Paden

%% Preprocess parameters
param.season_name = '2023_Antarctica_GroundGHOST';
param.radar_name = 'snow';

% Reading in files
param.preprocess.digital_system_type = 'arena';
param.preprocess.wg_type = 'arena';
param.preprocess.header_load_func = @basic_load_arena;
param.preprocess.board_map = {'digrx0'};
param.preprocess.tx_map = {'awg0'};

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

% Creating settings files
param.preprocess.max_data_rate = 60;
param.preprocess.max_duty_cycle = 0.1;
param.preprocess.prf_multiple = [10e6 10e6/20]; % Power supply sync signal that PRF must be a factor of these numbers
param.preprocess.PRI_guard = 1e-6;
param.preprocess.PRI_guard_percentage = 1;
param.preprocess.tx_enable = [1 1 1 1];
param.preprocess.max_tx = 1.0;
param.preprocess.max_tx_voltage = sqrt(1000*50)*10^(-2/20); % voltage at max_tx

%% snow Arena Parameters
arena = [];
fs = 1200e6;
fs_dac = 2400e6;
subsystem_idx = 0;
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA-CTU';
arena.subsystem(subsystem_idx).subSystem{1} = 'ctu';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA0';
arena.subsystem(subsystem_idx).subSystem{1} = 'awg0';
arena.subsystem(subsystem_idx).subSystem{2} = 'digrx0';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA1';
arena.subsystem(subsystem_idx).subSystem{1} = 'awg1';
arena.subsystem(subsystem_idx).subSystem{2} = 'digrx1';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA2';
arena.subsystem(subsystem_idx).subSystem{1} = 'awg2';
arena.subsystem(subsystem_idx).subSystem{2} = 'digrx2';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA3';
arena.subsystem(subsystem_idx).subSystem{1} = 'awg3';
arena.subsystem(subsystem_idx).subSystem{2} = 'digrx3';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'Data Server';

dac_idx = 0;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'awg0';
arena.dac(dac_idx).type = 'dac-ad9129_0012';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = -10;
arena.dac(dac_idx).desiredAlignMax = 4;
arena.dac(dac_idx).dcoPhase = 80;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'awg1';
arena.dac(dac_idx).type = 'dac-ad9129_0012';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = -14;
arena.dac(dac_idx).desiredAlignMax = 0;
arena.dac(dac_idx).dcoPhase = 80;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'awg2';
arena.dac(dac_idx).type = 'dac-ad9129_0012';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = 4;
arena.dac(dac_idx).desiredAlignMax = 18;
arena.dac(dac_idx).dcoPhase = 80;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'awg3';
arena.dac(dac_idx).type = 'dac-ad9129_0012';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = 0;
arena.dac(dac_idx).desiredAlignMax = 14;
arena.dac(dac_idx).dcoPhase = 80;

adc_idx = 0;
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx0';
arena.adc(adc_idx).type = 'adc-ad9680_0017';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 2;
arena.adc(adc_idx).desiredAlignMin = -12;
arena.adc(adc_idx).desiredAlignMax = 8;
arena.adc(adc_idx).stream = 'tcp';
arena.adc(adc_idx).ip = '10.0.0.100';
arena.adc(adc_idx).outputSelect = 0;
arena.adc(adc_idx).gain_dB = [2.4 2.4];
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx1';
arena.adc(adc_idx).type = 'adc-ad9680_0017';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 2;
arena.adc(adc_idx).desiredAlignMin = -17;
arena.adc(adc_idx).desiredAlignMax = 3;
arena.adc(adc_idx).stream = 'tcp';
arena.adc(adc_idx).ip = '10.0.0.100';
arena.adc(adc_idx).outputSelect = 0;
arena.adc(adc_idx).gain_dB = [2.4 2.4];
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx2';
arena.adc(adc_idx).type = 'adc-ad9680_0017';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 2;
arena.adc(adc_idx).desiredAlignMin = -21;
arena.adc(adc_idx).desiredAlignMax = -1;
arena.adc(adc_idx).stream = 'tcp';
arena.adc(adc_idx).ip = '10.0.0.100';
arena.adc(adc_idx).outputSelect = 0;
arena.adc(adc_idx).gain_dB = [2.4 2.4];
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx3';
arena.adc(adc_idx).type = 'adc-ad9680_0017';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 2;
arena.adc(adc_idx).desiredAlignMin = -20;
arena.adc(adc_idx).desiredAlignMax = 0;
arena.adc(adc_idx).stream = 'tcp';
arena.adc(adc_idx).ip = '10.0.0.100';
arena.adc(adc_idx).outputSelect = 0;
arena.adc(adc_idx).gain_dB = [2.4 2.4];

daq_idx = 0;
daq_idx = daq_idx + 1;
arena.daq(daq_idx).name = 'daq0';
arena.daq(daq_idx).type = 'daq_0001';
arena.daq(daq_idx).auxDir = '/data/';
arena.daq(daq_idx).fileStripe = '/data/%b/';
arena.daq(daq_idx).fileName = 'mcords';
arena.daq(daq_idx).udp_packet_headers = false;

arena.system.name = 'ku0001';

arena.param.tx_max = [1 1 1 1];
arena.param.PA_setup_time = 2e-6; % Time required to enable PA before transmit
arena.param.TTL_time_delay = 0.0; % TTL time delay relative to transmit start
arena.param.ADC_time_delay = 3.0720e-6; % ADC time delay relative to transmit start
   
% mode 0, subchannel 0, board_idx 1 is wf-adc 1-1
% mode 0, subchannel 0, board_idx 2 is wf-adc 2-1

arena.psc.type = 'psc_0003';

arena.daq.type = 'daq_0001';

arena.ctu.name = 'ctu';
arena.ctu.type = 'ctu_001D';
if 0
  % External GPS
  arena.ctu.nmea = 31;
  arena.ctu.nmea_baud = 9600;
  arena.ctu.pps = 10;
  arena.ctu.pps_polarity = 1;
else
  % Internal GPS
  arena.ctu.nmea = 60;
  arena.ctu.nmea_baud = 115200;
  arena.ctu.pps = 63;
  arena.ctu.pps_polarity = 1;
end
idx = 0;
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'EPRI';
arena.ctu.out.bit_group(idx).bits = 0;
arena.ctu.out.bit_group(idx).epri = [1 0 0];
arena.ctu.out.bit_group(idx).pri = [0 0 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'PRI';
arena.ctu.out.bit_group(idx).bits = 1;
arena.ctu.out.bit_group(idx).epri = [1 0 0];
arena.ctu.out.bit_group(idx).pri = [1 0 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'TR';
arena.ctu.out.bit_group(idx).bits = 2;
arena.ctu.out.bit_group(idx).epri = [1 0 0];
arena.ctu.out.bit_group(idx).pri = [1 0 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'Isolation';
arena.ctu.out.bit_group(idx).bits = 3;
arena.ctu.out.bit_group(idx).epri = [1 1 0];
arena.ctu.out.bit_group(idx).pri = [1 1 0];
% idx = idx + 1;
% arena.ctu.out.bit_group(idx).name = 'Atten';
% arena.ctu.out.bit_group(idx).bits = 4:8;
% arena.ctu.out.bit_group(idx).epri = [0 0];
% arena.ctu.out.bit_group(idx).pri = [0 0];

arena.ctu.out.time_cmd = {'2e-6+param.wfs(wf).Tpd+0.5e-6' '2e-6+param.wfs(wf).Tpd+2.5e-6' '2/param.prf'};

param.arena = arena;

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.gps.time_offset = 0;
param.records.frames.geotiff_fn = 'antarctica\Landsat-7\Antarctica_LIMA.tif';
param.records.frames.mode = 1;
param.records.file.version = 103;
param.records.file.midfix = param.radar_name;
param.records.file.suffix = '.dat';
param.records.file.boards = {'digrx0'};
param.records.file.board_folder_name = '%b';
param.records.file.clk = 10e6;

%% Quick Look worksheet
param.qlook.out_path = '';
param.qlook.block_size = 2000;
param.qlook.motion_comp = 0;
param.qlook.dec = 2;
param.qlook.inc_dec = 5;
param.qlook.surf.en = 1;
param.qlook.surf.method = 'fixed';
param.qlook.surf.fixed_value = 0;
param.qlook.resample = [2 1];

%% SAR worksheet
param.sar.out_path = '';
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 2000;
param.sar.chunk_overlap = 10;
param.sar.frm_overlap = 0;
param.sar.coh_noise_removal = 0;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = inf;
param.sar.pulse_rfi.en = [];
param.sar.pulse_rfi.inc_ave= [];
param.sar.pulse_rfi.thresh_scale = [];
param.sar.trim_vals = [];
param.sar.pulse_comp = 1;
param.sar.ft_dec = 1;
param.sar.ft_wind = @hanning;
param.sar.ft_wind_time = 0;
param.sar.lever_arm_fh = @lever_arm;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 0.5;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 1.53;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -2:2;
param.array.dbin = 1;
param.array.dline = 6;
param.array.DCM = [];
param.array.Nsv = 1;
param.array.theta_rng = [0 0];
param.array.sv_fh = @array_proc_sv;
param.array.diag_load = 0;
param.array.Nsig = 2;

%% Radar worksheet
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 1;
param.radar.Tadc_adjust = 8.3042e-06; % System time delay: leave this empty or set it to zero at first, determine this value later using data over surface with known height or from surface multiple
param.radar.lever_arm_fh = @lever_arm;

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.img = 0;
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 2;
param.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+25]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'antarctic';

%% Analysis worksheet
param.analysis_noise.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_noise.cmd{cmd_idx}.method = 'coh_noise';

%% Analysis worksheet
param.analysis_spec.block_size = 5000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_spec.cmd{cmd_idx}.method = 'specular';
param.analysis_spec.cmd{cmd_idx}.rlines = 128;
param.analysis_spec.cmd{cmd_idx}.threshold = 40;
param.analysis_spec.cmd{cmd_idx}.signal_doppler_bins = [1:4 125:128];
param.analysis_spec.cmd{cmd_idx}.noise_doppler_bins = [12:117];
param.analysis_spec.cmd{cmd_idx}.rbins = [-200 150];

%% Radar Settings

defaults = {};

%% Radar Settings: Survey Mode Thin Ice Single Polarization
% =========================================================================
default.records.arena.total_presums = 4*48;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
default.records.arena.epri_time_error_threshold = 1;
default.records.arena.mode_latch_fix = [1 0; 2 0; 3 0];

% No Profile Processor Digital System (use mode_latch,subchannel instead)
% Each row of param.records.data_map{board_idx} = [mode_latch subchannel wf adc]
default.records.data_map = {...
  [...
  0 0 1 1
  1 0 1 1
  2 0 1 1
  3 0 1 1
  ]};
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.analysis_spec.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
default.radar.wfs = [];
for wf = 1:1
  default.radar.wfs(wf).adc_gains_dB = 30; % ADC gain
  default.radar.wfs(wf).rx_paths = [1]; % ADC to rx path mapping
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0];
  default.radar.wfs(wf).tx_paths = {[1]};
  default.radar.wfs(wf).DDC_dec = 16;
  default.radar.wfs(wf).fmult = 56;
  default.radar.wfs(wf).fLO = -10e9;
  default.radar.wfs(wf).BW_window = [2000000000.00080132484436 7799466666.66979122161865];
end
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = 'ghost_config_thin_pingpong';
default.name = 'Snow Radar';
defaults{end+1} = default;

%% Radar Settings: Survey Mode Thick Ice Single Polarization
% =========================================================================
default.records.arena.total_presums = 4*(36+36);
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
default.records.arena.epri_time_error_threshold = 1;
default.records.arena.mode_latch_fix = [1 0; 2 0; 3 0];

% No Profile Processor Digital System (use mode_latch,subchannel instead)
% Each row of default.records.data_map{board_idx} = [mode_latch subchannel wf adc]
default.records.data_map = {...
  [...
  0 0 1 1
  1 0 1 1
  2 0 1 1
  3 0 1 1
  ]};
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
default.radar.wfs = [];
for wf = 1:1
  default.radar.wfs(wf).adc_gains_dB = 30; % ADC gain
  default.radar.wfs(wf).rx_paths = [1]; % ADC to rx path mapping
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [0];
  default.radar.wfs(wf).tx_paths = {[1]};
  default.radar.wfs(wf).DDC_dec = 16;
  default.radar.wfs(wf).fmult = 56;
  default.radar.wfs(wf).fLO = -10e9;
  default.radar.wfs(wf).BW_window = [2000000000.00080132484436 7799466666.66979122161865];
  default.radar.wfs(wf).Tsys = 0.05087e-6;
end
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = 'ghost_config_thick_pingpong';
default.name = 'Snow Radar';
defaults{end+1} = default;


param.preprocess.defaults = defaults;
