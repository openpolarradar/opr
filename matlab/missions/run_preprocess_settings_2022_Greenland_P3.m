% script run_preprocess_2022_Greenland_P3.m
%
% Support script for run_preprocess.m

param.preprocess = [];

%% HERC MCORDS6 GROUND
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = default_radar_params_2022_Greenland_P3_snow();
param.preprocess.base_dir{cur_idx} = '/mnt/data/';
param.preprocess.config_folder_names{cur_idx} = '20220419/';
param.preprocess.board_folder_names{cur_idx} = '20220419/%b';
param.preprocess.date_strs{cur_idx} = '20220419';
