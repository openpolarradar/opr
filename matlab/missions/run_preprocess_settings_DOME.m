% script run_preprocess_DOME.m
%
% Support script for run_preprocess.m

param.preprocess = [];

%% DOME MCORDS6 GROUND
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = default_radar_params_2018_Antarctica_Ground_rds();
param.preprocess.base_dir{cur_idx} = '/data/';
param.preprocess.config_folder_names{cur_idx} = '20181014';
param.preprocess.board_folder_names{cur_idx} = '20181014/%b';
param.preprocess.date_strs{cur_idx} = '20181014';

return;
