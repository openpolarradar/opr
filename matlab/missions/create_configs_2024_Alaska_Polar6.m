% script create_configs_2022_Greenland_Polar5
%
% Creates NI and Arena radar depth sounder settings

%% User Settings
% =========================================================================

% File locations
% -------------------------------------------------------------------------
% NI_base_dir: Location where National Instruments configuration files will
% be written.
% arena_base_dir: Location where Arena configuration files will be written.
if ispc
  NI_base_dir = 'C:\Users\uwb\Desktop\waveforms\arkr_2024\';
  % Air 1 Server: C:\Users\Administrator\Desktop\Arena_Shared\configs\
  %arena_base_dir = 'C:\Users\Administrator\Desktop\Arena_Shared\configs\';
  arena_base_dir = 'C:\Users\uwb\Desktop\Arena_Shared\configs\arkr_2024\'; % Comment out on Air 1 Server
  NI_base_dir = 'C:\waveforms\';
  arena_base_dir = 'C:\arena_waveforms\'; % Comment out on Air 1 Server
else
  NI_base_dir = '~/waveforms/';
  arena_base_dir = '~/rss_waveforms/';
end

% NI_calval_dir: Location where National Instruments configuration files
% for calibration and validation settings will be written (usually a
% subdirectory "calval") in the NI_base_dir
NI_calval_dir = fullfile(NI_base_dir, 'calval');
% -------------------------------------------------------------------------
% End file locations

% Specify a list of start/stop frequencies that you want to generate
% settings for. THIS OFTEN NEEDS TO BE CHANGED FOR A CAMPAIGN.
if 0
  f0_list = [180e6];
  f1_list = [210e6];
  DDC_select_list = [2];
  
else
  f0_list = [150e6 180e6];
  f1_list = [520e6 210e6];
  DDC_select_list = [1 2];
end

% Cal Settings
final_cal_fc = [];
final_DDS_phase = [];
final_DDS_amp = [];
final_DDS_time = [];
if 0
  % Initial conditions (usually all zeros phase/time with max amplitude)
  for idx = 1:length(f0_list)
    final_DDS_phase{idx} = [0 0 0 0 0 0 0 0];
    final_DDS_amp{idx} = [4000 4000 4000 4000 4000 4000 4000 4000];
    final_DDS_time{idx} =  [0 0 2.5 2.5 3.125 3.125 0 0];
  end
else
  % COPY AND PASTE RESULTS FROM basic_tx_chan_equalization_SEASON_NAME.m
  % HERE:

  % 150-520 MHz
  final_cal_fc(end+1) = 335e6;

  final_DDS_time{end+1} =  [0.0923	0.1321	-0.2024	-0.1941	0.0000	-0.2081	0.0683	0.1153];
  % final_DDS_phase_no_time{end+1} = [0 0 0 0 0 0 0 0]; % not used usually
  final_DDS_amp{end+1} = [959	1922	2883	4000	3967	2962	1902	994];
  final_DDS_phase{end+1} = [-14.0	-0.1	-48.6	-42.2	-22.3	-48.1	-12.4	1.1];

  % 180-210 MHz
  final_cal_fc(end+1) = 195e6;
 
  final_DDS_time{end+1} =  [0.0923	0.1321	-0.2024	-0.1941	0.0000	-0.2081	0.0683	0.1153];
  % final_DDS_phase_no_time{end+1} = [0 0 0 0 0 0 0 0]; % not used usually
  final_DDS_amp{end+1} = [959	1922	2883	4000	3967	2962	1902	994];
  final_DDS_phase{end+1} = [-2.0	-0.2	48.3	55.8	25.7	49.8	38.0	5.3];

end

% prf: Scaler double with units of Hertz. Desired pulse repetition
% frequency. Should be 10000 Hz.
prf = 10000;

% Hwindow_orig: Desired window created during transmit calibration. This is
% used any time a window that is different from that used during
% calibration is to be used. Should be chebwin(8,30).'.
Hwindow_orig = chebwin(8,30).';

% speed: scaler double in meters/second. Typical cruise speed of Basler is
% 90 m/s. Typical survey speed is 80 m/s. Set this speed to the maximum
% expected ground speed considering wind conditions. Using a setting of 100
% m/s is generally sufficient for all surveys.
speed = [100];
% presums: Positive scaler integer containing the number of hardware
% presums. Presums are also known as "stacking" and "coherent averaging".
% The radar only has one antenna arrayed in the along-track direction. This
% antenna has almost no directivity so the sample spacing (after
% presumming) should be one fourth of a wavelength to meet Nyquist criteria
% in all situations. We calculate the wavelength at the center frequency.
% Typically, this equation should not be modified:
%    presums = round(c./abs(f1_list+f0_list)/2 ./ speed * prf / 2)*2
physical_constants; % Loads "c" as speed of light
presums = round(c./abs(f1_list+f0_list)/2 ./ speed * prf / 2)*2

%% Load Antenna Positions/Lever Arms (do not change)
% =========================================================================
param = [];
param.season_name = '2022_Greenland_Polar5'; % Any polar5/6 season works
param.radar_name = 'rds';
param.gps_source = 'awi-final';
clear phase_centers;
for tx_chan = 1:8
  % Just enable the current antenna to determine its phase center
  tx_weights = zeros(1,8);
  tx_weights(tx_chan) = 1;
  rxchan = 4; % Fix the receiver (it should not matter which one you choose)
  % Determine phase center for the antenna
  phase_centers(:,tx_chan) = lever_arm(param, tx_weights, rxchan);
end
% Adjust phase centers to the mean phase center position
phase_centers = bsxfun(@minus,phase_centers,mean(phase_centers,2));

%% Load Arena Parameters (do not change)
% =========================================================================
[param_defaults,defaults] = default_radar_params_2024_Alaska_Polar6_rds;
arena = defaults{1}.arena;

%% Test Receiver Gain Working
% =========================================================================
ice_thickness = 1500;
for freq_idx = [1]
  param = struct('radar_name','mcords5','num_chan',8,'aux_dac',[255 255 255 255 255 255 255 255],'version','14.0f1','TTL_prog_delay',650,'xml_version',2.0,'fs',1600e6,'fs_sync',90.0e6,'fs_dds',1440e6,'TTL_clock',1440e6/16,'TTL_mode',[2.5e-6 260e-9 -1100e-9],'arena_base_dir',arena_base_dir);
  param = merge_structs(param,param_defaults);
  param.arena = arena;
  param.max_tx = [4000 4000 4000 4000 4000 4000 4000 4000]; param.max_data_rate = 755; param.flight_hours = 3.5; param.sys_delay = 0.75e-6; param.use_mcords4_names = true; param.arena = arena;
  BW = abs(f1_list(freq_idx)-f0_list(freq_idx));
  if BW <= 170e6
    param.DDC_select = 2; % 200 MHz sampling mode
  elseif BW <= 370e6
    param.DDC_select = 1; % 400 MHz sampling mode
  else
    error('Bandwidth (%g MHz) is too large. Must be less than or equal to 370 MHz.', BW/1e6)
  end
  cal_used_idx = -1;
  fc = abs(f0_list+f1_list)/2;
  for cal_idx = 1:length(final_cal_fc)
    if abs(final_cal_fc(cal_idx) - fc(freq_idx)) < 1e6
      cal_used_idx = cal_idx;
      break;
    end
  end
  if cal_used_idx == -1
    error('The center frequency %g MHz does not match any of the cal setting center frequencies specified in final_cal_fc. Either change the f0_list and f1_list or add a calibration setting with this center frequency.', fc/1e6);
  end
  param.max_duty_cycle = 0.12;
  param.create_IQ = false;
  param.tg.staged_recording = [1 1 1];
  param.tg.altitude_guard = 0*12*2.54/100;
  param.tg.Haltitude = 0*12*2.54/100;
  param.tg.Hice_thick = ice_thickness;
  param.tg.rg_stop_offset = [0 0 0];
  param.prf = prf;
  param.presums = [10 10 10];
  param.wfs(1).atten = 37;
  param.wfs(2).atten = 10;
  param.wfs(3).atten = 0;
  DDS_amp = final_DDS_amp{cal_used_idx};
  param.tx_weights = DDS_amp;
  param.tukey = 0.08;
  param.wfs(1).Tpd = 3e-6;
  param.wfs(2).Tpd = 3e-6;
  param.wfs(3).Tpd = 3e-6;
  param.wfs(1).phase = final_DDS_phase{cal_used_idx};
  param.wfs(2).phase = final_DDS_phase{cal_used_idx};
  param.wfs(3).phase = final_DDS_phase{cal_used_idx};
  param.delay = final_DDS_time{cal_used_idx};
  param.f0 = f0_list(freq_idx);
  param.f1 = f1_list(freq_idx);
  param.DDC_freq = (param.f0+param.f1)/2;
  [param.wfs(1:3).tx_mask] = deal([0 0 0 0 0 0 0 0]);
  % Test Receiver Gain Working Mode
  param.fn = fullfile(NI_base_dir,'test_receiver_gain.xml');
  write_cresis_xml(param);
end

%% Survey Mode + loopback, noise, and deconv modes
% Permax, 2800 +/- 700 ft AGL
% =========================================================================
ice_thickness = 300;
for freq_idx = [1]
  param = struct('radar_name','mcords5','num_chan',8,'aux_dac',[255 255 255 255 255 255 255 255],'version','14.0f1','TTL_prog_delay',650,'xml_version',2.0,'fs',1600e6,'fs_sync',90.0e6,'fs_dds',1440e6,'TTL_clock',1440e6/16,'TTL_mode',[2.5e-6 260e-9 -1100e-9],'arena_base_dir',arena_base_dir);
  param = merge_structs(param,param_defaults);
  param.arena = arena;
  param.max_tx = [4000 4000 4000 4000 4000 4000 4000 4000]; param.max_data_rate = 755; param.flight_hours = 3.5; param.sys_delay = 0.75e-6; param.use_mcords4_names = true; param.arena = arena;
  BW = abs(f1_list(freq_idx)-f0_list(freq_idx));
  if BW <= 170e6
    param.DDC_select = 2; % 200 MHz sampling mode
  elseif BW <= 370e6
    param.DDC_select = 1; % 400 MHz sampling mode
  else
    error('Bandwidth (%g MHz) is too large. Must be less than or equal to 370 MHz.', BW/1e6)
  end
  cal_used_idx = -1;
  fc = abs(f0_list+f1_list)/2;
  for cal_idx = 1:length(final_cal_fc)
    if abs(final_cal_fc(cal_idx) - fc(freq_idx)) < 1e6
      cal_used_idx = cal_idx;
      break;
    end
  end
  if cal_used_idx == -1
    error('The center frequency %g MHz does not match any of the cal setting center frequencies specified in final_cal_fc. Either change the f0_list and f1_list or add a calibration setting with this center frequency.', fc/1e6);
  end
  param.max_duty_cycle = 0.12;
  param.create_IQ = false;
  param.tg.staged_recording = [0 0];
  param.tg.altitude_guard = 700*12*2.54/100;
  %param.tg.Haltitude = 1200*12*2.54/100;
  %adapted for Green Laser Height of 2800 ft
  param.tg.Haltitude = 2800*12*2.54/100;
  param.tg.Hice_thick = ice_thickness;
  param.tg.rg_stop_offset = [300 300];
  param.prf = prf;
  param.presums = [10 presums(freq_idx)-10];
  param.wfs(1).atten = 37;
  param.wfs(2).atten = 0;
  DDS_amp = final_DDS_amp{freq_idx};
  % DDS_amp = final_DDS_amp{freq_idx} * 0.5; % Use in case of surface saturation
  param.tx_weights = DDS_amp;
  param.tukey = 0.08;
  param.wfs(1).Tpd = 1e-6;
  param.wfs(2).Tpd = 1e-6;
  param.wfs(1).phase = final_DDS_phase{cal_used_idx};
  param.wfs(2).phase = final_DDS_phase{cal_used_idx};
  param.delay = final_DDS_time{cal_used_idx};
  param.f0 = f0_list(freq_idx);
  param.f1 = f1_list(freq_idx);
  param.DDC_freq = (param.f0+param.f1)/2;
  [param.wfs(1:2).tx_mask] = deal([0 0 0 0 0 0 0 0]);
  param.fn = fullfile(NI_base_dir,sprintf('thinsurvey_permax%.0f-%.0fMHz_%.0fft_%.0fus_%.0fmthick.xml',param.f0/1e6,param.f1/1e6,param.tg.Haltitude*100/12/2.54,param.wfs(end).Tpd*1e6,param.tg.Hice_thick));
  write_cresis_xml(param);
  if freq_idx == 2
    % Default Mode
    param.fn = fullfile(NI_base_dir,'default.xml');
    write_cresis_xml(param);
  end
  % Loopback Mode without delay line
  param.tg.staged_recording = false;
  param.tg.altitude_guard = 1000*12*2.54/100;
  param.tg.Haltitude = 0e-6 * c/2;
  param.tg.Hice_thick = 0; % Long enough for 10 us delay line
  param.fn = fullfile(NI_calval_dir,sprintf('survey_%.0f-%.0fMHz_%.0fus_LOOPBACK_NO_DELAY.xml',param.f0/1e6,param.f1/1e6,param.wfs(end).Tpd*1e6));
  write_cresis_xml(param);
  % Loopback Mode (10e-6 delay line)
  param.tg.staged_recording = false;
  param.tg.altitude_guard = 1000*12*2.54/100;
  param.tg.Haltitude = 10e-6 * c/2;
  param.tg.Hice_thick = 0; % Long enough for 10 us delay line
  param.fn = fullfile(NI_calval_dir,sprintf('survey_%.0f-%.0fMHz_%.0fus_LOOPBACK.xml',param.f0/1e6,param.f1/1e6,param.wfs(end).Tpd*1e6));
  write_cresis_xml(param);
  % Deconvolution Mode (for over calm lake or sea ice lead)
  param.wfs(1).atten = 43;
  param.wfs(2).atten = 43;
  param.tg.staged_recording = false;
  param.tg.altitude_guard = 3000*12*2.54/100;
  param.tg.Haltitude = 4000*12*2.54/100;
  param.tg.Hice_thick = 0 * 12*2.54/100/sqrt(er_ice);
  param.fn = fullfile(NI_calval_dir,sprintf('survey_%.0f-%.0fMHz_%.0fft_%.0fus_DECONV.xml',param.f0/1e6,param.f1/1e6,param.tg.Haltitude*100/12/2.54,param.wfs(end).Tpd*1e6));
  write_cresis_xml(param);
  if 1
    % Noise Mode
    param.tx_weights = [0 0 0 0 0 0 0 0];
    [param.wfs(1:2).tx_mask] = deal([1 1 1 1 1 1 1 1]);
    param.wfs(1).atten = 37;
    param.wfs(2).atten = 0;
    param.tg.staged_recording = [1 2];
    param.tg.altitude_guard = 500*12*2.54/100;
    param.tg.Haltitude = 1400*12*2.54/100;
    param.tg.Hice_thick = 3250;
    param.fn = fullfile(NI_calval_dir,sprintf('survey_%.0f-%.0fMHz_%.0fus_NOISE.xml',param.f0/1e6,param.f1/1e6,param.wfs(end).Tpd*1e6));
    write_cresis_xml(param);
  end
end

%% Image Mode for PermaX, permafrost copied from create settings Antarctica 2019 (Low Altitude, Ice <1400 m thick)
% Ice thickness "param.tg.Hice_thick_min" m to "param.tg.Hice_thick" m, "param.tg.Haltitude" +/- "param.tg.altitude_guard" ft AGL
freq_idx_WB = 1; % Use wideband mode
freq_idx_NB = 2;
param = struct('radar_name','mcords5','num_chan',8,'aux_dac',[255 255 255 255 255 255 255 255],'version','14.0f1','TTL_prog_delay',650,'xml_version',2.0,'fs',1600e6,'fs_sync',90.0e6,'fs_dds',1440e6,'TTL_clock',1440e6/16,'TTL_mode',[2.5e-6 260e-9 -1100e-9],'arena_base_dir',arena_base_dir);
param = merge_structs(param,param_defaults);
param.arena = arena;
param.max_tx = [4000 4000 4000 4000 4000 4000 4000 4000]; param.max_data_rate = 700; param.flight_hours = 3.5; param.sys_delay = 0.75e-6; param.use_mcords4_names = true; param.arena = arena;
param.DDC_select = DDC_select_list(freq_idx_WB);
param.max_duty_cycle = 0.12;
param.create_IQ = false;
%This staged recording now needs 4 fields? What do the numbers say? 
param.tg.staged_recording = [0 0 0 0]; % one entry for each waveform, set all to zero to disable staged recording
param.tg.start_ref = {'surface','surface','surface','surface'};
param.tg.stop_ref = {'bottom','bottom','bottom','bottom'};
param.tg.altitude_guard = 700 * 12*2.54/100;
param.tg.Haltitude = 2800 * 12*2.54/100; % adapted for Green Laser Altitude
param.tg.Hice_thick_min = 0;
%"ice thickness" can be maybe 300m (to be safe)?
param.tg.Hice_thick = 300;
param.tg.look_angle_deg = [40 40 40 40]; % Maximum look angle expected, one entry per waveform
param.prf = prf;
param.presums = [6 6 6 6]; % You can have up to ~22 presums for wideband mode (check entry 1 of "presums" variable)
% Switch from tx calibration window to hanning window to broaden beam
DDS_amp = final_DDS_amp{freq_idx_WB} .* [0.5 1 0.5 0 0 0.5 1 0.5] ./ Hwindow_orig; % Amplitude scale must be the same for all waveforms
%in case receiver saturates:
%DDS_amp = final_DDS_amp{freq_idx_WB} .* [0.25 0.5 0.25 0 0 0.25 0.5 0.25] ./ Hwindow_orig;
% Renormalize the amplitudes
[~,relative_max_idx] = max(DDS_amp./param.max_tx);
DDS_amp = round(DDS_amp .* param.max_tx(relative_max_idx) / DDS_amp(relative_max_idx));
param.tx_weights = DDS_amp;
param.tukey = 0.08;
param.wfs(1).Tpd = 1e-6;
param.wfs(2).Tpd = 1e-6;
param.wfs(3).Tpd = 1e-6;
param.wfs(4).Tpd = 1e-6;
param.wfs(1).phase = final_DDS_phase{freq_idx_WB};
param.wfs(2).phase = final_DDS_phase{freq_idx_WB};
param.wfs(3).phase = final_DDS_phase{freq_idx_WB};
param.wfs(4).phase = final_DDS_phase{freq_idx_WB};
param.wfs(1).name = 'left_bleft';
param.wfs(2).name = 'left_bright';
param.wfs(3).name = 'right_bright';
param.wfs(4).name = 'right_bleft';
% Add in time delays to each position, subtract out the nadir time delays since tx_equalization already took care of those
% not quite sure what I have to adapt here... This now depends on the
% configuration what are the sending and receiving antennas? Only changed
% the angles here and added wf 4
beam_angle_deg = 20; % Positive to the left
param.wfs(1).delay = final_DDS_time{freq_idx_WB} ...
  - (phase_centers(2,:) / (c/2) * sind(beam_angle_deg))*1e9 ...
  - (phase_centers(3,:) / (c/2) * cosd(beam_angle_deg))*1e9 ...
  + (phase_centers(3,:) / (c/2) * cosd(0))*1e9;
beam_angle_deg = -20; % Negative to the right
param.wfs(2).delay = final_DDS_time{freq_idx_WB} ...
  - (phase_centers(2,:) / (c/2) * sind(beam_angle_deg))*1e9 ...
  - (phase_centers(3,:) / (c/2) * cosd(beam_angle_deg))*1e9 ...
  + (phase_centers(3,:) / (c/2) * cosd(0))*1e9;
beam_angle_deg = 20; % positive to the left
param.wfs(3).delay = final_DDS_time{freq_idx_WB} ...
  - (phase_centers(2,:) / (c/2) * sind(beam_angle_deg))*1e9 ...
  - (phase_centers(3,:) / (c/2) * cosd(beam_angle_deg))*1e9 ...
  + (phase_centers(3,:) / (c/2) * cosd(0))*1e9;
beam_angle_deg = -20; % Negative to the right
param.wfs(4).delay = final_DDS_time{freq_idx_WB} ...
  - (phase_centers(2,:) / (c/2) * sind(beam_angle_deg))*1e9 ...
  - (phase_centers(3,:) / (c/2) * cosd(beam_angle_deg))*1e9 ...
  + (phase_centers(3,:) / (c/2) * cosd(0))*1e9;
param.wfs(1).f0 = f0_list(freq_idx_WB);
param.wfs(2).f0 = f0_list(freq_idx_WB);
param.wfs(3).f0 = f0_list(freq_idx_WB);
param.wfs(4).f0 = f0_list(freq_idx_WB);
param.wfs(1).f1 = f1_list(freq_idx_WB);
param.wfs(2).f1 = f1_list(freq_idx_WB);
param.wfs(3).f1 = f1_list(freq_idx_WB);
param.wfs(4).f1 = f1_list(freq_idx_WB);
param.DDC_freq = (param.wfs(end).f0+param.wfs(end).f1)/2; % DDC frequency must be the same for all waveforms
% The mask disables transmit antenna elements. For example, to enable the
% left three antennas, set the first three entries to zero.
[param.wfs([1 2]).tx_mask] = deal([0 0 0 1 1 1 1 1]); % Left three tx antennas enabled
[param.wfs([3 4]).tx_mask] = deal([1 1 1 1 1 0 0 0]); % Right three tx antennas enabled
%[param.wfs(1:3).tx_mask] = deal([0 0 0 0 0 0 0 0]);
param.wfs(1).atten = 0;
param.wfs(2).atten = 0;
param.wfs(3).atten = 0;
param.wfs(4).atten = 0;
% it is referring to wfs and not to the three transmit antennas...
param.fn = fullfile(NI_base_dir,sprintf('image_permax_%.0f-%.0fMHz_%.0fft_%.0fus_%.0fmthick.xml', ...
  param.wfs(end).f0/1e6,param.wfs(end).f1/1e6,param.tg.Haltitude*100/2.54/12,param.wfs(end).Tpd*1e6,param.tg.Hice_thick));
write_cresis_xml(param);

%% Equalization (Using Ocean)
% Haltitude +/- 2000 ft
% For lower altitude, increase attenuation
% Use these settings over ocean or sea ice for fast-time equalization,
% transmit equalization, and receiver equalization.
% Creates one waveform for each of N DDS-transmitters plus a combined
% waveform with all transmitters going.
% =========================================================================
Haltitude = [2500 2500 0 3500 6000];
Tpd_list = [1e-6 1e-6 3e-6 3e-6 3e-6];
attenuation = [62 45 43 61 55];
fn_hint = {'WATER','ICE','NO_DELAY','WATER','WATER'};
for Tpd_idx = 1:length(Tpd_list)
  Tpd = Tpd_list(Tpd_idx);
  for freq_idx = [2]
    param = struct('radar_name','mcords5','num_chan',8,'aux_dac',[255 255 255 255 255 255 255 255],'version','14.0f1','TTL_prog_delay',650,'xml_version',2.0,'fs',1600e6,'fs_sync',90.0e6,'fs_dds',1440e6,'TTL_clock',1440e6/16,'TTL_mode',[2.5e-6 260e-9 -1100e-9],'arena_base_dir',arena_base_dir);
    param = merge_structs(param,param_defaults);
    param.arena = arena;
    param.max_tx = [4000 4000 4000 4000 4000 4000 4000 4000]; param.max_data_rate = 700; param.flight_hours = 3.5; param.sys_delay = 0.75e-6; param.use_mcords4_names = true; param.arena = arena;
    BW = abs(f1_list(freq_idx)-f0_list(freq_idx));
    if BW <= 170e6
      param.DDC_select = 2; % 200 MHz sampling mode
    elseif BW <= 370e6
      param.DDC_select = 1; % 400 MHz sampling mode
    else
      error('Bandwidth (%g MHz) is too large. Must be less than or equal to 370 MHz.', BW/1e6)
    end
    cal_used_idx = -1;
    fc = abs(f0_list+f1_list)/2;
    for cal_idx = 1:length(final_cal_fc)
      if abs(final_cal_fc(cal_idx) - fc(freq_idx)) < 1e6
        cal_used_idx = cal_idx;
        break;
      end
    end
    if cal_used_idx == -1
      error('The center frequency %g MHz does not match any of the cal setting center frequencies specified in final_cal_fc. Either change the f0_list and f1_list or add a calibration setting with this center frequency.', fc/1e6);
    end
    param.max_duty_cycle = 0.12;
    param.create_IQ = false;
    param.tg.staged_recording = false;
    param.tg.altitude_guard = 2000*12*2.54/100;
    param.tg.Haltitude = Haltitude(Tpd_idx)*12*2.54/100;
    param.tg.Hice_thick = 0;
    param.prf = prf;
    param.presums = [10 10 10 10 10 10 10 10 10];
    [param.wfs(1:8).atten] = deal(attenuation(Tpd_idx)-12);
    [param.wfs(9:9).atten] = deal(attenuation(Tpd_idx));
    param.tx_weights = final_DDS_amp{cal_used_idx};
    param.tukey = 0.08;
    param.Tpd = Tpd;
    for wf=1:9
      param.wfs(wf).phase = final_DDS_phase{cal_used_idx};
    end
    param.delay = final_DDS_time{cal_used_idx};
    param.f0 = f0_list(freq_idx);
    param.f1 = f1_list(freq_idx);
    param.DDC_freq = (param.f0+param.f1)/2;
    for wf=1:8
      param.wfs(wf).tx_mask = ones(1,8);
      param.wfs(wf).tx_mask(9-wf) = 0;
    end
    for wf=9:9
      param.wfs(wf).tx_mask = [0 0 0 0 0 0 0 0];
    end
    param.fn = fullfile(NI_calval_dir,sprintf('txequal_%.0f-%.0fMHz_%.0fft_%.0fft_%.0fus_%s.xml', ...
      param.f0/1e6, param.f1/1e6, (param.tg.Haltitude+[-param.tg.altitude_guard param.tg.altitude_guard])*100/12/2.54, ...
      param.Tpd*1e6,fn_hint{Tpd_idx}));
    write_cresis_xml(param);
  end
end

