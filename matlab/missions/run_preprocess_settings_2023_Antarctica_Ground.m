% script run_preprocess_settings_2023_Antarctica_Ground.m
%
% Support script for run_preprocess_EAGER.m
%
% Preprocess setup script for 2023_Antarctica_Ground.

param.preprocess = [];
param.preprocess.default = [];

if 0
  %% ACCUM3 SINGLE DAY
  date_str = '20240203';
  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_Ground_accum;
  param.preprocess.file{cur_idx}.base_dir = '/kucresis/scratch/data/Accum_Data/2023_Antarctica_Ground/';
  param.preprocess.file{cur_idx}.config_folder_name = date_str;
  param.preprocess.file{cur_idx}.board_folder_name = [date_str '/%b'];
  param.preprocess.date_str{cur_idx} = date_str;

else
  %% ACCUM3 MULTIPLE DAYS
  date_str = {'20231124', '20231125', '20231130', '20231204', '20240101', '20240102', '20240103', '20240104', '20240105', '20240107', '20240108', '20240120', '20240202', '20240203'};
  for idx = 1:length(date_str)
    cur_idx = length(param.preprocess.default)+1;
    param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_Ground_accum;
    param.preprocess.file{cur_idx}.base_dir = '/kucresis/scratch/data/Accum_Data/2023_Antarctica_Ground/';
    param.preprocess.file{cur_idx}.config_folder_name = date_str{idx};
    param.preprocess.file{cur_idx}.board_folder_name = [date_str{idx} '/%b'];
    param.preprocess.date_str{cur_idx} = date_str{idx};
  end
end
