function [param,defaults] = default_radar_params_2016_Antarctica_LC130_rds
% param = default_radar_params_2016_Antarctica_LC130_rds
%
% rds: 2016_Antarctica_LC130
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: John Paden

%% Preprocess parameters
param.season_name = '2016_Antarctica_LC130';
param.radar_name = 'rds';

% Reading in files
param.preprocess.digital_system_type = 'ldeo';
param.preprocess.header_load_func = @basic_load_ldeo;

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.gps.en = true;
param.records.gps.time_offset = 0;
param.records.frames.geotiff_fn = 'antarctica\Landsat-7\Antarctica_LIMA.tif';
param.records.frames.mode = 1;
param.records.file.version = 417;
param.records.file.suffix = '.dice';
param.records.file.boards = {''};
param.records.file.board_folder_name = '';
param.records.file.clk = NaN;
param.records.file.recursive = false;

%% Qlook worksheet
param.qlook.out_path = '';
param.qlook.block_size = 5000;
param.qlook.motion_comp = 0;
param.qlook.dec = 20;
param.qlook.inc_dec = 10;
param.qlook.surf.en = 1;
param.qlook.resample = [2 1];

%% SAR worksheet
param.sar.out_path = '';
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 5000;
param.sar.chunk_overlap = 10;
param.sar.frm_overlap = 0;
param.sar.coh_noise_removal = 0;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = inf;
param.sar.pulse_rfi.en = [];
param.sar.pulse_rfi.inc_ave= [];
param.sar.pulse_rfi.thresh_scale = [];
param.sar.trim_vals = [];
param.sar.pulse_comp = 1;
param.sar.ft_dec = 1;
param.sar.ft_wind = @hanning;
param.sar.ft_wind_time = 0;
param.sar.lever_arm_fh = @lever_arm;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 2.5;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -5:5;
param.array.dbin = 1;
param.array.dline = 6;
param.array.DCM = [];
param.array.Nsv = 1;
param.array.theta_rng = [0 0];
param.array.sv_fh = @array_proc_sv;
param.array.diag_load = 0;
param.array.Nsig = 2;

%% Radar worksheet
param.radar.adc_bits = 12;
param.radar.Vpp_scale = 1.125;
param.radar.lever_arm_fh = @lever_arm;

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.img = 0;
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 2;
param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+500]';
% param.post.echo.elev_comp = 3;
% param.post.echo.depth = '[min(Surface_Elev)-1500 max(Surface_Elev)+100]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'antarctic';

%% Analysis worksheet
param.analysis_noise.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_noise.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_noise.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average

%% Radar Settings

defaults = {};

%% Radar Settings: Survey Mode Thin Ice Single Polarization
% =========================================================================
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1],[2 1],[3 1],[4 1]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
default.radar.fs = 150e6;
for wf = 1:4
  if wf == 1
    param.radar.wfs(wf).Tpd = 3e-6;
    param.radar.wfs(wf).tx_weights = [1 0];
    param.radar.wfs(wf).rx_paths = [1]; % ADC to rx path mapping
  elseif wf == 2
    param.radar.wfs(wf).Tpd = 3e-6;
    param.radar.wfs(wf).tx_weights = [0 1];
    param.radar.wfs(wf).rx_paths = [2]; % ADC to rx path mapping
  elseif wf == 3
    param.radar.wfs(wf).Tpd = 1e-6;
    param.radar.wfs(wf).tx_weights = [1 0];
    param.radar.wfs(wf).rx_paths = [1]; % ADC to rx path mapping
  elseif wf == 4
    param.radar.wfs(wf).Tpd = 1e-6;
    param.radar.wfs(wf).tx_weights = [0 1];
    param.radar.wfs(wf).rx_paths = [2]; % ADC to rx path mapping
  end
  param.radar.wfs(wf).f0 = 158e6;
  param.radar.wfs(wf).f1 = 218e6;
  param.radar.wfs(wf).presums = 16;
  param.radar.wfs(wf).adc_gains_dB = [38]; % ADC gain
  param.radar.wfs(wf).gain_en = [0]; % Disable fast-time gain correction
  param.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  param.radar.wfs(wf).Tadc_adjust = 0;
  param.radar.wfs(wf).bit_shifts = [0];
  default.radar.wfs(wf).tx_paths = {[1 2]};
  default.radar.wfs(wf).Tsys = [0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0];
end
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+1500]';
% Note psc config name was incorrectly set, but it is for shallow ice:
default.config_regexp = '.*';
default.name = 'Survey Mode';

defaults{end+1} = default;
