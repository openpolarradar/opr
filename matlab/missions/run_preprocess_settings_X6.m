% script run_preprocess_settings_X6.m
%
% Support script for run_preprocess_X6.m

param.preprocess = [];

if ispc
  base_dir = fullfile('Z:\','data','HF_Sounder','2022_Greenland_X6');
else
  base_dir = fullfile('/cresis','snfs1','data','HF_Sounder','2022_Greenland_X6');
end

if 1
  %% X6 RDS SINGLE DAY

  data_info_list = { ...
    '',fullfile('gps','07312022'),fullfile('data','07312022'),'20220731'; ...
    };

  data_info_list_idx = 1;
  param.preprocess.default{data_info_list_idx} = @default_radar_params_2022_Greenland_X6_rds;
  param.preprocess.file{data_info_list_idx}.base_dir = fullfile(base_dir,data_info_list{data_info_list_idx,1});
  param.preprocess.file{data_info_list_idx}.config_folder_name = data_info_list{data_info_list_idx,2};
  param.preprocess.file{data_info_list_idx}.board_folder_name = data_info_list{data_info_list_idx,3};
  param.preprocess.date_str{data_info_list_idx} = data_info_list{data_info_list_idx,4};

elseif 1
  %% X6 RDS MULTIPLE DAYS

  data_info_list = { ...
    '',fullfile('gps','07272022'),fullfile('data','07272022'),'20220727'; ...
    '',fullfile('gps','07282022'),fullfile('data','07282022'),'20220728'; ...
    '',fullfile('gps','07292022'),fullfile('data','07292022'),'20220729'; ...
    '',fullfile('gps','07302022'),fullfile('data','07302022'),'20220730'; ...
    '',fullfile('gps','07312022'),fullfile('data','07312022'),'20220731'; ...
    '',fullfile('gps','08012022'),fullfile('data','08012022'),'20220801'; ...
    };
  for data_info_list_idx = 1:size(data_info_list,1)
    param.preprocess.default{data_info_list_idx} = @default_radar_params_2022_Greenland_X6_rds;
    param.preprocess.file{data_info_list_idx}.base_dir = fullfile(base_dir,data_info_list{data_info_list_idx,1});
    param.preprocess.file{data_info_list_idx}.config_folder_name = data_info_list{data_info_list_idx,2};
    param.preprocess.file{data_info_list_idx}.board_folder_name = data_info_list{data_info_list_idx,3};
    param.preprocess.date_str{data_info_list_idx} = data_info_list{data_info_list_idx,4};
  end
end
