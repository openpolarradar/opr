function [param, defaults] = default_radar_params_2025_RCfield_X6_snow
% param = default_radar_params_2025_RCfield_X6_snow
%
% Snow: 2025_RCfield_X6
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Author: John Paden

%% Preprocess parameters
param.season_name = '2025_RCfield_X6';
param.radar_name = 'snow';

param.preprocess.file.version = 14;
param.preprocess.file.prefix = 'X6';
param.preprocess.file.suffix = '.dat';
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

param.preprocess.digital_system_type = 'cresis';
param.preprocess.wg_type = 'cresis';
param.preprocess.header_load_func = @basic_load_X6;
param.preprocess.config.board_map = {''};
param.preprocess.tx_map = {''};


param.preprocess.xml_version = -1; % No XML file available

param.preprocess.tx_enable = [1];

%% CReSIS parameters
param.preprocess.cresis.clk = 100e6;
param.preprocess.cresis.expected_rec_sizes = [65536];

%% Command worksheet
default.cmd.records = 1;
default.cmd.qlook = 1;
default.cmd.generic = 1;

%% Records worksheet
param.records.file.boards = {'1'};
param.records.frames.geotiff_fn = fullfile('alaska','Landsat-7','Alaska_90m.tif');
param.records.frames.mode = 2;
param.records.gps.en = 1;
param.records.gps.time_offset = 1;
param.records.file.prefix = 'X6';
param.records.file.clk = 100e6;
param.records.file.version = 14;

%% Qlook worksheet
param.qlook.img_comb = [];
param.qlook.imgs = {[1 1]};
param.qlook.out_path = '';
param.qlook.block_size = 2000;
param.qlook.motion_comp = 0;
param.qlook.dec = 4;
param.qlook.inc_dec = 5;
param.qlook.surf.en = 1;
param.qlook.surf.profile = 'SNOW_AWI';


%% SAR worksheet
param.sar.out_path = '';
param.sar.imgs = param.qlook.imgs;
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 2000;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = 4e-6;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 1;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.imgs = param.qlook.imgs;
param.array.img_comb = param.qlook.img_comb;
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -2:2;
param.array.dbin = 1;
param.array.dline = 5;

%% Radar worksheet
param.radar.prf = 3200;
param.radar.fs = 128.8e6;
param.radar.adc_bits = 16;
param.radar.Vpp_scale = 2; % Digital receiver gain is 5, full scale Vpp is 2
param.radar.Tadc_adjust = []; % System time delay: leave this empty or set it to zero at first, determine this value later using data over surface with known height or from surface multiple
param.radar.lever_arm_fh = @lever_arm;
chan_equal_Tsys = [0]/1e9;
chan_equal_dB = [0];
chan_equal_deg = [0];
for wf = 1
  param.radar.wfs(wf).tx_weights = 1; % Watts
  param.radar.wfs(wf).adc_gains_dB = 40; % Radiometric calibration to 1/R^2
  param.radar.wfs(wf).rx_paths = [1]; % ADC to rx path mapping
  param.radar.wfs(wf).ref_fn = '';
  param.radar.wfs(wf).chan_equal_Tsys = chan_equal_Tsys;
  param.radar.wfs(wf).chan_equal_dB = chan_equal_dB;
  param.radar.wfs(wf).chan_equal_deg = chan_equal_deg;
  param.radar.wfs(wf).adcs = [1];
  param.radar.wfs(wf).nz_trim = {[0 0],[0 0],[0 0],[0 0]};
  param.radar.wfs(wf).nz_valid = [0 1 2 3];
end

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.layer_dir = 'layerData';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Alaska';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 2;
param.post.echo.depth = '[min(Surface_Depth)-2 max(Surface_Depth)+25]';
% param.post.echo.elev_comp = 3;
% param.post.echo.depth = '[min(Surface_Elev)-25 max(Surface_Elev)+2]';
param.post.echo.er_ice = round((1+0.51*0.3)^3 * 100)/100;
param.post.ops.location = 'alaska';
  
%% Radar Settings

defaults = {};
default = param;

% Survey Mode 2-18 GHz
default.radar.wfs(1).f0 = 2e9;
default.radar.wfs(1).f1 = 8e9;
default.radar.wfs(1).Tpd = 260e-6;
default.radar.wfs(1).BW_window = [2e9 8e9];
default.radar.wfs(1).t_ref = 0;

default.config_regexp = '.*';
default.name = 'Survey Mode';
defaults{end+1} = default;

%% Add default settings

% param.config.defaults = defaults;
