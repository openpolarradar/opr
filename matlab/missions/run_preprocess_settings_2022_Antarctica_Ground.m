% script run_preprocess_settings_2022_Antarctica_Ground.m
%
% Support script for run_preprocess_EAGER.m
%
% Preprocess setup script for 2022_Antarctica_Ground.

param.preprocess = [];

%% ACCUM3 SINGLE DAY
% cur_idx = length(param.preprocess.default)+1;
% param.preprocess.default{cur_idx} = @default_radar_params_2022_Antarctica_Ground_accum;
% param.preprocess.file{cur_idx}.base_dir = '/kucresis/scratch/data/Accum_Data/2022_Antarctica_Ground/';
% param.preprocess.file{cur_idx}.config_folder_name = '20221207';
% param.preprocess.file{cur_idx}.board_folder_name = '20221207/%b';
% param.preprocess.date_str{cur_idx} = '20221207';


%% ACCUM3 MULTIPLE DAYS
date_str = {'20221206', '20221208', '20221209', '20230108', '20230109', '20230110', '20230111', '20230112', '20230113', '20230114', '20230115', '20230118', '20230119', '20230121'};
% date_str = {'20230118'};
for idx = 1:length(date_str)
  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = @default_radar_params_2022_Antarctica_Ground_accum;
  param.preprocess.file{cur_idx}.base_dir = '/kucresis/scratch/data/Accum_Data/2022_Antarctica_Ground/';
  param.preprocess.file{cur_idx}.config_folder_name = date_str{idx};
  param.preprocess.file{cur_idx}.board_folder_name = [date_str{idx} '/%b'];
  param.preprocess.date_str{cur_idx} = date_str{idx};
end
