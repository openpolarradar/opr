function [param,defaults] = default_radar_params_2024_Greenland_Ground_accum
% [param,defaults] = default_radar_params_2024_Greenland_Ground_accum
%
% Accum: 2024_Greenland_Ground
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: John Paden

%% Preprocess parameters
param.season_name = '2024_Greenland_Ground';
param.radar_name = 'accum3';

% Reading in files
param.preprocess.digital_system_type = 'arena';
param.preprocess.wg_type = 'arena';
param.preprocess.header_load_func = @basic_load_arena;
param.preprocess.tx_map = {'awg0','awg1'};

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

% Creating settings files
param.preprocess.max_data_rate = 60;
param.preprocess.max_duty_cycle = 0.1;
param.preprocess.prf_multiple = [10e6 10e6/20]; % Power supply sync signal that PRF must be a factor of these numbers
param.preprocess.PRI_guard = 1e-6;
param.preprocess.PRI_guard_percentage = 450e6/500e6;
param.preprocess.tx_enable = [1 1];
param.preprocess.max_tx = 1.0;
param.preprocess.max_tx_voltage = sqrt(400*50)*10^(-2/20); % voltage at max_tx

%% EAGER 1 ACCUM Arena Parameters
arena = [];
arena.clk = 10e6;
fs = 1000e6;
fs_dac = 2000e6;
subsystem_idx = 0;
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'CTU51';
arena.subsystem(subsystem_idx).subSystem{1} = 'ctu';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA60';
arena.subsystem(subsystem_idx).subSystem{1} = 'awg0';
arena.subsystem(subsystem_idx).subSystem{2} = 'digrx0';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'ARENA61';
arena.subsystem(subsystem_idx).subSystem{1} = 'awg1';
arena.subsystem(subsystem_idx).subSystem{1} = 'digrx1';
subsystem_idx = subsystem_idx + 1;
arena.subsystem(subsystem_idx).name = 'Data Server';

dac_idx = 0;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'awg0';
arena.dac(dac_idx).type = 'dac-ad9129_0012';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = -10;
arena.dac(dac_idx).desiredAlignMax = 0;
arena.dac(dac_idx).dcoPhase = 80;
dac_idx = dac_idx + 1;
arena.dac(dac_idx).name = 'awg1';
arena.dac(dac_idx).type = 'dac-ad9129_0012';
arena.dac(dac_idx).dacClk = fs_dac;
arena.dac(dac_idx).desiredAlignMin = -4;
arena.dac(dac_idx).desiredAlignMax = 6;
arena.dac(dac_idx).dcoPhase = 80;

adc_idx = 0;
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx0';
arena.adc(adc_idx).type = 'adc-ad9680_0017';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 1;
arena.adc(adc_idx).desiredAlignMin = -8;
arena.adc(adc_idx).desiredAlignMax = 2;
arena.adc(adc_idx).stream = 'socket';
arena.adc(adc_idx).ip = '172.16.0.118';
arena.adc(adc_idx).outputSelect = 1;
arena.adc(adc_idx).wf_set = 1;
arena.adc(adc_idx).gain_dB = [0 0];
adc_idx = adc_idx + 1;
arena.adc(adc_idx).name = 'digrx1';
arena.adc(adc_idx).type = 'adc-ad9680_0017';
arena.adc(adc_idx).sampFreq = fs;
arena.adc(adc_idx).adcMode = 1;
arena.adc(adc_idx).desiredAlignMin = -2;
arena.adc(adc_idx).desiredAlignMax = 8;
arena.adc(adc_idx).stream = 'socket';
arena.adc(adc_idx).ip = '172.16.0.118';
arena.adc(adc_idx).outputSelect = 1;
arena.adc(adc_idx).wf_set = 2;
arena.adc(adc_idx).gain_dB = [0 0];

daq_idx = 0;
daq_idx = daq_idx + 1;
arena.daq(daq_idx).name = 'daq0';
arena.daq(daq_idx).type = 'daq_0001';
arena.daq(daq_idx).auxDir = '/data/';
arena.daq(daq_idx).fileStripe = '/data/%b/';
arena.daq(daq_idx).fileName = 'accum';
arena.daq(daq_idx).udp_packet_headers = false;

arena.system.name = 'ku0001';

arena.param.tx_max = [1 1];
arena.param.PA_setup_time = 2e-6; % Time required to enable PA before transmit
arena.param.TTL_time_delay = 0.0; % TTL time delay relative to transmit start
arena.param.ADC_time_delay = 3.0720e-6; % ADC time delay relative to transmit start

arena.psc.type = 'psc_0003';

arena.daq.type = 'daq_0001';

arena.ctu.name = 'ctu';
arena.ctu.type = 'ctu_001D';
if 1
  % External GPS
  arena.ctu.nmea = 31;
  arena.ctu.nmea_baud = 115200;
  arena.ctu.pps = 10;
  arena.ctu.pps_polarity = 1;
else
  % Internal GPS
  arena.ctu.nmea = 60;
  arena.ctu.nmea_baud = 115200;
  arena.ctu.pps = 63;
  arena.ctu.pps_polarity = 1;
end
idx = 0;
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'EPRI';
arena.ctu.out.bit_group(idx).bits = 0;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [0 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'PRI';
arena.ctu.out.bit_group(idx).bits = 1;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'TX_EN'; % 1 enables the transmitter, sets the T/R to transmit
arena.ctu.out.bit_group(idx).bits = 2;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'CAL_EN'; % 1 enables rx calibration switch
arena.ctu.out.bit_group(idx).bits = 3;
arena.ctu.out.bit_group(idx).epri = [1 0];
arena.ctu.out.bit_group(idx).pri = [1 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'Atten1_TRISO_NOT_USED'; % unused
arena.ctu.out.bit_group(idx).bits = 4;
arena.ctu.out.bit_group(idx).epri = [0 0];
arena.ctu.out.bit_group(idx).pri = [0 0];
idx = idx + 1;
arena.ctu.out.bit_group(idx).name = 'LOW_GAIN_EN'; % 1 is low gain
arena.ctu.out.bit_group(idx).bits = 5;
arena.ctu.out.bit_group(idx).epri = [1 1];
arena.ctu.out.bit_group(idx).pri = [1 1];

arena.ctu.out.time_cmd = {'2e-6+param.wfs(wf).Tpd+0.1e-6' '2e-6+param.wfs(wf).Tpd+0.1e-6' '2e-6+param.wfs(wf).Tpd+0.1e-6' '2/param.prf'};

param.arena = arena;

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.gps.time_offset = 0;
param.records.frames.geotiff_fn = fullfile('greenland','Landsat-7','mzl7geo_90m_lzw.tif');
param.records.frames.mode = 1;
param.records.file.version = 103;
param.records.file.midfix = param.radar_name;
param.records.file.suffix = '.dat';
%param.records.file.boards = {'digrx0'}; % H only polarization
param.records.file.boards = {'digrx0','digrx1'}; % HV Polarization
param.records.file.board_folder_name = '%b';
param.records.file.clk = 10e6;

%% Qlook worksheet
param.qlook.out_path = '';
param.qlook.block_size = 5000;
param.qlook.motion_comp = 0;
param.qlook.dec = 20;
param.qlook.inc_dec = 10;
param.qlook.surf.en = 1;
param.qlook.surf.min_bin = 0;
param.qlook.surf.method = 'fixed';
param.qlook.surf.fixed_value = 0;
param.qlook.resample = [2 1];

%% SAR worksheet
param.sar.out_path = '';
param.sar.chunk_len = 5000;
param.sar.mocomp.en = 1;
param.sar.sigma_x = 1;

%% Array worksheet
param.array.in_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.bin_rng = 0;
param.array.line_rng = -5:5;
param.array.dline = 6;
param.array.ft_over_sample = 2;

%% Radar worksheet
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 1.5;
param.radar.lever_arm_fh = @lever_arm;

%% Post worksheet
param.post.out_path = '';
param.post.data_dirs = {'qlook'};
param.post.img = 0;
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Greenland';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 2;
param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+2900]';
% param.post.echo.elev_comp = 3;
% param.post.echo.depth = '[min(Surface_Elev)-1500 max(Surface_Elev)+100]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'arctic';

%% Analysis worksheet
param.analysis_noise.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_noise.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_noise.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average

%% Radar Settings

defaults = {};

default = param;
default.arena = arena;

%% Radar Settings: Survey Mode Thick Ice Polarimetric
% No Profile Processor Digital System (use mode_latch,subchannel instead)
% Each row of param.records.data_map{board_idx} = [mode_latch subchannel wf adc]
default.records.arena.total_presums = 60+240+60+240;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
default.records.arena.epri_time_error_threshold = 1;
default.records.arena.mode_latch_fix = [[0 1 2 3 4 5 6 7].',[0 0 2 2 4 4 6 6].'];
default.records.data_map = {[0 0 1 1;1 0 1 1;   2 0 2 1;3 0 2 1;   4 0 3 1;5 0 3 1;   6 0 4 1;7 0 4 1], ...
  [0 0 1 2;1 0 1 2;   2 0 2 2;3 0 2 2;   4 0 3 2;5 0 3 2;   6 0 4 2;7 0 4 2]};
default.qlook.img_comb = [8e-06 -Inf 1e-06];
default.qlook.imgs = {[1 1],[2 1]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:4 
  default.radar.wfs(wf).adc_gains_dB = [38 38]; % ADC gain
  %default.radar.wfs(wf).adcs = [1 2];
  default.radar.wfs(wf).rx_paths = [1 2]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [6 8]; % From XML file
  default.radar.wfs(wf).Tsys = [0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0];
  default.radar.wfs(wf).tx_paths = {[1],[2]};
end
default.radar.wfs(1).tx_weights = [1 0];
default.radar.wfs(2).tx_weights = [1 0];
default.radar.wfs(3).tx_weights = [0 1];
default.radar.wfs(4).tx_weights = [0 1];
default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+2000]';
default.config_regexp = 'psc_eager_configHV';
default.name = 'Polarimetric Mode 600-900 MHz Thick Ice';
defaults{end+1} = default;


default = param;
default.arena = arena;

%% Radar Settings: Survey Mode Thin Ice Polarimetric
% No Profile Processor Digital System (use mode_latch,subchannel instead)
% Each row of param.records.data_map{board_idx} = [mode_latch subchannel wf adc]
default.records.arena.total_presums = 256;
default.records.arena.epri_alignment_guard = default.records.arena.total_presums;
default.records.arena.epri_time_error_threshold = 1;
default.records.arena.mode_latch_fix = [[0 1 2 3 4 5 6 7].',[0 0 2 2 4 4 6 6].'];
default.records.data_map = {[0 0 1 1;1 0 1 1;   4 0 2 1;5 0 2 1], ...
  [0 0 1 2;1 0 1 2;   4 0 2 2;5 0 2 2]};
default.qlook.img_comb = [];
default.qlook.imgs = {[1 1],[2 1],[1 2],[2 2]};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.analysis_noise.imgs = default.qlook.imgs;
default.radar.ref_fn = '';
for wf = 1:2
  default.radar.wfs(wf).adc_gains_dB = [38 38]; % ADC gain
  %default.radar.wfs(wf).adcs = [1 2];
  default.radar.wfs(wf).rx_paths = [1 2]; % ADC to rx path mapping
  default.radar.wfs(wf).gain_en = [0 0]; % Disable fast-time gain correction
  default.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
  default.radar.wfs(wf).Tadc_adjust = 0;
  default.radar.wfs(wf).bit_shifts = [6 8]; % From XML file
  default.radar.wfs(wf).Tsys = [0 0]/1e9;
  default.radar.wfs(wf).chan_equal_dB = [0 0];
  default.radar.wfs(wf).chan_equal_deg = [0 0];
  default.radar.wfs(wf).tx_paths = {[1],[2]};
end
default.radar.wfs(1).tx_weights = [0.3 0];
default.radar.wfs(2).tx_weights = [0 0.3];

default.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+500]';
default.config_regexp = 'psc_eager_thin_configHV';
default.name = 'Polarimetric Mode 600-900 MHz Thin Ice';
defaults{end+1} = default;
