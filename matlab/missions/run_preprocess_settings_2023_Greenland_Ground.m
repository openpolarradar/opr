% script run_preprocess_settings_2023_Greenland_Ground.m
%
% Support script for run_preprocess.m
%
% Preprocess setup script for 2023_Greenland_Ground.

param.preprocess = [];

%% ACCUM3 SINGLE DAY
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = @default_radar_params_2023_Greenland_Ground_accum;
param.preprocess.base_dir{cur_idx} = '/data/';
param.preprocess.config_folder_names{cur_idx} = '20230531';
param.preprocess.board_folder_names{cur_idx} = '20230531/%b';
param.preprocess.date_str{cur_idx} = '20230531';
