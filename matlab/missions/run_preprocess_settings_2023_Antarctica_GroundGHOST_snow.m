% script run_preprocess_settings_2023_Antarctica_GroundGHOST_snow.m
%
% Support script for run_preprocess.m
%
% Preprocess setup script for 2023_Antarctica_GroundGHOST_snow.

param.preprocess = [];

%% SNOW SINGLE DAY
date_str = '20231204';
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_GroundGHOST_snow;
param.preprocess.file{cur_idx}.base_dir = '/data/';
param.preprocess.file{cur_idx}.config_folder_name = date_str;
param.preprocess.file{cur_idx}.board_folder_name = [date_str '/%b'];
param.preprocess.date_str{cur_idx} = date_str;


%% SNOW MULTIPLE DAYS
%date_str = {'20231122','20231124','20231125','20231126','20231125','20231204','20231205'};
%for idx = 1:length(date_str)
%  cur_idx = length(param.preprocess.default)+1;
%  param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_GroundGHOST_snow;
%  param.preprocess.file{cur_idx}.base_dir = '/data/2023_Antarctica_GroundGHOST/';
%  param.preprocess.file{cur_idx}.config_folder_name = date_str{idx};
%  param.preprocess.file{cur_idx}.board_folder_name = [date_str{idx} '/%b'];
%  param.preprocess.date_str{cur_idx} = date_str{idx};
%end
