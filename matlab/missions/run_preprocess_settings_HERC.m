% script run_preprocess_HERC.m
%
% Support script for run_preprocess.m

param.preprocess = [];

%% HERC MCORDS6 GROUND
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = default_radar_params_2019_Antarctica_Ground_rds();
param.preprocess.base_dir{cur_idx} = '/data/';
param.preprocess.config_folder_names{cur_idx} = '20190920';
param.preprocess.board_folder_names{cur_idx} = '20190920/%b';
param.preprocess.date_strs{cur_idx} = '20190920';
