% script run_preprocess_settings_2023_Antarctica_BaslerMKB.m
%
% Support script for run_preprocess.m
%
% Preprocess setup script for 2023_Antarctica_BaslerMKB.

param.preprocess = [];
param.preprocess.default = [];

%% ACCUM3 SINGLE DAY
% date_str = '20231227';
% cur_idx = length(param.preprocess.default)+1;
% param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_BaslerMKB_accum;
% param.preprocess.file{cur_idx}.base_dir = '/cresis/snfs1/data/Accum_Data/2023_Antarctica_BaslerMKB/';
% param.preprocess.file{cur_idx}.config_folder_name = date_str;
% param.preprocess.file{cur_idx}.board_folder_name = [date_str '/%b'];
% param.preprocess.date_str{cur_idx} = date_str;


%% ACCUM3 MULTIPLE DAYS
date_str = {'20231211','20231212','20231221','20231224','20231225','20231226','20231227','20231228','20231229','20240104','20240105','20240107','20240108','20240109','20240111','20240112'};
% date_str = {'20240112'};
for idx = 1:length(date_str)
  cur_idx = length(param.preprocess.default)+1;
  param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_BaslerMKB_accum;
  param.preprocess.file{cur_idx}.base_dir = '/kucresis/scratch/data/Accum_Data/2023_Antarctica_BaslerMKB/';
  param.preprocess.file{cur_idx}.config_folder_name = date_str{idx};
  param.preprocess.file{cur_idx}.board_folder_name = [date_str{idx} '/%b'];
  param.preprocess.date_str{cur_idx} = date_str{idx};
end
