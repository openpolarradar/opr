function [param,defaults] = default_radar_params_2022_Antarctica_BaslerMKB_rds
% param = default_radar_params_2022_Antarctica_BaslerMKB_rds
%
% rds: 2022_Antarctica_BaslerMKB
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: John Paden
%
% See also: default_radar_params_*.m

%% Preprocess parameters
param.season_name = '2022_Antarctica_BaslerMKB';
param.radar_name = 'rds';

% Reading in files
param.preprocess.digital_system_type = 'utig';
param.preprocess.header_load_func = @basic_load_utig;

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

default = [];

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.gps.time_offset = 0;
param.records.gps.en = 1;
param.records.frames.geotiff_fn = 'antarctica\Landsat-7\Antarctica_LIMA.tif';
param.records.frames.mode = 1;
param.records.file.version = 415;
param.records.file.prefix = 'radar0';
param.records.file.suffix = '.dat';
param.records.file.boards = {''};
param.records.file.clk = 10e6;
param.records.arena.total_presums = 32;

%% Qlook worksheet
param.qlook.out_path = '';
param.qlook.block_size = 20000;
param.qlook.motion_comp = 0;
param.qlook.dec = 10;
param.qlook.inc_dec = 5;
param.qlook.surf.en = 1;
param.qlook.surf.method = 'rds';
param.qlook.resample = [2 1];
param.qlook.img_comb = [4e-06 -inf 1e-06];
param.qlook.imgs = {[ones(2,1) [1 3].'],[ones(2,1) [2 4].']};

%% SAR worksheet
param.sar.out_path = '';
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 5000;
param.sar.chunk_overlap = 10;
param.sar.frm_overlap = 0;
param.sar.coh_noise_removal = 0;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = inf;
param.sar.pulse_rfi.en = [];
param.sar.pulse_rfi.inc_ave= [];
param.sar.pulse_rfi.thresh_scale = [];
param.sar.trim_vals = [];
param.sar.pulse_comp = 1;
param.sar.ft_dec = 1;
param.sar.ft_wind = @hanning;
param.sar.ft_wind_time = 0;
param.sar.lever_arm_fh = @lever_arm;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 2.5;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;
param.sar.imgs = param.qlook.imgs;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.line_rng = -10:10;
param.array.dbin = 1;
param.array.dline = 11;
param.array.DCM = [];
param.array.Nsv = 1;
param.array.theta_rng = [0 0];
param.array.sv_fh = @array_proc_sv;
param.array.diag_load = 0;
param.array.Nsig = 2;
param.array.imgs = param.sar.imgs;
param.array.img_comb = param.qlook.img_comb;

%% Radar worksheet
param.radar.fs = 50e6;
param.radar.prf = 6250;
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 2;
param.radar.lever_arm_fh = @lever_arm;
param.radar.wfs = [];
for wf = 1:1
  param.radar.wfs(wf).adcs = [1 2 3 4]; % ADCs
  param.radar.wfs(wf).f0 = 52.5e6;
  param.radar.wfs(wf).f1 = 67.5e6;
  param.radar.wfs(wf).DDC_dec = 1;
  param.radar.wfs(wf).DDC_freq = 0;
  param.radar.wfs(wf).ft_wind = @(N)tukeywin(N,0.2);
  param.radar.wfs(wf).BW_window = [52000000 68000000];
  param.radar.wfs(wf).ft_dec = [1 1];
  param.radar.wfs(wf).Tpd = 1e-6;
  param.radar.wfs(wf).tukey = 0.1;
  param.radar.wfs(wf).system_dB = 0;
  param.radar.wfs(wf).rx_paths = [1 2 1 2];
  param.radar.wfs(wf).adc_gains_dB = [4 50 4 50]; % ADC gain
  param.radar.wfs(wf).chan_equal_dB = [0 0 0 0];
  param.radar.wfs(wf).chan_equal_deg = [0 0 0 0];
  param.radar.wfs(wf).Tsys = [0 0 0 0];
  param.radar.wfs(wf).presums = 32;
  param.radar.wfs(wf).bit_shifts = [0 0 0 0];
  param.radar.wfs(wf).Tadc_adjust = -1.2e-6;
  param.radar.wfs(wf).Tadc = 0;
  param.radar.wfs(wf).gain_en = [0 0 0 0]; % Disable fast-time gain correction
  param.radar.wfs(wf).coh_noise_method = ''; % No coherent noise removal
end

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.img = 0;
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
param.post.echo.elev_comp = 2;
param.post.echo.depth = '[min(Surface_Depth)-5 max(Surface_Depth)+4200]';
% param.post.echo.elev_comp = 3;
% param.post.echo.depth = '[min(Surface_Elev)-1500 max(Surface_Elev)+100]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'antarctic';

%% analysis_noise worksheet
param.analysis_noise.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_noise.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_noise.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average

%% analysis_equal worksheet
param.analysis_equal.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_equal.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_equal.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average

%% analysis_burst worksheet
param.analysis_burst.block_size = 10000;
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_burst.cmd{cmd_idx}.method = 'coh_noise';
param.analysis_burst.cmd{cmd_idx}.distance_weight = 1; % Enable distance weighting of the average
param.preprocess.param_worksheets = {};

%% analysis_noise worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_noise';
param.analysis_noise.block_size = 10000;
param.analysis_noise.imgs = param.sar.imgs;
param.analysis_noise.cmd{1}.method = 'coh_noise';
param.analysis_noise.cmd{1}.distance_weight = 1;
param.analysis_noise.imgs = param.sar.imgs;

%% analysis_equal worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_equal';
param.analysis_equal.block_size = 10000;
param.analysis_equal.imgs = param.sar.imgs;
param.analysis_equal.cmd{1}.method = 'waveform';
param.analysis_equal.cmd{1}.start_time = struct('eval',struct('cmd','s=s-1.5e-6;'));
param.analysis_equal.cmd{1}.Nt = 128;
param.analysis_equal.cmd{1}.dec = 10;

%% analysis_burst worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_burst';
param.analysis_burst.block_size = 10000;
param.analysis_burst.imgs = param.sar.imgs;
param.analysis_burst.cmd{1}.method = 'burst_noise';
param.analysis_burst.cmd{1}.dec = 1;

%% Radar Settings

defaults = {};

% Survey Mode Thick Ice
default.config_regexp = 'survey';
default.name = 'Survey Mode 52.5-67.5 MHz Thick Ice';
defaults{end+1} = default;

