% script run_preprocess_settings_2022_Antarctica_GroundGHOST.m
%
% Support script for run_preprocess.m
%
% Preprocess setup script for 2022_Antarctica_GroundGHOST.

param.preprocess = [];

%% RDS SINGLE DAY
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = @default_radar_params_2022_Antarctica_BaslerMKB_accum;
param.preprocess.base_dir{cur_idx} = '/data/';
% param.preprocess.config_folder_names{cur_idx} = '20221225a';
% param.preprocess.board_folder_names{cur_idx} = '20221225a/%b';
% param.preprocess.date_str{cur_idx} = '20221224';
% param.preprocess.config_folder_names{cur_idx} = '20230110';
% param.preprocess.board_folder_names{cur_idx} = '20230110/%b';
% param.preprocess.date_str{cur_idx} = '20230110';
% param.preprocess.config_folder_names{cur_idx} = '20230114';
% param.preprocess.board_folder_names{cur_idx} = '20230114/%b';
% param.preprocess.date_str{cur_idx} = '20230114';
% param.preprocess.config_folder_names{cur_idx} = '20230115';
% param.preprocess.board_folder_names{cur_idx} = '20230115/%b';
% param.preprocess.date_str{cur_idx} = '20230115';
% param.preprocess.config_folder_names{cur_idx} = '20230116';
% param.preprocess.board_folder_names{cur_idx} = '20230116/%b';
% param.preprocess.date_str{cur_idx} = '20230116';
% param.preprocess.config_folder_names{cur_idx} = '20230125';
% param.preprocess.board_folder_names{cur_idx} = '20230125/%b';
% param.preprocess.date_str{cur_idx} = '20230125';
% param.preprocess.config_folder_names{cur_idx} = '20230126';
% param.preprocess.board_folder_names{cur_idx} = '20230126/%b';
% param.preprocess.date_str{cur_idx} = '20230126';
% param.preprocess.config_folder_names{cur_idx} = '20230127';
% param.preprocess.board_folder_names{cur_idx} = '20230127/%b';
% param.preprocess.date_str{cur_idx} = '20230127';
param.preprocess.config_folder_names{cur_idx} = '20230128';
param.preprocess.board_folder_names{cur_idx} = '20230128/%b';
param.preprocess.date_str{cur_idx} = '20230128';

% 20230109_191654_accum_digrx0_0113.dat

