% script run_preprocess_settings_BASUAV.m
%
% Support script for run_preprocess_BASUAV.m
%
% Preprocess setup script for 2023_Antarctica_Windracers

param.preprocess = [];

%% ACCUM3 SINGLE DAY
cur_idx = length(param.preprocess.default)+1;
param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_Windracers_accum;
param.preprocess.file{cur_idx}.base_dir = '/mnt/data15/';
param.preprocess.file{cur_idx}.config_folder_name = '20231103';
param.preprocess.file{cur_idx}.board_folder_name = '20231103/%b';
param.preprocess.date_str{cur_idx} = '20231103';


%% ACCUM3 MULTIPLE DAYS
% date_str = {'20231031','20231103'};
% 
% for idx = 1:length(date_str)
%   cur_idx = length(param.preprocess.default)+1;
%   param.preprocess.default{cur_idx} = @default_radar_params_2023_Antarctica_Windracers_accum;
%   param.preprocess.file.base_dir{cur_idx} = '/mnt/data15/';
%   param.preprocess.file.config_folder_name{cur_idx} = sprintf('%s/',date_str{idx});
%   param.preprocess.file.board_folder_name{cur_idx} = sprintf('%s/%%b',date_str{idx});
%   param.preprocess.date_str{cur_idx} = date_str{idx};
% end
