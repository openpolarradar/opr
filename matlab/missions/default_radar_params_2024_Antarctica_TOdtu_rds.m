function [param,defaults] = default_radar_params_2024_Antarctica_TOdtu_rds
% [param,defaults] = default_radar_params_2024_Antarctica_TOdtu_rds
%
% rds: 2024_Antarctica_TOdtu
%
% Creates base "param" struct
% Creates defaults cell array for each type of radar setting
%
% Set the param.season_name to the correct season before running.
%
% Author: John Paden
%
% See also: default_radar_params_*.m

%% Preprocess parameters
param.season_name = '2024_Antarctica_TOdtu';
param.radar_name = 'rds';

% Reading in files
param.preprocess.digital_system_type = 'arena';
param.preprocess.gps_file_mask = '../logs/*.txt';
param.preprocess.wg_type = 'arena';
param.preprocess.header_load_func = @basic_load_arena;
param.preprocess.tx_map = {'awg0'};

% Creating segments
param.preprocess.max_time_gap = 10;
param.preprocess.min_seg_size = 1;

%% DTU HF RDS Arena Parameters
arena = [];
arena.clk = 10e6;

arena.daq.udp_packet_headers = false;

param.arena = arena;

%% Command worksheet
param.cmd.records = 1;
param.cmd.qlook = 1;
param.cmd.generic = 1;

%% Records worksheet
param.records.gps.time_offset = 0;
param.records.frames.geotiff_fn = fullfile('antarctica','Landsat-7','Antarctica_LIMA_480m.tif');
param.records.frames.mode = 1;
param.records.file.boards = {'digrx0'};
param.records.file.midfix = 'HFS';
param.records.file.suffix = '.dat';
param.records.file.version = 412;

%% Qlook worksheet
param.qlook.out_path = '';
param.qlook.block_size = 5000;
param.qlook.motion_comp = 0;
param.qlook.dec = 20;
param.qlook.inc_dec = 10;
param.qlook.surf.en = 1;
param.qlook.surf.profile = 'RDS';
param.qlook.imgs = {[1*ones(1,1),(1:1).']};

%% SAR worksheet
param.sar.out_path = '';
param.sar.imgs = param.qlook.imgs;
param.sar.frm_types = {0,[0 1],0,0,-1};
param.sar.chunk_len = 5000;
param.sar.chunk_overlap = 10;
param.sar.frm_overlap = 0;
param.sar.coh_noise_removal = 0;
param.sar.combine_rx = 0;
param.sar.time_of_full_support = inf;
param.sar.pulse_rfi.en = [];
param.sar.pulse_rfi.inc_ave= [];
param.sar.pulse_rfi.thresh_scale = [];
param.sar.trim_vals = [];
param.sar.pulse_comp = 1;
param.sar.ft_dec = 1;
param.sar.ft_wind = @hanning;
param.sar.ft_wind_time = 0;
param.sar.lever_arm_fh = @lever_arm;
param.sar.mocomp.en = 1;
param.sar.mocomp.type = 2;
param.sar.mocomp.filter = {@butter  [2]  [0.1000]};
param.sar.mocomp.uniform_en = 1;
param.sar.sar_type = 'fk';
param.sar.sigma_x = 5;
param.sar.sub_aperture_steering = 0;
param.sar.st_wind = @hanning;
param.sar.start_eps = 3.15;

%% Array worksheet
param.array.in_path = '';
param.array.array_path = '';
param.array.out_path = '';
param.array.method = 'standard';
param.array.imgs = param.qlook.imgs;
param.array.window = @hanning;
param.array.bin_rng = 0;
param.array.rline_rng = -5:5;
param.array.dbin = 1;
param.array.dline = 6;
param.array.DCM = [];
param.array.Nsv = 1;
param.array.theta_rng = [0 0];
param.array.sv_fh = @array_proc_sv;
param.array.diag_load = 0;
param.array.Nsig = 2;

%% Radar worksheet
param.radar.adc_bits = 14;
param.radar.Vpp_scale = 2;
param.radar.Tadc_adjust = 0; % System time delay: leave this empty or set it to zero at first, determine this value later using data over surface with known height or from surface multiple
param.radar.lever_arm_fh = @lever_arm;
param.radar.wfs(1).adc_gains_dB = 50; % After radiometric calibration
param.radar.wfs(1).rx_paths = [1]; % ADC to rx path mapping for wf 1
Tsys = [0]/1e9;
chan_equal_dB = [0];
chan_equal_deg = [0];

%% Post worksheet
param.post.data_dirs = {'qlook'};
param.post.layer_dir = 'layer';
param.post.maps_en = 1;
param.post.echo_en = 1;
param.post.layers_en = 0;
param.post.data_en = 0;
param.post.csv_en = 1;
param.post.concat_en = 1;
param.post.pdf_en = 1;
param.post.map.location = 'Antarctica';
param.post.map.type = 'combined';
% param.post.echo.elev_comp = 2;
% param.post.echo.depth = '[min(Surface_Depth)-100 max(Surface_Depth)+1500]';
param.post.echo.elev_comp = 3;
param.post.echo.depth = '[publish_echogram_switch(Bbad,0.25,Surface_Elev,-4000,DBottom,-4000),max(Surface_Elev+100)]';
% param.post.echo.elev_comp = 3;
% param.post.echo.depth = '[min(Surface_Elev)-1500 max(Surface_Elev)+100]';
param.post.echo.er_ice = 3.15;
param.post.ops.location = 'antarctic';

param.preprocess.param_worksheets = {};
%% analysis_noise worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_noise';
param.analysis_noise.block_size = 10000;
param.analysis_noise.imgs = param.qlook.imgs;
param.analysis_noise.cmd{1}.method = 'coh_noise';

%% analysis_equal worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_equal';
param.analysis_equal.block_size = 5000;
param.analysis_equal.imgs = param.qlook.imgs;
param.analysis_equal.cmd{1}.method = 'waveform';
param.analysis_equal.cmd{1}.start_time = struct('eval',struct('cmd','s=s-1.5e-6;'));
param.analysis_equal.cmd{1}.Nt = 128;
param.analysis_equal.cmd{1}.dec = 10;

%% analysis_spec worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_spec';
param.analysis_spec.block_size = 6000;
param.analysis_spec.imgs = param.qlook.imgs;
param.analysis_spec.cmd{1}.method = 'specular';
param.analysis_spec.cmd{1}.rlines = 128;
param.analysis_spec.cmd{1}.threshold = 40;
param.analysis_spec.cmd{1}.signal_doppler_bins = [1:4 125:128];
param.analysis_spec.cmd{1}.noise_doppler_bins = [12:117];

%% analysis_stats worksheet
param.preprocess.param_worksheets{end+1} = 'analysis_stats';
param.analysis_stats.block_size = 5000;
param.analysis_stats.imgs = {[1 1],[2 1]};
cmd_idx = 0;
cmd_idx = cmd_idx + 1;
param.analysis_stats.cmd{cmd_idx}.method = 'statistics';
param.analysis_stats.cmd{cmd_idx}.out_path = 'analysis_stats_mean';
param.analysis_stats.cmd{cmd_idx}.block_ave = 1000;
param.analysis_stats.cmd{cmd_idx}.stats = {@(x)nanmean(x.*conj(x),2)};
cmd_idx = cmd_idx + 1;
param.analysis_stats.cmd{cmd_idx}.method = 'statistics';
param.analysis_stats.cmd{cmd_idx}.out_path = 'analysis_stats_freq';
param.analysis_stats.cmd{cmd_idx}.block_ave = 1000;
param.analysis_stats.cmd{cmd_idx}.start_time = 's=es.Tend-es.Tpd-2e-6;';
param.analysis_stats.cmd{cmd_idx}.stop_time = 's=es.Tend-es.Tpd;';
param.analysis_stats.cmd{cmd_idx}.stats = {@(x)mean(abs(fft(x)).^2,2)  @(x)mean(abs(fft(fir_dec(x,10))).^2,2)  @(x)mean(abs(fft(fir_dec(x,100))).^2,2)};
cmd_idx = cmd_idx + 1;
param.analysis_stats.cmd{cmd_idx}.method = 'statistics';
param.analysis_stats.cmd{cmd_idx}.out_path = 'analysis_stats_max';
param.analysis_stats.cmd{cmd_idx}.block_ave = 1;
param.analysis_stats.cmd{cmd_idx}.pulse_comp = 1;
param.analysis_stats.cmd{cmd_idx}.stats = {'analysis_stats_task_stats_max'};
cmd_idx = cmd_idx + 1;
param.analysis_stats.cmd{cmd_idx}.method = 'statistics';
param.analysis_stats.cmd{cmd_idx}.en = 0;
param.analysis_stats.cmd{cmd_idx}.out_path = 'analysis_stats_kx';
param.analysis_stats.cmd{cmd_idx}.block_ave = 5000;
param.analysis_stats.cmd{cmd_idx}.stats = {'analysis_stats_task_stats_kx'};
param.analysis_stats.cmd{cmd_idx}.kx = 1000;


%% Radar Settings

defaults = {};

% Survey Mode
default.records.data_map = {[0 0 1 1; 128 0 1 1]};
default.qlook.img_comb = [1e-06 -inf 1e-06];
default.qlook.img_comb = [];
default.qlook.imgs = {[1*ones(1,1),(1:1).'],[2*ones(1,1),(1:1).']};
default.qlook.imgs = {[1*ones(1,1),(1:1).']};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.radar.ref_fn = '';
for wf = 1:1
  default.radar.wfs(wf).Tsys = Tsys;
  default.radar.wfs(wf).chan_equal_dB = chan_equal_dB;
  default.radar.wfs(wf).chan_equal_deg = chan_equal_deg;
  default.radar.wfs(wf).adcs = [1];
  default.radar.wfs(wf).tx_paths = {1};
  default.radar.wfs(wf).tx_weights = 1;
end

default.config_regexp = '.*shallow1wf.*';
default.name = 'Survey Mode 31-39 MHz, 1us';
defaults{end+1} = default;

% Other settings
default.records.data_map = {[0 0 1 1; 128 0 1 1]};
default.records.arena.mode_latch_fix = [128 0; 0 0];
default.qlook.img_comb = [1e-06 -inf 1e-06];
default.qlook.img_comb = [];
default.qlook.imgs = {[1*ones(1,1),(1:1).'],[2*ones(1,1),(1:1).']};
default.qlook.imgs = {[1*ones(1,1),(1:1).']};
default.sar.imgs = default.qlook.imgs;
default.array.imgs = default.qlook.imgs;
default.array.img_comb = default.qlook.img_comb;
default.radar.ref_fn = '';
for wf = 1:1
  default.radar.wfs(wf).Tsys = Tsys;
  default.radar.wfs(wf).chan_equal_dB = chan_equal_dB;
  default.radar.wfs(wf).chan_equal_deg = chan_equal_deg;
  default.radar.wfs(wf).adcs = [1];
  default.radar.wfs(wf).tx_paths = {1};
  default.radar.wfs(wf).tx_weights = 1;
end

default.config_regexp = '.*';
default.name = 'Survey Mode 31-39 MHz, 1us';
defaults{end+1} = default;

%% Add default settings

param.preprocess.defaults = defaults;
