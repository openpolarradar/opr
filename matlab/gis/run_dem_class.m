% script run_dem_class
%
% Script for demonstrating use of the dem_class.
%
% Author: John Paden

%% Typical Use
% =========================================================================
% Check to see if class already exists
global gdem;
if isempty(gdem) || ~isa(gdem,'dem_class') || ~isvalid(gdem)
  global gRadar;
  gdem = dem_class(gRadar);
end

% Set the desired DEM resolution
% gdem.set_res(2);
gdem.set_res(10);
% gdem.set_res(32);
% gdem.set_res(100);
% gdem.set_res(500);
% gdem.set_res(1000);

% if ~exist('records','var')
%   records = load(fullfile(gRadar.support_path,'records','rds','2018_Greenland_P3','records_20180418_04.mat'));
% end
% % Set the class up to look at this whole record
% if ~strcmpi(gdem.name,'rds:2018_Greenland_P3:20180418_04')
%   % Name the vector so the second time the script is run, we don't setup
%   % the vector again unless the name is changed to a different segment.
%   gdem.set_vector(records.lat,records.lon,'rds:2018_Greenland_P3:20180418_04');
% end

if 0
  % Antarctic example
  sys = 'accum';
  season_name = '2023_Antarctica_BaslerMKB';
  % day_seg = '20240112_10';
  day_seg = '20240105_04';
else
  % Arctic example
  sys = 'rds';
  season_name = '2012_Greenland_P3';
  day_seg = '20120507_07';
end
% clear; clear global gdem;

if ~exist('records','var')
  records = load(fullfile(gRadar.support_path,'records',sys,season_name,sprintf('records_%s.mat',day_seg)));
end
% Set the class up to look at this whole record
gdem_name = sprintf('%s:%s:%s',sys,season_name,day_seg);
if ~strcmpi(gdem.name,gdem_name)
  % Name the vector so the second time the script is run, we don't setup
  % the vector again unless the name is changed to a different segment.
  gdem.set_vector(records.lat,records.lon,gdem_name);
end
Nx = length(records.lat);
recs = max(1,min(Nx,[-5000 5000]+round(Nx/2))); % Grab up to 10000 records from the middle of the segment
recs = recs(1):recs(end);
gdem.set_vector(records.lat(recs),records.lon(recs));
[land_dem,msl,ocean_mask] = gdem.get_vector_dem();

figure(1); clf;
plot(land_dem)
hold on;
plot(msl)
plot(ocean_mask)
hold off;
legend('Land DEM','MSL','Ocean Mask','location','best')

return;


% This may be necessary to clear the class from memory when debugging
clear all;
startup;
dbstop if error;


% Delete object when you no longer need the dem and want to free memory
global gdem;
try
  delete(gdem);
end


