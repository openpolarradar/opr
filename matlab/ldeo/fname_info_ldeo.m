function fname = fname_info_ldeo(fn)
% fname = fname_info_ldeo(fn)
%
% Detects the type of LDEO filename and then parses it.
%
% fn = LDEO file name
%
% fname = structure containing each of the fields of the MCORDS
%   filename
%
%   For example: mcords_0_03012011_171839_01_0000.bin
%  .name = mcords
%  .board = 0
%  .group = 1
%  .file_idx = 0
%  .datenum = time stamp converted to Matlab date number. Common
%     usage: "[year month day hour min sec] = datevec(fname.datenum)"
%
% Example
%  fn = '/data/OPoRa/repos/ldeo/Rosetta_Data/trunk/icepod/antarctica/20162017/radar/sir/raw/F1009/AN03_F1009_20161202_075539_0200.sir';
%  fname = fname_info_ldeo(fn)
%
%  fn = '/data/OPoRa/repos/ldeo/Rosetta_Data/trunk/icepod/antarctica/20162017/radar/dice/raw/F1009/AN03_F1009_20161202_075511_0200.dice';
%  fname = fname_info_ldeo(fn)
%
% Author: John Paden
%
% See also datenum

[fn_dir fn_name fn_ext] = fileparts(fn);
fn = [fn_name fn_ext];

fname = [];

if length(fn_ext) >= 5 && strcmp(fn_ext(1:5),'.conf')
  fname.type = 'conf'; % Configuration
else
  if strcmp(fn_ext,'.sir')
    fname.type = 'sir'; % Shallow Ice radar
  elseif strcmp(fn_ext,'.dice')
    fname.type = 'dice'; % Deep Ice sounding radar
  elseif strcmp(fn_ext,'.head')
    fname.type = 'head'; % Header for segment
  else
    fname.type = 'invalid';
    return
  end
  
  [fname.campaign fn] = strtok(fn,'_');
  
  [fname.flight_str fn] = strtok(fn,'_');
  
  [day_str fn] = strtok(fn,'_');
  [time_str fn] = strtok(fn,'_');
  fname.datenum = datenum([day_str time_str],'yyyymmddHHMMSS');
  
  [fname.file_idx fn] = strtok(fn(2:end),'.');
  fname.file_idx = str2double(fname.file_idx);
  
end

