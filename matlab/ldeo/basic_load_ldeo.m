function [hdr,data] = basic_load_ldeo(fn)
% function [hdr,data] = basic_load_ldeo(fn)
%
% Examples:
%
% fn = '/data/OPoRa/repos/ldeo/Rosetta_Data/trunk/icepod/antarctica/20162017/radar/dice/raw/F1009/AN03_F1009_20161202_075511_0200.dice';
% [hdr1,data1] = basic_load_ldeo(fn);
% fn = '/data/OPoRa/repos/ldeo/Rosetta_Data/trunk/icepod/antarctica/20162017/radar/dice/raw/F1009/AN03_F1009_20161202_075521_0201.dice';
% [hdr2,data2] = basic_load_ldeo(fn);
%
% fn = '/data/OPoRa/repos/ldeo/Rosetta_Data/trunk/icepod/antarctica/20162017/radar/sir/raw/F1009/AN03_F1009_20161202_075539_0200.sir';
% [hdr1,data1] = basic_load_ldeo(fn);
% fn = '/data/OPoRa/repos/ldeo/Rosetta_Data/trunk/icepod/antarctica/20162017/radar/sir/raw/F1009/AN03_F1009_20161202_075549_0201.sir';
% [hdr2,data2] = basic_load_ldeo(fn);
%
% fn = '/data/OPoRa/repos/ldeo/Rosetta_Data/trunk/icepod/antarctica/20162017/radar/dice/raw/F1009/AN03_F1008_20161202_054520_0000.head';
% hdr = basic_load_ldeo(fn);
    

[fn_dir,fn_name,fn_ext] = fileparts(fn);

if strcmp(fn_ext,'.sir')
  %% SIR RAW DATA FILE
  fprintf('Detected sir format\n');
  wf = 1;
  
  [fid,msg] = fopen(fn,'rb','ieee-be');
  if fid < 0
    error('Failed to open file %s: %s\n', fn, msg);
  end
  hdr{wf}.dir_info = dir(fn);
  hdr{wf}.fname_info = fname_info_ldeo(fn);
  
  A = fread(fid,8,'uint8');
  if all(A == 255)
    fprintf('Detected sir-12474 frame size format\n');
    % data: This reader returns all samples, but LDEO processing usually
    % trims 744 samples off the beginning of the waveform and 468 samples
    % off the end.
    % data = data(750-6:12000,:);
    % data = data - mean(data(:));
    data{wf} = uint32([]);
    frm_size_bytes = 4*12474;
    fseek(fid,0,-1);
    
    [A,cnt] = fread(fid,[frm_size_bytes/4 inf],'uint32=>uint32');
    fclose(fid);
    Nx = size(A,2);
    
    if cnt ~= numel(A)
      warning('Last record in file is a partial record: this probably is not supposed to ever happen.');
      A = A(:,1:end-1);
    end
    % frame_sync should be all 1's 0xFF's in every byte
    hdr{wf}.frame_sync = uint64(A(1,:))*2^32 + uint64(A(2,:));
    if any(hdr{wf}.frame_sync ~= uint64(18446744073709551615))
      warning('%d records have bad frame syncs', sum(hdr{wf}.frame_sync ~= uint64(18446744073709551615)))
    end
    hdr{wf}.start_time = A(3,:); % unix seconds
    hdr{wf}.pps_count= A(4,:); % unix seconds
    hdr{wf}.pps_count_frac = A(5,:); % 125 MHz clock cycles since last PPS
    % 4ms EPRI or 250 Hz EPRF
    param.records.clk = 125e6;
    hdr{wf}.utc_time = double(A(4,:)) + double(A(5,:))/param.records.clk;
    
    % 16 hardware averages/presums
    hdr{wf}.pri_count = A(6,:);
    
    t_PRI = 250e-6;         % Pulse Repitition Interval (s)
    presums = 16;           % Number of hardware stacks
    t_EPRI = t_PRI*presums; % Effective PRI
    hdr{wf}.filename_utc_time = datenum_to_epoch(hdr{wf}.fname_info.datenum) + (0:Nx-1) * t_EPRI;
    
    data{wf} = A(7:end,:);
    
    hdr{wf}.offset = frm_size_bytes*(0:Nx-1) + 24;    
  else
    fprintf('Detected sir-9352 frame size format\n')
    % data: This reader returns all samples, but LDEO processing usually
    % trims 998 samples off the beginning of the waveform and 350 samples
    % off the end.
    % data = data(1000-2:9000,:);
    % data = data - mean(data(:));
    data{wf} = uint16([]);
    frm_size_bytes = 2*9352; % 2015-2016, AN01 only
    fseek(fid,0,-1);
    
    [A,cnt] = fread(fid,[frm_size_bytes/2 inf],'uint16=>uint16');
    fclose(fid)
    Nx = size(A,2);
    
    if cnt ~= numel(A)
      warning('Last record in file is a partial record: this probably is not supposed to ever happen.');
      A = A(:,1:end-1);
    end
    
    hdr{wf}.frame_sync = uint32(A(1,:))*2^16 + uint32(A(2,:));
    
    t_PRI = 750e-6;         % Pulse Repitition Interval (s)
    presums = 1;           % Number of hardware stacks
    t_EPRI = t_PRI*presums; % Effective PRI
    hdr{wf}.filename_utc_time = datenum_to_epoch(hdr{wf}.fname_info.datenum) + (0:Nx-1) * t_EPRI;
    
    data{wf} = A(3:end,:);
    
    hdr{wf}.offset = frm_size_bytes*(0:Nx-1) + 4;    
  end
  
elseif strcmp(fn_ext,'.dice')
  %% DICE RAW DATA FILE
  fprintf('Detected dice format\n');
  
  [fid,msg] = fopen(fn,'rb','ieee-le');
  if fid < 0
    error('Failed to open file %s: %s\n', fn, msg);
  end
  
  frm_size_bytes = 2*(12000+20 + 12000+20 + 2+12000+16+2 + 12000+20);
  
  [A,cnt] = fread(fid,[frm_size_bytes/2 inf],'int16=>int16');
  fclose(fid);
  Nx = size(A,2);
  
  if cnt ~= numel(A)
    warning('Last record in file is a partial record: this probably is not supposed to ever happen.');
    A = A(:,1:end-1);
  end
  
  hdr = {};
  data = {};
  for wf = 1:4
    hdr{wf}.dir_info = dir(fn);
    hdr{wf}.fname_info = fname_info_ldeo(fn);
    
    % frame_sync = '010203A5';
    wf_offset = (wf-1)*(12000+20);
    hdr{wf}.frame_sync = double(A(wf_offset+1,:)) + double(A(wf_offset+2,:))*2^16;
    if any(hdr{wf}.frame_sync ~= hex2dec('010203A5'))
      warning('%d records have bad frame syncs', sum(hdr{wf}.frame_sync ~= hex2dec('010203A5')))
    end
    
    wf_offset = (wf-1)*(12000+20) + 2;
    data{wf} = A(wf_offset+(1:12000),:);
    
    wf_offset = (wf)*(12000+20)-18;
    hdr{wf}.metadata = double(A(wf_offset+(1:16),:));
    % hdr{wf}.metadata definition unknown
    % hdr{wf}.metadata(1:8,:) == 0
    % hdr{wf}.metadata(9,:) == 12000
    % hdr{wf}.metadata(10,:) == 0
    % hdr{wf}.metadata(11:13,:) == cycle from -2^15 to +2^15
    % hdr{wf}.metadata(14,:) == saw tooth wave around 10000
    % hdr{wf}.metadata(15,:) == 8463
    % hdr{wf}.metadata(16,:) == 22593
    
    t_PRI = 160e-6;           % Pulse Repitition Interval (s)
    presums = 16;             % Number of hardware stacks
    t_EPRI = t_PRI*4*presums; % Effective PRI, 10.24 ms (seems like factor of 4 should be 2 for the 2 different pulses that are transmitted, 3 us and then 1 us)
    if wf < 2
      % First transmitted waveform
      hdr{wf}.filename_utc_time = datenum_to_epoch(hdr{wf}.fname_info.datenum) + (0:Nx-1) * t_EPRI;
    else
      % Second transmitted waveform so offset in time by t_PRI
      hdr{wf}.filename_utc_time = datenum_to_epoch(hdr{wf}.fname_info.datenum) + (0:Nx-1) * t_EPRI + t_PRI;
    end
    
    % frame_sync_end = '030201A5';
    wf_offset = (wf)*(12000+20)-2;
    hdr{wf}.frame_sync_end = double(A(wf_offset+1,:)) + double(A(wf_offset+2,:))*2^16;
    if any(hdr{wf}.frame_sync_end ~= hex2dec('030201A5'))
      warning('%d records have bad end frame syncs', sum(hdr{wf}.frame_sync_end ~= hex2dec('030201A5')))
    end
    
    hdr{wf}.offset = frm_size_bytes*(0:Nx-1) + 2*((wf-1)*(12000+20) + 2);
  end
  
elseif strcmp(fn_ext,'.head')
  %% HEAD DATA FILE
  fprintf('Detected head format\n');
  % AN03_F1009_20161202_092348_0000.head
  % AN03_F1008_20161202_060021_0000.head
  %
  % File Contents:
  % Columbia University
  % 20161202_054520
  % Project: AN03
  % Operator: TD
  % Flight: 008
  % Comments: Line 610 and  mag comp (may be)
  data = [];
  
  [fid,msg] = fopen(fn,'rb','ieee-le');
  if fid < 0
    error('Failed to open file %s: %s\n', fn, msg);
  end
  
  hdr.organization = fgets(fid);
  hdr.organization = hdr.organization(hdr.organization ~= 10 & hdr.organization ~= 13);
  
  hdr.datenum_str = fgets(fid);
  hdr.datenum_str = hdr.datenum_str(hdr.datenum_str ~= 10 & hdr.datenum_str ~= 13);
  
  hdr.datenum = datenum(hdr.datenum_str,'YYYYmmDD_hhMMss');
  hdr.organization = hdr.organization(hdr.organization ~= 10 & hdr.organization ~= 13);
  
  hdr.project = fgets(fid);
  hdr.project = hdr.project(hdr.project ~= 10 & hdr.project ~= 13);
  hdr.operator = fgets(fid);
  hdr.operator = hdr.operator(hdr.operator ~= 10 & hdr.operator ~= 13);
  hdr.flight_str = fgets(fid);
  hdr.flight_str = hdr.flight_str(hdr.flight_str ~= 10 & hdr.flight_str ~= 13);
  hdr.notes = fgets(fid);
  hdr.notes = hdr.notes(hdr.notes ~= 10 & hdr.notes ~= 13);
  
  fclose(fid);
  
elseif length(fn_ext) >= 5 && strcmp(fn_ext(1:5),'.conf')
  %% CONF FILE
  fprintf('Detected conf format\n');
  % dice.conf-20161202_054520
  % sir.conf-20161202_055302
  hdr = struct();
  data = [];
  
end

return




%% SIR
filePeriod = 10;
if strcmpi(Campaign, 'AN01')
  sirVersion = 1;
else
  sirVersion = 2;
end

switch sirVersion
  case 1
    fprintf('sirProcessConfig: Using settings for SIR hardware version: %d\n', sirVersion);
    fs = 12.5e6;            % Sampling Frequency
    tau = 750e-6;           % Chirp Period
    bw = 599996442;         % Chirp bandwidth Hz %599996441.603
    frameLen = 9352;        % number of samples collected for each chirp
    % data structure (samples): { < marker(2) >  (2) > < metadata(4) > < data(9352) > }{ < marker (2) >
    sigRange = 1000:9000;   % discard first and last 2 samples (Marker Bytes)
    numHWStacks = 1;        % Number of hardware stacks
    dataType = 'uint16';      %
    numMetadataSamples = 0; % in number of samples; each sample is of dataType
    frameSize = frameLen * 2; % size of a frame in bytes
    rangeLim = [100 650];   % meters in Ice-distance surface ~ 427m - narrow range  =>  faster processing
    RangePass = rangeLim;   %m Used for bp filter by Range
  otherwise
    fprintf('sirProcessConfig: Using settings for SIR hardware version %d\n', sirVersion);
    fs = 50e6;              % Sampling Frequency
    tau = 250e-6;           % Chirp Period
    bw = 600138426;         % Chirp bandwidth Hz % 600138425.83
    frameLen = 12474;       % number of samples collected for each chirp
    % data structure (samples): { < marker(2) >  (2) > < metadata(4) > < data(12474) > }{ < marker (2) >
    sigRange = 750:12000;   % discard samples at begining and end of the chirp
    numHWStacks = 16;       % Number of hardware stacks
    dataType = 'uint32';      %
    numMetadataSamples = 4; % starttime(unixsec) ppscount(unixsec) clkcount(sampleclk) echocount; in number of samples
    frameSize = frameLen * 4; % size of a frame in bytes;
    rangeLim = [100 874];   % meters in Ice-distance surface ~ 427m - narrow range  =>  faster processing
    RangePass = rangeLim;   %m by Range
end

prfPreSar = 1 / (tau * numHWStacks * numAvg); % chirp repition frequency
dataEndian = 'b';                       % Endianness of data being read
filesperbatch = 6;                      % Number of files compiled into each batch (integer)
maxLinesperFile = ceil(filePeriod / tau / numHWStacks * 1.1) + numAvg;     % max size for preallocation
maxLinesperBatch = ceil(filePeriod / tau / numHWStacks * 1.1) * filesperbatch + numAvg;
markerLen = 2;                              % lenth of data markers in samplecount 0xFFFF or 0xFFFFFFFF
f0 = 1.7e9;                                 % starting frequency
Kr = bw / tau;                                % Modulation rate
tsamp_full = linspace(0, tau - 1 / fs, fs * tau)';  % sample time within a chirp (fast time)
tsamp = tsamp_full(sigRange);               % sampling time for range of samples selected
fftdatalen = numel(sigRange);               % length of pre-fft chirp data
maxFFTLen = 32768;                          % determines the frequency resolution (more bins  =>  less power per bin)
elephantstoVolts = 2.415 / 2^16;              % adc conversion into volts

markerLen = 2;                              % lenth of data markers in samplecount 0xFFFF or 0xFFFFFFFF


dataind = repmat(logical([zeros(markerLen, 1);zeros(numMetadataSamples, 1);...
  zeros(sigRange(1) - (markerLen + numMetadataSamples + 1), 1);ones(fftdatalen, 1);...
  zeros(frameLen - sigRange(end), 1)]), maxLinesperFile, 1); %#ok<FNCOLND> sigRange is defined in sirProcessConfig.m
metadataind = repmat(logical([zeros(markerLen, 1);ones(numMetadataSamples, 1);...
  zeros(sigRange(1) - (numMetadataSamples + markerLen + 1), 1);zeros(fftdatalen, 1);...
  zeros(frameLen - sigRange(end), 1)]), maxLinesperFile, 1); %#ok<FNCOLND> sigRange is defined in sirProcessConfig.m


% 4 bytes markerLen
% 0 or 16 bytes metadata
uint16
wf_offset = 0
wf_offset = 2 % No metadata
wf_offset = 2 % Raw data
wf_offset = 1000
9000
9352 % Number of samples

uint32
wf_offset = 0
wf_offset = 2
wf_offset = 6
750-2-4
12474-12000


%% DICE

PRI = 160e-6;       % Pulse Repitition Interval (s)
sampWin = 80e-6;    % Period of time data is collected for
fs = 150e6;         % Sampling Frequency (Hz)
pw1 = 3e-6;         % pulse width for 1st plse (10 us)
pw2 = 1e-6;         % pulse width for 2nd pulse( p2)
bw = 60e6;          % Bandwidth
fc = 38e6;          % Center Frequency
fdemod = fc;        % Baseband demodulation frequency
datalenp1 = 12000;  % Number of samples in 3 us data (integer)
datalenp2 = 12000;  % Number of samples in 1 us data (integer)
tdata = [0:1/fs:sampWin-1/fs]';     % time for each data sample
numHWStacks = 16;   % Number of hardware stacks
numbits=16;         % Number of digitizer bits (integer)
numMetadataSamples=16;	% Number of samples in the data file that are metadata (integer)
